﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("プラン価格調査PCモジュール")]
[assembly: AssemblyDescription("RepChecker PC Module (x86)")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("paradigmshift")]
[assembly: AssemblyProduct("psinc#")]
[assembly: AssemblyCopyright("Copyright (c) 2018- paradigmshift")]
[assembly: AssemblyTrademark("RepChecker")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("e18e46b9-6d18-4120-9eb0-04f563f505e1")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("4.1.2024.0814")]
[assembly: AssemblyFileVersion("4.1.2024.0814")]
