﻿namespace PCModule
{
    public class Messages
    {
        #region :: TITLE

        public static readonly string Alert = "アラート";

        public static readonly string Caution = "注意";

        public static readonly string Confirm = "確認";

        public static readonly string Error = "エラー";

        public static readonly string Info = "情報";

        public static readonly string Notice = "通知";

        #endregion

        #region :: MESSAGES

        public static readonly string AccountExpired = "アカウント契約の状況を確認して下さい。";

        public static readonly string AuthorisationFail = "認証に失敗しました。確認して再入力してください。";

        public static readonly string AuthorisationInvalid = "有効なトークンがありません。再認証を行いでください。";

        public static readonly string AuthorisationSuccess = "認証されました。";

        public static readonly string CrawlingCompleted = "情報取得が終了しました。";

        public static readonly string CrawlingJobInProgress = "情報取得中です。他の操作はできません。";

        public static readonly string DeleteAllLog = "全ての履歴を削除しますか？";

        public static readonly string DeleteAllLogData = "ログファイルを削除しますか？";

        public static readonly string DeleteScheduledCrawler = "選択したタイマーを削除しますか？";

        public static readonly string DeleteScheduledCrawlerSucceed = "選択したタイマーを削除しました。";

        public static readonly string ErrorOccurred = "エラーが発生しました。";

        public static readonly string ExceedScheduledCrawlerMaximum = "タイマーの数が最大値を超えました。";

        public static readonly string InstantCrawlerIntervalWarning = "リアルタイム取得の間隔は１時間になっています。";

        public static readonly string LoginFailed = "「ログインID」又は「パスワード」が正しくありません。";

        public static readonly string LogListDeleted = "全ての履歴を削除しました。";

        public static readonly string LogListUpdated = "取得履歴を更新しました。";

        public static readonly string LogDataDeleted = "ログファイルを削除しました。";

        public static readonly string NoAccount = "アカウントが存在していないです。";

        public static readonly string NoInput = "「ログインID」又は「パスワード」を入力して下さい。";

        public static readonly string NoScheduledCrawlerSelected = "タイマーを選択して下さい。";

        public static readonly string NoSelected = "「施設リスト」又は「OTAリスト」を確認して下さい。";

        public static readonly string NotSupportedOTA = "連泊に対応してないOTA(るるぶトラベル、Expedia)が選択されています。該当OTAは１泊に固定となります。";

        public static readonly string ProxyIpAddressError = "「プロキシIP」が正しくありません。";

        public static readonly string ProxyPortNumberError = "「プロキシポート」が正しくありません。";

        public static readonly string ProxyServerConnectionFailed = "サーバーに接続できません。\r\n設定されているプロキシを確認して下さい。\r\nシステムを終了します。";

        public static readonly string QuitProgram = "終了しますか？";

        public static readonly string RegisterInstantCrawlerFailed = "リアルタイム取得情報の登録に失敗しました。";

        public static readonly string RegisterScheduledCrawler = "タイマー（自動取得）を設定しますか？";

        public static readonly string RegisterScheduledCrawlerFailed = "タイマー登録に失敗しました。";

        public static readonly string RegisterScheduledCrawlerSucceed = "タイマーを設定しました。";

        public static readonly string RemainingTimeToRun = "{0}分後に使用できます。";

        public static readonly string SaveChanges = "設定が変わりました。保存しますか？";

        public static readonly string ScheduledCrawlerIntervalWarning = "タイマーの時間や間隔を確認して下さい。";

        public static readonly string SelectOwnHotel = "自社施設を選択して下さい。";

        public static readonly string ServerConnectionFailed = "サーバーに接続できません。\r\nシステムを終了します。";

        public static readonly string StartInstantCrawler = "リアルタイム取得を開始しますか？";

        public static readonly string TriggerNumberOutOfRange = "日付は０日から指定できます。";

        public static readonly string UnauthorisedAccount = "モジュール使う許可を確認して下さい。";

        #endregion
    }
}