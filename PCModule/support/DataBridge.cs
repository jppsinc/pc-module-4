﻿using Psinc.Util;

namespace PCModule
{
    /// <summary>
    /// Class for data exchange.
    /// </summary>
    public class DataBridge
    {   // Created: 2018.01.18

        public static readonly string CommandLoginCustomer        = "login";
        public static readonly string CommandLoginAdmin           = "loginAdmin";
        public static readonly string CommandAccountInfo          = "accountInfo";
        public static readonly string CommandOtaInfo              = "otaInfo";
        public static readonly string CommandLastestUpdateInfo    = "latestUpdateInfo";
        public static readonly string CommandOwnHotelList         = "ownHotelList";
        public static readonly string CommandCompetitiveHotelList = "competitiveHotelList";
        public static readonly string CommandTimerList            = "timerList";
        public static readonly string CommandCrawlingCondition    = "crwlCondition";

        private static DataBridge _instance = null;

        private bool _isDevMode     = false;
        private bool _isSupportMode = false;
        private bool _ignoreUpdate  = false;

        private readonly string _prdServerUrlFormat = "https://moduleapi.repchecker.jp/api/{0}";
        private readonly string _devServerUrlFormat = "http://localhost:8000/api/{0}";


        /// <summary>
        /// Private constructor.
        /// </summary>
        private DataBridge()
        {
        }

        /// <summary>
        /// Gets an instance of 'DataBridge'.
        /// </summary>
        public static DataBridge Instance
        {
            get
            {
                _instance = _instance ?? new DataBridge();
                return _instance;
            }
        }

        /// <summary>
        /// Gets or sets a status that program runs on dev server.
        /// </summary>
        public bool IsDevMode
        {
            get
            {
                return _isDevMode;
            }

            set
            {
                _isDevMode = value;

                if (value)
                {
                    NetworkManager.Instance.UrlFormat = _devServerUrlFormat;
                }
                else
                {
                    NetworkManager.Instance.UrlFormat = _prdServerUrlFormat;
                }
            }
        }

        /// <summary>
        /// Gets or sets a status that program runs on customer support mode.
        /// </summary>
        public bool IsSupportMode
        {
            get
            {
                return _isSupportMode;
            }

            set
            {
                _isSupportMode = value;
            }
        }

        /// <summary>
        /// Gets or sets a status that program will ignore a new update or not.
        /// </summary>
        public bool IgnoreUpdate
        {
            get
            {
                return _ignoreUpdate;
            }

            set
            {
                _ignoreUpdate = value;
            }
        }
    }
}
