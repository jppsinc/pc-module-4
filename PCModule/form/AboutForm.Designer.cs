﻿namespace PCModule
{
    partial class AboutForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutForm));
            this.CloseButton = new System.Windows.Forms.Button();
            this.ProgramVersion = new System.Windows.Forms.Label();
            this.ProgramCopyright = new System.Windows.Forms.Label();
            this.ProgramLicenseLabel = new System.Windows.Forms.Label();
            this.ProgramLicenseText = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // CloseButton
            // 
            this.CloseButton.Location = new System.Drawing.Point(509, 297);
            this.CloseButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(93, 31);
            this.CloseButton.TabIndex = 9;
            this.CloseButton.Text = "閉じる";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // ProgramVersion
            // 
            this.ProgramVersion.AutoSize = true;
            this.ProgramVersion.Location = new System.Drawing.Point(17, 19);
            this.ProgramVersion.Name = "ProgramVersion";
            this.ProgramVersion.Size = new System.Drawing.Size(52, 15);
            this.ProgramVersion.TabIndex = 10;
            this.ProgramVersion.Text = "バージョン";
            // 
            // ProgramCopyright
            // 
            this.ProgramCopyright.AutoSize = true;
            this.ProgramCopyright.Location = new System.Drawing.Point(17, 44);
            this.ProgramCopyright.Name = "ProgramCopyright";
            this.ProgramCopyright.Size = new System.Drawing.Size(275, 15);
            this.ProgramCopyright.TabIndex = 11;
            this.ProgramCopyright.Text = "© 2019 Paradigmshift Inc. All rights reserved.";
            // 
            // ProgramLicenseLabel
            // 
            this.ProgramLicenseLabel.AutoSize = true;
            this.ProgramLicenseLabel.Location = new System.Drawing.Point(17, 79);
            this.ProgramLicenseLabel.Name = "ProgramLicenseLabel";
            this.ProgramLicenseLabel.Size = new System.Drawing.Size(130, 15);
            this.ProgramLicenseLabel.TabIndex = 12;
            this.ProgramLicenseLabel.Text = "バージョンと著作権の情報";
            // 
            // ProgramLicenseText
            // 
            this.ProgramLicenseText.BackColor = System.Drawing.SystemColors.Window;
            this.ProgramLicenseText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ProgramLicenseText.Location = new System.Drawing.Point(20, 97);
            this.ProgramLicenseText.Multiline = true;
            this.ProgramLicenseText.Name = "ProgramLicenseText";
            this.ProgramLicenseText.ReadOnly = true;
            this.ProgramLicenseText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ProgramLicenseText.Size = new System.Drawing.Size(582, 193);
            this.ProgramLicenseText.TabIndex = 13;
            this.ProgramLicenseText.Text = resources.GetString("ProgramLicenseText.Text");
            // 
            // AboutForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 341);
            this.Controls.Add(this.ProgramLicenseText);
            this.Controls.Add(this.ProgramLicenseLabel);
            this.Controls.Add(this.ProgramCopyright);
            this.Controls.Add(this.ProgramVersion);
            this.Controls.Add(this.CloseButton);
            this.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "AboutForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "バージョン情報";
            this.Load += new System.EventHandler(this.AboutForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Label ProgramVersion;
        private System.Windows.Forms.Label ProgramCopyright;
        private System.Windows.Forms.Label ProgramLicenseLabel;
        private System.Windows.Forms.TextBox ProgramLicenseText;
    }
}