﻿namespace PCModule
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginForm));
            this.LogoImage = new System.Windows.Forms.PictureBox();
            this.LabelUserId = new System.Windows.Forms.Label();
            this.UserId = new System.Windows.Forms.TextBox();
            this.LabelUserPw = new System.Windows.Forms.Label();
            this.UserPw = new System.Windows.Forms.TextBox();
            this.SaveUserId = new System.Windows.Forms.CheckBox();
            this.CloseButton = new System.Windows.Forms.Button();
            this.LoginButton = new System.Windows.Forms.Button();
            this.SupportModeButton = new System.Windows.Forms.Button();
            this.LabelAuthorisedUser = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.LogoImage)).BeginInit();
            this.SuspendLayout();
            // 
            // LogoImage
            // 
            this.LogoImage.Image = ((System.Drawing.Image)(resources.GetObject("LogoImage.Image")));
            this.LogoImage.Location = new System.Drawing.Point(85, 9);
            this.LogoImage.Name = "LogoImage";
            this.LogoImage.Size = new System.Drawing.Size(164, 60);
            this.LogoImage.TabIndex = 0;
            this.LogoImage.TabStop = false;
            // 
            // LabelUserId
            // 
            this.LabelUserId.AutoSize = true;
            this.LabelUserId.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LabelUserId.Location = new System.Drawing.Point(20, 82);
            this.LabelUserId.Name = "LabelUserId";
            this.LabelUserId.Size = new System.Drawing.Size(57, 15);
            this.LabelUserId.TabIndex = 1;
            this.LabelUserId.Text = "ログインID";
            // 
            // UserId
            // 
            this.UserId.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.UserId.Location = new System.Drawing.Point(85, 78);
            this.UserId.MaxLength = 100;
            this.UserId.Name = "UserId";
            this.UserId.Size = new System.Drawing.Size(204, 23);
            this.UserId.TabIndex = 2;
            // 
            // LabelUserPw
            // 
            this.LabelUserPw.AutoSize = true;
            this.LabelUserPw.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LabelUserPw.Location = new System.Drawing.Point(24, 111);
            this.LabelUserPw.Name = "LabelUserPw";
            this.LabelUserPw.Size = new System.Drawing.Size(53, 15);
            this.LabelUserPw.TabIndex = 3;
            this.LabelUserPw.Text = "パスワード";
            // 
            // UserPw
            // 
            this.UserPw.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.UserPw.Location = new System.Drawing.Point(85, 107);
            this.UserPw.MaxLength = 100;
            this.UserPw.Name = "UserPw";
            this.UserPw.Size = new System.Drawing.Size(204, 23);
            this.UserPw.TabIndex = 4;
            this.UserPw.UseSystemPasswordChar = true;
            // 
            // SaveUserId
            // 
            this.SaveUserId.AutoSize = true;
            this.SaveUserId.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.SaveUserId.Location = new System.Drawing.Point(85, 136);
            this.SaveUserId.Name = "SaveUserId";
            this.SaveUserId.Size = new System.Drawing.Size(204, 19);
            this.SaveUserId.TabIndex = 5;
            this.SaveUserId.Text = "次回からログインIDの入力を省略する";
            this.SaveUserId.UseVisualStyleBackColor = true;
            // 
            // CloseButton
            // 
            this.CloseButton.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.CloseButton.Location = new System.Drawing.Point(209, 161);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(80, 30);
            this.CloseButton.TabIndex = 8;
            this.CloseButton.Text = "終了";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.Close_Click);
            // 
            // LoginButton
            // 
            this.LoginButton.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LoginButton.Location = new System.Drawing.Point(85, 161);
            this.LoginButton.Name = "LoginButton";
            this.LoginButton.Size = new System.Drawing.Size(118, 30);
            this.LoginButton.TabIndex = 7;
            this.LoginButton.Text = "ログイン";
            this.LoginButton.UseVisualStyleBackColor = true;
            this.LoginButton.Click += new System.EventHandler(this.Login_Click);
            // 
            // SupportModeButton
            // 
            this.SupportModeButton.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.SupportModeButton.Location = new System.Drawing.Point(84, 103);
            this.SupportModeButton.Name = "SupportModeButton";
            this.SupportModeButton.Size = new System.Drawing.Size(206, 28);
            this.SupportModeButton.TabIndex = 9;
            this.SupportModeButton.Text = "CSモード認証";
            this.SupportModeButton.UseVisualStyleBackColor = true;
            this.SupportModeButton.Click += new System.EventHandler(this.SupportModeButton_Click);
            // 
            // LabelAuthorisedUser
            // 
            this.LabelAuthorisedUser.AutoSize = true;
            this.LabelAuthorisedUser.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LabelAuthorisedUser.Location = new System.Drawing.Point(84, 193);
            this.LabelAuthorisedUser.Name = "LabelAuthorisedUser";
            this.LabelAuthorisedUser.Size = new System.Drawing.Size(71, 15);
            this.LabelAuthorisedUser.TabIndex = 10;
            this.LabelAuthorisedUser.Text = "認証ユーザー";
            // 
            // LoginForm
            // 
            this.AcceptButton = this.LoginButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(314, 211);
            this.Controls.Add(this.LabelAuthorisedUser);
            this.Controls.Add(this.SupportModeButton);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.LoginButton);
            this.Controls.Add(this.SaveUserId);
            this.Controls.Add(this.UserPw);
            this.Controls.Add(this.LabelUserPw);
            this.Controls.Add(this.UserId);
            this.Controls.Add(this.LabelUserId);
            this.Controls.Add(this.LogoImage);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoginForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ログイン";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.LoginForm_FormClosed);
            this.Load += new System.EventHandler(this.LoginForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.LogoImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox LogoImage;
        private System.Windows.Forms.Label LabelUserId;
        private System.Windows.Forms.TextBox UserId;
        private System.Windows.Forms.Label LabelUserPw;
        private System.Windows.Forms.TextBox UserPw;
        private System.Windows.Forms.CheckBox SaveUserId;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Button LoginButton;
        private System.Windows.Forms.Button SupportModeButton;
        private System.Windows.Forms.Label LabelAuthorisedUser;
    }
}