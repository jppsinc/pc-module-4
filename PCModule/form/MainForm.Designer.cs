﻿namespace PCModule
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.MenuStrip = new System.Windows.Forms.MenuStrip();
            this.MenuHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuHelpProxy = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuHelpSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuHelpWeb = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuHelpQuestion = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuHelpAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuLogout = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuTest = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuTestGo = new System.Windows.Forms.ToolStripMenuItem();
            this.TabControl = new System.Windows.Forms.TabControl();
            this.CrwlTab = new System.Windows.Forms.TabPage();
            this.OtaCheckCtrl = new System.Windows.Forms.CheckBox();
            this.SystemMessageBox = new System.Windows.Forms.GroupBox();
            this.SystemMessageText = new System.Windows.Forms.TextBox();
            this.TimerSetup = new System.Windows.Forms.GroupBox();
            this.SetOnHold = new System.Windows.Forms.CheckBox();
            this.TimerNoticeText = new System.Windows.Forms.Label();
            this.TimerSetDelete = new System.Windows.Forms.Button();
            this.TimerSetSet = new System.Windows.Forms.Button();
            this.LabelTimerDailySetMinute = new System.Windows.Forms.Label();
            this.TimerDailySetMinute = new System.Windows.Forms.ComboBox();
            this.LabelTimerDailySetHour = new System.Windows.Forms.Label();
            this.TimerDailySetHour = new System.Windows.Forms.ComboBox();
            this.LabelTimerDailySet = new System.Windows.Forms.Label();
            this.LabelTimerList = new System.Windows.Forms.Label();
            this.TimerList = new System.Windows.Forms.DataGridView();
            this.TimerRowId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TimerRowOta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TimerRowStartDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TimerRowStartTrigger = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TimerRowPeriod = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TimerRowPerson = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TimerRowDays = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TimerRowExecDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TimerRowExecTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoticeBox = new System.Windows.Forms.GroupBox();
            this.NoticeText = new System.Windows.Forms.TextBox();
            this.CrwlSetup = new System.Windows.Forms.GroupBox();
            this.DaysFromToday = new System.Windows.Forms.TextBox();
            this.LabelDaysFromToday2 = new System.Windows.Forms.Label();
            this.LabelDaysFromToday1 = new System.Windows.Forms.Label();
            this.StartDatePicker = new System.Windows.Forms.DateTimePicker();
            this.StartCrawling = new System.Windows.Forms.Button();
            this.Days = new System.Windows.Forms.ComboBox();
            this.LabelDays = new System.Windows.Forms.Label();
            this.Person = new System.Windows.Forms.ComboBox();
            this.LabelPerson = new System.Windows.Forms.Label();
            this.Period = new System.Windows.Forms.ComboBox();
            this.LabelPeriod = new System.Windows.Forms.Label();
            this.LabelStartDate = new System.Windows.Forms.Label();
            this.OtaList = new System.Windows.Forms.GroupBox();
            this.OtaTree = new System.Windows.Forms.TreeView();
            this.HotelList = new System.Windows.Forms.GroupBox();
            this.HotelTree = new System.Windows.Forms.TreeView();
            this.LogTab = new System.Windows.Forms.TabPage();
            this.LogSplitContainer = new System.Windows.Forms.SplitContainer();
            this.LogTree = new System.Windows.Forms.TreeView();
            this.LogDelete = new System.Windows.Forms.Button();
            this.LogRefresh = new System.Windows.Forms.Button();
            this.LogDataDelete = new System.Windows.Forms.Button();
            this.StatusStrip = new System.Windows.Forms.StatusStrip();
            this.DebugIndicator = new System.Windows.Forms.ToolStripStatusLabel();
            this.LabelAccountInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.MainProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.NotifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.BackgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.NoPanel = new System.Windows.Forms.Panel();
            this.ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.MenuStrip.SuspendLayout();
            this.TabControl.SuspendLayout();
            this.CrwlTab.SuspendLayout();
            this.SystemMessageBox.SuspendLayout();
            this.TimerSetup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TimerList)).BeginInit();
            this.NoticeBox.SuspendLayout();
            this.CrwlSetup.SuspendLayout();
            this.OtaList.SuspendLayout();
            this.HotelList.SuspendLayout();
            this.LogTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LogSplitContainer)).BeginInit();
            this.LogSplitContainer.Panel1.SuspendLayout();
            this.LogSplitContainer.Panel2.SuspendLayout();
            this.LogSplitContainer.SuspendLayout();
            this.StatusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // MenuStrip
            // 
            this.MenuStrip.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.MenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuHelp,
            this.MenuLogout,
            this.MenuExit,
            this.MenuTest});
            this.MenuStrip.Location = new System.Drawing.Point(0, 0);
            this.MenuStrip.Name = "MenuStrip";
            this.MenuStrip.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.MenuStrip.Size = new System.Drawing.Size(790, 24);
            this.MenuStrip.TabIndex = 0;
            this.MenuStrip.Text = "Main MenuStrip";
            // 
            // MenuHelp
            // 
            this.MenuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuHelpProxy,
            this.MenuHelpSeparator1,
            this.MenuHelpWeb,
            this.MenuHelpQuestion,
            this.MenuHelpAbout});
            this.MenuHelp.Name = "MenuHelp";
            this.MenuHelp.Size = new System.Drawing.Size(67, 20);
            this.MenuHelp.Text = "ヘルプ(&H)";
            // 
            // MenuHelpProxy
            // 
            this.MenuHelpProxy.Name = "MenuHelpProxy";
            this.MenuHelpProxy.Size = new System.Drawing.Size(213, 22);
            this.MenuHelpProxy.Text = "プロキシ設定(&P)...";
            this.MenuHelpProxy.Click += new System.EventHandler(this.MenuHelpProxy_Click);
            // 
            // MenuHelpSeparator1
            // 
            this.MenuHelpSeparator1.Name = "MenuHelpSeparator1";
            this.MenuHelpSeparator1.Size = new System.Drawing.Size(210, 6);
            // 
            // MenuHelpWeb
            // 
            this.MenuHelpWeb.Name = "MenuHelpWeb";
            this.MenuHelpWeb.Size = new System.Drawing.Size(213, 22);
            this.MenuHelpWeb.Text = "RCホームページ(&R)";
            this.MenuHelpWeb.Click += new System.EventHandler(this.MenuHelpWeb_Click);
            // 
            // MenuHelpQuestion
            // 
            this.MenuHelpQuestion.Name = "MenuHelpQuestion";
            this.MenuHelpQuestion.Size = new System.Drawing.Size(213, 22);
            this.MenuHelpQuestion.Text = "お問合せ(&Q)";
            this.MenuHelpQuestion.Click += new System.EventHandler(this.MenuHelpQuestion_Click);
            // 
            // MenuHelpAbout
            // 
            this.MenuHelpAbout.Name = "MenuHelpAbout";
            this.MenuHelpAbout.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F1)));
            this.MenuHelpAbout.Size = new System.Drawing.Size(213, 22);
            this.MenuHelpAbout.Text = "バージョン情報(&A)";
            this.MenuHelpAbout.Click += new System.EventHandler(this.MenuHelpAbout_Click);
            // 
            // MenuLogout
            // 
            this.MenuLogout.Name = "MenuLogout";
            this.MenuLogout.Size = new System.Drawing.Size(80, 20);
            this.MenuLogout.Text = "ログアウト(&L)";
            this.MenuLogout.Click += new System.EventHandler(this.MenuLogout_Click);
            // 
            // MenuExit
            // 
            this.MenuExit.Name = "MenuExit";
            this.MenuExit.Size = new System.Drawing.Size(61, 20);
            this.MenuExit.Text = "終了(&X)";
            this.MenuExit.Click += new System.EventHandler(this.MenuExit_Click);
            // 
            // MenuTest
            // 
            this.MenuTest.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuTestGo});
            this.MenuTest.Name = "MenuTest";
            this.MenuTest.Size = new System.Drawing.Size(63, 20);
            this.MenuTest.Text = "テスト(&T)";
            // 
            // MenuTestGo
            // 
            this.MenuTestGo.Name = "MenuTestGo";
            this.MenuTestGo.Size = new System.Drawing.Size(100, 22);
            this.MenuTestGo.Text = "RUN";
            this.MenuTestGo.Click += new System.EventHandler(this.MenuTestGo_Click);
            // 
            // TabControl
            // 
            this.TabControl.Controls.Add(this.CrwlTab);
            this.TabControl.Controls.Add(this.LogTab);
            this.TabControl.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.TabControl.Location = new System.Drawing.Point(2, 30);
            this.TabControl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TabControl.Name = "TabControl";
            this.TabControl.SelectedIndex = 0;
            this.TabControl.Size = new System.Drawing.Size(782, 708);
            this.TabControl.TabIndex = 0;
            // 
            // CrwlTab
            // 
            this.CrwlTab.Controls.Add(this.OtaCheckCtrl);
            this.CrwlTab.Controls.Add(this.SystemMessageBox);
            this.CrwlTab.Controls.Add(this.TimerSetup);
            this.CrwlTab.Controls.Add(this.LabelTimerList);
            this.CrwlTab.Controls.Add(this.TimerList);
            this.CrwlTab.Controls.Add(this.NoticeBox);
            this.CrwlTab.Controls.Add(this.CrwlSetup);
            this.CrwlTab.Controls.Add(this.OtaList);
            this.CrwlTab.Controls.Add(this.HotelList);
            this.CrwlTab.Location = new System.Drawing.Point(4, 24);
            this.CrwlTab.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.CrwlTab.Name = "CrwlTab";
            this.CrwlTab.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.CrwlTab.Size = new System.Drawing.Size(774, 680);
            this.CrwlTab.TabIndex = 0;
            this.CrwlTab.Text = "取得";
            this.CrwlTab.UseVisualStyleBackColor = true;
            // 
            // OtaCheckCtrl
            // 
            this.OtaCheckCtrl.AutoSize = true;
            this.OtaCheckCtrl.Checked = true;
            this.OtaCheckCtrl.CheckState = System.Windows.Forms.CheckState.Checked;
            this.OtaCheckCtrl.Location = new System.Drawing.Point(183, 278);
            this.OtaCheckCtrl.Name = "OtaCheckCtrl";
            this.OtaCheckCtrl.Size = new System.Drawing.Size(123, 19);
            this.OtaCheckCtrl.TabIndex = 2;
            this.OtaCheckCtrl.Text = "すべてをチェックにする";
            this.OtaCheckCtrl.UseVisualStyleBackColor = true;
            this.OtaCheckCtrl.CheckedChanged += new System.EventHandler(this.OtaCheckCtrl_CheckedChanged);
            // 
            // SystemMessageBox
            // 
            this.SystemMessageBox.Controls.Add(this.SystemMessageText);
            this.SystemMessageBox.Location = new System.Drawing.Point(308, 417);
            this.SystemMessageBox.Name = "SystemMessageBox";
            this.SystemMessageBox.Size = new System.Drawing.Size(460, 136);
            this.SystemMessageBox.TabIndex = 8;
            this.SystemMessageBox.TabStop = false;
            this.SystemMessageBox.Text = " システムメッセージ ";
            // 
            // SystemMessageText
            // 
            this.SystemMessageText.BackColor = System.Drawing.Color.White;
            this.SystemMessageText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SystemMessageText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SystemMessageText.Location = new System.Drawing.Point(3, 19);
            this.SystemMessageText.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.SystemMessageText.Multiline = true;
            this.SystemMessageText.Name = "SystemMessageText";
            this.SystemMessageText.ReadOnly = true;
            this.SystemMessageText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.SystemMessageText.Size = new System.Drawing.Size(454, 98);
            this.SystemMessageText.TabIndex = 0;
            // 
            // TimerSetup
            // 
            this.TimerSetup.Controls.Add(this.SetOnHold);
            this.TimerSetup.Controls.Add(this.TimerNoticeText);
            this.TimerSetup.Controls.Add(this.TimerSetDelete);
            this.TimerSetup.Controls.Add(this.TimerSetSet);
            this.TimerSetup.Controls.Add(this.LabelTimerDailySetMinute);
            this.TimerSetup.Controls.Add(this.TimerDailySetMinute);
            this.TimerSetup.Controls.Add(this.LabelTimerDailySetHour);
            this.TimerSetup.Controls.Add(this.TimerDailySetHour);
            this.TimerSetup.Controls.Add(this.LabelTimerDailySet);
            this.TimerSetup.Location = new System.Drawing.Point(308, 291);
            this.TimerSetup.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TimerSetup.Name = "TimerSetup";
            this.TimerSetup.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TimerSetup.Size = new System.Drawing.Size(460, 120);
            this.TimerSetup.TabIndex = 4;
            this.TimerSetup.TabStop = false;
            this.TimerSetup.Text = " タイマー設定 ";
            // 
            // SetOnHold
            // 
            this.SetOnHold.AutoSize = true;
            this.SetOnHold.Location = new System.Drawing.Point(439, 99);
            this.SetOnHold.Name = "SetOnHold";
            this.SetOnHold.Size = new System.Drawing.Size(15, 14);
            this.SetOnHold.TabIndex = 8;
            this.SetOnHold.UseVisualStyleBackColor = true;
            this.SetOnHold.CheckedChanged += new System.EventHandler(this.SetOnHold_CheckedChanged);
            // 
            // TimerNoticeText
            // 
            this.TimerNoticeText.AutoSize = true;
            this.TimerNoticeText.Location = new System.Drawing.Point(13, 77);
            this.TimerNoticeText.Name = "TimerNoticeText";
            this.TimerNoticeText.Size = new System.Drawing.Size(415, 30);
            this.TimerNoticeText.TabIndex = 5;
            this.TimerNoticeText.Text = "・ タイマーは１施設につき、４つまで設定できます。\r\n・ タイマーの間隔は１時間になります。また、同時刻に２つのタイマーは設定できません。";
            // 
            // TimerSetDelete
            // 
            this.TimerSetDelete.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.TimerSetDelete.Location = new System.Drawing.Point(327, 25);
            this.TimerSetDelete.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TimerSetDelete.Name = "TimerSetDelete";
            this.TimerSetDelete.Size = new System.Drawing.Size(117, 38);
            this.TimerSetDelete.TabIndex = 7;
            this.TimerSetDelete.Text = "タイマー削除";
            this.TimerSetDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.TimerSetDelete.UseVisualStyleBackColor = true;
            this.TimerSetDelete.Click += new System.EventHandler(this.TimerSetDelete_Click);
            // 
            // TimerSetSet
            // 
            this.TimerSetSet.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.TimerSetSet.Location = new System.Drawing.Point(204, 25);
            this.TimerSetSet.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TimerSetSet.Name = "TimerSetSet";
            this.TimerSetSet.Size = new System.Drawing.Size(117, 38);
            this.TimerSetSet.TabIndex = 6;
            this.TimerSetSet.Text = "タイマー登録";
            this.TimerSetSet.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.TimerSetSet.UseVisualStyleBackColor = true;
            this.TimerSetSet.Click += new System.EventHandler(this.TimerSetSet_Click);
            // 
            // LabelTimerDailySetMinute
            // 
            this.LabelTimerDailySetMinute.AutoSize = true;
            this.LabelTimerDailySetMinute.Location = new System.Drawing.Point(171, 36);
            this.LabelTimerDailySetMinute.Name = "LabelTimerDailySetMinute";
            this.LabelTimerDailySetMinute.Size = new System.Drawing.Size(19, 15);
            this.LabelTimerDailySetMinute.TabIndex = 4;
            this.LabelTimerDailySetMinute.Text = "分";
            // 
            // TimerDailySetMinute
            // 
            this.TimerDailySetMinute.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TimerDailySetMinute.FormattingEnabled = true;
            this.TimerDailySetMinute.Items.AddRange(new object[] {
            "00",
            "30"});
            this.TimerDailySetMinute.Location = new System.Drawing.Point(122, 32);
            this.TimerDailySetMinute.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TimerDailySetMinute.Name = "TimerDailySetMinute";
            this.TimerDailySetMinute.Size = new System.Drawing.Size(46, 23);
            this.TimerDailySetMinute.TabIndex = 3;
            // 
            // LabelTimerDailySetHour
            // 
            this.LabelTimerDailySetHour.AutoSize = true;
            this.LabelTimerDailySetHour.Location = new System.Drawing.Point(97, 36);
            this.LabelTimerDailySetHour.Name = "LabelTimerDailySetHour";
            this.LabelTimerDailySetHour.Size = new System.Drawing.Size(19, 15);
            this.LabelTimerDailySetHour.TabIndex = 2;
            this.LabelTimerDailySetHour.Text = "時";
            // 
            // TimerDailySetHour
            // 
            this.TimerDailySetHour.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TimerDailySetHour.FormattingEnabled = true;
            this.TimerDailySetHour.Items.AddRange(new object[] {
            "00",
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23"});
            this.TimerDailySetHour.Location = new System.Drawing.Point(48, 32);
            this.TimerDailySetHour.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TimerDailySetHour.Name = "TimerDailySetHour";
            this.TimerDailySetHour.Size = new System.Drawing.Size(46, 23);
            this.TimerDailySetHour.TabIndex = 1;
            // 
            // LabelTimerDailySet
            // 
            this.LabelTimerDailySet.AutoSize = true;
            this.LabelTimerDailySet.Location = new System.Drawing.Point(13, 36);
            this.LabelTimerDailySet.Name = "LabelTimerDailySet";
            this.LabelTimerDailySet.Size = new System.Drawing.Size(31, 15);
            this.LabelTimerDailySet.TabIndex = 0;
            this.LabelTimerDailySet.Text = "毎日";
            // 
            // LabelTimerList
            // 
            this.LabelTimerList.AutoSize = true;
            this.LabelTimerList.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LabelTimerList.Location = new System.Drawing.Point(3, 557);
            this.LabelTimerList.Name = "LabelTimerList";
            this.LabelTimerList.Size = new System.Drawing.Size(75, 15);
            this.LabelTimerList.TabIndex = 5;
            this.LabelTimerList.Text = "タイマーリスト";
            // 
            // TimerList
            // 
            this.TimerList.AllowUserToAddRows = false;
            this.TimerList.AllowUserToDeleteRows = false;
            this.TimerList.BackgroundColor = System.Drawing.Color.White;
            this.TimerList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TimerList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.TimerList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TimerRowId,
            this.TimerRowOta,
            this.TimerRowStartDate,
            this.TimerRowStartTrigger,
            this.TimerRowPeriod,
            this.TimerRowPerson,
            this.TimerRowDays,
            this.TimerRowExecDate,
            this.TimerRowExecTime});
            this.TimerList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.TimerList.Location = new System.Drawing.Point(3, 576);
            this.TimerList.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TimerList.MultiSelect = false;
            this.TimerList.Name = "TimerList";
            this.TimerList.ReadOnly = true;
            this.TimerList.RowHeadersVisible = false;
            this.TimerList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.TimerList.RowTemplate.Height = 21;
            this.TimerList.RowTemplate.ReadOnly = true;
            this.TimerList.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.TimerList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TimerList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TimerList.Size = new System.Drawing.Size(768, 100);
            this.TimerList.TabIndex = 6;
            // 
            // TimerRowId
            // 
            this.TimerRowId.HeaderText = "TID";
            this.TimerRowId.Name = "TimerRowId";
            this.TimerRowId.ReadOnly = true;
            this.TimerRowId.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.TimerRowId.Visible = false;
            this.TimerRowId.Width = 50;
            // 
            // TimerRowOta
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.TimerRowOta.DefaultCellStyle = dataGridViewCellStyle1;
            this.TimerRowOta.HeaderText = "OTA";
            this.TimerRowOta.Name = "TimerRowOta";
            this.TimerRowOta.ReadOnly = true;
            this.TimerRowOta.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.TimerRowOta.Width = 205;
            // 
            // TimerRowStartDate
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.TimerRowStartDate.DefaultCellStyle = dataGridViewCellStyle2;
            this.TimerRowStartDate.HeaderText = "取得開始日";
            this.TimerRowStartDate.Name = "TimerRowStartDate";
            this.TimerRowStartDate.ReadOnly = true;
            this.TimerRowStartDate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // TimerRowStartTrigger
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.TimerRowStartTrigger.DefaultCellStyle = dataGridViewCellStyle3;
            this.TimerRowStartTrigger.HeaderText = "日後";
            this.TimerRowStartTrigger.Name = "TimerRowStartTrigger";
            this.TimerRowStartTrigger.ReadOnly = true;
            this.TimerRowStartTrigger.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.TimerRowStartTrigger.Width = 50;
            // 
            // TimerRowPeriod
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.TimerRowPeriod.DefaultCellStyle = dataGridViewCellStyle4;
            this.TimerRowPeriod.HeaderText = "取得期間";
            this.TimerRowPeriod.Name = "TimerRowPeriod";
            this.TimerRowPeriod.ReadOnly = true;
            this.TimerRowPeriod.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.TimerRowPeriod.Width = 80;
            // 
            // TimerRowPerson
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.TimerRowPerson.DefaultCellStyle = dataGridViewCellStyle5;
            this.TimerRowPerson.HeaderText = "宿泊人数";
            this.TimerRowPerson.Name = "TimerRowPerson";
            this.TimerRowPerson.ReadOnly = true;
            this.TimerRowPerson.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.TimerRowPerson.Width = 80;
            // 
            // TimerRowDays
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.TimerRowDays.DefaultCellStyle = dataGridViewCellStyle6;
            this.TimerRowDays.HeaderText = "宿泊日数";
            this.TimerRowDays.Name = "TimerRowDays";
            this.TimerRowDays.ReadOnly = true;
            this.TimerRowDays.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.TimerRowDays.Width = 80;
            // 
            // TimerRowExecDate
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.TimerRowExecDate.DefaultCellStyle = dataGridViewCellStyle7;
            this.TimerRowExecDate.HeaderText = "実行日";
            this.TimerRowExecDate.Name = "TimerRowExecDate";
            this.TimerRowExecDate.ReadOnly = true;
            this.TimerRowExecDate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.TimerRowExecDate.Width = 80;
            // 
            // TimerRowExecTime
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.TimerRowExecTime.DefaultCellStyle = dataGridViewCellStyle8;
            this.TimerRowExecTime.HeaderText = "実行時間";
            this.TimerRowExecTime.Name = "TimerRowExecTime";
            this.TimerRowExecTime.ReadOnly = true;
            this.TimerRowExecTime.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.TimerRowExecTime.Width = 80;
            // 
            // NoticeBox
            // 
            this.NoticeBox.Controls.Add(this.NoticeText);
            this.NoticeBox.Location = new System.Drawing.Point(308, 8);
            this.NoticeBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.NoticeBox.Name = "NoticeBox";
            this.NoticeBox.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.NoticeBox.Size = new System.Drawing.Size(460, 100);
            this.NoticeBox.TabIndex = 7;
            this.NoticeBox.TabStop = false;
            this.NoticeBox.Text = " アップデート内容 ";
            // 
            // NoticeText
            // 
            this.NoticeText.BackColor = System.Drawing.Color.White;
            this.NoticeText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.NoticeText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NoticeText.Location = new System.Drawing.Point(3, 20);
            this.NoticeText.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.NoticeText.Multiline = true;
            this.NoticeText.Name = "NoticeText";
            this.NoticeText.ReadOnly = true;
            this.NoticeText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.NoticeText.Size = new System.Drawing.Size(454, 76);
            this.NoticeText.TabIndex = 0;
            // 
            // CrwlSetup
            // 
            this.CrwlSetup.Controls.Add(this.DaysFromToday);
            this.CrwlSetup.Controls.Add(this.LabelDaysFromToday2);
            this.CrwlSetup.Controls.Add(this.LabelDaysFromToday1);
            this.CrwlSetup.Controls.Add(this.StartDatePicker);
            this.CrwlSetup.Controls.Add(this.StartCrawling);
            this.CrwlSetup.Controls.Add(this.Days);
            this.CrwlSetup.Controls.Add(this.LabelDays);
            this.CrwlSetup.Controls.Add(this.Person);
            this.CrwlSetup.Controls.Add(this.LabelPerson);
            this.CrwlSetup.Controls.Add(this.Period);
            this.CrwlSetup.Controls.Add(this.LabelPeriod);
            this.CrwlSetup.Controls.Add(this.LabelStartDate);
            this.CrwlSetup.Location = new System.Drawing.Point(308, 113);
            this.CrwlSetup.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.CrwlSetup.Name = "CrwlSetup";
            this.CrwlSetup.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.CrwlSetup.Size = new System.Drawing.Size(460, 172);
            this.CrwlSetup.TabIndex = 3;
            this.CrwlSetup.TabStop = false;
            this.CrwlSetup.Text = " 取得条件設定 ";
            // 
            // DaysFromToday
            // 
            this.DaysFromToday.Location = new System.Drawing.Point(140, 60);
            this.DaysFromToday.Name = "DaysFromToday";
            this.DaysFromToday.Size = new System.Drawing.Size(40, 23);
            this.DaysFromToday.TabIndex = 3;
            this.DaysFromToday.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.DaysFromToday.TextChanged += new System.EventHandler(this.DaysFromToday_TextChanged);
            // 
            // LabelDaysFromToday2
            // 
            this.LabelDaysFromToday2.AutoSize = true;
            this.LabelDaysFromToday2.Location = new System.Drawing.Point(182, 65);
            this.LabelDaysFromToday2.Name = "LabelDaysFromToday2";
            this.LabelDaysFromToday2.Size = new System.Drawing.Size(98, 15);
            this.LabelDaysFromToday2.TabIndex = 4;
            this.LabelDaysFromToday2.Text = "日後の情報を取得)";
            // 
            // LabelDaysFromToday1
            // 
            this.LabelDaysFromToday1.AutoSize = true;
            this.LabelDaysFromToday1.Location = new System.Drawing.Point(85, 65);
            this.LabelDaysFromToday1.Name = "LabelDaysFromToday1";
            this.LabelDaysFromToday1.Size = new System.Drawing.Size(49, 15);
            this.LabelDaysFromToday1.TabIndex = 2;
            this.LabelDaysFromToday1.Text = "(今日から";
            // 
            // StartDatePicker
            // 
            this.StartDatePicker.CustomFormat = "";
            this.StartDatePicker.Location = new System.Drawing.Point(86, 28);
            this.StartDatePicker.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.StartDatePicker.MinDate = new System.DateTime(2017, 2, 1, 17, 28, 0, 0);
            this.StartDatePicker.Name = "StartDatePicker";
            this.StartDatePicker.Size = new System.Drawing.Size(203, 23);
            this.StartDatePicker.TabIndex = 1;
            this.StartDatePicker.Value = new System.DateTime(2017, 2, 1, 17, 28, 0, 0);
            this.StartDatePicker.CloseUp += new System.EventHandler(this.StartDatePicker_CloseUp);
            // 
            // StartCrawling
            // 
            this.StartCrawling.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.StartCrawling.Location = new System.Drawing.Point(301, 100);
            this.StartCrawling.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.StartCrawling.Name = "StartCrawling";
            this.StartCrawling.Size = new System.Drawing.Size(143, 58);
            this.StartCrawling.TabIndex = 12;
            this.StartCrawling.Text = "設定した取得条件で\r\n「取得開始」";
            this.StartCrawling.UseVisualStyleBackColor = true;
            this.StartCrawling.Click += new System.EventHandler(this.StartCrawling_Click);
            // 
            // Days
            // 
            this.Days.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Days.FormattingEnabled = true;
            this.Days.Location = new System.Drawing.Point(144, 137);
            this.Days.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Days.Name = "Days";
            this.Days.Size = new System.Drawing.Size(60, 23);
            this.Days.TabIndex = 11;
            // 
            // LabelDays
            // 
            this.LabelDays.AutoSize = true;
            this.LabelDays.Location = new System.Drawing.Point(83, 140);
            this.LabelDays.Name = "LabelDays";
            this.LabelDays.Size = new System.Drawing.Size(55, 15);
            this.LabelDays.TabIndex = 10;
            this.LabelDays.Text = "宿泊日数";
            // 
            // Person
            // 
            this.Person.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Person.FormattingEnabled = true;
            this.Person.Location = new System.Drawing.Point(144, 100);
            this.Person.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Person.Name = "Person";
            this.Person.Size = new System.Drawing.Size(60, 23);
            this.Person.TabIndex = 9;
            // 
            // LabelPerson
            // 
            this.LabelPerson.AutoSize = true;
            this.LabelPerson.Location = new System.Drawing.Point(83, 103);
            this.LabelPerson.Name = "LabelPerson";
            this.LabelPerson.Size = new System.Drawing.Size(55, 15);
            this.LabelPerson.TabIndex = 8;
            this.LabelPerson.Text = "宿泊人数";
            // 
            // Period
            // 
            this.Period.BackColor = System.Drawing.Color.White;
            this.Period.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Period.FormattingEnabled = true;
            this.Period.Location = new System.Drawing.Point(359, 29);
            this.Period.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Period.Name = "Period";
            this.Period.Size = new System.Drawing.Size(85, 23);
            this.Period.TabIndex = 7;
            // 
            // LabelPeriod
            // 
            this.LabelPeriod.AutoSize = true;
            this.LabelPeriod.Location = new System.Drawing.Point(298, 33);
            this.LabelPeriod.Name = "LabelPeriod";
            this.LabelPeriod.Size = new System.Drawing.Size(55, 15);
            this.LabelPeriod.TabIndex = 6;
            this.LabelPeriod.Text = "取得期間";
            // 
            // LabelStartDate
            // 
            this.LabelStartDate.AutoSize = true;
            this.LabelStartDate.Location = new System.Drawing.Point(13, 32);
            this.LabelStartDate.Name = "LabelStartDate";
            this.LabelStartDate.Size = new System.Drawing.Size(67, 15);
            this.LabelStartDate.TabIndex = 0;
            this.LabelStartDate.Text = "取得開始日";
            // 
            // OtaList
            // 
            this.OtaList.Controls.Add(this.OtaTree);
            this.OtaList.Location = new System.Drawing.Point(3, 291);
            this.OtaList.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.OtaList.Name = "OtaList";
            this.OtaList.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.OtaList.Size = new System.Drawing.Size(299, 262);
            this.OtaList.TabIndex = 1;
            this.OtaList.TabStop = false;
            this.OtaList.Text = "OTAリスト ";
            // 
            // OtaTree
            // 
            this.OtaTree.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.OtaTree.CheckBoxes = true;
            this.OtaTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OtaTree.Location = new System.Drawing.Point(3, 20);
            this.OtaTree.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.OtaTree.Name = "OtaTree";
            this.OtaTree.Size = new System.Drawing.Size(293, 238);
            this.OtaTree.TabIndex = 0;
            this.OtaTree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.OtaTree_AfterSelect);
            // 
            // HotelList
            // 
            this.HotelList.Controls.Add(this.HotelTree);
            this.HotelList.Location = new System.Drawing.Point(3, 8);
            this.HotelList.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.HotelList.Name = "HotelList";
            this.HotelList.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.HotelList.Size = new System.Drawing.Size(299, 260);
            this.HotelList.TabIndex = 0;
            this.HotelList.TabStop = false;
            this.HotelList.Text = " 施設リスト ";
            // 
            // HotelTree
            // 
            this.HotelTree.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.HotelTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HotelTree.HideSelection = false;
            this.HotelTree.Location = new System.Drawing.Point(3, 20);
            this.HotelTree.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.HotelTree.Name = "HotelTree";
            this.HotelTree.Size = new System.Drawing.Size(293, 236);
            this.HotelTree.TabIndex = 0;
            this.HotelTree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.HotelTree_AfterSelect);
            // 
            // LogTab
            // 
            this.LogTab.Controls.Add(this.LogSplitContainer);
            this.LogTab.Location = new System.Drawing.Point(4, 24);
            this.LogTab.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LogTab.Name = "LogTab";
            this.LogTab.Size = new System.Drawing.Size(774, 680);
            this.LogTab.TabIndex = 2;
            this.LogTab.Text = "取得履歴/ログ削除";
            this.LogTab.UseVisualStyleBackColor = true;
            // 
            // LogSplitContainer
            // 
            this.LogSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LogSplitContainer.IsSplitterFixed = true;
            this.LogSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.LogSplitContainer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LogSplitContainer.Name = "LogSplitContainer";
            this.LogSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // LogSplitContainer.Panel1
            // 
            this.LogSplitContainer.Panel1.Controls.Add(this.LogTree);
            this.LogSplitContainer.Panel1.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            // 
            // LogSplitContainer.Panel2
            // 
            this.LogSplitContainer.Panel2.Controls.Add(this.LogDelete);
            this.LogSplitContainer.Panel2.Controls.Add(this.LogRefresh);
            this.LogSplitContainer.Panel2.Controls.Add(this.LogDataDelete);
            this.LogSplitContainer.Size = new System.Drawing.Size(774, 680);
            this.LogSplitContainer.SplitterDistance = 612;
            this.LogSplitContainer.SplitterWidth = 5;
            this.LogSplitContainer.TabIndex = 0;
            // 
            // LogTree
            // 
            this.LogTree.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LogTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LogTree.Location = new System.Drawing.Point(0, 5);
            this.LogTree.Name = "LogTree";
            this.LogTree.Size = new System.Drawing.Size(774, 607);
            this.LogTree.TabIndex = 0;
            // 
            // LogDelete
            // 
            this.LogDelete.Location = new System.Drawing.Point(170, 13);
            this.LogDelete.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LogDelete.Name = "LogDelete";
            this.LogDelete.Size = new System.Drawing.Size(150, 35);
            this.LogDelete.TabIndex = 1;
            this.LogDelete.Text = " 全ての履歴を削除する";
            this.LogDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.LogDelete.UseVisualStyleBackColor = true;
            this.LogDelete.Click += new System.EventHandler(this.LogDelete_Click);
            // 
            // LogRefresh
            // 
            this.LogRefresh.Location = new System.Drawing.Point(10, 13);
            this.LogRefresh.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LogRefresh.Name = "LogRefresh";
            this.LogRefresh.Size = new System.Drawing.Size(150, 35);
            this.LogRefresh.TabIndex = 0;
            this.LogRefresh.Text = " 取得履歴を更新する";
            this.LogRefresh.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.LogRefresh.UseVisualStyleBackColor = true;
            this.LogRefresh.Click += new System.EventHandler(this.LogRefresh_Click);
            // 
            // LogDataDelete
            // 
            this.LogDataDelete.Location = new System.Drawing.Point(330, 13);
            this.LogDataDelete.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LogDataDelete.Name = "LogDataDelete";
            this.LogDataDelete.Size = new System.Drawing.Size(150, 35);
            this.LogDataDelete.TabIndex = 1;
            this.LogDataDelete.Text = " ログファイルを削除する";
            this.LogDataDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.LogDataDelete.UseVisualStyleBackColor = true;
            this.LogDataDelete.Click += new System.EventHandler(this.LogDataDelete_Click);
            // 
            // StatusStrip
            // 
            this.StatusStrip.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.DebugIndicator,
            this.LabelAccountInfo,
            this.MainProgressBar});
            this.StatusStrip.Location = new System.Drawing.Point(0, 745);
            this.StatusStrip.Name = "StatusStrip";
            this.StatusStrip.Padding = new System.Windows.Forms.Padding(1, 0, 16, 0);
            this.StatusStrip.Size = new System.Drawing.Size(790, 22);
            this.StatusStrip.SizingGrip = false;
            this.StatusStrip.TabIndex = 1;
            this.StatusStrip.Text = "Main StatusStrip";
            // 
            // DebugIndicator
            // 
            this.DebugIndicator.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.DebugIndicator.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.DebugIndicator.ForeColor = System.Drawing.Color.Red;
            this.DebugIndicator.Name = "DebugIndicator";
            this.DebugIndicator.Size = new System.Drawing.Size(21, 17);
            this.DebugIndicator.Text = "SI";
            // 
            // LabelAccountInfo
            // 
            this.LabelAccountInfo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.LabelAccountInfo.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LabelAccountInfo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.LabelAccountInfo.Name = "LabelAccountInfo";
            this.LabelAccountInfo.Size = new System.Drawing.Size(710, 17);
            this.LabelAccountInfo.Spring = true;
            this.LabelAccountInfo.Text = "Account Info.";
            this.LabelAccountInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // MainProgressBar
            // 
            this.MainProgressBar.Name = "MainProgressBar";
            this.MainProgressBar.Size = new System.Drawing.Size(40, 16);
            this.MainProgressBar.Step = 1;
            // 
            // NotifyIcon
            // 
            this.NotifyIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.NotifyIcon.BalloonTipText = "モジュールが最小化されました。";
            this.NotifyIcon.BalloonTipTitle = "アラーム";
            this.NotifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("NotifyIcon.Icon")));
            this.NotifyIcon.Tag = "";
            this.NotifyIcon.Text = "プラン価格調査PCモジュール";
            this.NotifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.NotifyIcon_MouseDoubleClick);
            // 
            // BackgroundWorker
            // 
            this.BackgroundWorker.WorkerReportsProgress = true;
            this.BackgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BackgroundWorker_DoWork);
            this.BackgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.BackgroundWorker_ProgressChanged);
            this.BackgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BackgroundWorker_RunWorkerCompleted);
            // 
            // NoPanel
            // 
            this.NoPanel.Location = new System.Drawing.Point(0, 0);
            this.NoPanel.Name = "NoPanel";
            this.NoPanel.Size = new System.Drawing.Size(10, 10);
            this.NoPanel.TabIndex = 2;
            // 
            // ToolTip
            // 
            this.ToolTip.AutoPopDelay = 8000;
            this.ToolTip.InitialDelay = 500;
            this.ToolTip.ReshowDelay = 100;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoScrollMargin = new System.Drawing.Size(2, 2);
            this.ClientSize = new System.Drawing.Size(790, 767);
            this.Controls.Add(this.NoPanel);
            this.Controls.Add(this.StatusStrip);
            this.Controls.Add(this.TabControl);
            this.Controls.Add(this.MenuStrip);
            this.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.MenuStrip;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(806, 806);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "プラン価格調査PCモジュール";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            this.MenuStrip.ResumeLayout(false);
            this.MenuStrip.PerformLayout();
            this.TabControl.ResumeLayout(false);
            this.CrwlTab.ResumeLayout(false);
            this.CrwlTab.PerformLayout();
            this.SystemMessageBox.ResumeLayout(false);
            this.SystemMessageBox.PerformLayout();
            this.TimerSetup.ResumeLayout(false);
            this.TimerSetup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TimerList)).EndInit();
            this.NoticeBox.ResumeLayout(false);
            this.NoticeBox.PerformLayout();
            this.CrwlSetup.ResumeLayout(false);
            this.CrwlSetup.PerformLayout();
            this.OtaList.ResumeLayout(false);
            this.HotelList.ResumeLayout(false);
            this.LogTab.ResumeLayout(false);
            this.LogSplitContainer.Panel1.ResumeLayout(false);
            this.LogSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LogSplitContainer)).EndInit();
            this.LogSplitContainer.ResumeLayout(false);
            this.StatusStrip.ResumeLayout(false);
            this.StatusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MenuStrip;
        private System.Windows.Forms.ToolStripMenuItem MenuLogout;
        private System.Windows.Forms.ToolStripMenuItem MenuExit;
        private System.Windows.Forms.ToolStripMenuItem MenuHelp;
        private System.Windows.Forms.ToolStripMenuItem MenuHelpWeb;
        private System.Windows.Forms.ToolStripMenuItem MenuHelpQuestion;
        private System.Windows.Forms.ToolStripMenuItem MenuHelpProxy;
        private System.Windows.Forms.TabControl TabControl;
        private System.Windows.Forms.TabPage CrwlTab;
        private System.Windows.Forms.StatusStrip StatusStrip;
        private System.Windows.Forms.TabPage LogTab;
        private System.Windows.Forms.SplitContainer LogSplitContainer;
        private System.Windows.Forms.Button LogDelete;
        private System.Windows.Forms.Button LogRefresh;
        private System.Windows.Forms.Button LogDataDelete;
        private System.Windows.Forms.GroupBox OtaList;
        private System.Windows.Forms.GroupBox HotelList;
        private System.Windows.Forms.GroupBox CrwlSetup;
        private System.Windows.Forms.ComboBox Days;
        private System.Windows.Forms.Label LabelDays;
        private System.Windows.Forms.ComboBox Person;
        private System.Windows.Forms.Label LabelPerson;
        private System.Windows.Forms.ComboBox Period;
        private System.Windows.Forms.Label LabelPeriod;
        private System.Windows.Forms.Label LabelStartDate;
        private System.Windows.Forms.Button StartCrawling;
        private System.Windows.Forms.GroupBox NoticeBox;
        private System.Windows.Forms.TextBox NoticeText;
        private System.Windows.Forms.DateTimePicker StartDatePicker;
        private System.Windows.Forms.TreeView OtaTree;
        private System.Windows.Forms.TreeView HotelTree;
        private System.Windows.Forms.ToolStripStatusLabel LabelAccountInfo;
        private System.Windows.Forms.NotifyIcon NotifyIcon;
        private System.ComponentModel.BackgroundWorker BackgroundWorker;
        private System.Windows.Forms.Label LabelTimerList;
        private System.Windows.Forms.DataGridView TimerList;
        private System.Windows.Forms.GroupBox TimerSetup;
        private System.Windows.Forms.Label LabelTimerDailySetMinute;
        private System.Windows.Forms.ComboBox TimerDailySetMinute;
        private System.Windows.Forms.Label LabelTimerDailySetHour;
        private System.Windows.Forms.ComboBox TimerDailySetHour;
        private System.Windows.Forms.Label LabelTimerDailySet;
        private System.Windows.Forms.Button TimerSetDelete;
        private System.Windows.Forms.Button TimerSetSet;
        private System.Windows.Forms.Label TimerNoticeText;
        private System.Windows.Forms.TextBox DaysFromToday;
        private System.Windows.Forms.Label LabelDaysFromToday2;
        private System.Windows.Forms.Label LabelDaysFromToday1;
        private System.Windows.Forms.ToolStripStatusLabel DebugIndicator;
        private System.Windows.Forms.ToolStripProgressBar MainProgressBar;
        private System.Windows.Forms.Panel NoPanel;
        private System.Windows.Forms.TreeView LogTree;
        private System.Windows.Forms.ToolStripMenuItem MenuTest;
        private System.Windows.Forms.ToolStripMenuItem MenuTestGo;
        private System.Windows.Forms.CheckBox SetOnHold;
        private System.Windows.Forms.ToolTip ToolTip;
        private System.Windows.Forms.GroupBox SystemMessageBox;
        private System.Windows.Forms.TextBox SystemMessageText;
        private System.Windows.Forms.DataGridViewTextBoxColumn TimerRowId;
        private System.Windows.Forms.DataGridViewTextBoxColumn TimerRowOta;
        private System.Windows.Forms.DataGridViewTextBoxColumn TimerRowStartDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn TimerRowStartTrigger;
        private System.Windows.Forms.DataGridViewTextBoxColumn TimerRowPeriod;
        private System.Windows.Forms.DataGridViewTextBoxColumn TimerRowPerson;
        private System.Windows.Forms.DataGridViewTextBoxColumn TimerRowDays;
        private System.Windows.Forms.DataGridViewTextBoxColumn TimerRowExecDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn TimerRowExecTime;
        private System.Windows.Forms.CheckBox OtaCheckCtrl;
        private System.Windows.Forms.ToolStripMenuItem MenuHelpAbout;
        private System.Windows.Forms.ToolStripSeparator MenuHelpSeparator1;
    }
}

