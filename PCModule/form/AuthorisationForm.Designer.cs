﻿namespace PCModule
{
    partial class AuthorisationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CloseButton = new System.Windows.Forms.Button();
            this.AuthButton = new System.Windows.Forms.Button();
            this.UserPw = new System.Windows.Forms.TextBox();
            this.LabelUserPw = new System.Windows.Forms.Label();
            this.UserId = new System.Windows.Forms.TextBox();
            this.LabelUserId = new System.Windows.Forms.Label();
            this.LabelNotice = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // CloseButton
            // 
            this.CloseButton.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.CloseButton.Location = new System.Drawing.Point(210, 98);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(80, 30);
            this.CloseButton.TabIndex = 17;
            this.CloseButton.Text = "終了";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // AuthButton
            // 
            this.AuthButton.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.AuthButton.Location = new System.Drawing.Point(86, 98);
            this.AuthButton.Name = "AuthButton";
            this.AuthButton.Size = new System.Drawing.Size(118, 30);
            this.AuthButton.TabIndex = 16;
            this.AuthButton.Text = "認証";
            this.AuthButton.UseVisualStyleBackColor = true;
            this.AuthButton.Click += new System.EventHandler(this.AuthButton_Click);
            // 
            // UserPw
            // 
            this.UserPw.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.UserPw.Location = new System.Drawing.Point(86, 69);
            this.UserPw.MaxLength = 100;
            this.UserPw.Name = "UserPw";
            this.UserPw.Size = new System.Drawing.Size(204, 23);
            this.UserPw.TabIndex = 14;
            this.UserPw.UseSystemPasswordChar = true;
            // 
            // LabelUserPw
            // 
            this.LabelUserPw.AutoSize = true;
            this.LabelUserPw.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LabelUserPw.Location = new System.Drawing.Point(25, 73);
            this.LabelUserPw.Name = "LabelUserPw";
            this.LabelUserPw.Size = new System.Drawing.Size(53, 15);
            this.LabelUserPw.TabIndex = 13;
            this.LabelUserPw.Text = "パスワード";
            // 
            // UserId
            // 
            this.UserId.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.UserId.Location = new System.Drawing.Point(86, 40);
            this.UserId.MaxLength = 100;
            this.UserId.Name = "UserId";
            this.UserId.Size = new System.Drawing.Size(204, 23);
            this.UserId.TabIndex = 12;
            // 
            // LabelUserId
            // 
            this.LabelUserId.AutoSize = true;
            this.LabelUserId.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LabelUserId.Location = new System.Drawing.Point(21, 44);
            this.LabelUserId.Name = "LabelUserId";
            this.LabelUserId.Size = new System.Drawing.Size(57, 15);
            this.LabelUserId.TabIndex = 11;
            this.LabelUserId.Text = "ログインID";
            // 
            // LabelNotice
            // 
            this.LabelNotice.AutoSize = true;
            this.LabelNotice.Font = new System.Drawing.Font("Meiryo UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LabelNotice.ForeColor = System.Drawing.Color.RoyalBlue;
            this.LabelNotice.Location = new System.Drawing.Point(5, 11);
            this.LabelNotice.Name = "LabelNotice";
            this.LabelNotice.Size = new System.Drawing.Size(304, 19);
            this.LabelNotice.TabIndex = 18;
            this.LabelNotice.Text = "RC管理画面のログイン情報を入力してください。";
            // 
            // AuthorisationForm
            // 
            this.AcceptButton = this.AuthButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.CancelButton = this.CloseButton;
            this.ClientSize = new System.Drawing.Size(314, 141);
            this.Controls.Add(this.LabelNotice);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.AuthButton);
            this.Controls.Add(this.UserPw);
            this.Controls.Add(this.LabelUserPw);
            this.Controls.Add(this.UserId);
            this.Controls.Add(this.LabelUserId);
            this.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AuthorisationForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "カスタマーサポートモード認証";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Button AuthButton;
        private System.Windows.Forms.TextBox UserPw;
        private System.Windows.Forms.Label LabelUserPw;
        private System.Windows.Forms.TextBox UserId;
        private System.Windows.Forms.Label LabelUserId;
        private System.Windows.Forms.Label LabelNotice;
    }
}