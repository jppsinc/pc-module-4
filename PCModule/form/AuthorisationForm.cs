﻿using System;
using System.Net;
using System.Text;
using System.Windows.Forms;
using Psinc.Util;

namespace PCModule
{
    public partial class AuthorisationForm : Form
    {
        private LoginForm _loginForm;


        public AuthorisationForm(LoginForm parent)
        {
            InitializeComponent();

            _loginForm = parent;
        }

        private void AuthButton_Click(object sender, EventArgs e)
        {
            string password = WebUtility.UrlEncode(Convert.ToBase64String(Encoding.UTF8.GetBytes(UserPw.Text.Trim())));

            string res;
            if (NetworkManager.Instance.GetUrlResponse(string.Format(NetworkManager.Instance.UrlFormat, DataBridge.CommandLoginAdmin), out res, UserId.Text.Trim(), password))
            {
                if (res.Equals("fail"))
                {
                    _loginForm.IsAuthorised = false;
                    MessageBox.Show(Messages.AuthorisationFail, Messages.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    string encrypted = Tools.EncryptString(string.Format("{0}:{1}", UserId.Text, res), _loginForm.Passphrase);

                    Tools.WriteContentsToFile(_loginForm.AuthContainer, encrypted, false);

                    _loginForm.IsAuthorised = true;
                    MessageBox.Show(Messages.AuthorisationSuccess, Messages.Info, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                _loginForm.IsAuthorised = false;
                MessageBox.Show(Messages.ServerConnectionFailed + "\r\n" + NetworkManager.Instance.ErrorMessage, Messages.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Close();
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
