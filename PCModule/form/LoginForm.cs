﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;
using Psinc.Crawler.DataStructure;
using Psinc.Util;
using Psinc.Util.DataStructure;

namespace PCModule
{
    public partial class LoginForm : Form
    {
        private bool _useProxy = false;
        private bool _isAuthorised = false;
        private string _adminLoginId = string.Empty;
        private string _accessToken = string.Empty;

        private readonly string _passwordFile = "rosetta.stone";
        private readonly string _authFile = "support.auth";
        private readonly string _passphrase = "psinc";


        public LoginForm()
        {
            InitializeComponent();
        }

        internal bool IsAuthorised
        {
            set
            {
                _isAuthorised = value;
            }
        }

        internal string AuthContainer
        {
            get
            {
                return _authFile;
            }
        }

        internal string Passphrase
        {
            get
            {
                return _passphrase;
            }
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            ConfigManager.Instance.LoadConfigs();

            // Checks an user ID save or not. '0' means 'false'.
            if (int.Parse(ConfigManager.Instance.GetConfigData(LoginSection.InUse)) != 0)
            {
                SaveUserId.Checked = true;
                UserId.Text = ConfigManager.Instance.GetConfigData(LoginSection.UserId);

                ActiveControl = UserPw;
            }

            // Gets a proxy information and applies it. '0' means proxy is not in use.
            if (int.Parse(ConfigManager.Instance.GetConfigData(ProxySection.InUse)) != 0)
            {
                _useProxy = true;

                ProxySetting proxySet = new ProxySetting();
                proxySet.InUse = _useProxy;
                proxySet.Name = ConfigManager.Instance.GetConfigData(ProxySection.Name);
                proxySet.Ip = ConfigManager.Instance.GetConfigData(ProxySection.Ip);
                proxySet.Port = int.Parse(ConfigManager.Instance.GetConfigData(ProxySection.Port));

                NetworkManager.Instance.ProxySetting = proxySet;
            }

            if (!DataBridge.Instance.IgnoreUpdate)
            {
                if (!DataBridge.Instance.IsDevMode) // '-checkupdate' option do not support 'dev mode'.
                {
                    // Checks an update of program.
                    Process.Start("updater.exe", "-4 -checkupdate").WaitForExit(); // Argument order is important.

                    string filename = "update_info";
                    string result = Tools.ReadContentsFromFile(filename);

                    if (File.Exists(filename))
                    {
                        File.Delete(filename);
                    }

                    if (!string.IsNullOrEmpty(result))
                    {
                        if (result.Equals("true"))
                        {
                            Process.Start("updater.exe", "-4 -quiet");
                            Environment.Exit(0);
                        }
                    }
                }
            }

            if (DataBridge.Instance.IsSupportMode)
            {
                SupportModeButton.Visible = true;
                LabelAuthorisedUser.Visible = true;
                UserPw.Visible = false;

                if (File.Exists(_authFile))
                {
                    LoadAuthInformation();
                }
                else
                {
                    LabelAuthorisedUser.Text = "認証ユーザー：未認証";
                }
            }
            else
            {
                SupportModeButton.Visible = false;
                LabelAuthorisedUser.Visible = false;
                UserPw.Visible = true;
            }
        }

        private void LoginForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            ConfigManager.Instance.SaveConfigs();
        }

        private void Login_Click(object sender, EventArgs e)
        {
            // User ID or password is empty.
            if ((string.IsNullOrEmpty(UserId.Text.Trim()) || string.IsNullOrEmpty(UserPw.Text.Trim())) && !_isAuthorised)
            {
                MessageBox.Show(Messages.NoInput, Messages.Alert, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                string password = WebUtility.UrlEncode(Convert.ToBase64String(Encoding.UTF8.GetBytes(UserPw.Text.Trim())));

                if (string.IsNullOrEmpty(password))
                {
                    password = "dummy";
                }

                string res;
                string extra = string.Format("?admin_id={0}&token={1}", _adminLoginId, _accessToken);
                if (NetworkManager.Instance.GetUrlResponseWithTail(string.Format(NetworkManager.Instance.UrlFormat, DataBridge.CommandLoginCustomer), out res, extra, UserId.Text.Trim(), password))
                {
                    int accountId;

                    if (int.TryParse(res, out accountId))
                    {
                        if (accountId > 0)
                        {
                            string accountName;
                            NetworkManager.Instance.GetUrlResponse(string.Format(NetworkManager.Instance.UrlFormat, DataBridge.CommandAccountInfo), out accountName, accountId.ToString());

                            if (accountName.Equals("err"))
                            {
                                MessageBox.Show(Messages.ErrorOccurred, Messages.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return;
                            }

                            string encrypted = Tools.EncryptString(UserPw.Text.Trim(), _passphrase);

                            ConfigManager.Instance.SetConfigData(LoginSection.AccountId, accountId.ToString());

                            Tools.WriteContentsToFile(_passwordFile, encrypted, false);

                            Hide();

                            string dummy = UserId.Text.Trim();

                            if (SaveUserId.Checked)
                            {
                                ConfigManager.Instance.SetConfigData(LoginSection.UserId, UserId.Text.Trim());
                                ConfigManager.Instance.SetConfigData(LoginSection.InUse, "1");
                            }
                            else
                            {
                                ConfigManager.Instance.SetConfigData(LoginSection.InUse, "0");

                                UserId.Text = string.Empty;
                            }

                            UserPw.Text = string.Empty;

                            ConfigManager.Instance.SaveConfigs();

                            AccountInfo.Instance.Initialize(accountId);
                            AccountInfo.Instance.AccountName = accountName;
                            AccountInfo.Instance.LoginId = dummy;
                            AccountInfo.Instance.LoginDate = DateTime.Now;
                            AccountInfo.Instance.Version = Application.ProductVersion;

                            if (DataBridge.Instance.IsDevMode)
                            {
                                NetworkManager.Instance.SendLog(AccountInfo.Instance.AccountId, LogType.INFO.ToString().ToLower(), string.Format("----- DEV MODE ({0} / {1})", AccountInfo.Instance.AccountId, Tools.GetLocalIPAddress()));
                            }

                            if (DataBridge.Instance.IsSupportMode)
                            {
                                NetworkManager.Instance.SendLog(AccountInfo.Instance.AccountId, LogType.INFO.ToString().ToLower(), string.Format("----- SUPPORT MODE ({0} / {1})", AccountInfo.Instance.AccountId, Tools.GetLocalIPAddress()));
                            }

                            MainForm mf = new MainForm();
                            mf.ShowDialog();
                        }
                        else
                        {
                            MessageBox.Show(Messages.NoAccount, Messages.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else if (res.Equals("ng"))
                    {
                        MessageBox.Show(Messages.NoAccount, Messages.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else if (res.Equals("fail"))
                    {
                        MessageBox.Show(Messages.LoginFailed, Messages.Alert, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else if (res.Equals("expired"))
                    {
                        MessageBox.Show(Messages.AccountExpired, Messages.Alert, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else if (res.Equals("unauthorised"))
                    {
                        MessageBox.Show(Messages.UnauthorisedAccount, Messages.Alert, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else if (res.Equals("invalid"))
                    {
                        MessageBox.Show(Messages.AuthorisationInvalid, Messages.Alert, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else if (res.Equals("error"))
                    {
                        MessageBox.Show(Messages.ErrorOccurred, Messages.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    // Fails to get response.
                    if (_useProxy)
                    {
                        MessageBox.Show(Messages.ProxyServerConnectionFailed + "\r\n" + NetworkManager.Instance.ErrorMessage, Messages.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show(Messages.ServerConnectionFailed + "\r\n" + NetworkManager.Instance.ErrorMessage, Messages.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    Close();
                }
            }
        }

        private void Close_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void SupportModeButton_Click(object sender, EventArgs e)
        {
            using (var auth = new AuthorisationForm(this))
            {
                auth.ShowDialog();

                if (_isAuthorised)
                {
                    LoadAuthInformation();
                }
            }
        }

        private void LoadAuthInformation()
        {
            string encrypted = Tools.ReadContentsFromFile(_authFile);

            string[] dummy = Tools.DecryptString(encrypted, _passphrase).Split(':');
            if (dummy.Length == 2)
            {
                _adminLoginId = dummy[0];
                _accessToken  = dummy[1];

                _isAuthorised = true;
                SupportModeButton.Enabled = false;

                LabelAuthorisedUser.Text = string.Format("認証ユーザー：{0}", _adminLoginId);
            }
            else
            {
                _adminLoginId = string.Empty;
                _accessToken  = string.Empty;

                _isAuthorised = false;
                SupportModeButton.Enabled = true;

                LabelAuthorisedUser.Text = "認証ユーザー：未認証";
            }
        }
    }
}