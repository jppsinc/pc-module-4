﻿using System;
using System.Windows.Forms;

namespace PCModule
{
    public partial class AboutForm : Form
    {
        private string _version = string.Empty;


        public AboutForm(string version)
        {
            InitializeComponent();

            _version = version;
        }

        private void AboutForm_Load(object sender, EventArgs e)
        {
            ProgramVersion.Text = string.Format("バージョン: {0} (32 ビット)", _version);
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
