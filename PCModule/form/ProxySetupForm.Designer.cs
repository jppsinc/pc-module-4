﻿namespace PCModule
{
    partial class ProxySetupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelProxyName = new System.Windows.Forms.Label();
            this.ProxyName = new System.Windows.Forms.TextBox();
            this.LabelProxyIP = new System.Windows.Forms.Label();
            this.ProxyIP = new System.Windows.Forms.TextBox();
            this.LabelProxyPort = new System.Windows.Forms.Label();
            this.ProxyPort = new System.Windows.Forms.TextBox();
            this.UseProxy = new System.Windows.Forms.CheckBox();
            this.ApplyButton = new System.Windows.Forms.Button();
            this.CloseButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LabelProxyName
            // 
            this.LabelProxyName.AutoSize = true;
            this.LabelProxyName.Location = new System.Drawing.Point(35, 20);
            this.LabelProxyName.Name = "LabelProxyName";
            this.LabelProxyName.Size = new System.Drawing.Size(57, 15);
            this.LabelProxyName.TabIndex = 0;
            this.LabelProxyName.Text = "プロキシ名";
            // 
            // ProxyName
            // 
            this.ProxyName.Location = new System.Drawing.Point(106, 16);
            this.ProxyName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ProxyName.MaxLength = 100;
            this.ProxyName.Name = "ProxyName";
            this.ProxyName.Size = new System.Drawing.Size(192, 23);
            this.ProxyName.TabIndex = 1;
            // 
            // LabelProxyIP
            // 
            this.LabelProxyIP.AutoSize = true;
            this.LabelProxyIP.Location = new System.Drawing.Point(37, 55);
            this.LabelProxyIP.Name = "LabelProxyIP";
            this.LabelProxyIP.Size = new System.Drawing.Size(57, 15);
            this.LabelProxyIP.TabIndex = 2;
            this.LabelProxyIP.Text = "プロキシIP";
            // 
            // ProxyIP
            // 
            this.ProxyIP.Location = new System.Drawing.Point(106, 51);
            this.ProxyIP.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ProxyIP.MaxLength = 15;
            this.ProxyIP.Name = "ProxyIP";
            this.ProxyIP.Size = new System.Drawing.Size(192, 23);
            this.ProxyIP.TabIndex = 3;
            // 
            // LabelProxyPort
            // 
            this.LabelProxyPort.AutoSize = true;
            this.LabelProxyPort.Location = new System.Drawing.Point(16, 90);
            this.LabelProxyPort.Name = "LabelProxyPort";
            this.LabelProxyPort.Size = new System.Drawing.Size(73, 15);
            this.LabelProxyPort.TabIndex = 4;
            this.LabelProxyPort.Text = "プロキシポート";
            // 
            // ProxyPort
            // 
            this.ProxyPort.Location = new System.Drawing.Point(106, 86);
            this.ProxyPort.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ProxyPort.MaxLength = 5;
            this.ProxyPort.Name = "ProxyPort";
            this.ProxyPort.Size = new System.Drawing.Size(58, 23);
            this.ProxyPort.TabIndex = 5;
            // 
            // UseProxy
            // 
            this.UseProxy.AutoSize = true;
            this.UseProxy.Location = new System.Drawing.Point(106, 114);
            this.UseProxy.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UseProxy.Name = "UseProxy";
            this.UseProxy.Size = new System.Drawing.Size(93, 19);
            this.UseProxy.TabIndex = 6;
            this.UseProxy.Text = "プロキシを使う";
            this.UseProxy.UseVisualStyleBackColor = true;
            // 
            // ApplyButton
            // 
            this.ApplyButton.Location = new System.Drawing.Point(106, 142);
            this.ApplyButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(93, 31);
            this.ApplyButton.TabIndex = 7;
            this.ApplyButton.Text = "保存";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // CloseButton
            // 
            this.CloseButton.Location = new System.Drawing.Point(205, 142);
            this.CloseButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(93, 31);
            this.CloseButton.TabIndex = 8;
            this.CloseButton.Text = "閉じる";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // ProxySetupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(314, 187);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.ApplyButton);
            this.Controls.Add(this.UseProxy);
            this.Controls.Add(this.ProxyPort);
            this.Controls.Add(this.LabelProxyPort);
            this.Controls.Add(this.ProxyIP);
            this.Controls.Add(this.LabelProxyIP);
            this.Controls.Add(this.ProxyName);
            this.Controls.Add(this.LabelProxyName);
            this.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "ProxySetupForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "プロキシ設定";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ProxySetupForm_FormClosing);
            this.Load += new System.EventHandler(this.ProxySetupForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelProxyName;
        private System.Windows.Forms.TextBox ProxyName;
        private System.Windows.Forms.Label LabelProxyIP;
        private System.Windows.Forms.TextBox ProxyIP;
        private System.Windows.Forms.Label LabelProxyPort;
        private System.Windows.Forms.TextBox ProxyPort;
        private System.Windows.Forms.CheckBox UseProxy;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.Button CloseButton;
    }
}