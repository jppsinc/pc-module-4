﻿using System;
using System.Net;
using System.Windows.Forms;
using Psinc.Util;
using Psinc.Util.DataStructure;

namespace PCModule
{
    public partial class ProxySetupForm : Form
    {
        private ProxySetting _proxyBackup;

        public ProxySetupForm()
        {
            InitializeComponent();

            _proxyBackup = new ProxySetting();
        }

        private void ProxySetupForm_Load(object sender, EventArgs e)
        {
            _proxyBackup.Name = ConfigManager.Instance.GetConfigData(ProxySection.Name);
            _proxyBackup.Ip = ConfigManager.Instance.GetConfigData(ProxySection.Ip);
            _proxyBackup.Port = int.Parse(ConfigManager.Instance.GetConfigData(ProxySection.Port));

            int dummy = int.Parse(ConfigManager.Instance.GetConfigData(ProxySection.InUse));

            if (dummy == 0)
            {
                // '0' means 'false'.
                _proxyBackup.InUse = false;
            }
            else
            {
                _proxyBackup.InUse = true;
            }

            ProxyName.Text = _proxyBackup.Name;
            ProxyIP.Text = _proxyBackup.Ip;
            ProxyPort.Text = _proxyBackup.Port.ToString();
            UseProxy.Checked = _proxyBackup.InUse;
        }

        private void ProxySetupForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if ((_proxyBackup.Name != ProxyName.Text.Trim()) || (_proxyBackup.Ip != ProxyIP.Text.Trim()) ||
                (_proxyBackup.Port != int.Parse(ProxyPort.Text.Trim())) || (_proxyBackup.InUse != UseProxy.Checked))
            {
                if (MessageBox.Show(Messages.SaveChanges, Messages.Alert, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    if (!CheckAndSaveSettings())
                    {
                        e.Cancel = true;
                    }
                }
            }
        }

        private void ApplyButton_Click(object sender, EventArgs e)
        {
            if (CheckAndSaveSettings())
            {
                // Update '_proxy' for closing.
                _proxyBackup.Name = ProxyName.Text.Trim();
                _proxyBackup.Ip = ProxyIP.Text.Trim();
                _proxyBackup.Port = int.Parse(ProxyPort.Text.Trim());
                _proxyBackup.InUse = UseProxy.Checked;

                Close();
            }
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private bool CheckAndSaveSettings()
        {
            // PROXY - NAME
            ConfigManager.Instance.SetConfigData(ProxySection.Name, ProxyName.Text.Trim());

            // PROXY - IP
            IPAddress dummy1;
            if (!IPAddress.TryParse(ProxyIP.Text.Trim(), out dummy1))
            {
                MessageBox.Show(Messages.ProxyIpAddressError, Messages.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else
            {
                ConfigManager.Instance.SetConfigData(ProxySection.Ip, ProxyIP.Text.Trim());
            }

            // PROXY - PORT
            int dummy2;
            if (!int.TryParse(ProxyPort.Text.Trim(), out dummy2))
            {
                MessageBox.Show(Messages.ProxyPortNumberError, Messages.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else if (dummy2 >= 0 && dummy2 <= 65535) // port number range (0 ~ 65535)
            {
                ConfigManager.Instance.SetConfigData(ProxySection.Port, ProxyPort.Text.Trim());
            }
            else
            {
                MessageBox.Show(Messages.ProxyPortNumberError, Messages.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            // PROXY - USE OR NOT
            if (UseProxy.Checked)
            {
                ConfigManager.Instance.SetConfigData(ProxySection.InUse, "1");
            }
            else
            {
                ConfigManager.Instance.SetConfigData(ProxySection.InUse, "0");
            }

            // SAVE
            ConfigManager.Instance.SaveConfigs();

            // Updates settings in 'NetworkManager'.
            ProxySetting dummy3 = new ProxySetting();
            dummy3.Name = ProxyName.Text.Trim();
            dummy3.Ip = ProxyIP.Text.Trim();
            dummy3.Port = int.Parse(ProxyPort.Text.Trim());
            dummy3.InUse = UseProxy.Checked;

            NetworkManager.Instance.ProxySetting = dummy3;

            return true;
        }
    }
}