﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Microsoft.Win32;
using Psinc.Crawler;
using Psinc.Crawler.DataStructure;
using Psinc.Util;

namespace PCModule
{
    public partial class MainForm : Form
    {
        private static int _systemMessageCounter = 0;

        private bool _isLogout;
        private int _selectedHotelId;
        private string _rawAccountInfo;

        private readonly string _rawTimerListTitle = "タイマーリスト / {0}";

        private readonly string _msgDefaultError   = "ERROR OCCURRED (MF): {0}";
        private readonly string _msgDatabaseError  = "ERROR OCCURRED (MF): CANNOT CONNECT TO DATABASE / {0}";
        private readonly string _msgLogin          = "LOGGED IN ({0} / {1} / VER.{2})";
        private readonly string _msgLogout         = "LOGGED OUT ({0} / {1})";
        private readonly string _msgQuit           = "QUITTED ({0} / {1})";
        private readonly string _msgZipDeleted     = "ZIP FILE DELETED (P{1}, T{2} -> {0}) / {3}";
        private readonly string _msgArDeleted      = "AR FILE DELETED (P{1}, T{2} -> {0}) / {3}";
        private readonly string _msgZipAllDeleted  = "ZIP FILE ALL DELETED ({1} -> {0}) / {2}";
        private readonly string _msgArAllDeleted   = "AR FILE ALL DELETED ({1} -> {0}) / {2}";
        private readonly string _msgUpdateRequired = "UPDATE REQUIRED / {0}";
        private readonly string _msgListReloaded   = "TIMER LIST RELOADED / {0}";
        private readonly string _msgJobStart       = "INSTANT JOB STARTED: {4} / {0}-{1}-{2} / {3}";
        private readonly string _msgJobComplete    = "INSTANT JOB COMPLETED: {4} / {0}-{1}-{2} / {3}";

        private delegate void AppendTimerListItemCallback(ScheduledCrawlerBase item);
        private delegate void RemoveTimerListItemCallback(int id);
        private delegate void UpdateTimerListItemCallback(int id, string cellName, string contents);
        private delegate void ReloadTimerListItemCallback();
        private delegate void AddTextOnSystemMessageCallback(string message);


        public MainForm()
        {
            InitializeComponent();

            _isLogout = false;
            _selectedHotelId = 0;
            _rawAccountInfo = string.Empty;
        }

        #region :: MAIN FORM

        private void MainForm_Load(object sender, EventArgs e)
        {
            MenuTest.Visible = false;
            SetOnHold.Visible = false;

            // SET DEBUG STATUS
            DebugIndicator.Text = string.Empty;

            if (DataBridge.Instance.IsSupportMode)
            {
                DebugIndicator.Text += "S";
            }

            if (DataBridge.Instance.IsDevMode)
            {
                DebugIndicator.Text += "D";
            }

            if (DataBridge.Instance.IgnoreUpdate)
            {
                DebugIndicator.Text += "x";
            }

            // SET TITLE
            Text = Text + " / VERSION: " + AccountInfo.Instance.Version + " / ID: " + AccountInfo.Instance.LoginId;

            if (NetworkManager.Instance.ProxySetting.InUse)
            {
                Text = Text + " / PROXY: " + NetworkManager.Instance.ProxySetting.Ip + ":" + NetworkManager.Instance.ProxySetting.Port.ToString();
            }

            NetworkManager.Instance.SendLog(AccountInfo.Instance.AccountId, LogType.INFO.ToString().ToLower(),
                                            string.Format(_msgLogin, AccountInfo.Instance.LoginId, Tools.GetLocalIPAddress(), AccountInfo.Instance.Version));

            // SET STATUS BAR
            _rawAccountInfo = string.Format(" {0} (2{1:0000})", AccountInfo.Instance.AccountName, AccountInfo.Instance.AccountId);
            LabelAccountInfo.Text = _rawAccountInfo;

            // SET NOTICE
            string res;
            if (NetworkManager.Instance.GetUrlResponse(string.Format(NetworkManager.Instance.UrlFormat, DataBridge.CommandLastestUpdateInfo), out res))
            {
                if (!res.Equals("err"))
                {
                    string[] tmp = res.Split(new char[] { '$' }, StringSplitOptions.RemoveEmptyEntries);

                    int cnt = 0;
                    string notice = string.Empty;

                    foreach (var item in tmp)
                    {
                        if (cnt == 0)
                        {
                            notice = string.Format("< {0} >", item);
                            cnt++;

                            continue;
                        }
                        else
                        {
                            notice += Environment.NewLine;
                        }

                        notice += string.Format("{0}. {1}", cnt, item.Trim());
                        cnt++;
                    }

                    NoticeText.Text = notice;
                }
            }

            // CONNECT TO DATABASE
            if (!SQLiteManager.Instance.ConnectionOpen("repchecker.sqlite"))
            {
                NetworkManager.Instance.SendLog(AccountInfo.Instance.AccountId, LogType.ERROR.ToString().ToLower(), string.Format(_msgDatabaseError, Tools.GetLocalIPAddress()));
            }

            // SET DELEGATE
            ScheduledCrawler.Instance.DelegateForAddTimer    = AddTimerListItem;
            ScheduledCrawler.Instance.DelegateForRemoveTimer = RemoveTimerListItem;
            ScheduledCrawler.Instance.DelegateForUpdateTimer = UpdateTimerListItem;
            ScheduledCrawler.Instance.DelegateForAddText     = AddTextOnSystemMessageList;

            // DATA CONFIGURATION --> moved

            // DELETE TEMPORARY FILES (EVERY A WEEK)
            DeleteTempFiles(7);

            // RUN METHOD (EVERY 24 HOURS)
            Tools.PeriodicRun(24, CheckUpdate, DeleteTempFiles, ReloadTimerList);

            // WATCH SYSTEM EVENT
            SystemEvents.PowerModeChanged += SystemEvents_PowerModeChanged;
            SystemEvents.SessionEnded += SystemEvents_SessionEnded;
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            NoPanel.Size = new Size(784, 738);

            // DATA CONFIGURATION
            SharedCommand.Instance.IsDevMode = DataBridge.Instance.IsDevMode;

            Dictionary<string, OtaInfoBase> otaInfo;
            if (LoadOtaInformation(out otaInfo))
            {
                SharedCommand.Instance.OtaInfo = otaInfo;
            }

            LoadAccountInformation();

            LoadCrawlingCondition();

            LoadLogData();

            LabelTimerList.Text = Messages.SelectOwnHotel;
            ScheduledCrawler.Instance.LoadTimerList();

            if (Period.Items.IndexOf("60") > 0)
            {
                Period.SelectedIndex = Period.Items.IndexOf("60");
            }
            else
            {
                Period.SelectedIndex = 0;
            }

            Person.SelectedIndex = 0;
            Days.SelectedIndex = 0;
            StartDatePicker.Value = DateTime.Now;
            StartDatePicker.MinDate = DateTime.Now;
            DaysFromToday.Text = "0";
            TimerDailySetHour.SelectedIndex = 0;
            TimerDailySetMinute.SelectedIndex = 0;

            NoPanel.Visible = false;

            Cursor = Cursors.Default;
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                NotifyIcon.Visible = true;
                NotifyIcon.ShowBalloonTip(1500);

                ShowInTaskbar = false;

                _isLogout = true;

                Hide();
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                return;
            }

            if (JobManager.Instance.IsJobInProgress)
            {
                MessageBox.Show(Messages.CrawlingJobInProgress, Messages.Alert, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Cancel = true;
            }
            else
            {
                if (_isLogout)
                {
                    NetworkManager.Instance.SendLog(AccountInfo.Instance.AccountId, LogType.INFO.ToString().ToLower(), string.Format(_msgLogout, AccountInfo.Instance.LoginId, Tools.GetLocalIPAddress()));

                    SQLiteManager.Instance.ConnectionClose();

                    ScheduledCrawler.Instance.InitializeTimerList();

                    foreach (var form in Application.OpenForms)
                    {
                        ((Form)form).Show();
                    }
                }
                else
                {
                    if (MessageBox.Show(Messages.QuitProgram, Messages.Confirm, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        SQLiteManager.Instance.ConnectionClose();
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }
            }
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (!_isLogout)
            {
                NetworkManager.Instance.SendLog(AccountInfo.Instance.AccountId, LogType.INFO.ToString().ToLower(), string.Format(_msgQuit, AccountInfo.Instance.LoginId, Tools.GetLocalIPAddress()));
                Application.Exit();
            }
        }

        private void NotifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            WindowState = FormWindowState.Normal;

            NotifyIcon.Visible = false;

            ShowInTaskbar = true;

            _isLogout = false;

            Show();
            Focus();
        }

        #endregion

        #region :: MENU STRIP

        private void MenuLogout_Click(object sender, EventArgs e)
        {
            if (JobManager.Instance.IsJobInProgress)
            {
                MessageBox.Show(Messages.CrawlingJobInProgress, Messages.Alert, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                _isLogout = true;
                Close();
            }
        }

        private void MenuExit_Click(object sender, EventArgs e)
        {
            if (JobManager.Instance.IsJobInProgress)
            {
                MessageBox.Show(Messages.CrawlingJobInProgress, Messages.Alert, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                Close();
            }
        }

        private void MenuHelpWeb_Click(object sender, EventArgs e) // HARD-CODED
        {
            Process.Start("https://repchecker.jp/");
        }

        private void MenuHelpQuestion_Click(object sender, EventArgs e) // HARD-CODED
        {
            Process.Start("https://repchecker.jp/contact/");
        }

        private void MenuHelpProxy_Click(object sender, EventArgs e)
        {
            if (JobManager.Instance.IsJobInProgress)
            {
                MessageBox.Show(Messages.CrawlingJobInProgress, Messages.Alert, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                ProxySetupForm proxySetup = new ProxySetupForm();
                proxySetup.ShowDialog();
            }
        }

        private void MenuHelpAbout_Click(object sender, EventArgs e)
        {
            AboutForm about = new AboutForm(AccountInfo.Instance.Version);
            about.ShowDialog();
        }

        private void MenuTestGo_Click(object sender, EventArgs e)
        {
            SharedCommand.Instance.TestAnything();
        }

        #endregion

        #region :: CRAWLING TAB

        private void HotelTree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            int dummy;

            TreeNode node = null;

            if (e.Node.Parent == null) // OWN HOTEL
            {
                node = e.Node;
            }
            else // COMPETITIVE HOTEL
            {
                node = e.Node.Parent;
            }

            int previouslySelectedHotelId = _selectedHotelId;

            if (int.TryParse(node.Tag.ToString(), out dummy))
            {
                _selectedHotelId = dummy;

                LabelTimerList.Text = string.Format(_rawTimerListTitle, node.Text);
                LabelAccountInfo.Text = string.Format("{0} → {1} ({2})", _rawAccountInfo, node.Text, _selectedHotelId);

                if (_selectedHotelId != previouslySelectedHotelId)
                {
                    TimerList.Rows.Clear();

                    foreach (KeyValuePair<int, OwnHotelBase> hotel in AccountInfo.Instance.Hotels)
                    {
                        if (hotel.Key == _selectedHotelId)
                        {
                            foreach (KeyValuePair<int, ScheduledCrawlerBase> timer in hotel.Value.TimerList)
                            {
                                AddTimerListItem(timer.Value);
                            }

                            break;
                        }
                    }
                }
            }
            else
            {
                _selectedHotelId = 0;
                LabelTimerList.Text = Messages.SelectOwnHotel;
            }
        }

        private void OtaTree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            e.Node.Checked = !e.Node.Checked;
        }

        private void OtaCheckCtrl_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < OtaTree.Nodes.Count; i++)
            {
                OtaTree.Nodes[i].Checked = OtaCheckCtrl.Checked;
            }
        }

        private void StartDatePicker_CloseUp(Object sender, EventArgs e)
        {
            DaysFromToday.Text = Math.Floor((StartDatePicker.Value - DateTime.Now.Date).TotalDays).ToString();
        }

        private void DaysFromToday_TextChanged(object sender, EventArgs e)
        {
            int dummy;
            if (int.TryParse(((TextBox)sender).Text, out dummy))
            {
                // range: 0 ~ days
                if (dummy >= 0)
                {
                    StartDatePicker.Value = DateTime.Now.AddDays(dummy);
                    return;
                }
            }

            if (!string.IsNullOrEmpty(((TextBox)sender).Text))
            {
                MessageBox.Show(Messages.TriggerNumberOutOfRange, Messages.Alert, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            ((TextBox)sender).Text = "0";
            ((TextBox)sender).SelectAll();
        }

        private void StartCrawling_Click(object sender, EventArgs e)
        {
            if (JobManager.Instance.IsJobInProgress)
            {
                MessageBox.Show(Messages.CrawlingJobInProgress, Messages.Alert, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (!DataBridge.Instance.IsDevMode)
                {
                    int remaining;
                    if (!InstantCrawler.Instance.CheckAvailability(out remaining))
                    {
                        MessageBox.Show(Messages.InstantCrawlerIntervalWarning + Environment.NewLine + string.Format(Messages.RemainingTimeToRun, remaining),
                                        Messages.Alert,
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Warning);
                        return;
                    }
                }

                if (MessageBox.Show(Messages.StartInstantCrawler, Messages.Confirm, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    bool hasNotSupportedOta = false;
                    int days = int.Parse(Days.SelectedItem.ToString());

                    string selectedOtaList = string.Empty;

                    int counter = 0;

                    for (int i = 0; i < OtaTree.GetNodeCount(false); i++)
                    {
                        if (OtaTree.Nodes[i].Checked)
                        {
                            if (counter != 0)
                            {
                                selectedOtaList += ",";
                            }

                            selectedOtaList += OtaTree.Nodes[i].Tag.ToString();

                            counter++;

                            //                                                       ↓expedia                                       ↓rurubu
                            if (days > 1 && (OtaTree.Nodes[i].Tag.ToString().Equals("8") || OtaTree.Nodes[i].Tag.ToString().Equals("21")))
                            {
                                hasNotSupportedOta = true;
                            }
                        }
                    }

                    if (string.IsNullOrEmpty(selectedOtaList) || _selectedHotelId == 0)
                    {
                        MessageBox.Show(Messages.NoSelected, Messages.Alert, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    if (hasNotSupportedOta)
                    {
                        MessageBox.Show(Messages.NotSupportedOTA, Messages.Caution, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }

                    int startTrigger = int.Parse(DaysFromToday.Text);
                    int period = int.Parse(Period.SelectedItem.ToString());

                    DateTime startDate = new DateTime();

                    if (startTrigger > 0)
                    {
                        startDate = DateTime.Now.AddDays(startTrigger);
                    }
                    else
                    {
                        startDate = StartDatePicker.Value;
                    }

                    string endDate = startDate.AddDays(period - 1).ToString("yyyy-MM-dd");

                    string[] now = DateTime.Now.ToString("yyyy-MM-dd HH mm").Split(' ');

                    CrawlerBase parameters = new CrawlerBase();
                    parameters.AccountId     = AccountInfo.Instance.AccountId;
                    parameters.HotelId       = _selectedHotelId;
                    parameters.OtaList       = selectedOtaList;
                    parameters.StartDate     = startDate.ToString("yyyy-MM-dd");
                    parameters.EndDate       = endDate;
                    parameters.StartTrigger  = startTrigger;
                    parameters.Period        = period;
                    parameters.Person        = int.Parse(Person.SelectedItem.ToString());
                    parameters.Days          = int.Parse(Days.SelectedItem.ToString());
                    parameters.ExecuteDate   = now[0];
                    parameters.ExecuteHour   = int.Parse(now[1]);
                    parameters.ExecuteMinute = int.Parse(now[2]);

                    string res;

                    //--> SERVER
                    if (NetworkManager.Instance.GetUrlResponse(string.Format(NetworkManager.Instance.UrlFormat, SharedCommand.CommandInsertRealtime),
                                                               out res,
                                                               parameters.AccountId.ToString(),
                                                               parameters.HotelId.ToString(),
                                                               string.Format("[{0}]", parameters.OtaList),
                                                               parameters.StartDate,
                                                               parameters.EndDate,
                                                               parameters.Person.ToString(),
                                                               parameters.Days.ToString()))
                    {
                        if (res.Equals("err"))
                        {
                            NetworkManager.Instance.SendLog(parameters.AccountId,
                                                            LogType.ERROR.ToString().ToLower(),
                                                            string.Format(SharedCommand.LogMessageInsertItemError, "NEW_REALTIME"));

                            MessageBox.Show(Messages.RegisterInstantCrawlerFailed, Messages.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                        else
                        {
                            parameters.CrawlerId = int.Parse(res);
                        }
                    }
                    else
                    {
                        NetworkManager.Instance.SendLog(parameters.AccountId,
                                                        LogType.ERROR.ToString().ToLower(),
                                                        string.Format(_msgDefaultError, NetworkManager.Instance.ErrorMessage));

                        MessageBox.Show(Messages.RegisterInstantCrawlerFailed, Messages.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    SharedCommand.Instance.Parameters = new CrawlerBase();
                    SharedCommand.Instance.Parameters = parameters;

                    BackgroundWorker.RunWorkerAsync();
                }
            }
        }

        private void BackgroundWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            NetworkManager.Instance.SendLog(AccountInfo.Instance.AccountId,
                                            LogType.INFO.ToString().ToLower(),
                                            string.Format(_msgJobStart, SharedCommand.Instance.Parameters.Period,
                                                                        SharedCommand.Instance.Parameters.Person,
                                                                        SharedCommand.Instance.Parameters.Days,
                                                                        Tools.GetLocalIPAddress(),
                                                                        SharedCommand.Instance.Parameters.CrawlerId));

            AddTextOnSystemMessageList(string.Format(SharedCommand.MessageStartInstantCrawling, DateTime.Now));

            string msg = string.Format(SharedCommand.RawMessageInstantCrawlingInfo, SharedCommand.Instance.Parameters.Person,
                                                                                    SharedCommand.Instance.Parameters.StartDate,
                                                                                    SharedCommand.Instance.Parameters.EndDate,
                                                                                    SharedCommand.Instance.Parameters.Period,
                                                                                    SharedCommand.Instance.Parameters.Days);

            AddTextOnSystemMessageList(string.Format(SharedCommand.MessageDefaultFormat, DateTime.Now, msg));

            int elapsed;

            if (InstantCrawler.Instance.Start(_selectedHotelId, out elapsed))
            {
                NetworkManager.Instance.SendLog(AccountInfo.Instance.AccountId,
                                                LogType.INFO.ToString().ToLower(),
                                                string.Format(_msgJobComplete, SharedCommand.Instance.Parameters.Period,
                                                                               SharedCommand.Instance.Parameters.Person,
                                                                               SharedCommand.Instance.Parameters.Days,
                                                                               Tools.GetLocalIPAddress(),
                                                                               SharedCommand.Instance.Parameters.CrawlerId));

                AddTextOnSystemMessageList(string.Format(SharedCommand.MessageElapsedTimeInfo, DateTime.Now, elapsed));
                AddTextOnSystemMessageList(string.Format(SharedCommand.MessageEndInstantCrawling, DateTime.Now));

                MessageBox.Show(Messages.CrawlingCompleted, Messages.Notice, MessageBoxButtons.OK, MessageBoxIcon.Information);

                string res;

                //--> SERVER
                if (NetworkManager.Instance.GetUrlResponse(string.Format(NetworkManager.Instance.UrlFormat, SharedCommand.CommandUpdateRealtime),
                                                           out res,
                                                           SharedCommand.Instance.Parameters.CrawlerId.ToString()))
                {
                    if (res.Equals("err"))
                    {
                        NetworkManager.Instance.SendLog(AccountInfo.Instance.AccountId,
                                                        LogType.ERROR.ToString().ToLower(),
                                                        string.Format(SharedCommand.LogMessageInsertItemError, SharedCommand.Instance.Parameters.CrawlerId));
                    }
                }
                else
                {
                    NetworkManager.Instance.SendLog(AccountInfo.Instance.AccountId,
                                                    LogType.ERROR.ToString().ToLower(),
                                                    string.Format(_msgDefaultError, NetworkManager.Instance.ErrorMessage));
                }

                // Inserts a log to local database.
                string otaNames = string.Empty;
                string[] otaListSplit = SharedCommand.Instance.Parameters.OtaList.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                int counter = 0;
                for (int i = 0; i < otaListSplit.Length; i++)
                {
                    if (counter != 0)
                    {
                        otaNames += " / ";
                    }

                    bool checker = false;

                    foreach (KeyValuePair<string, OtaInfoBase> ota in SharedCommand.Instance.OtaInfo)
                    {
                        if (ota.Value.Id == int.Parse(otaListSplit[i]))
                        {
                            otaNames += ota.Value.Name;

                            checker = true;
                            break;
                        }
                    }

                    if (!checker)
                    {
                        otaNames += "*";
                    }

                    counter++;
                }

                string query = string.Format(SharedCommand.QueryInsertLog, SharedCommand.Instance.Parameters.AccountId,
                                                                           SharedCommand.Instance.Parameters.HotelId,
                                                                           string.Format("{0} {1:00}:{2:00}:00", SharedCommand.Instance.Parameters.ExecuteDate,
                                                                                                                 SharedCommand.Instance.Parameters.ExecuteHour,
                                                                                                                 SharedCommand.Instance.Parameters.ExecuteMinute),
                                                                           CrawlerType.REALTIME.ToString(),
                                                                           otaNames,
                                                                           SharedCommand.Instance.Parameters.StartDate,
                                                                           SharedCommand.Instance.Parameters.Period,
                                                                           SharedCommand.Instance.Parameters.Person,
                                                                           SharedCommand.Instance.Parameters.Days);
                SQLiteManager.Instance.ExecuteNonQuery(query);
            }
            else
            {
                NetworkManager.Instance.SendLog(AccountInfo.Instance.AccountId, LogType.ERROR.ToString().ToLower(), InstantCrawler.Instance.ErrorMessage);

                AddTextOnSystemMessageList(string.Format(SharedCommand.MessageDefaultFormat, DateTime.Now, Messages.Error));
                MessageBox.Show(InstantCrawler.Instance.ErrorMessage, Messages.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BackgroundWorker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
        }

        private void BackgroundWorker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
        }

        private void TimerSetSet_Click(object sender, EventArgs e)
        {
            if (JobManager.Instance.IsJobInProgress)
            {
                MessageBox.Show(Messages.CrawlingJobInProgress, Messages.Alert, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                bool hasNotSupportedOta = false;
                int days = int.Parse(Days.SelectedItem.ToString());

                string selectedOtaList = string.Empty;

                int counter = 0;

                for (int i = 0; i < OtaTree.GetNodeCount(false); i++)
                {
                    if (OtaTree.Nodes[i].Checked)
                    {
                        if (counter != 0)
                        {
                            selectedOtaList += ",";
                        }

                        selectedOtaList += OtaTree.Nodes[i].Tag.ToString();

                        counter++;

                        //                                                       ↓expedia                                       ↓rurubu
                        if (days > 1 && (OtaTree.Nodes[i].Tag.ToString().Equals("8") || OtaTree.Nodes[i].Tag.ToString().Equals("21")))
                        {
                            hasNotSupportedOta = true;
                        }
                    }
                }

                if (string.IsNullOrEmpty(selectedOtaList) || _selectedHotelId == 0)
                {
                    MessageBox.Show(Messages.NoSelected, Messages.Alert, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (hasNotSupportedOta)
                {
                    MessageBox.Show(Messages.NotSupportedOTA, Messages.Caution, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                ScheduledCrawlerBase item = new ScheduledCrawlerBase();
                item.AccountId     = AccountInfo.Instance.AccountId;
                item.HotelId       = _selectedHotelId;
                item.TimerId       = 0;
                item.OtaList       = selectedOtaList;
                item.StartDate     = StartDatePicker.Value.ToString("yyyy-MM-dd");
                item.StartTrigger  = int.Parse(DaysFromToday.Text);
                item.Period        = int.Parse(Period.SelectedItem.ToString());
                item.Person        = int.Parse(Person.SelectedItem.ToString());
                item.Days          = int.Parse(Days.SelectedItem.ToString());
                item.ExecuteDate   = DateTime.Now.ToString("yyyy-MM-dd");
                item.ExecuteHour   = int.Parse(TimerDailySetHour.SelectedItem.ToString());
                item.ExecuteMinute = int.Parse(TimerDailySetMinute.SelectedItem.ToString());

                if (!AccountInfo.Instance.CheckTimerAvailability(item.HotelId, item))
                {
                    MessageBox.Show(Messages.ScheduledCrawlerIntervalWarning, Messages.Alert, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (!AccountInfo.Instance.CheckTimerCapacity(item.HotelId))
                {
                    AddTextOnSystemMessageList(string.Format(SharedCommand.MessageReachLimitation, DateTime.Now, AccountInfo.Instance.TimerLimit));

                    MessageBox.Show(Messages.ExceedScheduledCrawlerMaximum, Messages.Alert, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (MessageBox.Show(Messages.RegisterScheduledCrawler, Messages.Confirm, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    ScheduledCrawlerBase dummy;

                    if (ScheduledCrawler.Instance.AddScheduledCrawlerItem(item, out dummy, true))
                    {
                        AddTimerListItem(dummy);
                        MessageBox.Show(Messages.RegisterScheduledCrawlerSucceed, Messages.Notice, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show(Messages.RegisterScheduledCrawlerFailed, Messages.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void TimerSetDelete_Click(object sender, EventArgs e) // HARD-CODED
        {
            if (JobManager.Instance.IsJobInProgress)
            {
                MessageBox.Show(Messages.CrawlingJobInProgress, Messages.Alert, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                int selectedCount = TimerList.SelectedRows.Count;

                if (selectedCount < 1)
                {
                    MessageBox.Show(Messages.NoScheduledCrawlerSelected, Messages.Alert, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    if (MessageBox.Show(Messages.DeleteScheduledCrawler, Messages.Confirm, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        int selectedTimerId = -1;

                        for (int i = 0; i < selectedCount; i++)
                        {
                            selectedTimerId = int.Parse(TimerList.SelectedRows[0].Cells["TimerRowId"].Value.ToString());

                            if (ScheduledCrawler.Instance.RemoveScheduledCrawlerItem(selectedTimerId))
                            {
                                TimerList.Rows.Remove(TimerList.SelectedRows[0]);
                            }
                        }

                        MessageBox.Show(Messages.DeleteScheduledCrawlerSucceed, Messages.Notice, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        private void SetOnHold_CheckedChanged(object sender, EventArgs e)
        {
            ScheduledCrawler.Instance.OnHoldTimer = ((CheckBox)sender).Checked;
        }

        #endregion

        #region :: LOG TAB

        private void LogRefresh_Click(object sender, EventArgs e)
        {
            if (JobManager.Instance.IsJobInProgress)
            {
                MessageBox.Show(Messages.CrawlingJobInProgress, Messages.Alert, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                LoadLogData();

                MessageBox.Show(Messages.LogListUpdated, Messages.Notice, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void LogDelete_Click(object sender, EventArgs e)
        {
            if (JobManager.Instance.IsJobInProgress)
            {
                MessageBox.Show(Messages.CrawlingJobInProgress, Messages.Alert, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (LogTree.Nodes.Count != 0)
            {
                if (MessageBox.Show(Messages.DeleteAllLog, Messages.Confirm, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    string query = "delete from crwl_log where account_id=" + AccountInfo.Instance.AccountId;

                    if (SQLiteManager.Instance.ExecuteNonQuery(query))
                    {
                        LogTree.Nodes.Clear();

                        MessageBox.Show(Messages.LogListDeleted, Messages.Notice, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }
        private void LogDataDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(Messages.DeleteAllLogData, Messages.Confirm, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                DeleteTempAllFiles();
                MessageBox.Show(Messages.LogDataDeleted, Messages.Notice, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        #endregion

        #region :: METHODS

        private bool LoadOtaInformation(out Dictionary<string, OtaInfoBase> info)
        {
            info = new Dictionary<string, OtaInfoBase>();

            bool result = true;

            try
            {
                string res;

                // OTA INFORMATION
                if (NetworkManager.Instance.GetUrlResponse(string.Format(NetworkManager.Instance.UrlFormat, DataBridge.CommandOtaInfo), out res))
                {
                    if (string.IsNullOrEmpty(res))
                    {
                        result = false;
                    }
                    else
                    {
                        Dictionary<int, string> tmp = new Dictionary<int, string>();

                        string[] ota = res.Split('$');

                        for (int i = 0; i < ota.Length; i++)
                        {
                            string[] otaInfo = ota[i].Split('^');

                            if (otaInfo.Length != 6)
                            {
                                continue;
                            }

                            OtaInfoBase dummy = new OtaInfoBase();
                            dummy.Id           = int.Parse(otaInfo[0]);
                            dummy.Name         = otaInfo[2];
                            dummy.Abbreviation = otaInfo[1];
                            dummy.Url          = otaInfo[5];
                            dummy.SortOrder    = int.Parse(otaInfo[4]);

                            switch (otaInfo[3])
                            {
                                case "0":
                                    dummy.Type = OtaType.OTA;
                                    break;

                                case "1":
                                    dummy.Type = OtaType.ENGINE;
                                    break;

                                case "2":
                                    dummy.Type = OtaType.OVERSEAS;
                                    break;

                                case "3":
                                    dummy.Type = OtaType.LEISURE;
                                    break;

                                case "4":
                                    dummy.Type = OtaType.SNS;
                                    break;
                            }


                            info.Add(otaInfo[1], dummy);

                            tmp.Add(dummy.SortOrder, string.Format("{0}${1}", dummy.Name, dummy.Id));
                        }

                        OtaTree.Nodes.Clear();

                        foreach (KeyValuePair<int, string> item in tmp)
                        {
                            string[] dummy = item.Value.Split('$');

                            OtaTree.Nodes.Add(item.Key.ToString(), dummy[0]);
                            OtaTree.Nodes[item.Key.ToString()].Checked = true;
                            OtaTree.Nodes[item.Key.ToString()].Tag = dummy[1];
                        }
                    }
                }
            }
            catch
            {
                result = false;
            }

            return result;
        }

        private bool LoadAccountInformation()
        {
            bool result = true;

            try
            {
                string res;

                HotelTree.Nodes.Clear();

                // OWN HOTEL LIST
                if (NetworkManager.Instance.GetUrlResponse(string.Format(NetworkManager.Instance.UrlFormat, DataBridge.CommandOwnHotelList), out res, AccountInfo.Instance.AccountId.ToString()))
                {
                    if (string.IsNullOrEmpty(res))
                    {
                        result = false;
                    }
                    else
                    {
                        string[] ownHotels = res.Split('$');

                        MainProgressBar.Minimum = 0;
                        MainProgressBar.Maximum = ownHotels.Length;

                        for (int i = 0; i < ownHotels.Length; i++)
                        {
                            string[] ownHtlInfo = ownHotels[i].Split('^');

                            /*
                             * --> Hotel ID, Hotel Name, OTA List, Hotel Code List
                             */
                            if (ownHtlInfo.Length != 4)
                            {
                                continue;
                            }

                            string[] otas;
                            string[] codes;

                            int ownHotelId = int.Parse(ownHtlInfo[0]);

                            HotelTree.Nodes.Add(ownHtlInfo[0], ownHtlInfo[1]);
                            HotelTree.Nodes[ownHtlInfo[0]].Tag = ownHtlInfo[0];

                            // COMPETITIVE HOTEL LIST
                            Dictionary<int, CompetitiveHotelBase> dummy1 = new Dictionary<int, CompetitiveHotelBase>();

                            if (NetworkManager.Instance.GetUrlResponse(string.Format(NetworkManager.Instance.UrlFormat, DataBridge.CommandCompetitiveHotelList), out res, AccountInfo.Instance.AccountId.ToString(), ownHtlInfo[0]))
                            {
                                if (!string.IsNullOrEmpty(res))
                                {
                                    string[] comHotels = res.Split('$');

                                    for (int j = 0; j < comHotels.Length; j++)
                                    {
                                        string[] comHtlInfo = comHotels[j].Split('^');

                                        /*
                                         * --> Hotel ID, Hotel Name, OTA List, Hotel Code List
                                         */
                                        if (comHtlInfo.Length != 4)
                                        {
                                            continue;
                                        }

                                        otas  = comHtlInfo[2].Split(',');
                                        codes = comHtlInfo[3].Split(',');

                                        Dictionary<string, string> dummy3 = new Dictionary<string, string>();

                                        if (otas.Length == codes.Length)
                                        {
                                            for (int k = 0; k < otas.Length; k++)
                                            {
                                                if (string.IsNullOrEmpty(otas[k]))
                                                {
                                                    continue;
                                                }

                                                if (string.IsNullOrEmpty(codes[k]))
                                                {
                                                    continue;
                                                }

                                                string test = SharedCommand.Instance.GetOtaAbbreviation(int.Parse(otas[k]));

                                                if (!string.IsNullOrEmpty(test))
                                                {
                                                    dummy3.Add(test, codes[k]);
                                                }
                                            }
                                        }

                                        int competitiveHotelId = int.Parse(comHtlInfo[0]);

                                        CompetitiveHotelBase competitiveHotel = new CompetitiveHotelBase();
                                        competitiveHotel.HotelId          = competitiveHotelId;
                                        competitiveHotel.HotelName        = comHtlInfo[1];
                                        competitiveHotel.OtaList          = comHtlInfo[2];
                                        competitiveHotel.OtaHotelCodeList = dummy3;

                                        dummy1.Add(competitiveHotelId, competitiveHotel);

                                        HotelTree.Nodes[ownHtlInfo[0]].Nodes.Add(comHtlInfo[0], comHtlInfo[1]);
                                        HotelTree.Nodes[ownHtlInfo[0]].Nodes[comHtlInfo[0]].Tag = comHtlInfo[0];
                                    }
                                }
                            }

                            // TIMER LIST
                            Dictionary<int, ScheduledCrawlerBase> dummy2 = new Dictionary<int, ScheduledCrawlerBase>();

                            if (NetworkManager.Instance.GetUrlResponse(string.Format(NetworkManager.Instance.UrlFormat, DataBridge.CommandTimerList), out res, AccountInfo.Instance.AccountId.ToString(), ownHtlInfo[0]))
                            {
                                if (!string.IsNullOrEmpty(res))
                                {
                                    string[] timer = res.Split('$');

                                    for (int j = 0; j < timer.Length; j++)
                                    {
                                        string[] timerInfo = timer[j].Split('#');

                                        /*
                                         * --> Timer ID, Account ID, Hotel ID, OTA List, Start Date, Period, Days, Person, Execute Hour/Minute, Start Trigger
                                         */
                                        if (timerInfo.Length != 10)
                                        {
                                            continue;
                                        }

                                        int tmpAccountId = int.Parse(timerInfo[1]);
                                        int tmpHotelId   = int.Parse(timerInfo[2]);

                                        // Checks a timer item has right values.
                                        if (tmpAccountId != AccountInfo.Instance.AccountId && tmpHotelId != ownHotelId)
                                        {
                                            continue;
                                        }

                                        int timerId = int.Parse(timerInfo[0]);

                                        string[] dummy3 = timerInfo[8].Split(':');

                                        ScheduledCrawlerBase scheduledCrawlerItem = new ScheduledCrawlerBase();
                                        scheduledCrawlerItem.AccountId     = tmpAccountId;
                                        scheduledCrawlerItem.HotelId       = tmpHotelId;
                                        scheduledCrawlerItem.TimerId       = timerId;
                                        scheduledCrawlerItem.OtaList       = timerInfo[3].Split(new char[] { '[', ']' })[1];
                                        scheduledCrawlerItem.StartDate     = timerInfo[4];
                                        scheduledCrawlerItem.StartTrigger  = int.Parse(timerInfo[9]);
                                        scheduledCrawlerItem.Period        = int.Parse(timerInfo[5]);
                                        scheduledCrawlerItem.Person        = int.Parse(timerInfo[7]);
                                        scheduledCrawlerItem.Days          = int.Parse(timerInfo[6]);
                                        scheduledCrawlerItem.ExecuteDate   = DateTime.Now.ToString("yyyy-MM-dd");
                                        scheduledCrawlerItem.ExecuteHour   = int.Parse(dummy3[0]);
                                        scheduledCrawlerItem.ExecuteMinute = int.Parse(dummy3[1]);

                                        dummy2.Add(timerId, scheduledCrawlerItem);
                                    }
                                }
                            }

                            otas  = ownHtlInfo[2].Split(',');
                            codes = ownHtlInfo[3].Split(',');

                            Dictionary<string, string> dummy4 = new Dictionary<string, string>();

                            if (otas.Length == codes.Length)
                            {
                                for (int k = 0; k < otas.Length; k++)
                                {
                                    if (string.IsNullOrEmpty(otas[k]))
                                    {
                                        continue;
                                    }

                                    if (string.IsNullOrEmpty(codes[k]))
                                    {
                                        continue;
                                    }

                                    string test = SharedCommand.Instance.GetOtaAbbreviation(int.Parse(otas[k]));

                                    if (!string.IsNullOrEmpty(test))
                                    {
                                        dummy4.Add(test, codes[k]);
                                    }
                                }
                            }

                            OwnHotelBase ownHotel = new OwnHotelBase();
                            ownHotel.HotelId              = ownHotelId;
                            ownHotel.HotelName            = ownHtlInfo[1];
                            ownHotel.OtaList              = ownHtlInfo[2];
                            ownHotel.CompetitiveHotelList = dummy1;
                            ownHotel.TimerList            = dummy2;
                            ownHotel.OtaHotelCodeList     = dummy4;

                            AccountInfo.Instance.Hotels.Add(ownHotelId, ownHotel);

                            MainProgressBar.PerformStep();
                        }
                    }
                }
            }
            catch
            {
                result = false;
            }

            return result;
        }

        private void LoadCrawlingCondition()
        {
            string res;

            List<string> period = new List<string>(new string[] { "7", "14", "21", "30", "45", "60" });
            List<string> person = new List<string>(new string[] { "1", "2", "3", "4" });
            List<string> days = new List<string>(new string[] { "1", "2", "3", "4", "5", "6" });

            if (NetworkManager.Instance.GetUrlResponse(string.Format(NetworkManager.Instance.UrlFormat, DataBridge.CommandCrawlingCondition), out res, AccountInfo.Instance.AccountId.ToString(), "0"))
            {
                if (!string.IsNullOrEmpty(res))
                {
                    if (!res.Equals("no") && !res.Equals("err"))
                    {
                        string[] tmp1 = res.Split('$');
                        string[] tmp2;

                        for (int i = 0; i < tmp1.Length; i++)
                        {
                            switch (i)
                            {
                                case 0: // PERIOD
                                    {
                                        tmp2 = tmp1[i].Split(',');

                                        period = new List<string>();

                                        for (int j = 0; j < tmp2.Length; j++)
                                        {
                                            period.Add(tmp2[j]);
                                        }
                                    }
                                    break;

                                case 1: // PERSON
                                    {
                                        tmp2 = tmp1[i].Split(',');

                                        person = new List<string>();

                                        for (int j = 0; j < tmp2.Length; j++)
                                        {
                                            person.Add(tmp2[j]);
                                        }
                                    }
                                    break;

                                case 2: // DAYS
                                    {
                                        tmp2 = tmp1[i].Split(',');

                                        days = new List<string>();

                                        for (int j = 0; j < tmp2.Length; j++)
                                        {
                                            days.Add(tmp2[j]);
                                        }
                                    }
                                    break;

                                default:
                                    continue;
                            }
                        }
                    }
                }
            }

            foreach (var item in period)
            {
                Period.Items.Add(item);
            }

            foreach (var item in person)
            {
                Person.Items.Add(item);
            }

            foreach (var item in days)
            {
                Days.Items.Add(item);
            }
        }

        private void LoadLogData() // HARD-CODED
        {
            string query = "select * from crwl_log where account_id=" + AccountInfo.Instance.AccountId + " order by seq desc";

            DataSet dummy;
            if (SQLiteManager.Instance.ExecuteQuery(query, out dummy))
            {
                LogTree.Nodes.Clear();

                string seq = string.Empty;
                string hotelName = string.Empty;

                for (int i = 0; i < dummy.Tables[0].Rows.Count; i++)
                {
                    seq = dummy.Tables[0].Rows[i]["seq"].ToString();

                    hotelName = AccountInfo.Instance.GetHotelName(int.Parse(dummy.Tables[0].Rows[i]["hotel_id"].ToString()));

                    if (string.IsNullOrEmpty(hotelName))
                    {
                        hotelName = "削除されたホテル";
                    }

                    if (hotelName.Length > 45)
                    {
                        hotelName = hotelName.Substring(0, 45) + "..";
                    }

                    // NODE LV.1
                    LogTree.Nodes.Add(seq, string.Format("{2} / {0} / {1}", hotelName, dummy.Tables[0].Rows[i]["hotel_id"], dummy.Tables[0].Rows[i]["exec_date"]));

                    // NODE LV.2
                    LogTree.Nodes[seq].Nodes.Add(string.Format("タイプ: {0}", dummy.Tables[0].Rows[i]["crwl_type"]));
                    LogTree.Nodes[seq].Nodes.Add("OTA");
                    LogTree.Nodes[seq].Nodes.Add(string.Format("取得開始日: {0}", dummy.Tables[0].Rows[i]["start_date"]));
                    LogTree.Nodes[seq].Nodes.Add(string.Format("期間: {0}", dummy.Tables[0].Rows[i]["period"]));
                    LogTree.Nodes[seq].Nodes.Add(string.Format("人数: {0}", dummy.Tables[0].Rows[i]["person"]));
                    LogTree.Nodes[seq].Nodes.Add(string.Format("日数: {0}", dummy.Tables[0].Rows[i]["days"]));

                    // NODE LV.3
                    string[] tmp2 = dummy.Tables[0].Rows[i]["ota"].ToString().Split('/');

                    for (int j = 0; j < tmp2.Length; j++)
                    {
                        if (string.IsNullOrEmpty(tmp2[j]))
                        {
                            continue;
                        }

                        LogTree.Nodes[seq].Nodes[1].Nodes.Add(tmp2[j].Trim());
                    }
                }
            }
        }

        private void AddTimerListItem(ScheduledCrawlerBase item)
        {
            if (_selectedHotelId == 0)
            {
                return;
            }

            if (TimerList.InvokeRequired)
            {
                AppendTimerListItemCallback d = new AppendTimerListItemCallback(AddTimerListItem);
                Invoke(d, new object[] { item });
            }
            else
            {
                string startTrigger = "*";

                if (item.StartTrigger > 0)
                {
                    startTrigger = item.StartTrigger.ToString();
                }

                string otaNames = string.Empty;
                string[] otaListSplit = item.OtaList.Split(',');

                int counter = 0;
                for (int i = 0; i < otaListSplit.Length; i++)
                {
                    if (counter != 0)
                    {
                        otaNames += " / ";
                    }

                    if (string.IsNullOrEmpty(otaListSplit[i]))
                    {
                        continue;
                    }

                    bool checker = false;
                    foreach (KeyValuePair<string, OtaInfoBase> ota in SharedCommand.Instance.OtaInfo)
                    {
                        if (ota.Value.Id == int.Parse(otaListSplit[i]))
                        {
                            otaNames += ota.Value.Name;

                            checker = true;
                            break;
                        }
                    }

                    if (!checker)
                    {
                        otaNames += "*";
                    }

                    counter++;
                }

                TimerList.Rows.Add(item.TimerId, otaNames, item.StartDate, startTrigger, item.Period, item.Person, item.Days, item.ExecuteDate, string.Format("{0:00}:{1:00}", item.ExecuteHour, item.ExecuteMinute));
            }
        }

        private void AddTextOnSystemMessageList(string message)
        {
            if (string.IsNullOrEmpty(message))
            {
                return;
            }

            if (SystemMessageText.InvokeRequired)
            {
                AddTextOnSystemMessageCallback d = new AddTextOnSystemMessageCallback(AddTextOnSystemMessageList);
                Invoke(d, new object[] { message });
            }
            else
            {
                string oldMessages = string.Empty;

                if (_systemMessageCounter < 30)
                {
                    oldMessages = SystemMessageText.Text;
                }
                else
                {
                    _systemMessageCounter = 0;
                }

                SystemMessageText.Clear();

                SystemMessageText.Text += message;

                if (_systemMessageCounter != 0)
                {
                    SystemMessageText.Text += Environment.NewLine;
                    SystemMessageText.Text += oldMessages;
                }

                _systemMessageCounter++;
            }
        }

        private void RemoveTimerListItem(int id) // HARD-CODED
        {
            if (TimerList.InvokeRequired)
            {
                RemoveTimerListItemCallback d = new RemoveTimerListItemCallback(RemoveTimerListItem);
                Invoke(d, new object[] { id });
            }
            else
            {
                for (int i = 0; i < TimerList.Rows.Count; i++)
                {
                    if (int.Parse(TimerList.Rows[i].Cells["TimerRowId"].Value.ToString()) == id)
                    {
                        TimerList.Rows.Remove(TimerList.Rows[i]);
                        break;
                    }
                }
            }
        }

        private void DeleteTempFiles()
        {
            DeleteTempFiles(7);
        }

        private void DeleteTempFiles(int days)
        {
            DateTime today = DateTime.Today;
            DateTime previousDay = today.Subtract(TimeSpan.FromDays(days));

            // ZIP
            string zipPath = Path.Combine(Environment.CurrentDirectory, "zip");

            if (Directory.Exists(zipPath))
            {
                DirectoryInfo di = new DirectoryInfo(zipPath);
                int counter = 0;

                foreach (var fi in di.GetFiles("*.zip"))
                {
                    if (fi.LastWriteTime.Date <= previousDay.Date)
                    {
                        fi.Delete();
                        counter++;
                    }
                }

                if (counter > 0)
                {
                    NetworkManager.Instance.SendLog(AccountInfo.Instance.AccountId, LogType.INFO.ToString().ToLower(), string.Format(_msgZipDeleted, counter, previousDay.ToString("yyMMdd"), today.ToString("yyMMdd"), Tools.GetLocalIPAddress()));
                }
            }

            // AR (Analysis Result)
            string resPath = Path.Combine(Environment.CurrentDirectory, "res");

            if (Directory.Exists(resPath))
            {
                DirectoryInfo di = new DirectoryInfo(resPath);
                int counter = 0;

                foreach (var fi in di.GetFiles("*.ar"))
                {
                    if (fi.LastWriteTime.Date <= previousDay.Date)
                    {
                        fi.Delete();
                        counter++;
                    }
                }

                if (counter > 0)
                {
                    NetworkManager.Instance.SendLog(AccountInfo.Instance.AccountId, LogType.INFO.ToString().ToLower(), string.Format(_msgArDeleted, counter, previousDay.ToString("yyMMdd"), today.ToString("yyMMdd"), Tools.GetLocalIPAddress()));
                }
            }

            //rc4フォルダーにある削除されていないログファイルを削除
            //rc4 -> Crawle Folder, AR File
            previousDay = today.Subtract(TimeSpan.FromDays(1));
            string arPath = Environment.CurrentDirectory;
            if (Directory.Exists(arPath))
            {
                DirectoryInfo di = new DirectoryInfo(arPath);
                int counter = 0;

                //Crawle Folder
                foreach (var fi in di.GetDirectories())
                {
                    string[] arr = fi.Name.Split('_');
                    if (arr.Length != 6)
                    {
                        continue;
                    }
                    if (fi.LastWriteTime.Date <= previousDay.Date)
                    {
                        fi.Delete(true);
                        counter++;
                    }
                }

                if (counter > 0)
                {
                    string _msgCrawlFolderDeleted = "DEFAULT DIRECTORY FOLDER DELETED (P{1}, T{2} -> {0}) / {3}";
                    NetworkManager.Instance.SendLog(AccountInfo.Instance.AccountId, LogType.INFO.ToString().ToLower(), string.Format(_msgCrawlFolderDeleted, counter, previousDay.ToString("yyMMdd"), today.ToString("yyMMdd"), Tools.GetLocalIPAddress()));
                }

                //AR Files
                counter = 0;
                foreach (var fi in di.GetFiles("*.ar"))
                {
                    if (fi.LastWriteTime.Date <= previousDay.Date)
                    {
                        fi.Delete();
                        counter++;
                    }
                }

                if (counter > 0)
                {
                    string _msgArFileDeleted = "DEFAULT DIRECTORY AR DELETED (P{1}, T{2} -> {0}) / {3}";
                    NetworkManager.Instance.SendLog(AccountInfo.Instance.AccountId, LogType.INFO.ToString().ToLower(), string.Format(_msgArFileDeleted, counter, previousDay.ToString("yyMMdd"), today.ToString("yyMMdd"), Tools.GetLocalIPAddress()));
                }
            }
        }

        private void DeleteTempAllFiles()
        {
            DateTime today = DateTime.Today;

            // ZIP
            string zipPath = Path.Combine(Environment.CurrentDirectory, "zip");

            if (Directory.Exists(zipPath))
            {
                DirectoryInfo di = new DirectoryInfo(zipPath);
                int counter = 0;

                foreach (var fi in di.GetFiles("*.zip"))
                {
                    fi.Delete();
                    counter++;
                }
                
                if (counter > 0)
                {
                    NetworkManager.Instance.SendLog(AccountInfo.Instance.AccountId, LogType.INFO.ToString().ToLower(), string.Format(_msgZipAllDeleted, counter, today.ToString("yyMMdd"), Tools.GetLocalIPAddress()));
                }
            }

            // AR (Analysis Result)
            string resPath = Path.Combine(Environment.CurrentDirectory, "res");

            if (Directory.Exists(resPath))
            {
                DirectoryInfo di = new DirectoryInfo(resPath);
                int counter = 0;

                foreach (var fi in di.GetFiles("*.ar"))
                {
                    fi.Delete();
                    counter++;
                }

                if (counter > 0)
                {
                    NetworkManager.Instance.SendLog(AccountInfo.Instance.AccountId, LogType.INFO.ToString().ToLower(), string.Format(_msgArAllDeleted, counter, today.ToString("yyMMdd"), Tools.GetLocalIPAddress()));
                }
            }

            //rc4フォルダーにある削除されていないログファイルを削除
            //rc4 -> Crawle Folder, AR File
            DateTime previousDay = today.Subtract(TimeSpan.FromDays(1));
            previousDay = today.Subtract(TimeSpan.FromDays(1));
            string arPath = Environment.CurrentDirectory;
            if (Directory.Exists(arPath))
            {
                DirectoryInfo di = new DirectoryInfo(arPath);
                int counter = 0;

                //Crawle Folder
                foreach (var fi in di.GetDirectories())
                {
                    string[] arr = fi.Name.Split('_');
                    if (arr.Length != 6)
                    {
                        continue;
                    }
                    if (fi.LastWriteTime.Date <= previousDay.Date)
                    {
                        fi.Delete(true);
                        counter++;
                    }
                }

                if (counter > 0)
                {
                    string _msgCrawlFolderDeleted = "DEFAULT DIRECTORY FOLDER DELETED (P{1}, T{2} -> {0}) / {3}";
                    NetworkManager.Instance.SendLog(AccountInfo.Instance.AccountId, LogType.INFO.ToString().ToLower(), string.Format(_msgCrawlFolderDeleted, counter, previousDay.ToString("yyMMdd"), today.ToString("yyMMdd"), Tools.GetLocalIPAddress()));
                }

                //AR Files
                counter = 0;
                foreach (var fi in di.GetFiles("*.ar"))
                {
                    if (fi.LastWriteTime.Date <= previousDay.Date)
                    {
                        fi.Delete();
                        counter++;
                    }
                }

                if (counter > 0)
                {
                    string _msgArFileDeleted = "DEFAULT DIRECTORY AR DELETED (P{1}, T{2} -> {0}) / {3}";
                    NetworkManager.Instance.SendLog(AccountInfo.Instance.AccountId, LogType.INFO.ToString().ToLower(), string.Format(_msgArFileDeleted, counter, previousDay.ToString("yyMMdd"), today.ToString("yyMMdd"), Tools.GetLocalIPAddress()));
                }
            }
        }

        private void UpdateTimerListItem(int id, string cellName, string contents) // HARD-CODED
        {
            if (string.IsNullOrEmpty(cellName))
            {
                return;
            }

            if (TimerList.InvokeRequired)
            {
                UpdateTimerListItemCallback d = new UpdateTimerListItemCallback(UpdateTimerListItem);
                Invoke(d, new object[] { id, cellName, contents });
            }
            else
            {
                for (int i = 0; i < TimerList.Rows.Count; i++)
                {
                    if (int.Parse(TimerList.Rows[i].Cells["TimerRowId"].Value.ToString()) == id)
                    {
                        TimerList.Rows[i].Cells[cellName].Value = contents;
                        break;
                    }
                }

                TimerList.Invalidate();
            }
        }

        private void CheckUpdate()
        {
            if (JobManager.Instance.IsJobInProgress)
            {
                return;
            }

            if (DataBridge.Instance.IgnoreUpdate)
            {
                return;
            }

            Process.Start("updater.exe", "-4 -checkupdate").WaitForExit(); // Argument order is important.

            string filename = "update_info";
            string result = Tools.ReadContentsFromFile(filename);

            if (File.Exists(filename))
            {
                File.Delete(filename);
            }

            if (!string.IsNullOrEmpty(result))
            {
                if (result.Equals("true"))
                {
                    NetworkManager.Instance.SendLog(AccountInfo.Instance.AccountId, LogType.PULSE.ToString().ToLower(), string.Format(_msgUpdateRequired, Tools.GetLocalIPAddress()));

                    string args = string.Empty;

                    if (DataBridge.Instance.IsDevMode)
                    {
                        args += "-dev";
                    }

                    args = Tools.AddSpace(args);
                    args += "-restart -quiet -4";

                    Process.Start("updater.exe", args);
                    Environment.Exit(0);
                }
            }
        }

        private void ReloadTimerList()
        {
            if (JobManager.Instance.IsJobInProgress)
            {
                return;
            }

            if (TimerList.InvokeRequired)
            {
                ReloadTimerListItemCallback d = new ReloadTimerListItemCallback(ReloadTimerList);
                Invoke(d, null);
            }
            else
            {
                TimerList.Rows.Clear();

                _selectedHotelId = 0;

                ScheduledCrawler.Instance.InitializeTimerList();
                ScheduledCrawler.Instance.LoadTimerList();

                NetworkManager.Instance.SendLog(AccountInfo.Instance.AccountId, LogType.PULSE.ToString().ToLower(), string.Format(_msgListReloaded, Tools.GetLocalIPAddress()));
            }
        }

        private void SystemEvents_PowerModeChanged(object sender, PowerModeChangedEventArgs e)
        {
            if (e.Mode == PowerModes.Suspend)
            {
                NetworkManager.Instance.SendLog(AccountInfo.Instance.AccountId, LogType.INFO.ToString().ToLower(), string.Format("POWER MODE: SUSPEND / {0}", Tools.GetLocalIPAddress()));
            }
            else if (e.Mode == PowerModes.Resume)
            {
                ReloadTimerList();

                NetworkManager.Instance.SendLog(AccountInfo.Instance.AccountId, LogType.INFO.ToString().ToLower(), string.Format("POWER MODE: RESUME / {0}", Tools.GetLocalIPAddress()));
            }
        }

        private void SystemEvents_SessionEnded(object sender, SessionEndedEventArgs e)
        {
            if (e.Reason == SessionEndReasons.Logoff)
            {
                NetworkManager.Instance.SendLog(AccountInfo.Instance.AccountId, LogType.INFO.ToString().ToLower(), string.Format("SESSION END: LOG OFF / {0}", Tools.GetLocalIPAddress()));
            }
            else if (e.Reason == SessionEndReasons.SystemShutdown)
            {
                NetworkManager.Instance.SendLog(AccountInfo.Instance.AccountId, LogType.INFO.ToString().ToLower(), string.Format("SESSION END: SHUT DOWN / {0}", Tools.GetLocalIPAddress()));
            }
        }

        #endregion
    }
}