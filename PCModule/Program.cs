﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace PCModule
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            if (Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName).Length > 1)
            {
                return;
            }

            string[] args = Environment.GetCommandLineArgs();

            for (int i = 0; i < args.Length; i++)
            {
                if (args[i].Equals("-dev"))
                {
                    DataBridge.Instance.IsDevMode = true;
                    continue;
                }

                if (args[i].Equals("-support"))
                {
                    DataBridge.Instance.IsSupportMode = true;
                    continue;
                }

                if (args[i].Equals("-noupdate"))
                {
                    DataBridge.Instance.IgnoreUpdate = true;
                    continue;
                }
            }

            if (!DataBridge.Instance.IsDevMode)
            {
                // Action to set an URL on 'NetworkManager' class.
                DataBridge.Instance.IsDevMode = false;
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new LoginForm());
        }
    }
}