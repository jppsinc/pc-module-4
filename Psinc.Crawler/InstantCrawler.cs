﻿using System;
using System.IO;
using Psinc.Crawler.DataStructure;
using Psinc.Util;

namespace Psinc.Crawler
{
    /// <summary>
    /// Class for instant web crawler.
    /// </summary>
    public class InstantCrawler
    {
        private static InstantCrawler _instance = null;

        private string _errorMessage = string.Empty;


        /// <summary>
        /// Private constructor.
        /// </summary>
        private InstantCrawler()
        {
        }

        /// <summary>
        /// Gets an instance of 'InstantCrawler'.
        /// </summary>
        public static InstantCrawler Instance
        {
            get
            {
                _instance = _instance ?? new InstantCrawler();
                return _instance;
            }
        }

        /// <summary>
        /// Gets a message of last occurred error.
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return _errorMessage;
            }
        }

        /// <summary>
        /// Checks an availability of crawler.
        /// </summary>
        /// <param name="remainingTime">time to remaining in minutes</param>
        /// <returns>available or not</returns>
        public bool CheckAvailability(out int remainingTime)
        {
            remainingTime = 0;

            bool result = false;

            string res;
            bool req = NetworkManager.Instance.GetUrlResponse(string.Format(NetworkManager.Instance.UrlFormat, SharedCommand.CommandLastInstantCrwlInfo), out res, AccountInfo.Instance.AccountId.ToString());

            if (req)
            {
                if (string.IsNullOrEmpty(res))
                {
                    result = true;
                }
                else
                {
                    DateTime modifiedNow = DateTime.Now.AddHours(-1d);
                    DateTime executedTime = DateTime.Parse(res);

                    if (modifiedNow >= executedTime)
                    {
                        result = true;
                    }
                    else
                    {
                        double dummy = (executedTime - modifiedNow).TotalMinutes;

                        if (dummy >= 59.0)
                        {
                            remainingTime = 59; // over 59 min.
                        }
                        else
                        {
                            remainingTime = (int)dummy; // 1 ~ 58 min.
                        }
                    }
                }
            }

            if (!result)
            {
                _errorMessage = "[InstantCrawler] CAUTION: EXECUTION TIME INTERVAL";
            }

            return result;
        }

        /// <summary>
        /// Starts an instant crawling job.
        /// </summary>
        /// <param name="hotelId">hotel ID to crawl</param>
        /// <param name="elapsed">elapsed time of crawling</param>
        /// <returns>success or not</returns>
        public bool Start(int hotelId, out int elapsed)
        {
            elapsed = 0;

            bool result = true;

            // e.g. 05201023_33423_170609_45_2_3
            string id = string.Format("{0}_{1}_{2}_{3}_{4}_{5}", DateTime.Now.ToString("MMddHHmm"), hotelId,
                                      SharedCommand.Instance.Parameters.StartDate.Replace("-", string.Empty).Substring(2, 6),
                                      SharedCommand.Instance.Parameters.Period, SharedCommand.Instance.Parameters.Person, SharedCommand.Instance.Parameters.Days);

            string outputPath = Path.Combine(Environment.CurrentDirectory, id);

            if (Directory.Exists(outputPath))
            {
                Directory.Delete(outputPath, true);
            }

            WebCrawler crwl = new WebCrawler(true, SharedCommand.Instance.Parameters);

            if (JobManager.Instance.AddJob(CrawlerType.REALTIME, id))
            {
                if (!crwl.StartJob(id, outputPath, out elapsed))
                {
                    _errorMessage = crwl.ErrorMessage;
                    result = false;
                }
            }
            else
            {
                _errorMessage = "[InstantCrawler] ALREADY EXISTING JOB";
                result = false;
            }

            return result;
        }
    }
}