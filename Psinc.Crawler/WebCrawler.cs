﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Threading;
using Psinc.Analyst;
using Psinc.Crawler.DataStructure;
using Psinc.Util;

namespace Psinc.Crawler
{
    /// <summary>
    /// Class for multi-threaded web crawling.
    /// </summary>
    internal class WebCrawler
    {
        private bool _isInstantCrawling;
        private CrawlerBase _parameters;
        private string _currentJobId = string.Empty;
        private string _tempFolder = string.Empty;
        private string _errorMessage = string.Empty;

        private Dictionary<string, List<string>> _crwlErrorList;

        private readonly string _msgFileCompressed  = "ZIP: {0} MB / {2} / {1}";
        private readonly string _msgProcessErrorUrl = "PROCESSING ERROR URLs: {0}/{1} - {2} REMAINING ({4} / {3})";
        private readonly string _msgJobCompleted    = "ELAPSED: {0} SEC. / {1}-{2}-{3} / {5} / {4}";


        /// <summary>
        /// Internal constructor.
        /// </summary>
        /// <param name="isInstant">if job is an instant crawling, set to true</param>
        /// <param name="parameters">parameters for crawler</param>
        internal WebCrawler(bool isInstant, CrawlerBase parameters)
        {
            _isInstantCrawling = isInstant;
            _parameters = parameters;
        }

        /// <summary>
        /// Gets a message of last occurred error.
        /// </summary>
        internal string ErrorMessage
        {
            get
            {
                return _errorMessage;
            }
        }

        #region :: INTERNAL METHOD

        /// <summary>
        /// Starts a crawling job.
        /// </summary>
        /// <param name="jobId">crawling job ID</param>
        /// <param name="tempFolder">temporary folder for crawling</param>
        /// <param name="elapsed">elapsed time of crawling</param>
        /// <returns>success or not</returns>
        internal bool StartJob(string jobId, string tempFolder, out int elapsed)
        {
            elapsed = 0;

            bool result = true;

            if (_parameters.AccountId < 1 || _parameters.HotelId < 1 ||
                string.IsNullOrEmpty(_parameters.StartDate) || string.IsNullOrEmpty(_parameters.EndDate) || _parameters.StartTrigger < 0 ||
                _parameters.Period < 1 || _parameters.Person < 1 || _parameters.Days < 1 ||
                string.IsNullOrEmpty(_parameters.ExecuteDate) || _parameters.ExecuteHour < 0 || _parameters.ExecuteMinute < 0)
            {
                _errorMessage = "[WebCrawler] PARAMETER ERROR";

                JobManager.Instance.RemoveJob(jobId);
                return false;
            }

            DateTime startDate = DateTime.Parse(_parameters.StartDate);

            if (startDate.Date < DateTime.Today)
            {
                // Adjusts a start date if start date less than today.
                startDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 0, 0, 0);
            }

            _currentJobId = jobId;
            _tempFolder = tempFolder;

            _crwlErrorList = new Dictionary<string, List<string>>();

            try
            {
                /*
                 * PREPARE **********
                 */
                DateTime endDate = startDate.AddDays(_parameters.Period - 1); // 'Period' includes today.
                _parameters.EndDate = endDate.ToString("yyyy-MM-dd");

                Dictionary<string, List<string>> urlList = new Dictionary<string, List<string>>();

                int[] hotelIdArray = new int[1];
                hotelIdArray[0] = _parameters.HotelId;

                if (_isInstantCrawling)
                {
                    urlList = SharedCommand.Instance.GetUrlList(hotelIdArray, false, new CrawlerBase());
                }
                else
                {
                    urlList = SharedCommand.Instance.GetUrlList(hotelIdArray, true, _parameters);
                }


                /*
                 * START CRAWLING **********
                 */
                JobManager.Instance.ChangeJobStep(_currentJobId, JobStep.CRAWL);

                var watch = Stopwatch.StartNew();

                Directory.CreateDirectory(tempFolder);

                // MULTI-THREADED CRAWLING
                Parallel.ForEach(urlList, item =>
                {
                    if (item.Key.StartsWith("rurubu"))
                    {
                        Thread.Sleep(5000);
                        ProcessRurubu(item);
                    }
                    else if (item.Key.StartsWith("jtb"))
                    {
                        ProcessJtb(item);
                    }
                    else if (item.Key.StartsWith("booking"))
                    {
                        ProcessBooking(item);
                    }
                    else if (item.Key.StartsWith("agoda"))
                    {
                        Thread.Sleep(5000);
                        ProcessAgoda(item);
                    }
                    else
                    {
                        ProcessAny(item);
                    }
                });

                // Processes error-occurred URLs if exists.
                if (_crwlErrorList.Count > 0)
                {
                    GC.Collect();
                    GC.WaitForPendingFinalizers();

                    TryCrawlingErrorOccurredUrl();
                }

                // Makes a zip file.
                Directory.CreateDirectory(Path.Combine(Environment.CurrentDirectory, "zip"));

                string zipFile = string.Format("{0}.zip", _currentJobId);
                string zipPath = Path.Combine(Environment.CurrentDirectory, "zip", zipFile);

                GC.Collect();
                GC.WaitForPendingFinalizers();

                ZipFile.CreateFromDirectory(tempFolder, zipPath);

                FileInfo zipInfo = new FileInfo(zipPath);
                int zipSize = (int)Math.Round((zipInfo.Length / 1024d) / 1024d); // bytes to megabytes

                NetworkManager.Instance.SendLog(_parameters.AccountId, LogType.INFO.ToString().ToLower(), string.Format(_msgFileCompressed, zipSize, Tools.GetLocalIPAddress(), _currentJobId));


                /*
                 * START ANALYSIS **********
                 */
                JobManager.Instance.ChangeJobStep(_currentJobId, JobStep.ANALYSE);

                if (!AnalysisManager.Instance.StartAnalysis(_parameters.AccountId, _parameters.HotelId, new DirectoryInfo(tempFolder)))
                {
                    _errorMessage = AnalysisManager.Instance.ErrorMessage;
                    result = false;
                }

                string resPath = Path.Combine(Environment.CurrentDirectory, "res");
                Directory.CreateDirectory(resPath);

                FileInfo[] fis;
                Tools.GetFilesByExtensions(new string[] { ".ar" }, out fis);

                NetworkManager.Instance.SendLog(_parameters.AccountId, LogType.INFO.ToString().ToLower(), string.Format("MOVE AR FILE STARTED: {0}", _currentJobId));
                foreach (var info in fis)
                {
                    NetworkManager.Instance.SendLog(_parameters.AccountId, LogType.INFO.ToString().ToLower(), string.Format("MOVE AR FILE: {0} / {1}", _currentJobId, info.Name));
                    info.MoveTo(Path.Combine(resPath, info.Name));
                }
                NetworkManager.Instance.SendLog(_parameters.AccountId, LogType.INFO.ToString().ToLower(), string.Format("MOVE AR FILE COMPLETED: {0}", _currentJobId));

                string res;
                NetworkManager.Instance.GetUrlResponse(string.Format(NetworkManager.Instance.UrlFormat, "insertResult"),
                                                       out res,
                                                       string.Format("{0}{1}", _isInstantCrawling ? "R" : "T", _parameters.CrawlerId));

                /*
                 * FINISH CRAWLING **********
                 */
                GC.Collect();
                GC.WaitForPendingFinalizers();

                // Cleans up crawled files.
                Directory.Delete(tempFolder, true);

                watch.Stop();
                elapsed = (int)(watch.ElapsedMilliseconds / 1000);

                NetworkManager.Instance.SendLog(_parameters.AccountId, LogType.INFO.ToString().ToLower(),
                                                string.Format(_msgJobCompleted, elapsed, _parameters.Period, _parameters.Person, _parameters.Days, Tools.GetLocalIPAddress(), _currentJobId));
            }
            catch (Exception e)
            {
                NetworkManager.Instance.SendLog(_parameters.AccountId, LogType.ERROR.ToString().ToLower(), string.Format(SharedCommand.LogMessageDefaultError+" / WebCrawler", _currentJobId, e.Message));

                _errorMessage = e.Message;
                result = false;
            }

            JobManager.Instance.RemoveJob(_currentJobId);

            return result;
        }

        #endregion

        #region :: PRIVATE METHODS

        private string BuildFileName(string hotelId, string ota, string hotelCode, string searchDate, string id, bool exceptPath)
        {
            // Builds a filename.

            string uniqueId = string.Format("{0}{1}", DateTime.Now.ToString("ddHHmm"), id); // Uses for file separation.

            string modifiedHotelCode = string.Empty;

            if (ota.Equals("agoda") || ota.Equals("rurubu") || ota.Equals("booking") || ota.Equals("expedia"))
            {
                modifiedHotelCode = hotelCode.Split('_').First();
            }
            else
            {
                modifiedHotelCode = hotelCode;
            }

            // e.g. jalan_44_304032_R122_180223_180220_123_201500003004
            string filename = string.Format("{0}_{1}_{2}_{3}{4}_{5}_{6}_{7}_{8}.txt", ota,
                                                                                      _parameters.AccountId,
                                                                                      modifiedHotelCode,
                                                                                      _isInstantCrawling ? "R" : "T",
                                                                                      _parameters.CrawlerId,
                                                                                      searchDate.Replace("-", string.Empty).Substring(2, 6),
                                                                                      _parameters.ExecuteDate.Replace("-", string.Empty).Substring(2, 6),
                                                                                      hotelId,
                                                                                      uniqueId);
            if (!exceptPath)
            {
                filename = Path.Combine(_tempFolder, filename);
            }

            return filename;
        }

        private void ProcessAny(KeyValuePair<string, List<string>> source)
        {
            // Processes any OTA except some OTA.

            string[] dummy1 = source.Key.Split('#');

            string ota = dummy1[0];
            string hotelCode = dummy1[1];
            string hotelId = dummy1[2];

            //              ↓ processed by other method (180816)
            if (ota.Equals("rurubu") || ota.Equals("jtb") || ota.Equals("booking") || ota.Equals("agoda"))
            {
                return;
            }

            Random rnd = new Random();

            WebProxy proxy = null;

            if (NetworkManager.Instance.ProxySetting.InUse)
            {
                proxy = new WebProxy(NetworkManager.Instance.ProxySetting.Ip, NetworkManager.Instance.ProxySetting.Port);
            }

            for (int i = 0; i < source.Value.Count; i++)
            {
                ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true; // All SSL certificate is accepted.

                if (ota.Contains("ikyu") || ota.Contains("rakuten"))
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                }
                else
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls;
                }

                string uri = source.Value[i].Split('#').First();
                string filename = string.Empty;

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);

                if (proxy != null)
                {
                    request.Proxy = proxy;
                }

                // Sets an user agent by 'key'.
                request.UserAgent = CrawlerAgent.BasicSet[rnd.Next(0, 5)];

                try
                {
                    filename = BuildFileName(hotelId, ota, hotelCode, source.Value[i].Split('#')[1], string.Format("{0:000}{1:000}", i, source.Value.Count - i), false);

                    if (ota.Equals("rakuten"))
                    {
                        request.Headers.Add("Accept-Language", "ja");
                    }

                    if (ota.Equals("directin"))
                    {
                        byte[] paramsByte = Encoding.UTF8.GetBytes(string.Format("cmbADULT={0}&seekCount=9999", _parameters.Person));

                        request.Method        = WebRequestMethods.Http.Post;
                        request.ContentType   = "application/x-www-form-urlencoded; charset=Shift_JIS";
                        request.ContentLength = paramsByte.Length;

                        using (var rs = request.GetRequestStream())
                        {
                            rs.Write(paramsByte, 0, paramsByte.Length);
                        }
                    }
                    
                    using (var response = (HttpWebResponse)request.GetResponse())
                    {
                        Stream stream = response.GetResponseStream();
                        StreamReader sr;

                        if (ota.Equals("jalan") || ota.Equals("directin"))
                        {
                            // MS Code Page 932 = シフトJIS
                            sr = new StreamReader(stream, Encoding.GetEncoding(932));
                        }
                        else
                        {
                            // UTF-8
                            sr = new StreamReader(stream, Encoding.UTF8);
                        }

                        using (var sw = new StreamWriter(filename, false, Encoding.UTF8))
                        {
                            int person = 1;
                            int days = 1;

                            if (ota.Contains("ikyu"))
                            {
                                DateTime sd = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                                DateTime ed = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);

                                foreach (var item in uri.Split('&'))
                                {
                                    if (item.StartsWith("cid"))
                                    {
                                        string tmp1 = item.Split('=')[1];
                                        sd = new DateTime(int.Parse(tmp1.Substring(0, 4)), int.Parse(tmp1.Substring(4, 2)), int.Parse(tmp1.Substring(6, 2)), 0, 0, 0);
                                    }

                                    if (item.StartsWith("cod"))
                                    {
                                        string tmp1 = item.Split('=')[1];
                                        ed = new DateTime(int.Parse(tmp1.Substring(0, 4)), int.Parse(tmp1.Substring(4, 2)), int.Parse(tmp1.Substring(6, 2)), 0, 0, 0);
                                    }

                                    //宿泊人数
                                    if (item.StartsWith("ppc="))
                                    {
                                        person = int.Parse(item.Split('=')[1]);
                                    }
                                }

                                //宿泊日数
                                days = (ed - sd).Days;

                                sw.WriteLine(string.Format("{0}_{1}", person, days));
                            }
                            else if (ota.Equals("expedia"))
                            {
                                string hotelName = string.Empty;

                                if (hotelCode.Split('_').Length > 1)
                                {
                                    hotelName = hotelCode.Split('_')[1];
                                }

                                foreach (var item in uri.Split('&'))
                                {
                                    //宿泊人数
                                    if (item.StartsWith("cp="))
                                    {
                                        person = int.Parse(item.Split('=')[1]);
                                    }

                                    //宿泊日数
                                    if (item.StartsWith("sn="))
                                    {
                                        days = int.Parse(item.Split('=')[1]);
                                    }
                                }

                                sw.WriteLine(string.Format("{0}_{1}_{2}", person, days, hotelName));
                            }

                            sw.Write(sr.ReadToEnd());
                        }
                    }
                }
                catch (WebException we)
                {
                    int statusCode = -1;

                    if (we.Status == WebExceptionStatus.ProtocolError)
                    {
                        var response = (HttpWebResponse)we.Response;
                        if (response != null)
                        {
                            statusCode = (int)response.StatusCode;
                        }
                    }

                    if (statusCode < 0 || statusCode == 200 || statusCode == 301)
                    {
                        using (var sw = new StreamWriter(filename, false, Encoding.UTF8))
                        {
                            sw.WriteLine(uri);
                            sw.Write(we.ToString());
                        }
                    }
                    else
                    {
                        if (_crwlErrorList.ContainsKey(source.Key))
                        {
                            _crwlErrorList[source.Key].Add(source.Value[i]);
                        }
                        else
                        {
                            _crwlErrorList.Add(source.Key, new List<string>());
                            _crwlErrorList[source.Key].Add(source.Value[i]);
                        }
                    }
                    continue;
                }
                catch (Exception e)
                {
                    using (var sw = new StreamWriter(filename, false, Encoding.UTF8))
                    {
                        sw.WriteLine(uri);
                        sw.Write(e.ToString());
                    }
                    continue;
                }
            }
        }

        private void ProcessJtb(KeyValuePair<string, List<string>> source)
        {
            // Processes only OTA(jtb).

            string[] dummy1 = source.Key.Split('#');

            string ota = dummy1[0];
            string hotelCode = dummy1[1];
            string hotelId = dummy1[2];

            if (!ota.Equals("jtb"))
            {
                return;
            }

            Random rnd = new Random();

            WebProxy proxy = null;

            if (NetworkManager.Instance.ProxySetting.InUse)
            {
                proxy = new WebProxy(NetworkManager.Instance.ProxySetting.Ip, NetworkManager.Instance.ProxySetting.Port);
            }

            for (int i = 0; i < source.Value.Count; i++)
            {
                ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true; // All SSL certificate is accepted.
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                string uri = source.Value[i].Split('#').First();
                string filename = string.Empty;
                
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);

                if (proxy != null)
                {
                    request.Proxy = proxy;
                }

                request.UserAgent = CrawlerAgent.BasicSet[rnd.Next(0, 5)];

                try
                {
                    filename = BuildFileName(hotelId, ota, hotelCode, source.Value[i].Split('#')[1], string.Format("{0:000}{1:000}", i, source.Value.Count - i), false);

                    // Gets a first page of plans.
                    using (var response = (HttpWebResponse)request.GetResponse())
                    {
                        Stream stream = response.GetResponseStream();
                        StreamReader sr = new StreamReader(stream, Encoding.UTF8);

                        using (var sw = new StreamWriter(filename, false, Encoding.UTF8))
                        {
                            sw.WriteLine(uri);
                            sw.Write(sr.ReadToEnd());
                        }
                    }

                    string filenameOriginal = filename.Split('.').First();

                    int plansCount;
                    if (JtbAnalyser.GetPlansCount(filename, out plansCount))
                    {
                        int plansPerPage = 40;

                        if (plansCount > plansPerPage) // Over 41 plans.
                        {
                            int pages = (plansCount / plansPerPage) + 1;

                            for (int j = 1; j < pages; j++)
                            {
                                uri = string.Format("{0}&page={1}", uri, j + 1);
                                filename = string.Format("{0}_{1}.txt", filenameOriginal, j);
                                
                                request = (HttpWebRequest)WebRequest.Create(uri);

                                if (proxy != null)
                                {
                                    request.Proxy = proxy;
                                }

                                request.UserAgent = CrawlerAgent.BasicSet[rnd.Next(0, 5)];

                                // Gets second and later pages of plans.
                                using (var response = (HttpWebResponse)request.GetResponse())
                                {
                                    Stream stream = response.GetResponseStream();
                                    StreamReader sr = new StreamReader(stream, Encoding.UTF8);

                                    using (var sw = new StreamWriter(filename, false, Encoding.UTF8))
                                    {
                                        sw.WriteLine(uri);
                                        sw.Write(sr.ReadToEnd());
                                    }
                                }
                            }
                        }
                    }
                }
                catch (WebException we)
                {
                    int statusCode = -1;

                    if (we.Status == WebExceptionStatus.ProtocolError)
                    {
                        var response = (HttpWebResponse)we.Response;
                        if (response != null)
                        {
                            statusCode = (int)response.StatusCode;
                        }
                    }

                    if (statusCode < 0 || statusCode == 200 || statusCode == 301)
                    {
                        using (var sw = new StreamWriter(filename, false, Encoding.UTF8))
                        {
                            sw.WriteLine(uri);
                            sw.Write(we.ToString());
                        }
                    }
                    else
                    {
                        if (_crwlErrorList.ContainsKey(source.Key))
                        {
                            _crwlErrorList[source.Key].Add(source.Value[i]);
                        }
                        else
                        {
                            _crwlErrorList.Add(source.Key, new List<string>());
                            _crwlErrorList[source.Key].Add(source.Value[i]);
                        }
                    }
                    continue;
                }
                catch (Exception e)
                {
                    using (var sw = new StreamWriter(filename, false, Encoding.UTF8))
                    {
                        sw.WriteLine(uri);
                        sw.Write(e.ToString());
                    }
                    continue;
                }
            }
        }

        private void ProcessBooking(KeyValuePair<string, List<string>> source)
        {
            // Processes only OTA(booking).

            string[] dummy1 = source.Key.Split('#');

            string ota = dummy1[0];
            string hotelCode = dummy1[1];
            string hotelId = dummy1[2];

            if (!ota.Equals("booking"))
            {
                return;
            }

            WebProxy proxy = null;

            if (NetworkManager.Instance.ProxySetting.InUse)
            {
                proxy = new WebProxy(NetworkManager.Instance.ProxySetting.Ip, NetworkManager.Instance.ProxySetting.Port);
            }

            for (int i = 0; i < source.Value.Count; i++)
            {
                StreamReader sr = null;

                ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true; // All SSL certificate is accepted.
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                string uri = source.Value[i].Split('#')[0];
                string filename = string.Empty;

                Thread.Sleep(15000);

                try
                {
                    filename = BuildFileName(hotelId, ota, hotelCode, source.Value[i].Split('#')[2], string.Format("{0:000}{1:000}", i, source.Value.Count - i), false);

                    Cookie cookie = null;

                    // Gets a token value (b_sid).
                    HttpWebRequest request1 = (HttpWebRequest)WebRequest.Create(uri);

                    if (proxy != null)
                    {
                        request1.Proxy = proxy;
                    }

                    request1.AllowAutoRedirect      = true;
                    request1.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                    request1.UserAgent              = CrawlerAgent.Booking;

                    string extractedStr = string.Empty;

                    using (var response1 = (HttpWebResponse)request1.GetResponse())
                    {
                        for (int j = 0; j < response1.Headers.Count; j++)
                        {
                            if (!response1.Headers.GetKey(j).Equals("Set-Cookie"))
                            {
                                continue;
                            }

                            string value = response1.Headers.Get(j);

                            foreach (var singleCookie in value.Split(','))
                            {
                                Match match = Regex.Match(singleCookie, "(.+?)=(.+?);");

                                if (match.Captures.Count == 0)
                                {
                                    continue;
                                }

                                if (match.Groups[1].ToString().Equals("bkng"))
                                {
                                    cookie = new Cookie(match.Groups[1].ToString(), match.Groups[2].ToString(), "/", request1.Host.Split(':')[0]);
                                    break;
                                }
                            }
                        }

                        Stream stream1 = response1.GetResponseStream();

                        sr = new StreamReader(stream1, Encoding.UTF8);

                        string contents = sr.ReadToEnd();

                        string[] split = contents.Split(new char[] { ':', ',' });

                        for (int j = 0; j < split.Length; j++)
                        {
                            if (split[j].Equals("\nb_sid"))
                            {
                                extractedStr = split[j + 1].Split('\'')[1];
                                break;
                            }
                        }
                    }

                    // Gets an actual page.
                    uri = uri + string.Format(";sid={0}", extractedStr);

                    HttpWebRequest request2 = (HttpWebRequest)WebRequest.Create(uri);

                    if (proxy != null)
                    {
                        request2.Proxy = proxy;
                    }

                    request2.AllowAutoRedirect      = true;
                    request2.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                    request2.UserAgent              = CrawlerAgent.Booking;

                    if (cookie != null)
                    {
                        request2.CookieContainer = new CookieContainer();
                        request2.CookieContainer.Add(cookie);
                    }

                    using (var response2 = (HttpWebResponse)request2.GetResponse())
                    {
                        Stream stream2 = response2.GetResponseStream();

                        sr = new StreamReader(stream2, Encoding.UTF8);
                        
                        using (var sw = new StreamWriter(filename, false, Encoding.UTF8))
                        {
                            int person = 1;

                            foreach (var item in uri.Split(';'))
                            {
                                //宿泊人数
                                if (item.StartsWith("group_adults"))
                                {
                                    person = int.Parse(item.Split('=')[1]);
                                }
                            }

                            sw.WriteLine(string.Format("{0}_{1}", person, extractedStr));
                            sw.Write(sr.ReadToEnd());
                        }
                    }
                }
                catch (WebException we)
                {
                    int statusCode = -1;

                    if (we.Status == WebExceptionStatus.ProtocolError)
                    {
                        var response = (HttpWebResponse)we.Response;

                        if (response != null)
                        {
                            statusCode = (int)response.StatusCode;
                        }
                    }

                    if (statusCode < 0 || statusCode == 200 || statusCode == 301)
                    {
                        using (var sw = new StreamWriter(filename, false, Encoding.UTF8))
                        {
                            sw.WriteLine(uri);
                            sw.Write(we.ToString());
                        }
                    }
                    else
                    {
                        if (_crwlErrorList.ContainsKey(source.Key))
                        {
                            _crwlErrorList[source.Key].Add(source.Value[i]);
                        }
                        else
                        {
                            _crwlErrorList.Add(source.Key, new List<string>());
                            _crwlErrorList[source.Key].Add(source.Value[i]);
                        }
                    }
                    continue;
                }
                catch (Exception e)
                {
                    using (var sw = new StreamWriter(filename, false, Encoding.UTF8))
                    {
                        sw.WriteLine(uri);
                        sw.Write(e.ToString());
                    }
                    continue;
                }
            }
        }

        private void ProcessAgoda(KeyValuePair<string, List<string>> source)
        {
            // Processes only OTA(agoda).
            string[] dummy1  = source.Key.Split('#');

            string ota       = dummy1[0];
            string hotelCode = dummy1[1];
            string hotelId   = dummy1[2];

            if (!ota.Equals("agoda"))
            {
                return;
            }
            
            WebProxy proxy = null;

            if (NetworkManager.Instance.ProxySetting.InUse)
            {
                proxy = new WebProxy(NetworkManager.Instance.ProxySetting.Ip, NetworkManager.Instance.ProxySetting.Port);
            }
            
            for (int i = 0; i < source.Value.Count; i++)
            {
                ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true; // All SSL certificate is accepted.
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                string uri = source.Value[i].Split('#').First();
                string filename = string.Empty;
                
                Thread.Sleep(25000);
                
                CookieContainer cc = new CookieContainer();
                
                try
                {
                    filename = BuildFileName(hotelId, ota, hotelCode, source.Value[i].Split('#')[1], string.Format("{0:000}{1:000}", i, source.Value.Count - i), false);
                    
                    string hotelUrl = string.Empty;

                    if (hotelCode.Split('_').Length > 1)
                    {
                        hotelUrl = hotelCode.Split('_')[1];
                    }
                    else
                    {
                        hotelUrl = "err";
                    }
                    
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);

                    if (proxy != null)
                    {
                        request.Proxy = proxy;
                    }

                    // Sets an user agent by 'key'.
                    request.UserAgent = CrawlerAgent.Agoda;
                    request.CookieContainer = cc;
                    request.CookieContainer.Add(new Uri(uri), new Cookie("DLang", "ja-jp"));
                    request.CookieContainer.Add(new Uri(uri), new Cookie("CurLabel", "JPY"));
                    
                    request.Method = "POST";
                    request.ContentType = "application/json";
                    request.Headers.Add("ag-language-locale", "ja-jp");
                    request.Headers.Add("cr-currency-code", "JPY");
                    
                    using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                    {
                        string json = "{\"SearchType\":\"4\",\"ObjectID\":#object_id#,\"CheckIn\":\"#check_in#T00:00:00\",\"Origin\":\"JP\",\"LengthOfStay\":#los#,\"Adults\":#adult#,\"Children\":0,\"Rooms\":1,\"IsEnableAPS\":\"true\",\"RateplanIDs\":\"\",\"PlatformID\":\"0\",\"CurrencyCode\":\"JPY\",\"ChildAgesStr\":\"null\",\"HashId\":\"\",\"PackageToken\":null,\"PriceView\":\"2\"}";

                        json = json.Replace("#object_id#", hotelCode.Split('_')[0]);
                        json = json.Replace("#check_in#", _parameters.StartDate);
                        json = json.Replace("#los#", _parameters.Days.ToString());
                        json = json.Replace("#adult#", _parameters.Person.ToString());
                        streamWriter.Write(json);
                    }
                    
                    using (var response = (HttpWebResponse)request.GetResponse())
                    {
                        Stream stream = response.GetResponseStream();
                        StreamReader sr;

                        // UTF-8
                        sr = new StreamReader(stream, Encoding.UTF8);

                        using (var sw = new StreamWriter(filename, false, Encoding.UTF8))
                        {
                            int person = _parameters.Person;
                            int days = _parameters.Days;

                            sw.WriteLine(string.Format("{0}_{1}_{2}", person, days, hotelUrl));
                            sw.Write(sr.ReadToEnd());
                        }

                        /*
                        if (sr.ToString().Contains("/ja-jp/tlsupdate"))
                        {
                            using (var response2 = (HttpWebResponse)request.GetResponse())
                            {
                                Stream stream2 = response2.GetResponseStream();
                                StreamReader sr2;

                                // UTF-8
                                sr2 = new StreamReader(stream2, Encoding.UTF8);

                                using (var sw2 = new StreamWriter(filename, false, Encoding.UTF8))
                                {
                                    int person2 = 1;
                                    int days2   = 1;

                                    foreach (var item2 in uri.Split('&'))
                                    {
                                        //宿泊人数
                                        if (item2.StartsWith("Adults"))
                                        {
                                            person2 = int.Parse(item2.Split('=')[1]);
                                        }

                                        //宿泊日数
                                        if (item2.StartsWith("LengthOfStay"))
                                        {
                                            days2 = int.Parse(item2.Split('=')[1]);
                                        }
                                    }

                                    sw2.WriteLine(string.Format("{0}_{1}_{2}", person2, days2, hotelUrl));
                                    sw2.Write(sr2.ReadToEnd());
                                }
                            }
                        }
                        else
                        {
                            using (var sw = new StreamWriter(filename, false, Encoding.UTF8))
                            {
                                int person = 1;
                                int days   = 1;

                                foreach (var item in uri.Split('&'))
                                {
                                    //宿泊人数
                                    if (item.StartsWith("Adults"))
                                    {
                                        person = int.Parse(item.Split('=')[1]);
                                    }

                                    //宿泊日数
                                    if (item.StartsWith("LengthOfStay"))
                                    {
                                        days = int.Parse(item.Split('=')[1]);
                                    }
                                }

                                sw.WriteLine(string.Format("{0}_{1}_{2}", person, days, hotelUrl));
                                sw.Write(sr.ReadToEnd());
                            }
                        }*/
                    }
                }
                catch (WebException we)
                {
                    int statusCode = -1;

                    if (we.Status == WebExceptionStatus.ProtocolError)
                    {
                        var response = (HttpWebResponse)we.Response;
                        if (response != null)
                        {
                            statusCode = (int)response.StatusCode;
                        }
                    }

                    if (statusCode < 0 || statusCode == 200 || statusCode == 301)
                    {
                        using (var sw = new StreamWriter(filename, false, Encoding.UTF8))
                        {
                            sw.WriteLine(uri);
                            sw.Write(we.ToString());
                        }
                    }
                    else
                    {
                        if (_crwlErrorList.ContainsKey(source.Key))
                        {
                            _crwlErrorList[source.Key].Add(source.Value[i]);
                        }
                        else
                        {
                            _crwlErrorList.Add(source.Key, new List<string>());
                            _crwlErrorList[source.Key].Add(source.Value[i]);
                        }
                    }
                    continue;
                }
                catch (Exception e)
                {
                    using (var sw = new StreamWriter(filename, false, Encoding.UTF8))
                    {
                        sw.WriteLine(uri);
                        sw.Write(e.ToString());
                    }
                    continue;
                }
            }
        }

        private void ProcessRurubu(KeyValuePair<string, List<string>> source)
        {
            // Processes only OTA(rurubu).
            string[] dummy1 = source.Key.Split('#');
            
            string ota = dummy1[0];
            string hotelCode = dummy1[1];
            string hotelId = dummy1[2];
            
            if (!ota.Equals("rurubu"))
            {
                return;
            }

            WebProxy proxy = null;

            if (NetworkManager.Instance.ProxySetting.InUse)
            {
                proxy = new WebProxy(NetworkManager.Instance.ProxySetting.Ip, NetworkManager.Instance.ProxySetting.Port);
            }

            for (int i = 0; i < source.Value.Count; i++)
            {
                ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true; // All SSL certificate is accepted.
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                
                string uri = source.Value[i].Split('#').First();
                string periodDate = source.Value[i].Split('#')[1];
                string filename = string.Empty;

                Thread.Sleep(30000);

                CookieContainer cc = new CookieContainer();

                try
                {
                    filename = BuildFileName(hotelId, ota, hotelCode, periodDate, string.Format("{0:000}{1:000}", i, source.Value.Count - i), false);
                    
                    string hotelUrl = string.Empty;

                    if (hotelCode.Split('_').Length > 1)
                    {
                        hotelUrl = hotelCode.Split('_')[1];
                    }
                    else
                    {
                        hotelUrl = "err";
                    }

                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);

                    if (proxy != null)
                    {
                        request.Proxy = proxy;
                    }

                    // Sets an user agent by 'key'.
                    request.UserAgent = CrawlerAgent.Rurubu;
                    request.CookieContainer = cc;
                    request.CookieContainer.Add(new Uri(uri), new Cookie("DLang", "ja-jp"));
                    request.CookieContainer.Add(new Uri(uri), new Cookie("CurLabel", "JPY"));

                    request.Method = "POST";
                    request.ContentType = "application/json";
                    request.Headers.Add("ag-language-locale", "ja-jp");
                    request.Headers.Add("cr-currency-code", "JPY");

                    using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                    {
                        string json = "{\"SearchType\":\"4\",\"ObjectID\":#object_id#,\"CheckIn\":\"#check_in#T00:00:00\",\"Origin\":\"JP\",\"LengthOfStay\":#los#,\"Adults\":#adult#,\"Children\":0,\"Rooms\":1,\"IsEnableAPS\":\"true\",\"RateplanIDs\":\"\",\"PlatformID\":\"0\",\"CurrencyCode\":\"JPY\",\"ChildAgesStr\":\"null\",\"HashId\":\"\",\"PackageToken\":null,\"PriceView\":\"2\"}";

                        json = json.Replace("#object_id#", hotelCode.Split('_')[0]);
                        json = json.Replace("#check_in#", periodDate);
                        json = json.Replace("#los#", _parameters.Days.ToString());
                        json = json.Replace("#adult#", _parameters.Person.ToString());
                        streamWriter.Write(json);
                    }

                    using (var response = (HttpWebResponse)request.GetResponse())
                    {
                        Stream stream = response.GetResponseStream();
                        StreamReader sr;

                        // UTF-8
                        sr = new StreamReader(stream, Encoding.UTF8);

                        using (var sw = new StreamWriter(filename, false, Encoding.UTF8))
                        {
                            int person = _parameters.Person;
                            int days = _parameters.Days;

                            sw.WriteLine(string.Format("{0}_{1}_{2}", person, days, hotelUrl));
                            sw.Write(sr.ReadToEnd());
                        }
                    }
                }
                catch (WebException we)
                {
                    int statusCode = -1;

                    if (we.Status == WebExceptionStatus.ProtocolError)
                    {
                        var response = (HttpWebResponse)we.Response;
                        if (response != null)
                        {
                            statusCode = (int)response.StatusCode;
                        }
                    }

                    if (statusCode < 0 || statusCode == 200 || statusCode == 301)
                    {
                        using (var sw = new StreamWriter(filename, false, Encoding.UTF8))
                        {
                            sw.WriteLine(uri);
                            sw.Write(we.ToString());
                        }
                    }
                    else
                    {
                        if (_crwlErrorList.ContainsKey(source.Key))
                        {
                            _crwlErrorList[source.Key].Add(source.Value[i]);
                        }
                        else
                        {
                            _crwlErrorList.Add(source.Key, new List<string>());
                            _crwlErrorList[source.Key].Add(source.Value[i]);
                        }
                    }
                    continue;
                }
                catch (Exception e)
                {
                    using (var sw = new StreamWriter(filename, false, Encoding.UTF8))
                    {
                        sw.WriteLine(uri);
                        sw.Write(e.ToString());
                    }
                    continue;
                }
            }
        }

        private void TryCrawlingErrorOccurredUrl(int repeat = 3)
        {
            // Tries to process error-occurred URLs.

            Dictionary<string, List<string>> workspace;

            for (int i = 0; i < repeat; i++)
            {
                if (_crwlErrorList.Count == 0)
                {
                    continue;
                }

                int counter = 0;

                foreach (KeyValuePair<string, List<string>> item in _crwlErrorList)
                {
                    counter += item.Value.Count;
                }

                NetworkManager.Instance.SendLog(_parameters.AccountId, LogType.INFO.ToString().ToLower(), string.Format(_msgProcessErrorUrl, i + 1,
                                                                                                                                             repeat,
                                                                                                                                             counter,
                                                                                                                                             Tools.GetLocalIPAddress(),
                                                                                                                                             _currentJobId));
                workspace = new Dictionary<string, List<string>>(_crwlErrorList);

                _crwlErrorList = new Dictionary<string, List<string>>();

                // MULTI-THREADED CRAWLING
                Parallel.ForEach(workspace, item =>
                {
                    if (item.Key.StartsWith("rurubu"))
                    {
                        Thread.Sleep(5000);
                        ProcessRurubu(item);
                    }
                    else if (item.Key.StartsWith("jtb"))
                    {
                        ProcessJtb(item);
                    }
                    else if (item.Key.StartsWith("booking"))
                    {
                        ProcessBooking(item);
                    }
                    else if (item.Key.StartsWith("agoda"))
                    {
                        Thread.Sleep(5000);
                        ProcessAgoda(item);
                    }
                    else
                    {
                        ProcessAny(item);
                    }
                });
            }
        }

        #endregion
    }
}