﻿using System;
using System.Collections.Generic;
using Psinc.Crawler.DataStructure;
using Psinc.Util;

namespace Psinc.Crawler
{
    /// <summary>
    /// Class for shared command that uses for web crawler.
    /// </summary>
    public class SharedCommand
    {
        public static readonly string MessageDefaultFormat          = "[{0}] {1}";
        public static readonly string MessageReachLimitation        = "[{0}] タイマーは１つの施設当たり、{1}個までになります。";
        public static readonly string MessageStartScheduledCrawling = "[{0}] タイマー（{1}）取得が始まりました。";
        public static readonly string MessageEndScheduledCrawling   = "[{0}] タイマー（{1}）取得が終わりました。";
        public static readonly string MessageStartInstantCrawling   = "[{0}] リアルタイム取得が始まりました。";
        public static readonly string MessageEndInstantCrawling     = "[{0}] リアルタイム取得が終わりました。";
        public static readonly string MessageElapsedTimeInfo        = "[{0}] 情報取得が終了しました。({1}秒)";

        public static readonly string RawMessageScheduledCrawlingInfo = "【情報取得中：タイマー】 {0}名様 {1}から {2}まで {3}日間 {4}泊";
        public static readonly string RawMessageInstantCrawlingInfo   = "【情報取得中】 {0}名様 {1}から {2}まで {3}日間 {4}泊";

        public static readonly string LogMessageDefaultError    = "ERROR OCCURRED (C): {0} / {1}";
        public static readonly string LogMessageInsertItemError = "ERROR OCCURRED (C): INSERT ITEM({0})";
        public static readonly string LogMessageUpdateItemError = "ERROR OCCURRED (C): UPDATE ITEM({0})";
        public static readonly string LogMessageDeleteItemError = "ERROR OCCURRED (C): DELETE ITEM({0})";

        public static readonly string QueryInsertLog = "insert into crwl_log(seq, account_id, hotel_id, exec_date, crwl_type, ota, start_date, period, person, days) values(null, '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}')";

        public static readonly string CommandInsertTimer         = "insertTimer";
        public static readonly string CommandDeleteTimer         = "deleteTimer";
        public static readonly string CommandUpdateTimer         = "updateTimer";
        public static readonly string CommandInsertRealtime      = "insertRealtime";
        public static readonly string CommandUpdateRealtime      = "updateRealtime";
        public static readonly string CommandLastInstantCrwlInfo = "lastInstantCrwl";


        private static SharedCommand _instance = null;

        private bool _isDevMode = false;
        private CrawlerBase _parameters;

        /// <summary>
        /// key = OtaInfoBase.Abbreviation
        /// </summary>
        private Dictionary<string, OtaInfoBase> _otaList;


        /// <summary>
        /// Private constructor.
        /// </summary>
        private SharedCommand()
        {
            _parameters = new CrawlerBase();
            _otaList = new Dictionary<string, OtaInfoBase>();
        }

        /// <summary>
        /// Gets an instance of 'SharedCommand'.
        /// </summary>
        public static SharedCommand Instance
        {
            get
            {
                _instance = _instance ?? new SharedCommand();
                return _instance;
            }
        }

        #region :: PROPERTIES

        /// <summary>
        /// Gets or sets a status that program runs on dev server.
        /// </summary>
        public bool IsDevMode
        {
            get
            {
                return _isDevMode;
            }

            set
            {
                _isDevMode = value;
            }
        }

        /// <summary>
        /// Gets or sets a web crawler parameters.
        /// </summary>
        public CrawlerBase Parameters
        {
            get
            {
                return _parameters;
            }

            set
            {
                _parameters = value;
            }
        }

        /// <summary>
        /// Gets or sets a base OTA information.
        /// </summary>
        public Dictionary<string, OtaInfoBase> OtaInfo
        {
            get
            {
                return _otaList;
            }

            set
            {
                _otaList = value;
            }
        }

        #endregion

        #region :: METHODS

        /// <summary>
        /// Gets an abbreviation of specified OTA ID.
        /// </summary>
        /// <param name="id">OTA ID</param>
        /// <returns>abbreviation or 'null' if do not exist</returns>
        public string GetOtaAbbreviation(int id)
        {
            string result = null;

            foreach (KeyValuePair<string, OtaInfoBase> item in _otaList)
            {
                if (item.Value.Id == id)
                {
                    result = item.Key;
                    break;
                }
            }

            return result;
        }

        /// <summary>
        /// Gets an URL list of passed hotel ID. Hotel ID can be multiple.
        /// </summary>
        /// <param name="hotelId">hotel ID</param>
        /// <param name="useAltParameters">alternative parameters use or not</param>
        /// <param name="altParameters">alternative parameters</param>
        /// <returns>URL list</returns>
        internal Dictionary<string, List<string>> GetUrlList(int[] hotelId, bool useAltParameters, CrawlerBase altParameters)
        {
            CrawlerBase parameters = new CrawlerBase();

            if (useAltParameters)
            {
                parameters = altParameters;
            }
            else
            {
                parameters = _parameters;
            }

            int counter;

            Dictionary<string, List<string>> result = new Dictionary<string, List<string>>();

            foreach (int id in hotelId)
            {
                foreach (KeyValuePair<int, OwnHotelBase> item in AccountInfo.Instance.Hotels)
                {
                    if (item.Key == id)
                    {
                        result = MakeUrlList(item.Value, parameters, out counter);

                        NetworkManager.Instance.SendLog(AccountInfo.Instance.AccountId, LogType.INFO.ToString().ToLower(), string.Format("{0} / HOTEL ID: {1} / URLs: {2}", Tools.GetLocalIPAddress(), id, counter));
                        break;
                    }
                }
            }

            return result;
        }

        private string GetUrlBase(string ota)
        {
            // Gets an URL base of specified OTA.

            string result = null;

            foreach (KeyValuePair<string, OtaInfoBase> item in _otaList)
            {
                if (item.Key.Equals(ota))
                {
                    result = item.Value.Url;
                    break;
                }
            }

            return result;
        }

        private Dictionary<string, List<string>> MakeUrlList(OwnHotelBase hotelInfo, CrawlerBase parameters, out int counter)
        {
            // Makes an URL list using passed data.

            counter = 0;

            Dictionary<string, List<string>> urlList = new Dictionary<string, List<string>>();

            string[] targetOta = parameters.OtaList.Split(',');

            string[] ownHotelOta = hotelInfo.OtaList.Split(',');
            string[] comHotelOta;

            // Processes an own hotel. Count = 1
            for (int i = 0; i < ownHotelOta.Length; i++) // OTA
            {
                if (string.IsNullOrEmpty(ownHotelOta[i]))
                {
                    continue;
                }

                if (!CheckOtaExistenceInList(ownHotelOta[i], targetOta))
                {
                    continue;
                }

                string dummy = GetOtaAbbreviation(int.Parse(ownHotelOta[i]));

                if (string.IsNullOrEmpty(dummy))
                {
                    continue;
                }

                string hotelCode;
                if (!hotelInfo.OtaHotelCodeList.TryGetValue(dummy, out hotelCode))
                {
                    hotelCode = null;
                }

                counter += UpdateUrlList(hotelInfo.HotelId, dummy, hotelCode, parameters.StartDate.Split('-'), parameters.Period, parameters.Person, parameters.Days, ref urlList);
            }

            // Processes competitive hotels. Count = n
            foreach (KeyValuePair<int, CompetitiveHotelBase> item in hotelInfo.CompetitiveHotelList)
            {
                comHotelOta = item.Value.OtaList.Split(',');

                for (int i = 0; i < comHotelOta.Length; i++) // OTA
                {
                    if (string.IsNullOrEmpty(comHotelOta[i]))
                    {
                        continue;
                    }

                    if (!CheckOtaExistenceInList(comHotelOta[i], targetOta))
                    {
                        continue;
                    }

                    string dummy = GetOtaAbbreviation(int.Parse(comHotelOta[i]));

                    if (string.IsNullOrEmpty(dummy))
                    {
                        continue;
                    }

                    string hotelCode;
                    if (!item.Value.OtaHotelCodeList.TryGetValue(dummy, out hotelCode))
                    {
                        hotelCode = null;
                    }

                    counter += UpdateUrlList(item.Value.HotelId, dummy, hotelCode, parameters.StartDate.Split('-'), parameters.Period, parameters.Person, parameters.Days, ref urlList);
                }
            }

            return urlList;
        }

        private int UpdateUrlList(int hotelId, string ota, string hotelCode, string[] searchStartDate, int period, int person, int days, ref Dictionary<string, List<string>> urlList)
        {
            // Updates a referred URL list using passed data.

            if (string.IsNullOrEmpty(hotelCode))
            {
                return 0;
            }

            List<string> urlListTemp = new List<string>();

            string[] stayStartDate;
            string[] stayEndDate;

            string urlModified = string.Empty;
            string urlOriginal = string.Empty;

            DateTime periodDate = new DateTime(int.Parse(searchStartDate[0]), int.Parse(searchStartDate[1]), int.Parse(searchStartDate[2]));
            
            switch (ota)
            {
                case "rakuten":
                    {
                        urlOriginal = GetUrlBase("rakuten");

                        for (int i = 0; i < period; i++) // PERIOD
                        {
                            urlModified = urlOriginal;

                            stayStartDate = periodDate.ToString("yyyy-MM-dd").Split('-');
                            stayEndDate   = periodDate.AddDays(days).ToString("yyyy-MM-dd").Split('-');

                            urlModified = urlModified.Replace("#f_no#", hotelCode);
                            urlModified = urlModified.Replace("#f_nen1#", stayStartDate[0]);
                            urlModified = urlModified.Replace("#f_tuki1#", stayStartDate[1]);
                            urlModified = urlModified.Replace("#f_hi1#", stayStartDate[2]);
                            urlModified = urlModified.Replace("#f_nen2#", stayEndDate[0]);
                            urlModified = urlModified.Replace("#f_tuki2#", stayEndDate[1]);
                            urlModified = urlModified.Replace("#f_hi2#", stayEndDate[2]);
                            urlModified = urlModified.Replace("#f_otona_su#", person.ToString());

                            urlModified += string.Format("#{0}", periodDate.ToString("yyyy-MM-dd")); // search_date

                            urlListTemp.Add(urlModified);
                            urlModified = string.Empty;

                            periodDate = periodDate.AddDays(1);
                        }

                        urlList.Add(string.Format("rakuten#{0}#{1}", hotelCode, hotelId), urlListTemp);
                    }
                    break;

                case "jalan":
                    {
                        urlOriginal = GetUrlBase("jalan");

                        for (int i = 0; i < period; i++) // PERIOD
                        {
                            urlModified = urlOriginal;

                            stayStartDate = periodDate.ToString("yyyy-MM-dd").Split('-');

                            urlModified = urlModified.Replace("#yadNo#", hotelCode);
                            urlModified = urlModified.Replace("#stayYear#", stayStartDate[0]);
                            urlModified = urlModified.Replace("#stayMonth#", stayStartDate[1]);
                            urlModified = urlModified.Replace("#stayDay#", stayStartDate[2]);
                            urlModified = urlModified.Replace("#stayCount#", days.ToString());
                            urlModified = urlModified.Replace("#adultNum#", person.ToString());
                            urlModified = urlModified.Replace("#roomCrack#", string.Format("{0}00000", person.ToString()));

                            urlModified += string.Format("#{0}", periodDate.ToString("yyyy-MM-dd")); // search_date

                            urlListTemp.Add(urlModified);
                            urlModified = string.Empty;

                            periodDate = periodDate.AddDays(1);
                        }

                        urlList.Add(string.Format("jalan#{0}#{1}", hotelCode, hotelId), urlListTemp);
                    }
                    break;

                case "ikyu":
                case "ikyubiz":
                case "ikyucaz":
                    {
                        if (ota.Equals("ikyubiz"))
                        {
                            urlOriginal = GetUrlBase("ikyubiz");
                        }
                        else if (ota.Equals("ikyucaz"))
                        {
                            urlOriginal = GetUrlBase("ikyucaz");
                        }
                        else
                        {
                            urlOriginal = GetUrlBase("ikyu");
                        }

                        for (int i = 0; i < period; i++) // PERIOD
                        {
                            urlModified = urlOriginal;

                            urlModified = urlModified.Replace("#aid#", hotelCode);
                            urlModified = urlModified.Replace("#cid#", periodDate.ToString("yyyy-MM-dd").Replace("-", string.Empty));
                            urlModified = urlModified.Replace("#cod#", periodDate.AddDays(days).ToString("yyyy-MM-dd").Replace("-", string.Empty));
                            urlModified = urlModified.Replace("#ppc#", person.ToString());

                            urlModified += string.Format("#{0}", periodDate.ToString("yyyy-MM-dd")); // search_date

                            urlListTemp.Add(urlModified);
                            urlModified = string.Empty;

                            periodDate = periodDate.AddDays(1);
                        }

                        if (ota.Equals("ikyubiz"))
                        {
                            urlList.Add(string.Format("ikyubiz#{0}#{1}", hotelCode, hotelId), urlListTemp);
                        }
                        else if (ota.Equals("ikyucaz"))
                        {
                            urlList.Add(string.Format("ikyucaz#{0}#{1}", hotelCode, hotelId), urlListTemp);
                        }
                        else
                        {
                            urlList.Add(string.Format("ikyu#{0}#{1}", hotelCode, hotelId), urlListTemp);
                        }
                    }
                    break;

                case "rurubu":
                    {
                        // 0 = code
                        // 1 = URL
                        string[] hotelCodeSplit = hotelCode.Split('#');

                        if (hotelCodeSplit.Length != 2)
                        {
                            return 0;
                        }

                        if (string.IsNullOrEmpty(hotelCodeSplit[0]) || string.IsNullOrEmpty(hotelCodeSplit[1]))
                        {
                            return 0;
                        }

                        urlOriginal = GetUrlBase("rurubu");
                        urlOriginal = "https://www.rurubu.travel/api/cronos/property/BelowFoldParams/RoomGridData";
                        for (int i = 0; i < period; i++) // PERIOD
                        {
                            urlModified = urlOriginal;

                            //urlModified = urlModified.Replace("#object_id#", hotelCodeSplit[0]);
                            //urlModified = urlModified.Replace("#check_in#", periodDate.ToString("yyyy-MM-dd"));
                            //urlModified = urlModified.Replace("#los#", days.ToString());
                            //urlModified = urlModified.Replace("#adult#", person.ToString());

                            urlModified += string.Format("#{0}", periodDate.ToString("yyyy-MM-dd")); // search_date

                            urlListTemp.Add(urlModified);
                            urlModified = string.Empty;

                            periodDate = periodDate.AddDays(1);
                        }

                        urlList.Add(string.Format("rurubu#{0}_{1}#{2}", hotelCodeSplit[0], hotelCodeSplit[1], hotelId), urlListTemp);
                    }
                    break;

                case "jtb":
                    {
                        // 0-3 = region code
                        // 4   = hotel code
                        string[] hotelCodeSplit = hotelCode.Split('#');

                        if (hotelCodeSplit.Length != 5)
                        {
                            return 0;
                        }

                        if (string.IsNullOrEmpty(hotelCodeSplit[0]) || string.IsNullOrEmpty(hotelCodeSplit[1]) || 
                            string.IsNullOrEmpty(hotelCodeSplit[2]) || string.IsNullOrEmpty(hotelCodeSplit[4]))
                        {
                            return 0;
                        }
                        
                        urlOriginal = GetUrlBase("jtb");

                        for (int i = 0; i < period; i++) // PERIOD
                        {
                            //urlModified = urlOriginal;
                            urlModified = "https://www.jtb.co.jp/kokunai_air/front-core/v1/dp/hotels/#hcd#/plans?searchtype=yado&godate=#startdate#&checkindate=#startdate#&kodawariyado=jtbplan&room=1&hotelid=#hcd#&planlistsort=low&roomassign=m#person#&backdate=#enddate#&staynight=#los#&itemperpage=40&sitetype=1&approachinfo=none&isyadocondition=1&fields=plans,planCount,dataversion,feature,features";

                            urlModified = urlModified.Replace("#hcd#", hotelCodeSplit[4]);
                            urlModified = urlModified.Replace("#startdate#", periodDate.ToString("yyyyMMdd"));
                            urlModified = urlModified.Replace("#enddate#", periodDate.AddDays(days).ToString("yyyyMMdd"));
                            urlModified = urlModified.Replace("#los#", days.ToString());
                            urlModified = urlModified.Replace("#person#", person.ToString());

                            urlModified += string.Format("#{0}", periodDate.ToString("yyyy-MM-dd")); // search_date
                            
                            urlListTemp.Add(urlModified);
                            urlModified = string.Empty;

                            periodDate = periodDate.AddDays(1);
                        }

                        urlList.Add(string.Format("jtb#{0}#{1}", hotelCodeSplit[4], hotelId), urlListTemp);
                    }
                    break;

                case "bestrsv":
                    {
                        urlOriginal = GetUrlBase("bestrsv");

                        for (int i = 0; i < period; i++) // PERIOD
                        {
                            urlModified = urlOriginal;

                            stayStartDate = periodDate.ToString("yyyy-MM-dd").Split('-');

                            urlModified = urlModified.Replace("#hotel_cd#", hotelCode);
                            urlModified = urlModified.Replace("#year_month#", string.Format("{0}-{1}", stayStartDate[0], stayStartDate[1]));
                            urlModified = urlModified.Replace("#day#", stayStartDate[2]);
                            urlModified = urlModified.Replace("#today#", DateTime.Today.ToString("yyyy-MM-dd"));
                            urlModified = urlModified.Replace("#stay#", days.ToString());
                            urlModified = urlModified.Replace("#adult[0]#", person.ToString());

                            urlModified += string.Format("#{0}", periodDate.ToString("yyyy-MM-dd")); // search_date

                            urlListTemp.Add(urlModified);
                            urlModified = string.Empty;

                            periodDate = periodDate.AddDays(1);
                        }

                        urlList.Add(string.Format("bestrsv#{0}#{1}", hotelCode, hotelId), urlListTemp);
                    }
                    break;

                case "agoda":
                    {
                        // 0 = code
                        // 1 = URL
                        string[] hotelCodeSplit = hotelCode.Split('#');

                        if (hotelCodeSplit.Length != 2)
                        {
                            return 0;
                        }

                        if (string.IsNullOrEmpty(hotelCodeSplit[0]) || string.IsNullOrEmpty(hotelCodeSplit[1]))
                        {
                            return 0;
                        }

                        urlOriginal = GetUrlBase("agoda");
                        urlOriginal = "https://www.agoda.com/api/cronos/property/BelowFoldParams/RoomGridData";
                        for (int i = 0; i < period; i++) // PERIOD
                        {
                            urlModified = urlOriginal;

                            //urlModified = urlModified.Replace("#object_id#", hotelCodeSplit[0]);
                            //urlModified = urlModified.Replace("#check_in#", periodDate.ToString("yyyy-MM-dd"));
                            //urlModified = urlModified.Replace("#los#", days.ToString());
                            //urlModified = urlModified.Replace("#adult#", person.ToString());

                            urlModified += string.Format("#{0}", periodDate.ToString("yyyy-MM-dd")); // search_date

                            urlListTemp.Add(urlModified);
                            urlModified = string.Empty;

                            periodDate = periodDate.AddDays(1);
                        }

                        urlList.Add(string.Format("agoda#{0}_{1}#{2}", hotelCodeSplit[0], hotelCodeSplit[1], hotelId), urlListTemp);
                    }
                    break;

                case "booking":
                    {
                        // 0 = hotel code
                        // 1 = hotel name
                        // 2 = country code
                        string[] hotelCodeSplit = hotelCode.Split('#');

                        if (hotelCodeSplit.Length != 3)
                        {
                            return 0;
                        }

                        if (string.IsNullOrEmpty(hotelCodeSplit[0]) || string.IsNullOrEmpty(hotelCodeSplit[1]) || string.IsNullOrEmpty(hotelCodeSplit[2]))
                        {
                            return 0;
                        }

                        urlOriginal = GetUrlBase("booking");

                        for (int i = 0; i < period; i++) // PERIOD
                        {
                            urlModified = urlOriginal;

                            urlModified = urlModified.Replace("#country#", hotelCodeSplit[2]);
                            urlModified = urlModified.Replace("#hotelname#", hotelCodeSplit[1]);
                            urlModified = urlModified.Replace("#ci#", periodDate.ToString("yyyy-MM-dd"));
                            urlModified = urlModified.Replace("#co#", periodDate.AddDays(days).ToString("yyyy-MM-dd"));
                            urlModified = urlModified.Replace("#person#", person.ToString());
                            urlModified = urlModified.Replace("{bookingno}", hotelCodeSplit[0]);

                            urlModified += string.Format("#{0}", periodDate.ToString("yyyy-MM-dd")); // search_date

                            urlListTemp.Add(urlModified);
                            urlModified = string.Empty;

                            periodDate = periodDate.AddDays(1);
                        }

                        urlList.Add(string.Format("booking#{0}_{1}#{2}", hotelCodeSplit[0], hotelCodeSplit[1], hotelId), urlListTemp);
                    }
                    break;

                case "expedia": // BIGLOBE(EXPEDIA)
                    {
                        // 0 = code (biglobe)
                        // 1 = name (URL, expedia)
                        string[] hotelCodeSplit = hotelCode.Split('#');

                        if (hotelCodeSplit.Length < 1)
                        {
                            return 0;
                        }

                        urlOriginal = GetUrlBase("expedia");

                        for (int i = 0; i < period; i++) // PERIOD
                        {
                            urlModified = urlOriginal;

                            stayStartDate = periodDate.ToString("yyyy-MM-dd").Split('-');

                            urlModified = urlModified.Replace("#code#", hotelCodeSplit[0]);
                            urlModified = urlModified.Replace("#date#", string.Format("{0}{1}{2}", stayStartDate[0], stayStartDate[1], stayStartDate[2]));
                            urlModified = urlModified.Replace("#person#", person.ToString());
                            urlModified = urlModified.Replace("#los#", "1"); // FIXED

                            urlModified += string.Format("#{0}", periodDate.ToString("yyyy-MM-dd")); // search_date

                            urlListTemp.Add(urlModified);
                            urlModified = string.Empty;

                            periodDate = periodDate.AddDays(1);
                        }

                        if (hotelCodeSplit.Length > 1)
                        {
                            urlList.Add(string.Format("expedia#{0}_{1}#{2}", hotelCodeSplit[0], hotelCodeSplit[1], hotelId), urlListTemp);
                        }
                        else
                        {
                            urlList.Add(string.Format("expedia#{0}#{1}", hotelCodeSplit[0], hotelId), urlListTemp);
                        }
                    }
                    break;

                case "directin":
                    {
                        string[] hotelCodeSplit = hotelCode.Split('#');

                        if (hotelCodeSplit.Length != 2)
                        {
                            return 0;
                        }

                        if (string.IsNullOrEmpty(hotelCodeSplit[0]) || string.IsNullOrEmpty(hotelCodeSplit[1]))
                        {
                            return 0;
                        }

                        urlOriginal = GetUrlBase("directin");

                        for (int i = 0; i < period; i++) // PERIOD
                        {
                            urlModified = urlOriginal;

                            stayStartDate = periodDate.ToString("yyyy-MM-dd").Split('-');

                            urlModified = urlModified.Replace("#hcod1#", hotelCodeSplit[0]);
                            urlModified = urlModified.Replace("#hcod2#", hotelCodeSplit[1]);
                            urlModified = urlModified.Replace("#YMD#", string.Format("{0}/{1}/{2}", stayStartDate[0], stayStartDate[1], stayStartDate[2]));
                            urlModified = urlModified.Replace("#HAKSU#", days.ToString());

                            urlModified += string.Format("#{0}", periodDate.ToString("yyyy-MM-dd")); // search_date

                            urlListTemp.Add(urlModified);
                            urlModified = string.Empty;

                            periodDate = periodDate.AddDays(1);
                        }

                        urlList.Add(string.Format("directin#{0}_{1}#{2}", hotelCodeSplit[0], hotelCodeSplit[1], hotelId), urlListTemp);
                    }
                    break;

                default:
                    throw new NotSupportedException();
            }

            return urlListTemp.Count;
        }

        private bool CheckOtaExistenceInList(string ota, string[] list)
        {
            // Checks an existence of OTA in specified list.

            bool result = false;

            foreach (string item in list)
            {
                if (item.Equals(ota))
                {
                    result = true;
                    break;
                }
            }

            return result;
        }

        #endregion

        public void TestAnything()
        {
            //Analyst.AnalysisManager.Instance.StartAnalysis(38, 10052, new System.IO.DirectoryInfo(@""));
        }
    }
}