﻿using System;
using System.Collections.Generic;

namespace Psinc.Crawler.DataStructure
{
    /// <summary>
    /// アカウント情報
    /// </summary>
    public class AccountInfo
    {
        private static AccountInfo _instance = null;

        private readonly int _timerLimit = 4;

        private int _accountId;
        private string _accountName;
        private string _loginId;
        private DateTime _loginDate;
        private string _moduleVersion;

        /// <summary>
        /// key = OwnHotelBase.HotelId
        /// </summary>
        private Dictionary<int, OwnHotelBase> _hotelList;


        /// <summary>
        /// Private constructor.
        /// </summary>
        private AccountInfo()
        {
        }

        /// <summary>
        /// Gets an instance of 'AccountInfo'.
        /// </summary>
        public static AccountInfo Instance
        {
            get
            {
                _instance = _instance ?? new AccountInfo();
                return _instance;
            }
        }

        #region :: PROPERTIES

        /// <summary>
        /// Gets a limit value of timer list.
        /// </summary>
        public int TimerLimit
        {
            get
            {
                return _timerLimit;
            }
        }

        /// <summary>
        /// Gets an account ID.
        /// </summary>
        public int AccountId
        {
            get
            {
                return _accountId;
            }
        }

        /// <summary>
        ///  Gets or sets a name for account.
        /// </summary>
        public string AccountName
        {
            get
            {
                return _accountName;
            }

            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentNullException();
                }
                else
                {
                    // Account name must not empty or null.
                    _accountName = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets a login ID.
        /// </summary>
        public string LoginId
        {
            get
            {
                return _loginId;
            }

            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentNullException();
                }
                else
                {
                    // Login ID must not empty or null.
                    _loginId = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets a logged in date.
        /// </summary>
        public DateTime LoginDate
        {
            get
            {
                return _loginDate;
            }

            set
            {
                _loginDate = value;
            }
        }

        /// <summary>
        ///  Gets or sets a version of currently ran program.
        /// </summary>
        public string Version
        {
            get
            {
                return _moduleVersion;
            }

            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentNullException();
                }
                else
                {
                    // Version must not empty or null.
                    _moduleVersion = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets a list of own hotels that belongs specified account ID.
        /// </summary>
        public Dictionary<int, OwnHotelBase> Hotels
        {
            get
            {
                return _hotelList;
            }

            set
            {
                _hotelList = value;
            }
        }

        #endregion

        #region :: METHODS

        /// <summary>
        /// Initializes inner data.
        /// </summary>
        /// <param name="accountId">account id to set</param>
        public void Initialize(int accountId = 0)
        {
            if (accountId < 1)
            {
                // Account ID is must over 1.
                throw new ArgumentNullException();
            }

            _accountId = accountId;
            _accountName = string.Empty;
            _loginId = string.Empty;
            _loginDate = new DateTime();
            _moduleVersion = string.Empty;
            _hotelList = new Dictionary<int, OwnHotelBase>();
        }

        /// <summary>
        /// Gets a hotel name that holds specified hotel ID.
        /// </summary>
        /// <param name="hotelId">hotel ID</param>
        /// <returns>hotel name ('null' means no result)</returns>
        public string GetHotelName(int hotelId)
        {
            string result = null;

            foreach (KeyValuePair<int, OwnHotelBase> item in _hotelList)
            {
                if (item.Key == hotelId)
                {
                    result = item.Value.HotelName;
                    break;
                }
            }

            return result;
        }

        /// <summary>
        /// Adds a scheduled crawler item to timer list of specified hotel.
        /// </summary>
        /// <param name="hotelId">hotel ID</param>
        /// <param name="item">scheduled crawler item</param>
        public void AddTimer(int hotelId, ScheduledCrawlerBase item)
        {
            foreach (KeyValuePair<int, OwnHotelBase> hotel in _hotelList)
            {
                if (hotel.Key == hotelId)
                {
                    hotel.Value.TimerList.Add(item.TimerId, item);
                    break;
                }
            }
        }

        /// <summary>
        /// Removes a scheduled crawler item from timer list of specified hotel.
        /// </summary>
        /// <param name="hotelId">hotel ID</param>
        /// <param name="timerId">item(timer) ID</param>
        /// <returns>success or not</returns>
        public bool RemoveTimer(int hotelId, int timerId)
        {
            bool result = false;

            foreach (KeyValuePair<int, OwnHotelBase> hotel in _hotelList)
            {
                if (hotel.Key == hotelId)
                {
                    foreach (KeyValuePair<int, ScheduledCrawlerBase> timer in hotel.Value.TimerList)
                    {
                        if (timer.Key == timerId)
                        {
                            hotel.Value.TimerList.Remove(timerId);

                            result = true;
                            break;
                        }
                    }

                    break;
                }
            }

            return result;
        }

        /// <summary>
        /// Updates a scheduled crawler item in timer list of specified hotel.
        /// </summary>
        /// <param name="hotelId">hotel ID</param>
        /// <param name="timerId">item(timer) ID</param>
        /// <param name="item">scheduled crawler item</param>
        /// <returns>success or not</returns>
        public bool UpdateTimer(int hotelId, int timerId, ScheduledCrawlerBase item)
        {
            bool result = true;

            foreach (KeyValuePair<int, OwnHotelBase> hotel in _hotelList)
            {
                if (hotel.Key == hotelId)
                {
                    ScheduledCrawlerBase dummy;
                    hotel.Value.TimerList.TryGetValue(timerId, out dummy);

                    if (dummy.AccountId > 0)
                    {
                        hotel.Value.TimerList.Remove(timerId);
                        hotel.Value.TimerList.Add(item.TimerId, item);
                    }
                    else
                    {
                        result = false;
                    }
                    break;
                }
            }

            return result;
        }

        /// <summary>
        /// Checks an availability of passed item for specified hotel.
        /// </summary>
        /// <param name="hotelId">hotel ID</param>
        /// <param name="item">scheduled crawler item</param>
        /// <returns>available or not</returns>
        public bool CheckTimerAvailability(int hotelId, ScheduledCrawlerBase item)
        {
            foreach (KeyValuePair<int, OwnHotelBase> hotel in _hotelList)
            {
                if (hotel.Key == hotelId)
                {
                    DateTime dummy1 = DateTime.Now;
                    DateTime dummy2 = new DateTime(dummy1.Year, dummy1.Month, dummy1.Day, item.ExecuteHour, item.ExecuteMinute, 0);

                    foreach (KeyValuePair<int, ScheduledCrawlerBase> timer in hotel.Value.TimerList)
                    {
                        if (timer.Value.StartDate.Equals(item.StartDate) && timer.Value.StartTrigger == item.StartTrigger &&
                            timer.Value.Period == item.Period && timer.Value.Person == item.Person && timer.Value.Days == item.Days &&
                            timer.Value.ExecuteHour == item.ExecuteHour && timer.Value.ExecuteMinute == item.ExecuteMinute)
                        {
                            // SAME ITEM PROBLEM
                            return false;
                        }

                        DateTime picked = new DateTime(dummy1.Year, dummy1.Month, dummy1.Day, timer.Value.ExecuteHour, timer.Value.ExecuteMinute, 0);

                        if (picked > dummy2.AddHours(-1d) && picked < dummy2.AddHours(1d))
                        {
                            // TIMER INTERVAL PROBLEM
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Checks a scheduled crawler item can be added to timer list of specified hotel.
        /// </summary>
        /// <param name="hotelId">hotel ID</param>
        /// <returns>available or not</returns>
        public bool CheckTimerCapacity(int hotelId)
        {
            bool result = true;

            foreach (KeyValuePair<int, OwnHotelBase> hotel in _hotelList)
            {
                if (hotel.Key == hotelId)
                {
                    if (hotel.Value.TimerList.Count == _timerLimit)
                    {
                        result = false;
                        break;
                    }
                }
            }

            return result;
        }

        #endregion
    }

    /// <summary>
    /// 自社施設情報ベース
    /// </summary>
    public struct OwnHotelBase
    {
        public int HotelId;

        public string HotelName;

        public string OtaList;

        /// <summary>
        /// key = CompetitiveHotelBase.HotelId
        /// </summary>
        public Dictionary<int, CompetitiveHotelBase> CompetitiveHotelList;

        /// <summary>
        /// key = ScheduledCrawlerBase.TimerId
        /// </summary>
        public Dictionary<int, ScheduledCrawlerBase> TimerList;

        /// <summary>
        /// key = OtaInfoBase.Abbreviation
        /// </summary>
        public Dictionary<string, string> OtaHotelCodeList;
    }

    /// <summary>
    /// 競合施設情報ベース
    /// </summary>
    public struct CompetitiveHotelBase
    {
        public int HotelId;

        public string HotelName;

        public string OtaList;

        /// <summary>
        /// key = OtaInfoBase.Abbreviation
        /// </summary>
        public Dictionary<string, string> OtaHotelCodeList;
    }

    /// <summary>
    /// クローラ情報ベース
    /// </summary>
    public struct CrawlerBase
    {
        public int AccountId; // AccountInfo.AccountId

        public int HotelId;   // AccountInfo.Hotels.HotelId

        public int CrawlerId;

        public string OtaList;

        public string StartDate;   // yyyy-MM-dd

        public string EndDate;     // yyyy-MM-dd

        public int StartTrigger;

        public int Period;

        public int Person;

        public int Days;

        public string ExecuteDate; // yyyy-MM-dd

        public int ExecuteHour;

        public int ExecuteMinute;
    }

    /// <summary>
    /// 予約されたクローラ情報ベース
    /// </summary>
    public struct ScheduledCrawlerBase
    {
        public int AccountId; // AccountInfo.AccountId

        public int HotelId;   // AccountInfo.Hotels.HotelId

        public int TimerId;

        public string OtaList;

        public string StartDate;   // yyyy-MM-dd

        public int StartTrigger;

        public int Period;

        public int Person;

        public int Days;

        public string ExecuteDate; // yyyy-MM-dd

        public int ExecuteHour;

        public int ExecuteMinute;
    }

    /// <summary>
    /// 媒体情報ベース
    /// </summary>
    public struct OtaInfoBase
    {
        public int Id;

        public string Name;

        public string Abbreviation;

        public OtaType Type;

        public string Url;

        public int SortOrder;
    }

    /// <summary>
    /// クローラタスク情報ベース
    /// </summary>
    public struct JobInfoBase
    {
        public string Id;

        public CrawlerType Type;

        public DateTime Started;

        public JobStep Step;

        public bool InProgress;
    }

    /// <summary>
    /// ログ種類
    /// </summary>
    public enum LogType
    {
        INFO,

        ERROR,

        PULSE
    }

    /// <summary>
    /// 媒体種類
    /// </summary>
    public enum OtaType
    {
        OTA,      // OTA

        ENGINE,   // 予約エンジン

        OVERSEAS, // 海外OTA

        LEISURE,  // レジャーホテルOTA

        SNS       // SNS
    }

    /// <summary>
    /// クローラ種類
    /// </summary>
    public enum CrawlerType
    {
        REALTIME,

        TIMER
    }

    /// <summary>
    /// タスク段階
    /// </summary>
    public enum JobStep
    {
        PREPARE,

        CRAWL,

        ANALYSE,

        SEND
    }

    /// <summary>
    /// クローラエージェント
    /// </summary>
    public class CrawlerAgent
    {
        public static readonly string Agoda   = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:57.0) Gecko/20100101 Firefox/57.0";
                                              // Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:45.0) Gecko/20100101 Firefox/45.0
                                              // Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:26.0) Gecko/20100101 Firefox/26.0
        public static readonly string Rurubu = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:26.0) Gecko/20100101 Firefox/26.0";

        public static readonly string Booking = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36";
                                              // Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.63 Safari/537.36 <- backup

        public static readonly string Expedia = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36";

        public static readonly string[] BasicSet = {
            "Mozilla/5.0 (Windows; U; Windows NT 5.1; ja; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13 GTB7.1 ( .NET CLR 3.5.30729)",
            "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.13 (KHTML, like Gecko) Chrome/9.0.597.98 Safari/534.13",
            "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)",
            "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.0; Trident/5.0)",
            "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0",
            "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"
        };
    }
}