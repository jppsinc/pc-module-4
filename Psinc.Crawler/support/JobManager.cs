﻿using System;
using System.Collections.Generic;
using Psinc.Crawler.DataStructure;

namespace Psinc.Crawler
{
    /// <summary>
    /// Class for crawling job management.
    /// </summary>
    public class JobManager
    {
        private static JobManager _instance = null;

        private Dictionary<string, JobInfoBase> _jobList = null;


        /// <summary>
        /// Private constructor.
        /// </summary>
        private JobManager()
        {
            _jobList = new Dictionary<string, JobInfoBase>();
        }

        /// <summary>
        /// Gets an instance of 'JobManager'.
        /// </summary>
        public static JobManager Instance
        {
            get
            {
                _instance = _instance ?? new JobManager();
                return _instance;
            }
        }

        /// <summary>
        /// Gets a status that currently executing job exists or not.
        /// </summary>
        public bool IsJobInProgress
        {
            get
            {
                bool result = false;

                foreach (KeyValuePair<string, JobInfoBase> item in _jobList)
                {
                    if (item.Value.InProgress)
                    {
                        result = true;
                        break;
                    }
                }

                return result;
            }
        }

        /// <summary>
        /// Adds a new job to manager. If there has same id, returns false.
        /// </summary>
        /// <param name="type">crawler type</param>
        /// <param name="id">id</param>
        /// <returns>success or not</returns>
        public bool AddJob(CrawlerType type, string id)
        {
            bool result = true;

            if (_jobList.ContainsKey(id))
            {
                result = false;
            }
            else
            {
                JobInfoBase dummy = new JobInfoBase();
                dummy.Id = id;
                dummy.Type = type;
                dummy.Started = DateTime.Now;
                dummy.Step = JobStep.PREPARE;
                dummy.InProgress = true;

                _jobList.Add(id, dummy);
            }

            return result;
        }

        /// <summary>
        /// Removes a job from manager.
        /// </summary>
        /// <param name="id">id</param>
        public void RemoveJob(string id)
        {
            foreach (KeyValuePair<string, JobInfoBase> item in _jobList)
            {
                if (item.Key.Equals(id))
                {
                    _jobList.Remove(id);
                    break;
                }
            }
        }

        /// <summary>
        /// Changes a job step in job that has same ID with passed one.
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="step">job step to set</param>
        public void ChangeJobStep(string id, JobStep step)
        {
            foreach (KeyValuePair<string, JobInfoBase> item in _jobList)
            {
                if (item.Key.Equals(id))
                {
                    JobInfoBase dummy = new JobInfoBase();
                    dummy.Id = item.Value.Id;
                    dummy.Type = item.Value.Type;
                    dummy.Started = item.Value.Started;
                    dummy.Step = step;
                    dummy.InProgress = item.Value.InProgress;

                    _jobList.Remove(id);

                    _jobList.Add(id, dummy);

                    break;
                }
            }
        }
    }
}