﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Timers;
using Psinc.Crawler.DataStructure;
using Psinc.Util;

namespace Psinc.Crawler
{
    /// <summary>
    /// Class for scheduled web crawler.
    /// </summary>
    public class ScheduledCrawler
    {
        private static ScheduledCrawler _instance = null;

        /// <summary>
        /// key = ScheduledCrawlerBase.TimerId
        /// </summary>
        private Dictionary<int, ScheduledCrawlerBase> _timerInfo;

        /// <summary>
        /// key = ScheduledCrawlerBase.TimerId
        /// </summary>
        private Dictionary<int, Timer> _timerList;

        private bool _isOnHold = false;

        private string _errorMessage = string.Empty;
        private readonly string _msgWrongExecuteDate = "ERROR OCCURRED (SC): WRONG EXECUTION DATE/TIME";

        private readonly string _msgJobStart    = "SCHEDULED JOB STARTED: {0} / {1:00}:{2:00} / {3}-{4}-{5} / {6}";
        private readonly string _msgJobComplete = "SCHEDULED JOB COMPLETED: {0} / {1:00}:{2:00} / {3}-{4}-{5} / {6}";

        private AddTimer _delegateAddTimer = null;
        private RemoveTimer _delegateRemoveTimer = null;
        private UpdateTimer _delegateUpdateTimer = null;
        private AddText _delegateAddTextToSystemMessage = null;

        public delegate void AddTimer(ScheduledCrawlerBase item);
        public delegate void RemoveTimer(int seq);
        public delegate void UpdateTimer(int seq, string cellName, string contents);
        public delegate void AddText(string message);


        /// <summary>
        /// Private constructor.
        /// </summary>
        private ScheduledCrawler()
        {
            _timerInfo = new Dictionary<int, ScheduledCrawlerBase>();
            _timerList = new Dictionary<int, Timer>();
        }

        /// <summary>
        /// Gets an instance of 'ScheduledCrawler'.
        /// </summary>
        public static ScheduledCrawler Instance
        {
            get
            {
                _instance = _instance ?? new ScheduledCrawler();
                return _instance;
            }
        }

        #region :: PROPERTIES

        /// <summary>
        /// Sets all timers on hold.
        /// </summary>
        public bool OnHoldTimer
        {
            set
            {
                _isOnHold = value;
            }
        }

        /// <summary>
        /// Sets a delegate method of adding scheduled crawler item to list.
        /// </summary>
        public AddTimer DelegateForAddTimer
        {
            set
            {
                _delegateAddTimer = value;
            }
        }

        /// <summary>
        /// Sets a delegate method of removing scheduled crawler item from list.
        /// </summary>
        public RemoveTimer DelegateForRemoveTimer
        {
            set
            {
                _delegateRemoveTimer = value;
            }
        }

        /// <summary>
        /// Sets a delegate method of updating scheduled crawler item.
        /// </summary>
        public UpdateTimer DelegateForUpdateTimer
        {
            set
            {
                _delegateUpdateTimer = value;
            }
        }

        /// <summary>
        /// Sets a delegate method of adding text to system message list of main-form.
        /// </summary>
        public AddText DelegateForAddText
        {
            set
            {
                _delegateAddTextToSystemMessage = value;
            }
        }

        #endregion

        #region :: METHODS

        /// <summary>
        /// Initializes a timer list.
        /// </summary>
        public void InitializeTimerList()
        {
            foreach (var item in _timerList)
            {
                item.Value.Elapsed -= (sender, e) => OnTimer_Elapsed(sender, e, item.Key);
                item.Value.Enabled = false;

                item.Value.Dispose();
            }

            _timerInfo.Clear();
            _timerList.Clear();
        }

        /// <summary>
        /// Loads all of scheduled crawlers from 'AccountInfo' class.
        /// </summary>
        public void LoadTimerList()
        {
            foreach (KeyValuePair<int, OwnHotelBase> hotel in AccountInfo.Instance.Hotels)
            {
                Dictionary<int, ScheduledCrawlerBase> dummy = new Dictionary<int, ScheduledCrawlerBase>();

                foreach (KeyValuePair<int, ScheduledCrawlerBase> timer in hotel.Value.TimerList)
                {
                    ScheduledCrawlerBase itemModified;
                    if (AddScheduledCrawlerItem(timer.Value, out itemModified, false))
                    {
                        dummy.Add(timer.Key, itemModified);
                    }
                    else
                    {
                        NetworkManager.Instance.SendLog(timer.Value.AccountId, LogType.ERROR.ToString().ToLower(), string.Format(SharedCommand.LogMessageInsertItemError, timer.Key));
                    }
                }

                foreach (KeyValuePair<int, ScheduledCrawlerBase> timer in dummy)
                {
                    AccountInfo.Instance.UpdateTimer(hotel.Key, timer.Key, timer.Value);
                }
            }
        }

        /// <summary>
        /// Adds a scheduled crawler item to timer list.
        /// </summary>
        /// <param name="itemOriginal">item to add</param>
        /// <param name="itemModified">modified passed item</param>
        /// <param name="isNewItem">if item is new, set 'true'</param>
        /// <returns>success or not</returns>
        public bool AddScheduledCrawlerItem(ScheduledCrawlerBase itemOriginal, out ScheduledCrawlerBase itemModified, bool isNewItem)
        {
            itemModified = new ScheduledCrawlerBase();

            bool result = true;

            DateTime now = DateTime.Now;

            // EXECUTE DATE
            DateTime executeDate;

            if (string.IsNullOrEmpty(itemOriginal.ExecuteDate))
            {
                executeDate = new DateTime(now.Year, now.Month, now.Day, itemOriginal.ExecuteHour, itemOriginal.ExecuteMinute, 0);
            }
            else
            {
                string[] executeDateOriginal = itemOriginal.ExecuteDate.Split('-');
                executeDate = new DateTime(int.Parse(executeDateOriginal[0]), int.Parse(executeDateOriginal[1]), int.Parse(executeDateOriginal[2]),
                                           itemOriginal.ExecuteHour, itemOriginal.ExecuteMinute, 0);
            }

            if (executeDate <= now)
            {
                executeDate = executeDate.AddDays(now.Subtract(executeDate).Days + 1);
            }

            itemOriginal.ExecuteDate = executeDate.ToString("yyyy-MM-dd");

            // START DATE
            string[] startDateOriginal = itemOriginal.StartDate.Split('-');

            DateTime startDate = new DateTime(int.Parse(startDateOriginal[0]), int.Parse(startDateOriginal[1]), int.Parse(startDateOriginal[2]),
                                              itemOriginal.ExecuteHour, itemOriginal.ExecuteMinute, 0);
            bool updateStartDate = false;
            if (startDate.Date < executeDate.Date)
            {
                updateStartDate = true;
                itemOriginal.StartDate = executeDate.ToString("yyyy-MM-dd");
            }

            TimeSpan remainedTime = executeDate.Subtract(now);

            if (remainedTime.TotalMilliseconds > 0)
            {
                string res;

                if (isNewItem) // NEW ITEM
                {
                    //--> SERVER
                    if (NetworkManager.Instance.GetUrlResponse(string.Format(NetworkManager.Instance.UrlFormat, SharedCommand.CommandInsertTimer), out res,
                                                               itemOriginal.AccountId.ToString(), itemOriginal.HotelId.ToString(), string.Format("[{0}]", itemOriginal.OtaList),
                                                               itemOriginal.StartDate, itemOriginal.Period.ToString(), itemOriginal.Person.ToString(), itemOriginal.Days.ToString(),
                                                               string.Format("{0:00}:{1:00}:00", itemOriginal.ExecuteHour.ToString(), itemOriginal.ExecuteMinute.ToString()),
                                                               itemOriginal.StartTrigger.ToString()))
                    {
                        if (res.Equals("err"))
                        {
                            NetworkManager.Instance.SendLog(itemOriginal.AccountId, LogType.ERROR.ToString().ToLower(), string.Format(SharedCommand.LogMessageInsertItemError, "NEW_TIMER"));
                            result = false;
                        }
                        else
                        {
                            itemOriginal.TimerId = int.Parse(res);

                            //--> AccountInfo.Hotels.TimerList
                            AccountInfo.Instance.AddTimer(itemOriginal.HotelId, itemOriginal);
                        }
                    }
                    else
                    {
                        NetworkManager.Instance.SendLog(itemOriginal.AccountId, LogType.ERROR.ToString().ToLower(),
                                                        string.Format(SharedCommand.LogMessageDefaultError, Tools.GetLocalIPAddress(), NetworkManager.Instance.ErrorMessage));
                        result = false;
                    }
                }
                else // EXISTING ITEM
                {
                    if (updateStartDate)
                    {
                        //--> SERVER
                        if (NetworkManager.Instance.GetUrlResponse(string.Format(NetworkManager.Instance.UrlFormat, SharedCommand.CommandUpdateTimer), out res,
                                                                   itemOriginal.TimerId.ToString(), itemOriginal.StartDate))
                        {
                            if (res.Equals("err"))
                            {
                                NetworkManager.Instance.SendLog(itemOriginal.AccountId, LogType.ERROR.ToString().ToLower(), string.Format(SharedCommand.LogMessageUpdateItemError, itemOriginal.TimerId));
                            }
                        }
                        else
                        {
                            NetworkManager.Instance.SendLog(itemOriginal.AccountId, LogType.ERROR.ToString().ToLower(),
                                                            string.Format(SharedCommand.LogMessageDefaultError, Tools.GetLocalIPAddress(), NetworkManager.Instance.ErrorMessage));
                        }
                    }
                }

                if (result)
                {
                    //--> ScheduledCrawler._timerInfo
                    _timerInfo.Add(itemOriginal.TimerId, itemOriginal);

                    Timer dummy = new Timer();
                    dummy.AutoReset = false;
                    dummy.Enabled = true;
                    dummy.Interval = remainedTime.TotalMilliseconds;
                    dummy.Elapsed += (sender, e) => OnTimer_Elapsed(sender, e, itemOriginal.TimerId);

                    _timerList.Add(itemOriginal.TimerId, dummy);

                    itemModified = itemOriginal;
                }
            }
            else
            {
                NetworkManager.Instance.SendLog(itemOriginal.AccountId, LogType.ERROR.ToString().ToLower(), _msgWrongExecuteDate);
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Removes an item that same ID value from timer list.
        /// </summary>
        /// <param name="timerId">timer ID</param>
        /// <returns>success or not</returns>
        public bool RemoveScheduledCrawlerItem(int timerId)
        {
            bool result = false;

            ScheduledCrawlerBase dummy1;
            _timerInfo.TryGetValue(timerId, out dummy1);

            if (dummy1.AccountId > 0)
            {
                //<-- ScheduledCrawler._timerInfo
                _timerInfo.Remove(timerId);

                //<-- AccountInfo.Hotels.TimerList
                AccountInfo.Instance.RemoveTimer(dummy1.HotelId, dummy1.TimerId);

                string res;

                //<-- SERVER
                if (NetworkManager.Instance.GetUrlResponse(string.Format(NetworkManager.Instance.UrlFormat, SharedCommand.CommandDeleteTimer), out res, timerId.ToString()))
                {
                    if (res.Equals("err"))
                    {
                        NetworkManager.Instance.SendLog(AccountInfo.Instance.AccountId, LogType.ERROR.ToString().ToLower(), string.Format(SharedCommand.LogMessageDeleteItemError, timerId));
                    }
                }
                else
                {
                    NetworkManager.Instance.SendLog(AccountInfo.Instance.AccountId, LogType.ERROR.ToString().ToLower(),
                                                    string.Format(SharedCommand.LogMessageDefaultError, Tools.GetLocalIPAddress(), NetworkManager.Instance.ErrorMessage));
                }

                // DELETE TIMER
                Timer dummy2;
                _timerList.TryGetValue(timerId, out dummy2);

                if (dummy2 != null)
                {
                    dummy2.Elapsed -= (sender, e) => OnTimer_Elapsed(sender, e, timerId);
                    dummy2.Enabled = false;

                    dummy2.Dispose();

                    _timerList.Remove(timerId);

                    result = true;
                }
            }

            return result;
        }


        private void OnTimer_Elapsed(object sender, ElapsedEventArgs e, int timerId)
        {
            // Starts a scheduled crawling job.

            if (_isOnHold)
            {
                return;
            }

            ScheduledCrawlerBase source = new ScheduledCrawlerBase();

            foreach (KeyValuePair<int, ScheduledCrawlerBase> item in _timerInfo)
            {
                if (item.Key == timerId)
                {
                    source = item.Value;
                    break;
                }
            }

            CrawlerBase timerParameters = new CrawlerBase();
            timerParameters.AccountId     = source.AccountId;
            timerParameters.HotelId       = source.HotelId;
            timerParameters.CrawlerId     = source.TimerId;
            timerParameters.OtaList       = source.OtaList;
            timerParameters.StartDate     = source.StartDate;
            timerParameters.StartTrigger  = source.StartTrigger;
            timerParameters.Period        = source.Period;
            timerParameters.Person        = source.Person;
            timerParameters.Days          = source.Days;
            timerParameters.ExecuteDate   = source.ExecuteDate;
            timerParameters.ExecuteHour   = source.ExecuteHour;
            timerParameters.ExecuteMinute = source.ExecuteMinute;

            DateTime now = DateTime.Now;

            if (timerParameters.StartTrigger > 0)
            {
                timerParameters.StartDate = now.AddDays(timerParameters.StartTrigger).ToString("yyyy-MM-dd");
            }

            DateTime startDate = DateTime.Parse(timerParameters.StartDate);

            timerParameters.EndDate = startDate.AddDays(timerParameters.Period - 1).ToString("yyyy-MM-dd");

            NetworkManager.Instance.SendLog(timerParameters.AccountId, LogType.INFO.ToString().ToLower(),
                                                string.Format(_msgJobStart, timerId, timerParameters.ExecuteHour, timerParameters.ExecuteMinute,
                                                              timerParameters.Period, timerParameters.Person, timerParameters.Days, Tools.GetLocalIPAddress()));

            _delegateAddTextToSystemMessage?.Invoke(string.Format(SharedCommand.MessageStartScheduledCrawling, DateTime.Now, timerId));

            string msg = string.Format(SharedCommand.RawMessageScheduledCrawlingInfo,
                                       timerParameters.Person, timerParameters.StartDate, timerParameters.EndDate, timerParameters.Period, timerParameters.Days);

            _delegateAddTextToSystemMessage?.Invoke(string.Format(SharedCommand.MessageDefaultFormat, DateTime.Now, msg));

            int elapsed;
            bool result = true;

            // e.g. 05201023_33423_170609_45_2_3
            string id = string.Format("{0}_{1}_{2}_{3}_{4}_{5}", now.ToString("MMddHHmm"), timerParameters.HotelId, timerParameters.StartDate.Replace("-", string.Empty).Substring(2, 6),
                                      timerParameters.Period, timerParameters.Person, timerParameters.Days);

            string outputPath = Path.Combine(Environment.CurrentDirectory, id);

            if (Directory.Exists(outputPath))
            {
                Directory.Delete(outputPath, true);
            }

            WebCrawler crwl = new WebCrawler(false, timerParameters);

            if (JobManager.Instance.AddJob(CrawlerType.TIMER, id))
            {
                if (!crwl.StartJob(id, outputPath, out elapsed))
                {
                    _errorMessage = crwl.ErrorMessage;
                    result = false;
                }
            }
            else
            {
                _errorMessage = "[ScheduledCrawler] ALREADY EXISTING JOB";

                elapsed = 0;
                result = false;
            }

            if (result)
            {
                NetworkManager.Instance.SendLog(timerParameters.AccountId, LogType.INFO.ToString().ToLower(),
                                                string.Format(_msgJobComplete, timerId, timerParameters.ExecuteHour, timerParameters.ExecuteMinute,
                                                              timerParameters.Period, timerParameters.Person, timerParameters.Days, Tools.GetLocalIPAddress()));

                _delegateAddTextToSystemMessage?.Invoke(string.Format(SharedCommand.MessageElapsedTimeInfo, DateTime.Now, elapsed));
                _delegateAddTextToSystemMessage?.Invoke(string.Format(SharedCommand.MessageEndScheduledCrawling, DateTime.Now, timerId));

                // Inserts a log to local database.
                string otaNames = string.Empty;
                string[] otaListSplit = timerParameters.OtaList.Split(',');

                int counter = 0;
                for (int i = 0; i < otaListSplit.Length; i++)
                {
                    if (counter != 0)
                    {
                        otaNames += " / ";
                    }

                    if (string.IsNullOrEmpty(otaListSplit[i]))
                    {
                        continue;
                    }

                    bool checker = false;
                    foreach (KeyValuePair<string, OtaInfoBase> ota in SharedCommand.Instance.OtaInfo)
                    {
                        if (ota.Value.Id == int.Parse(otaListSplit[i]))
                        {
                            otaNames += ota.Value.Name;

                            checker = true;
                            break;
                        }
                    }

                    if (!checker)
                    {
                        otaNames += "*";
                    }

                    counter++;
                }

                string query = string.Format(SharedCommand.QueryInsertLog, timerParameters.AccountId, timerParameters.HotelId,
                                             string.Format("{0} {1:00}:{2:00}:00", timerParameters.ExecuteDate, timerParameters.ExecuteHour, timerParameters.ExecuteMinute),
                                             CrawlerType.TIMER.ToString(), otaNames, timerParameters.StartDate,
                                             timerParameters.Period, timerParameters.Person, timerParameters.Days);

                SQLiteManager.Instance.ExecuteNonQuery(query);
            }
            else
            {
                NetworkManager.Instance.SendLog(timerParameters.AccountId, LogType.ERROR.ToString().ToLower(), string.Format(SharedCommand.LogMessageDefaultError, id, _errorMessage));
                return;
            }

            // Updates an executed timer.
            foreach (KeyValuePair<int, Timer> item in _timerList)
            {
                if (item.Key == timerId)
                {
                    DateTime tmpStartDate = DateTime.Now.AddDays(1);
                    DateTime tmpEndDate = new DateTime(tmpStartDate.Year, tmpStartDate.Month, tmpStartDate.Day, source.ExecuteHour, source.ExecuteMinute, 0);

                    item.Value.Interval = tmpEndDate.Subtract(DateTime.Now).TotalMilliseconds;

                    break;
                }
            }

            // Updates dates of scheduled crawler item.
            foreach (KeyValuePair<int, ScheduledCrawlerBase> info in _timerInfo)
            {
                if (info.Key == timerId)
                {
                    ScheduledCrawlerBase newItem = new ScheduledCrawlerBase();
                    newItem.AccountId     = info.Value.AccountId;
                    newItem.HotelId       = info.Value.HotelId;
                    newItem.TimerId       = info.Value.TimerId;
                    newItem.OtaList       = info.Value.OtaList;
                    newItem.StartTrigger  = info.Value.StartTrigger;
                    newItem.Period        = info.Value.Period;
                    newItem.Person        = info.Value.Person;
                    newItem.Days          = info.Value.Days;
                    newItem.ExecuteHour   = info.Value.ExecuteHour;
                    newItem.ExecuteMinute = info.Value.ExecuteMinute;

                    string[] parsed = info.Value.StartDate.Split('-');
                    string[] parsed2 = info.Value.ExecuteDate.Split('-');

                    DateTime newStartDate = new DateTime(int.Parse(parsed[0]), int.Parse(parsed[1]), int.Parse(parsed[2]), source.ExecuteHour, source.ExecuteMinute, 0);
                    //DateTime newExecuteDate = DateTime.Now.AddDays(1);
                    DateTime ExecuteDate = new DateTime(int.Parse(parsed2[0]), int.Parse(parsed2[1]), int.Parse(parsed2[2]), source.ExecuteHour, source.ExecuteMinute, 0);

                    DateTime newExecuteDate;
                    if (ExecuteDate.Date <= now.Date && ExecuteDate.TimeOfDay > now.TimeOfDay)
                    {
                        newExecuteDate = now;
                    }
                    else
                    {
                        newExecuteDate = now.AddDays(1);
                    }

                    if (newStartDate.Date < newExecuteDate.Date)
                    {
                        newItem.StartDate = newExecuteDate.ToString("yyyy-MM-dd");
                    }
                    else
                    {
                        newItem.StartDate = newStartDate.AddDays(1).ToString("yyyy-MM-dd");
                    }
                    //newItem.ExecuteDate = newExecuteDate.AddDays(1).ToString("yyyy-MM-dd");
                    newItem.ExecuteDate = newExecuteDate.ToString("yyyy-MM-dd");

                    // UPDATE INFO.
                    _timerInfo.Remove(info.Key);

                    //--> ScheduledCrawler._timerInfo
                    _timerInfo.Add(newItem.TimerId, newItem);

                    //--> AccountInfo.Hotels.TimerList
                    AccountInfo.Instance.UpdateTimer(info.Value.HotelId, info.Value.TimerId, newItem);

                    string res;

                    //--> SERVER
                    if (NetworkManager.Instance.GetUrlResponse(string.Format(NetworkManager.Instance.UrlFormat, SharedCommand.CommandUpdateTimer), out res,
                                                               newItem.TimerId.ToString(), newItem.StartDate))
                    {
                        if (res.Equals("err"))
                        {
                            NetworkManager.Instance.SendLog(AccountInfo.Instance.AccountId, LogType.ERROR.ToString().ToLower(), string.Format(SharedCommand.LogMessageUpdateItemError, timerId));
                        }
                    }
                    else
                    {
                        NetworkManager.Instance.SendLog(AccountInfo.Instance.AccountId, LogType.ERROR.ToString().ToLower(),
                                                        string.Format(SharedCommand.LogMessageDefaultError, Tools.GetLocalIPAddress(), NetworkManager.Instance.ErrorMessage));
                    }

                    _delegateUpdateTimer(timerId, "TimerRowStartDate", newItem.StartDate);
                    _delegateUpdateTimer(timerId, "TimerRowExecDate", newItem.ExecuteDate);

                    break;
                }
            }
        }

        #endregion
    }
}