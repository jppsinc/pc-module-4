﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Psinc.Analyst.DataStructure;
using Psinc.Util;

namespace Psinc.Analyst
{
    /// <summary>
    /// 一休データ分析器（一休ビズ、一休キラリト含む）
    /// </summary>
    public class IkyuAnalyser : Analyser
    {
        private AgencyType _type;

        private readonly string _parseError           = "[{0}] PARSE ERROR IA{1:00}";
        private readonly string _parseErrNoData       = "[{2}] NO DATA IN CRAWLED FILE ({0}/{1})";
        private readonly string _parseErrCounter      = "[{0}] COUNTER TOKEN IS MISSING";
        private readonly string _parseErrList         = "[{0}] LIST TOKEN IS MISSING";
        private readonly string _parseErrPlans        = "[{0}] NO PLAN ELEMENT";
        private readonly string _parseErrRooms        = "[{0}] NO ROOM ELEMENT";
        private readonly string _parseErrMeals        = "[{0}] UNDEFINED MEAL TEXT";
        private readonly string _parseErrPlanUrl      = "[{0}] PLAN URL ELEMENT IS MISSING";
        private readonly string _parseErrPlanName     = "[{0}] PLAN NAME IS MISSING";
        private readonly string _parseErrPlanCode     = "[{0}] PLAN CODE IS MISSING";
        private readonly string _parseErrRoomType     = "[{0}] ROOM TYPE IS MISSING";
        private readonly string _parseErrRoomName     = "[{0}] ROOM NAME IS MISSING";
        private readonly string _parseErrRoomCode     = "[{0}] ROOM CODE IS MISSING";
        private readonly string _parseErrRoomVacancy  = "[{0}] ROOM VACANCY IS MISSING";
        private readonly string _parseErrMealFlag     = "[{0}] MEAL FLAG IS MISSING";
        private readonly string _parseErrPrice        = "[{0}] PRICE IS MISSING";
        private readonly string _parseErrPriceWithTax = "[{0}] PRICE(WITH TAX) IS MISSING";

        private readonly string _fileError = "[{0}] THERE IS SOME PROBLEM AT CRAWLED FILE";


        /// <summary>
        /// Public constructor.
        /// </summary>
        /// <param name="ikyu">agency type of 'ikyu'</param>
        public IkyuAnalyser(AgencyType ikyu)
        {
            _type = ikyu;
        }

        /// <summary>
        /// Analyses target files.
        /// </summary>
        /// <param name="accountId">account ID</param>
        /// <param name="hotelId">hotel ID</param>
        /// <param name="targets">target file list</param>
        /// <returns>success or not</returns>
        public override bool DoAnalysis(int accountId, int hotelId, FileInfo[] targets)
        {
            bool result = true;

            try
            {
                JToken token1 = null;
                JToken token2 = null;

                string dummy = string.Empty;

                // SET SHARED INFO.
                string[] filenameSplit = targets.First().Name.Split('_');

                _crawledDate = string.Format("20{0}-{1}-{2}", filenameSplit[5].Substring(0, 2), filenameSplit[5].Substring(2, 2), filenameSplit[5].Substring(4, 2));

                if (filenameSplit[3].Substring(0, 1).Equals("T"))
                {
                    _isInstantCrawling = false;
                }
                else
                {
                    _isInstantCrawling = true;
                }

                _crawlerId = int.Parse(filenameSplit[3].Substring(1));

                // DO ANALYSIS
                _analysisResults = new List<AnalysisResultBase>();

                foreach (var fi in targets)
                {
                    filenameSplit = fi.Name.Split('_');
                    _searchedDate = string.Format("20{0}-{1}-{2}", filenameSplit[4].Substring(0, 2), filenameSplit[4].Substring(2, 2), filenameSplit[4].Substring(4, 2));

                    _hotelId = int.Parse(filenameSplit[6]);

                    string[] contents = File.ReadAllLines(fi.FullName);

                    if (contents.Length < 2)
                    {
                        NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseErrNoData, accountId, hotelId, _type.ToString().ToLower()));
                        continue;
                    }

                    object obj;

                    try
                    {
                        obj = JsonConvert.DeserializeObject(contents[1]);
                    }
                    catch
                    {
                        continue;
                    }

                    token1 = ((JContainer)obj)["AcmAllCnt"];

                    if (token1 == null)
                    {
                        NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseErrCounter, _type.ToString().ToLower()));
                        continue;
                    }
                    else
                    {
                        dummy = token1.Value<string>().Trim();

                        int counter;
                        if (int.TryParse(dummy, out counter))
                        {
                            if (counter < 1)
                            {
                                continue;
                            }
                        }
                        else
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseError, _type.ToString().ToLower(), 1));
                            continue;
                        }
                    }

                    // PLAN
                    JToken plans = null;
                    bool errorOccurred = false;

                    token1 = ((JContainer)obj)["AcmList"];

                    if (token1 == null)
                    {
                        errorOccurred = true;
                    }
                    else
                    {
                        token2 = token1.SelectToken("[0]")?.SelectToken("PlnList");

                        if (token2 == null)
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseErrPlans, _type.ToString().ToLower()));
                            continue;
                        }
                        else
                        {
                            plans = token2;
                        }
                    }

                    if (errorOccurred || plans == null)
                    {
                        NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseErrList, _type.ToString().ToLower()));
                        continue;
                    }

                    bool longStay = false;
                    bool conditionIsNotSuitable = false;

                    for (int p = 0; p < plans.Count(); p++)
                    {
                        var planItem = plans.SelectToken(string.Format("[{0}]", p));

                        // 連泊プラン確認
                        token1 = planItem.SelectToken("LongStayLimitedFlag");

                        if (token1 != null)
                        {
                            dummy = token1.Value<string>().Trim().ToLower();
                            if (dummy.Contains("true"))
                            {
                                longStay = true;
                            }
                        }

                        // --> plan_name
                        //
                        string planName = string.Empty;

                        token1 = planItem.SelectToken("PlnNm");

                        if (token1 == null)
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseErrPlanName, _type.ToString().ToLower()));
                            continue;
                        }
                        else
                        {
                            planName = token1.Value<string>().Trim().Replace(",", string.Empty);
                        }

                        // --> plan_code
                        //
                        string planCode = string.Empty;

                        token1 = planItem.SelectToken("PlnId");

                        if (token1 == null)
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseErrPlanCode, _type.ToString().ToLower()));
                            continue;
                        }
                        else
                        {
                            planCode = token1.Value<string>().Trim();
                        }

                        // ROOM
                        JToken rooms = null;

                        token1 = planItem.SelectToken("RmList");

                        if (token1 == null)
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseErrRooms, _type.ToString().ToLower()));
                            continue;
                        }
                        else
                        {
                            rooms = token1;
                        }

                        for (int r = 0; r < rooms.Count(); r++)
                        {
                            AnalysisResultBase item = new AnalysisResultBase();
                            item.AccountId = accountId;
                            item.HotelId   = _hotelId;
                            item.PlanName  = planName;
                            item.PlanCode  = planCode;

                            var roomItem = rooms.SelectToken(string.Format("[{0}]", r));

                            // --> ota_price, tax_included_price, service_fee, tax_excluded_price, tax
                            //
                            token1 = roomItem.SelectToken("AmountNoTax");

                            if (token1 == null)
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseErrPrice, _type.ToString().ToLower()));
                                continue;
                            }
                            else
                            {
                                dummy = token1.Value<string>().Trim();

                                int price;
                                if (int.TryParse(dummy, out price))
                                {
                                    item.OtaPrice = price;
                                }
                                else
                                {
                                    NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseError, _type.ToString().ToLower(), 2));
                                    item.OtaPrice = 0;
                                }
                            }

                            token1 = roomItem.SelectToken("Amount");

                            if (token1 == null)
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseErrPriceWithTax, _type.ToString().ToLower()));
                                continue;
                            }
                            else
                            {
                                dummy = token1.Value<string>().Trim();

                                int price;
                                if (int.TryParse(dummy, out price))
                                {
                                    item.TaxIncludedPrice = price;
                                }
                                else
                                {
                                    NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseError, _type.ToString().ToLower(), 3));
                                    item.TaxIncludedPrice = 0;
                                }
                            }

                            item.ServiceFee       = 0;
                            item.TaxExcludedPrice = item.OtaPrice + item.ServiceFee;
                            item.Tax              = item.TaxIncludedPrice - item.OtaPrice;

                            // --> room_type
                            //
                            token1 = roomItem.SelectToken("RmTypeNm");

                            if (token1 == null)
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseErrRoomType, _type.ToString().ToLower()));
                                continue;
                            }
                            else
                            {
                                dummy = token1.Value<string>().Trim();
                                item.RoomType = GetRegularRoomType(dummy);
                            }

                            // --> room_name, smoking_flg
                            //
                            token1 = roomItem.SelectToken("RmNm");

                            if (token1 == null)
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseErrRoomName, _type.ToString().ToLower()));
                                continue;
                            }
                            else
                            {
                                item.RoomName = token1.Value<string>().Trim().Replace(",", string.Empty);
                            }

                            if (item.RoomName.Contains("禁煙"))
                            {
                                item.SmokingFlag = 0;
                            }
                            //else if (item.RoomName.Contains("喫煙"))
                            //{
                            //    item.SmokingFlag = 1;
                            //}
                            else
                            {
                                item.SmokingFlag = 1;//9;
                            }

                            // --> room_code
                            //
                            token1 = roomItem.SelectToken("RmId");

                            if (token1 == null)
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseErrRoomCode, _type.ToString().ToLower()));
                                continue;
                            }
                            else
                            {
                                item.RoomCode = token1.Value<string>().Trim();
                            }

                            // --> stock
                            //
                            token1 = roomItem.SelectToken("RemainingRmCnt");

                            if (token1 == null)
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseErrRoomVacancy, _type.ToString().ToLower()));
                                continue;
                            }
                            else
                            {
                                dummy = token1.Value<string>().Trim();

                                int vacancy;
                                if (int.TryParse(dummy, out vacancy))
                                {
                                    item.RoomVacancy = vacancy;
                                }
                                else
                                {
                                    NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseError, _type.ToString().ToLower(), 4));
                                    item.RoomVacancy = 0;
                                }
                            }

                            // --> meal_flg
                            //
                            bool breakfast = false;
                            bool lunch     = false;
                            bool dinner    = false;

                            token1 = roomItem.SelectToken("MealNm");

                            if (token1 == null)
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseErrMealFlag, _type.ToString().ToLower()));
                                continue;
                            }
                            else
                            {
                                dummy = token1.Value<string>().Trim();

                                switch (dummy)
                                {
                                    case "朝食付":
                                        breakfast = true;
                                        break;

                                    case "夕食付":
                                        dinner = true;
                                        break;

                                    case "夕朝食付":
                                        dinner = true;
                                        breakfast = true;
                                        break;

                                    case "朝昼食付":
                                        breakfast = true;
                                        lunch = true;
                                        break;

                                    case "昼食付":
                                        lunch = true;
                                        break;

                                    case "昼夕食付":
                                        lunch = true;
                                        dinner = true;
                                        break;

                                    case "3食付":
                                        breakfast = true;
                                        lunch = true;
                                        dinner = true;
                                        break;

                                    case "食事なし":
                                        // NOTHING TO DO
                                        break;

                                    default:
                                        NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseErrMeals, _type.ToString().ToLower()));
                                        break;
                                }
                            }

                            item.MealFlag = GetRegularMealFlag(breakfast, lunch, dinner);

                            // --> adult_cnt, stay_days
                            //
                            string[] tmpStr = contents[0].Split('_');

                            if (tmpStr.Length != 2)
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_fileError, _type.ToString().ToLower()));
                                item.Person = 0;
                                item.Days   = 0;
                            }
                            else
                            {
                                item.Person = int.Parse(tmpStr[0]);
                                item.Days   = int.Parse(tmpStr[1]);
                            }

                            // 人数と泊数確認
                            int person = 0;
                            int days   = 0;

                            token1 = roomItem.SelectToken("PeopleCnt");

                            if (token1 != null)
                            {
                                int tmp;
                                if (int.TryParse(token1.Value<string>().Trim(), out tmp))
                                {
                                    person = tmp;
                                }
                            }

                            token1 = roomItem.SelectToken("StayCnt");

                            if (token1 != null)
                            {
                                int tmp;
                                if (int.TryParse(token1.Value<string>().Trim(), out tmp))
                                {
                                    days = tmp;
                                }
                            }

                            if (item.Person < person || item.Days < days)
                            {
                                conditionIsNotSuitable = true; // 1泊、1人の場合「連泊プラン」は保存しない
                            }

                            // --> plan_url
                            //
                            string hotelCode = string.Empty;

                            token1 = roomItem.SelectToken("AcmId");

                            if (token1 == null)
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseErrPlanUrl, _type.ToString().ToLower()));
                                continue;
                            }
                            else
                            {
                                hotelCode = token1.Value<string>().Trim();

                                string urlBase = "https://www.ikyu.com/{0}/{1}/{2}/?cid={3}&cod={4}&ppc={5}&st=1&lc=1&rc=1";

                                DateTime cid = DateTime.Parse(_searchedDate);
                                DateTime cod = new DateTime(cid.AddDays(item.Days).Ticks);

                                item.PlanUrl = string.Format(urlBase, hotelCode, item.PlanCode, item.RoomCode, cid.ToString("yyyyMMdd"), cod.ToString("yyyyMMdd"), item.Person);
                            }

                            // --> timer_id or realtime_id, plan_point, search_date, crawl_date
                            //
                            item.CrawlerId    = _crawlerId;
                            item.PlanPoint    = 0;
                            item.SearchedDate = _searchedDate;
                            item.CrawledDate  = _crawledDate;

                            if (longStay && conditionIsNotSuitable)
                            {
                                // NOTHING TO DO
                            }
                            else
                            {
                                _analysisResults.Add(item);
                            }
                        }
                    }
                }

                if (_analysisResults.Count < 1)
                {
                    NetworkManager.Instance.SendLog(accountId, LogType.INFO.ToString().ToLower(), string.Format(_msgNoResult, _type.ToString().ToLower(), _crawlerId));
                }
                else
                {
                    string filename = string.Format("{4}{5}_{0}_{1}_{2}_{3}.ar", DateTime.Today.ToString("yyMMdd"),
                                                                                 _type.ToString().ToLower(),
                                                                                 accountId,
                                                                                 hotelId,
                                                                                 filenameSplit[3].Substring(0, 1),
                                                                                 _crawlerId);
                    // WRITE RESULT TO TEXT
                    using (var sw = new StreamWriter(filename, false, Encoding.UTF8))
                    {
                        foreach (var item in _analysisResults)
                        {
                            int timer_id = 0;
                            int realtime_id = 0;

                            if (_isInstantCrawling)
                            {
                                realtime_id = item.CrawlerId;
                            }
                            else
                            {
                                timer_id = item.CrawlerId;
                            }

                            string line = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22}",
                                                        item.AccountId,
                                                        item.HotelId,
                                                        item.SmokingFlag,
                                                        item.MealFlag,
                                                        item.RoomType,
                                                        item.RoomName,
                                                        item.PlanPoint,
                                                        item.PlanCode,
                                                        item.RoomCode,
                                                        item.PlanUrl,
                                                        item.PlanName,
                                                        item.RoomVacancy,
                                                        item.OtaPrice,
                                                        item.TaxExcludedPrice,
                                                        item.Tax,
                                                        item.ServiceFee,
                                                        item.TaxIncludedPrice,
                                                        item.Person,
                                                        item.Days,
                                                        item.SearchedDate,
                                                        item.CrawledDate,
                                                        timer_id,
                                                        realtime_id);
                            sw.WriteLine(line);
                        }
                    }

                    // SEND FILE TO SERVER
                    int tried;
                    if (SendAnalysisResult(new FileInfo(filename), 3, out tried))
                    {
                        NetworkManager.Instance.SendLog(accountId, LogType.INFO.ToString().ToLower(), string.Format(_msgResultUploaded, filename, tried));
                    }
                    else
                    {
                        NetworkManager.Instance.SendLog(accountId, LogType.ERROR.ToString().ToLower(), string.Format(_msgDefaultError, _type.ToString().ToLower(),
                                                                                                                                        _crawlerId,
                                                                                                                                        _msgErrorSendFile));
                        result = false;
                    }
                }
            }
            catch (Exception e)
            {
                NetworkManager.Instance.SendLog(accountId, LogType.ERROR.ToString().ToLower(), string.Format(_msgDefaultError, _type.ToString().ToLower(),
                                                                                                                               _crawlerId,
                                                                                                                               e.Message));
                result = false;
            }

            return result;
        }
    }
}