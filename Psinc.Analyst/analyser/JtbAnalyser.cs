﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Psinc.Analyst.DataStructure;
using Psinc.Util;

namespace Psinc.Analyst
{
    /// <summary>
    /// JTBデータ分析器
    /// </summary>
    public class JtbAnalyser : Analyser
    {
        private readonly string _parseError                = "[jtb] PARSE ERROR JA{1:00}";
        private readonly string _parseErrPlans             = "[jtb] NO PLAN ELEMENT";
        private readonly string _parseErrPlanUrl           = "[jtb] PLAN URL IS MISSING";
        private readonly string _parseErrPlanName          = "[jtb] PLAN NAME IS MISSING";
        private readonly string _parseErrPlanCode          = "[jtb] PLAN CODE IS MISSING";
        private readonly string _parseErrPlanPrice         = "[jtb] PLAN PRICE IS MISSING";
        private readonly string _parseErrRoomName          = "[jtb] ROOM NAME IS MISSING";
        private readonly string _parseErrRoomCode          = "[jtb] ROOM CODE IS MISSING";
        private readonly string _parseErrRoomType          = "[jtb] ROOM TYPE IS MISSING";
        private readonly string _parseErrRoomVacancy       = "[jtb] ROOM VACANCY IS MISSING";
        private readonly string _parseErrMealFlag          = "[jtb] MEAL FLAG IS MISSING";
        private readonly string _parseErrMealFlagUndefined = "[jtb] UNDEFINED MEAL FLAG {0}-{1}";
        private readonly string _parseErrSmokingFlag       = "[jtb] SMOKING FLAG IS MISSING";


        /// <summary>
        /// Analyses target files.
        /// </summary>
        /// <param name="accountId">account ID</param>
        /// <param name="hotelId">hotel ID</param>
        /// <param name="targets">target file list</param>
        /// <returns>success or not</returns>
        public override bool DoAnalysis(int accountId, int hotelId, FileInfo[] targets)
        {
            bool result = true;

            try
            {
                JToken token = null;
                string dummy = string.Empty;

                // SET SHARED INFO.
                string[] filenameSplit = targets.First().Name.Split('_');

                _crawledDate = string.Format("20{0}-{1}-{2}", filenameSplit[5].Substring(0, 2), filenameSplit[5].Substring(2, 2), filenameSplit[5].Substring(4, 2));

                if (filenameSplit[3].Substring(0, 1).Equals("T"))
                {
                    _isInstantCrawling = false;
                }
                else
                {
                    _isInstantCrawling = true;
                }

                _crawlerId = int.Parse(filenameSplit[3].Substring(1));

                // DO ANALYSIS
                _analysisResults = new List<AnalysisResultBase>();

                foreach (var fi in targets)
                {
                    filenameSplit = fi.Name.Split('_');
                    _searchedDate = string.Format("20{0}-{1}-{2}", filenameSplit[4].Substring(0, 2), filenameSplit[4].Substring(2, 2), filenameSplit[4].Substring(4, 2));
                    
                    _hotelId = int.Parse(filenameSplit[6]);
                    string _otaHotelId = filenameSplit[2];

                    string[] raw = File.ReadAllLines(fi.FullName);
                    
                    if (raw.Length < 2)
                    {
                        continue;
                    }

                    object obj;

                    try
                    {
                        obj = JsonConvert.DeserializeObject(raw[1]);
                    }
                    catch
                    {
                        continue;
                    }

                    if (obj == null)
                    {
                        continue;
                    }

                    int cnt;

                    dummy = null;
                    dummy = (((JContainer)obj)["planCount"]["planListCount"])?.Value<string>().Trim();

                    if (!string.IsNullOrEmpty(dummy))
                    {
                        if (int.TryParse(dummy, out cnt))
                        {
                            if (cnt < 1)
                            {
                                continue;
                            }
                        }
                        else
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseError, 1));
                            continue;
                        }
                    }
                    
                    string PlanUrlBase = "https://www.jtb.co.jp/kokunai-hotel/htl/{0}/plan/{1}/?godate={2}&checkindate={3}&staynight={4}&backdate={5}&roomassign=m{6}&room=1&mealtype=&kodawariyado=jtbplan&traveldays=2";

                    // --> adult_cnt, stay_days
                    //
                    int person = 0;
                    int days   = 0;

                    string godate = string.Empty;
                    string checkindate = string.Empty;
                    string backdate = string.Empty;

                    dummy = raw[0];

                    if (!string.IsNullOrEmpty(dummy))
                    {
                        var test1 = dummy.Split('&');
                        var test2 = test1.Where(e => e.Contains("roomassign"));

                        if (test2.Count() > 0)
                        {
                            var test3 = test2.First().Split('=');

                            if (test3.Length == 2)
                            {
                                dummy = Regex.Match(test3[1], @"\d+").Value;

                                if (!int.TryParse(dummy, out person))
                                {
                                    person = 0;
                                }
                            }
                        }

                        test2 = test1.Where(e => e.Contains("staynight"));

                        if (test2.Count() > 0)
                        {
                            var test3 = test2.First().Split('=');

                            if (test3.Length == 2)
                            {
                                days = int.Parse(test3[1]);
                            }
                        }

                        test2 = test1.Where(e => e.Contains("godate"));

                        if (test2.Count() > 0)
                        {
                            var test3 = test2.First().Split('=');

                            if (test3.Length == 2)
                            {
                                godate = test3[1];
                            }
                        }

                        test2 = test1.Where(e => e.Contains("checkindate"));

                        if (test2.Count() > 0)
                        {
                            var test3 = test2.First().Split('=');

                            if (test3.Length == 2)
                            {
                                checkindate = test3[1];
                            }
                        }

                        test2 = test1.Where(e => e.Contains("backdate"));

                        if (test2.Count() > 0)
                        {
                            var test3 = test2.First().Split('=');

                            if (test3.Length == 2)
                            {
                                backdate = test3[1];
                            }
                        }
                    }

                    // PLAN
                    JToken plans = null;
                    
                    token = ((JContainer)obj)["plans"]["planListGroup"][0]["planList"];

                    if (token == null)
                    {
                        NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrPlans);
                        continue;
                    }
                    else
                    {
                        plans = token;
                    }

                    for (int p = 0; p < plans.Count(); p++)
                    {
                        AnalysisResultBase item = new AnalysisResultBase();
                        item.AccountId = accountId;
                        item.HotelId   = _hotelId;
                        item.Person    = person;
                        item.Days      = days;

                        var planItem = plans.SelectToken(string.Format("[{0}]", p));

                        // --> plan_name
                        //
                        token = planItem.SelectToken("displayPlanName");

                        if (token == null)
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrPlanName);
                            item.PlanName = string.Empty;
                        }
                        else
                        {
                            item.PlanName = token.Value<string>().Trim();
                        }

                        // --> room_name
                        //
                        token = planItem.SelectToken("roomTypeAlias");
                        if (token == null)
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrRoomName);
                            item.RoomName = string.Empty;
                        }
                        else
                        {
                            item.RoomName = token.Value<string>().Trim();
                        }

                        // --> plan_code
                        //
                        token = planItem.SelectToken("planId");

                        if (token == null)
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrPlanCode);
                            item.PlanCode = string.Empty;
                        }
                        else
                        {
                            item.PlanCode = token.Value<string>().Trim();
                        }

                        // --> room_code
                        //
                        token = planItem.SelectToken("roomTypeCode");
                        if (token == null)
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrRoomCode);
                            item.RoomCode = string.Empty;
                        }
                        else
                        {
                            item.RoomCode = token.Value<string>().Trim();
                        }

                        // --> plan_url
                        item.PlanUrl = string.Format(PlanUrlBase, _otaHotelId, item.PlanCode, godate, checkindate, item.Days, backdate, item.Person);

                        /*token = planItem.SelectToken("planDetailUrl");

                        if (token == null)
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrPlanUrl);
                            item.PlanUrl = string.Empty;
                        }
                        else
                        {
                            dummy = token.Value<string>().Trim();
                            dummy = dummy.Replace("&amp;","&");
                            item.PlanUrl = string.Format("https://www.jtb.co.jp{0}", dummy);
                        }*/

                        // --> ota_price, tax_included_price, tax_excluded_price, tax, service_fee
                        //
                        token = planItem.SelectToken("totalPriceInfo");
                        token = token.SelectToken("minTotalPrice");

                        if (token == null)
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrPlanPrice);
                        }
                        else
                        {
                            dummy = token.Value<string>().Trim();

                            int test;
                            if (int.TryParse(dummy, out test))
                            {
                                item.OtaPrice         = test;
                                item.TaxIncludedPrice = item.OtaPrice;
                            }
                            else
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseError, 2));
                                item.OtaPrice         = 0;
                                item.TaxIncludedPrice = 0;
                            }
                        }
                        
                        double tmpValue = item.OtaPrice / (1 + GetTaxRate(DateTime.Parse(_searchedDate)) / 100d);

                        item.TaxExcludedPrice = (int)Math.Round(tmpValue, 0, MidpointRounding.AwayFromZero);
                        item.Tax              = item.OtaPrice - item.TaxExcludedPrice;
                        item.ServiceFee       = 0;

                        // --> room_type
                        //
                        token = planItem.SelectToken("planFacilityIcon");
                        token = token.SelectToken("roomStyleInfo");
                        token = token.SelectToken("name");

                        if (token == null)
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrRoomType);
                            dummy = string.Empty;
                        }
                        else
                        {
                            dummy = token.Value<string>().Trim();
                        }

                        item.RoomType = GetRegularRoomType(dummy);

                        // --> stock
                        //
                        token = planItem.SelectToken("stock");

                        if (token == null)
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrRoomVacancy);
                            item.RoomVacancy = 0;
                        }
                        else
                        {
                            dummy = token.Value<string>();

                            if (dummy == null)
                            {
                                item.RoomVacancy = 0;
                            }
                            else
                            {
                                int test;
                                if (int.TryParse(dummy.Trim(), out test))
                                {
                                    item.RoomVacancy = test;
                                }
                                else
                                {
                                    NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseError, 3));
                                    item.RoomVacancy = 0;
                                }
                            }
                        }

                        // --> meal_flg
                        //
                        bool breakfast = false;
                        bool lunch     = false;
                        bool dinner    = false;

                        token = planItem.SelectToken("planFacilityIcon");
                        token = token.SelectToken("mealTypeInfo");
                        token = token.SelectToken("code");

                        if (token == null)
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrMealFlag);
                        }
                        else
                        {
                            dummy = token.Value<string>().Trim();

                            if (dummy.Equals("1")) // 夕食/朝食付き
                            {
                                breakfast = true;
                                dinner = true;
                            }
                            else if (dummy.Equals("2")) // 朝食のみ
                            {
                                breakfast = true;
                            }
                            else if (dummy.Equals("4")) // 食事なし
                            {
                                // NOTHING TO DO
                            }
                            else if (dummy.Equals("3")) // 夕食のみ
                            {
                                dinner = true;
                            }
                            else
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseErrMealFlagUndefined, accountId, hotelId));
                            }
                        }

                        item.MealFlag = GetRegularMealFlag(breakfast, lunch, dinner);

                        // --> smoking_flg
                        //
                        token = planItem.SelectToken("planFacilityIcon");
                        token = planItem.SelectToken("isNoSmoking");
                        if (token == null)
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrSmokingFlag);
                            item.SmokingFlag = 9;
                        }
                        else
                        {
                            if (token.Value<string>().Trim().ToLower().Equals("true"))
                            {
                                item.SmokingFlag = 0; // 禁煙
                            }
                            else
                            {
                                item.SmokingFlag = 1; // 喫煙
                            }
                        }

                        // --> timer_id or realtime_id, plan_point, search_date, crawl_date
                        //
                        item.CrawlerId    = _crawlerId;
                        item.PlanPoint    = 0;
                        item.SearchedDate = _searchedDate;
                        item.CrawledDate  = _crawledDate;

                        _analysisResults.Add(item);
                    }
                }

                if (_analysisResults.Count < 1)
                {
                    NetworkManager.Instance.SendLog(accountId, LogType.INFO.ToString().ToLower(), string.Format(_msgNoResult, AgencyType.JTB.ToString().ToLower(), _crawlerId));
                }
                else
                {
                    string filename = string.Format("{4}{5}_{0}_{1}_{2}_{3}.ar", DateTime.Today.ToString("yyMMdd"),
                                                                                 AgencyType.JTB.ToString().ToLower(),
                                                                                 accountId,
                                                                                 hotelId,
                                                                                 filenameSplit[3].Substring(0, 1),
                                                                                 _crawlerId);
                    // WRITE RESULT TO TEXT
                    using (var sw = new StreamWriter(filename, false, Encoding.UTF8))
                    {
                        foreach (var item in _analysisResults)
                        {
                            int timer_id = 0;
                            int realtime_id = 0;

                            if (_isInstantCrawling)
                            {
                                realtime_id = item.CrawlerId;
                            }
                            else
                            {
                                timer_id = item.CrawlerId;
                            }

                            string line = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22}",
                                                        item.AccountId,
                                                        item.HotelId,
                                                        item.SmokingFlag,
                                                        item.MealFlag,
                                                        item.RoomType,
                                                        item.RoomName,
                                                        item.PlanPoint,
                                                        item.PlanCode,
                                                        item.RoomCode,
                                                        item.PlanUrl,
                                                        item.PlanName,
                                                        item.RoomVacancy,
                                                        item.OtaPrice,
                                                        item.TaxExcludedPrice,
                                                        item.Tax,
                                                        item.ServiceFee,
                                                        item.TaxIncludedPrice,
                                                        item.Person,
                                                        item.Days,
                                                        item.SearchedDate,
                                                        item.CrawledDate,
                                                        timer_id,
                                                        realtime_id);
                            sw.WriteLine(line);
                        }
                    }

                    // SEND FILE TO SERVER
                    int tried;
                    if (SendAnalysisResult(new FileInfo(filename), 3, out tried))
                    {
                        NetworkManager.Instance.SendLog(accountId, LogType.INFO.ToString().ToLower(), string.Format(_msgResultUploaded, filename, tried));
                    }
                    else
                    {
                        NetworkManager.Instance.SendLog(accountId, LogType.ERROR.ToString().ToLower(), string.Format(_msgDefaultError, AgencyType.JTB.ToString().ToLower(),
                                                                                                                                        _crawlerId,
                                                                                                                                        _msgErrorSendFile));
                        result = false;
                    }
                }
            }
            catch (Exception e)
            {
                NetworkManager.Instance.SendLog(accountId, LogType.ERROR.ToString().ToLower(), string.Format(_msgDefaultError, AgencyType.JTB.ToString().ToLower(),
                                                                                                                               _crawlerId,
                                                                                                                               e.Message));
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Gets a count of plans in specified 'jtb' data.
        /// </summary>
        /// <param name="filename">crawled file</param>
        /// <param name="count">count of plans</param>
        /// <returns>success or not</returns>
        public static bool GetPlansCount(string filename, out int count)
        {
            count = 0;

            bool result = false;
            
            try
            {
                string raw = File.ReadAllText(filename);

                if (!string.IsNullOrEmpty(raw))
                {
                    var obj = JsonConvert.DeserializeObject(raw);
                    int cnt;
                    
                    JToken token = ((JContainer)obj)["planCount"]["planListCount"];
                    
                    if (token != null)
                    {
                        string dummy = token.Value<string>().Trim();

                        if (int.TryParse(dummy, out cnt))
                        {
                            count = cnt;
                            result = true;
                        }
                    }
                }
            }
            catch
            {
                // NOTHING TO DO
            }

            return result;
        }
    }
}