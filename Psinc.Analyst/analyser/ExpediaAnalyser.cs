﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Psinc.Analyst.DataStructure;
using Psinc.Util;

namespace Psinc.Analyst
{
    /// <summary>
    /// Expedia.co.jpデータ分析器 (データはbiglobeから)
    /// </summary>
    public class ExpediaAnalyser : Analyser
    {
        private readonly string _parseErrNoData        = "[expedia] NO DATA IN CRAWLED FILE ({0}/{1})";
        private readonly string _parseErrJson          = "[expedia] JSON STRUCTURE MAYBE CHANGED";
        private readonly string _parseErrPlanUrl       = "[expedia] PLAN URL ELEMENT IS MISSING";
        private readonly string _parseErrPlanName      = "[expedia] PLAN NAME IS MISSING";
        private readonly string _parseErrPlanCode      = "[expedia] PLAN CODE IS MISSING";
        private readonly string _parseErrPlanPrice     = "[expedia] PLAN PRICE IS MISSING";
        private readonly string _parseErrRoomVacancy   = "[expedia] ROOM VACANCY IS MISSING";
        private readonly string _parseErrMealFlag      = "[expedia] MEAL FLAG IS MISSING";
        private readonly string _parseErrMealFlagValue = "[expedia] MEAL FLAG IS NOT INTEGER ({0})";

        private readonly string _fileError = "[expedia] THERE IS SOME PROBLEM AT CRAWLED FILE";

        private readonly string _urlBase = @"https://www.expedia.co.jp/#hotelname#.h#hotelcd#.Hotel-Information?chkin=#startdate#&chkout=#enddate#&rm1=a#adultcnt#";


        /// <summary>
        /// Analyses target files.
        /// </summary>
        /// <param name="accountId">account ID</param>
        /// <param name="hotelId">hotel ID</param>
        /// <param name="targets">target file list</param>
        /// <returns>success or not</returns>
        public override bool DoAnalysis(int accountId, int hotelId, FileInfo[] targets)
        {
            bool result = true;

            try
            {
                // SET SHARED INFO.
                string[] filenameSplit = targets.First().Name.Split('_');

                _crawledDate = string.Format("20{0}-{1}-{2}", filenameSplit[5].Substring(0, 2), filenameSplit[5].Substring(2, 2), filenameSplit[5].Substring(4, 2));

                if (filenameSplit[3].Substring(0, 1).Equals("T"))
                {
                    _isInstantCrawling = false;
                }
                else
                {
                    _isInstantCrawling = true;
                }

                _crawlerId = int.Parse(filenameSplit[3].Substring(1));

                // DO ANALYSIS
                _analysisResults = new List<AnalysisResultBase>();

                foreach (var fi in targets)
                {
                    filenameSplit = fi.Name.Split('_');
                    _searchedDate = string.Format("20{0}-{1}-{2}", filenameSplit[4].Substring(0, 2), filenameSplit[4].Substring(2, 2), filenameSplit[4].Substring(4, 2));

                    _hotelId = int.Parse(filenameSplit[6]);

                    string[] contents = File.ReadAllLines(fi.FullName);

                    if (contents.Length < 2)
                    {
                        NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseErrNoData, accountId, hotelId));
                        continue;
                    }

                    contents[1] = contents[1].Replace("callback(0,[[", "[[");
                    contents[1] = contents[1].Replace("]]);", "]]");

                    if (!IsCorrectJson(contents[1]))
                    {
                        NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _fileError);
                        continue;
                    }

                    var json = (JContainer)JsonConvert.DeserializeObject(contents[1]);

                    if (json.Count != 20)
                    {
                        NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrJson);
                        continue;
                    }

                    string dummy = string.Empty;
                    string[] array = null;

                    JToken counter = json[1];

                    for (int i = 0; i < counter.Count(); i++)
                    {
                        JToken planItem      = json[5].SelectToken(string.Format("[{0}]", i));
                        JToken expCodeItem   = json[8].SelectToken(string.Format("[{0}]", i));
                        JToken planCodeItem  = json[9].SelectToken(string.Format("[{0}]", i));
                        JToken breakfastItem = json[10].SelectToken(string.Format("[{0}]", i));
                        JToken dinnerItem    = json[11].SelectToken(string.Format("[{0}]", i));
                        JToken priceItem     = json[17].SelectToken(string.Format("[{0}]", i));
                        JToken vacancyItem   = json[18].SelectToken(string.Format("[{0}]", i));

                        AnalysisResultBase item = new AnalysisResultBase();
                        item.AccountId = accountId;
                        item.HotelId   = _hotelId;

                        if (planItem == null)
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrPlanName);
                            continue;
                        }
                        if (expCodeItem == null)
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrPlanUrl);
                            continue;
                        }
                        if (planCodeItem == null)
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrPlanCode);
                            continue;
                        }
                        if (breakfastItem == null || dinnerItem == null)
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrMealFlag);
                            continue;
                        }
                        if (priceItem == null)
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrPlanPrice);
                            continue;
                        }
                        if (vacancyItem == null)
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrRoomVacancy);
                            continue;
                        }

                        // --> plan_name, room_name, room_type, smoking_flg
                        //
                        item.PlanName = planItem.Value<string>();
                        item.PlanName = item.PlanName.Replace(",", "、");
                        item.RoomName = item.PlanName;

                        if (item.PlanName.Contains("(") || item.PlanName.Contains("（"))
                        {
                            dummy = item.PlanName.Split(new char[] { '(', '（' }, StringSplitOptions.RemoveEmptyEntries)[0];
                        }
                        else
                        {
                            dummy = item.PlanName;
                        }

                        item.RoomType = GetRegularRoomType(dummy);

                        //if (item.PlanName.Contains("禁煙") && item.PlanName.Contains("喫煙")) // e.g. 喫煙または禁煙
                        //{
                        //    item.SmokingFlag = 9;
                        //}
                        //else
                        //{
                            if (item.PlanName.Contains("禁煙"))
                            {
                                item.SmokingFlag = 0;
                            }
                            //else if (item.PlanName.Contains("喫煙"))
                            //{
                            //    item.SmokingFlag = 1;
                            //}
                            else
                            {
                            item.SmokingFlag = 1;//9;
                            }
                        //}

                        // --> plan_code, room_code
                        //
                        item.PlanCode = planCodeItem.Value<string>().Replace("|", "_");
                        item.RoomCode = item.PlanCode;

                        // --> meal_flg
                        //
                        bool breakfast = false;
                        bool lunch     = false;
                        bool dinner    = false;

                        int tmp0;

                        if (int.TryParse(breakfastItem.Value<string>(), out tmp0))
                        {
                            if (tmp0 == 1)
                            {
                                breakfast = true;
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(breakfastItem.Value<string>()))
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseErrMealFlagValue, "breakfast"));
                                continue;
                            }
                        }

                        if (int.TryParse(dinnerItem.Value<string>(), out tmp0))
                        {
                            if (tmp0 == 1)
                            {
                                dinner = true;
                            }
                        }
                        else
                        {
                            // FOUND: 190417 -> CASE: string is empty
                            if (!string.IsNullOrEmpty(dinnerItem.Value<string>()))
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseErrMealFlagValue, "dinner"));
                                continue;
                            }
                        }

                        item.MealFlag = GetRegularMealFlag(breakfast, lunch, dinner);

                        DateTime dt1 = new DateTime(int.Parse(string.Format("20{0}", filenameSplit[5].Substring(0, 2))), int.Parse(filenameSplit[5].Substring(2, 2)), int.Parse(filenameSplit[5].Substring(4, 2)));
                        DateTime dt2 = new DateTime(int.Parse(string.Format("20{0}", filenameSplit[4].Substring(0, 2))), int.Parse(filenameSplit[4].Substring(2, 2)), int.Parse(filenameSplit[4].Substring(4, 2)));
                        int diff = dt2.Subtract(dt1).Days;

                        // --> ota_price, tax_excluded_price, service_fee, tax, tax_included_price, plan_url, adult_cnt, stay_days
                        //
                        dummy = priceItem.Value<string>();
                        array = dummy.Split(',');

                        item.OtaPrice = int.Parse(array[diff]);

                        string[] tmp = contents[0].Split('_');

                        if (tmp.Length != 3)
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _fileError);
                            item.Person = 0;
                            item.Days   = 0;
                        }
                        else
                        {
                            item.OtaPrice *= int.Parse(tmp[0]);
                            item.Person    = int.Parse(tmp[0]);
                            item.Days      = int.Parse(tmp[1]);
                        }

                        item.TaxExcludedPrice = item.OtaPrice;
                        item.ServiceFee       = 0;

                        double tmpValue = item.OtaPrice * (GetTaxRate(DateTime.Parse(_searchedDate)) / 100d);

                        item.Tax              = (int)Math.Round(tmpValue, 0, MidpointRounding.AwayFromZero);
                        item.TaxIncludedPrice = item.TaxExcludedPrice + item.Tax;

                        if (string.IsNullOrEmpty(tmp[2]))
                        {
                            item.PlanUrl = string.Empty;
                        }
                        else
                        {
                            string[] stayStartDate = dt2.ToString("yyyy-MM-dd").Split('-');
                            string[] stayEndDate = dt2.AddDays(int.Parse(tmp[1])).ToString("yyyy-MM-dd").Split('-');

                            dummy = _urlBase;
                            dummy = dummy.Replace("#hotelname#", tmp[2]);
                            dummy = dummy.Replace("#hotelcd#", expCodeItem.Value<string>());
                            dummy = dummy.Replace("#startdate#", string.Format("{0}/{1}/{2}", stayStartDate[0], stayStartDate[1], stayStartDate[2]));
                            dummy = dummy.Replace("#enddate#", string.Format("{0}/{1}/{2}", stayEndDate[0], stayEndDate[1], stayEndDate[2]));
                            dummy = dummy.Replace("#adultcnt#", tmp[0]);

                            item.PlanUrl = dummy;
                        }

                        // --> stock
                        //
                        dummy = vacancyItem.Value<string>();
                        array = dummy.Split(',');

                        item.RoomVacancy = int.Parse(array[diff]);

                        if (item.RoomVacancy == 0 || item.RoomVacancy == -9)
                        {
                            continue;
                        }

                        // --> timer_id or realtime_id, plan_point, search_date, crawl_date
                        //
                        item.CrawlerId    = _crawlerId;
                        item.PlanPoint    = 0;
                        item.SearchedDate = _searchedDate;
                        item.CrawledDate  = _crawledDate;

                        _analysisResults.Add(item);
                    }
                }

                if (_analysisResults.Count < 1)
                {
                    NetworkManager.Instance.SendLog(accountId, LogType.INFO.ToString().ToLower(), string.Format(_msgNoResult, AgencyType.EXPEDIA.ToString().ToLower(), _crawlerId));
                }
                else
                {
                    string filename = string.Format("{4}{5}_{0}_{1}_{2}_{3}.ar", DateTime.Today.ToString("yyMMdd"),
                                                                                 AgencyType.EXPEDIA.ToString().ToLower(),
                                                                                 accountId,
                                                                                 hotelId,
                                                                                 filenameSplit[3].Substring(0, 1),
                                                                                 _crawlerId);
                    // WRITE RESULT TO TEXT
                    using (var sw = new StreamWriter(filename, false, Encoding.UTF8))
                    {
                        foreach (var item in _analysisResults)
                        {
                            int timer_id = 0;
                            int realtime_id = 0;

                            if (_isInstantCrawling)
                            {
                                realtime_id = item.CrawlerId;
                            }
                            else
                            {
                                timer_id = item.CrawlerId;
                            }

                            string line = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22}",
                                                        item.AccountId,
                                                        item.HotelId,
                                                        item.SmokingFlag,
                                                        item.MealFlag,
                                                        item.RoomType,
                                                        item.RoomName,
                                                        item.PlanPoint,
                                                        item.PlanCode,
                                                        item.RoomCode,
                                                        item.PlanUrl,
                                                        item.PlanName,
                                                        item.RoomVacancy,
                                                        item.OtaPrice,
                                                        item.TaxExcludedPrice,
                                                        item.Tax,
                                                        item.ServiceFee,
                                                        item.TaxIncludedPrice,
                                                        item.Person,
                                                        item.Days,
                                                        item.SearchedDate,
                                                        item.CrawledDate,
                                                        timer_id,
                                                        realtime_id);
                            sw.WriteLine(line);
                        }
                    }

                    // SEND FILE TO SERVER
                    int tried;
                    if (SendAnalysisResult(new FileInfo(filename), 3, out tried))
                    {
                        NetworkManager.Instance.SendLog(accountId, LogType.INFO.ToString().ToLower(), string.Format(_msgResultUploaded, filename, tried));
                    }
                    else
                    {
                        NetworkManager.Instance.SendLog(accountId, LogType.ERROR.ToString().ToLower(), string.Format(_msgDefaultError, AgencyType.EXPEDIA.ToString().ToLower(),
                                                                                                                                        _crawlerId,
                                                                                                                                        _msgErrorSendFile));
                        result = false;
                    }
                }
            }
            catch (Exception e)
            {
                NetworkManager.Instance.SendLog(accountId, LogType.ERROR.ToString().ToLower(), string.Format(_msgDefaultError, AgencyType.EXPEDIA.ToString().ToLower(),
                                                                                                                               _crawlerId,
                                                                                                                               e.Message));
                result = false;
            }

            return result;
        }

        private bool IsCorrectJson(string json)
        {
            // Checks a passed string is correct JSON or not.

            bool result = false;

            json = json.Trim();

            if ((json.StartsWith("{") && json.EndsWith("}")) || (json.StartsWith("[") && json.EndsWith("]")))
            {
                try
                {
                    var garbage = JToken.Parse(json);

                    result = true;
                }
                catch
                {
                    result = false;
                }
            }

            return result;
        }
    }
}