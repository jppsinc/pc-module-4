﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Psinc.Analyst.DataStructure;
using Psinc.Util;
using System.Text.RegularExpressions;

namespace Psinc.Analyst
{
    /// <summary>
    /// rurubuデータ分析器
    /// </summary>
    public class RurubuAnalyser : Analyser
    {
        private readonly string _parseErrPlans = "[rurubu] NO PLAN ELEMENT";
        private readonly string _parseErrRooms = "[rurubu] NO ROOM ELEMENT";
        private readonly string _parseErrPlanUrl = "[rurubu] PLAN URL IS MISSING";
        private readonly string _parseErrPlanName = "[rurubu] PLAN NAME IS MISSING";
        private readonly string _parseErrPlanCode = "[rurubu] PLAN CODE IS MISSING";
        private readonly string _parseErrRoomName = "[rurubu] ROOM NAME IS MISSING";
        private readonly string _parseErrRoomCode = "[rurubu] ROOM CODE IS MISSING";
        private readonly string _parseErrRoomVacancy = "[rurubu] ROOM VACANCY IS MISSING OR NOT AN INTEGER ({0})";
        private readonly string _parseErrMealFlag = "[rurubu] 'features' TOKEN IS MISSING";
        private readonly string _parseErrPrice = "[rurubu] PRICE HAS SOME PROBLEM ({0})";
        private readonly string _parseErrOtaPrice = "[rurubu] PRICE IS MISSING";
        private readonly string _parseErrExclusivePrice = "[rurubu] PRICE (EXCL. TAX) IS MISSING";

        private readonly string _fileError = "[rurubu] THERE IS SOME PROBLEM AT CRAWLED FILE";

        /// <summary>
        /// Analyses target files.
        /// </summary>
        /// <param name="accountId">account ID</param>
        /// <param name="hotelId">hotel ID</param>
        /// <param name="targets">target file list</param>
        /// <returns>success or not</returns>
        public override bool DoAnalysis(int accountId, int hotelId, FileInfo[] targets)
        {
            bool result = true;

            try
            {
                JToken test = null;

                // SET SHARED INFO.
                string[] filenameSplit = targets.First().Name.Split('_');

                _crawledDate = string.Format("20{0}-{1}-{2}", filenameSplit[5].Substring(0, 2), filenameSplit[5].Substring(2, 2), filenameSplit[5].Substring(4, 2));

                if (filenameSplit[3].Substring(0, 1).Equals("T"))
                {
                    _isInstantCrawling = false;
                }
                else
                {
                    _isInstantCrawling = true;
                }

                _crawlerId = int.Parse(filenameSplit[3].Substring(1));

                // DO ANALYSIS
                _analysisResults = new List<AnalysisResultBase>();

                foreach (var fi in targets)
                {
                    filenameSplit = fi.Name.Split('_');
                    _searchedDate = string.Format("20{0}-{1}-{2}", filenameSplit[4].Substring(0, 2), filenameSplit[4].Substring(2, 2), filenameSplit[4].Substring(4, 2));

                    _hotelId = int.Parse(filenameSplit[6]);

                    string[] contents = File.ReadAllLines(fi.FullName);

                    if (contents.Length < 2)
                    {
                        continue;
                    }

                    object tmp;

                    try
                    {
                        tmp = JsonConvert.DeserializeObject(contents[1]);
                    }
                    catch
                    {
                        continue;
                    }

                    if (tmp == null)
                    {
                        continue;
                    }

                    // --> adult_cnt, stay_days
                    //
                    int person = 0;
                    int days = 0;
                    //string planUrlBase = string.Empty;

                    var tmpStr = contents[0].Split('_');

                    if (tmpStr.Length != 3)
                    {
                        NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _fileError);
                    }
                    else
                    {
                        person = int.Parse(tmpStr[0]);
                        days = int.Parse(tmpStr[1]);
                        //planUrlBase = tmpStr[2];
                    }

                    test = ((JContainer)tmp)["currencyInfo"];

                    if (test != null)
                    {
                        string currency = test.SelectToken("currencyCode")?.Value<string>().ToUpper();

                        if (!string.IsNullOrEmpty(currency))
                        {
                            if (!currency.Equals("JPY"))
                            {
                                continue;
                            }
                        }
                    }

                    JToken plans;

                    test = ((JContainer)tmp)["masterRooms"];

                    if (test == null)
                    {
                        NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrPlans);
                        continue;
                    }
                    else
                    {
                        plans = test;
                    }

                    for (int p = 0; p < plans.Count(); p++)
                    {
                        var planItem = plans.SelectToken(string.Format("[{0}]", p));

                        JToken rooms;

                        test = planItem.SelectToken("rooms");

                        if (test == null)
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrRooms);
                            continue;
                        }
                        else
                        {
                            rooms = test;
                        }

                        for (int r = 0; r < rooms.Count(); r++)
                        {
                            // --> account_id, hotel_id, timer_id or realtime_id, plan_point, search_date, crawl_date
                            //
                            AnalysisResultBase item = new AnalysisResultBase();
                            item.AccountId = accountId;
                            item.HotelId = _hotelId;
                            item.CrawlerId = _crawlerId;
                            item.PlanPoint = 0;
                            item.SearchedDate = _searchedDate;
                            item.CrawledDate = _crawledDate;

                            item.Person = person;
                            item.Days = days;

                            var roomItem = rooms.SelectToken(string.Format("[{0}]", r));

                            // --> plan_url
                            //
                            test = roomItem.SelectToken("bookingForm")?.SelectToken("arguments");

                            if (test == null)
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrPlanUrl);
                                item.PlanUrl = string.Empty;
                            }
                            else
                            {
                                item.PlanUrl = test.Value<string>().Trim();
                            }

                            // --> plan_code
                            //
                            test = roomItem.SelectToken("ratePlanId");

                            if (test == null)
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrPlanCode);
                                item.PlanCode = string.Empty;
                            }
                            else
                            {
                                item.PlanCode = test.Value<string>().Trim();
                            }

                            // --> room_code
                            //
                            test = roomItem.SelectToken("id");

                            if (test == null)
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrRoomCode);
                                item.RoomCode = string.Empty;
                            }
                            else
                            {
                                item.RoomCode = test.Value<string>().Trim();
                            }

                            // --> stock
                            //
                            test = roomItem.SelectToken("availability");

                            if (test == null)
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseErrRoomVacancy, "0"));
                                item.RoomVacancy = 0;
                            }
                            else
                            {
                                int dummy;
                                if (int.TryParse(test.Value<string>().Trim(), out dummy))
                                {
                                    item.RoomVacancy = dummy;
                                }
                                else
                                {
                                    NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseErrRoomVacancy, "1"));
                                    item.RoomVacancy = 0;
                                }
                            }

                            // --> plan_name
                            //
                            test = roomItem.SelectToken("ratePlanName");

                            if (test == null)
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrPlanName);
                                item.PlanName = string.Empty;
                            }
                            else
                            {
                                item.PlanName = test.Value<string>().Trim();

                                if (item.PlanName.Contains(","))
                                {
                                    item.PlanName = item.PlanName.Replace(",", string.Empty);
                                }

                                item.PlanName = RemoveNewLine(item.PlanName);
                            }

                            // --> room_name, room_type, smoking_flg
                            //
                            test = roomItem.SelectToken("name");

                            if (test == null)
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrRoomName);
                                item.RoomName = string.Empty;
                            }
                            else
                            {
                                item.RoomName = test.Value<string>().Trim();

                                if (item.RoomName.Contains(","))
                                {
                                    item.RoomName = item.RoomName.Replace(",", string.Empty);
                                }

                                item.RoomName = RemoveNewLine(item.RoomName);
                            }

                            item.RoomType = GetRegularRoomType(item.RoomName);

                            if (item.RoomName.Contains("禁煙"))
                            {
                                item.SmokingFlag = 0;
                            }
                            //else if (item.RoomName.Contains("喫煙"))
                            //{
                            //    item.SmokingFlag = 1;
                            //}
                            else
                            {
                                item.SmokingFlag = 1;//9;
                            }

                            // --> meal_flg
                            //
                            bool breakfast = false;
                            bool lunch = false;
                            bool dinner = false;

                            test = roomItem.SelectToken("features");

                            if (test == null)
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrMealFlag);
                            }
                            else
                            {
                                for (int f = 0; f < test.Count(); f++)
                                {
                                    var featureItem = test.SelectToken(string.Format("[{0}]", f));

                                    if (featureItem.SelectToken("type").Value<string>().Equals("0"))
                                    {
                                        JToken meals = featureItem.SelectToken("benefits");

                                        if (meals != null)
                                        {
                                            for (int m = 0; m < meals.Count(); m++)
                                            {
                                                var mealItem = meals.SelectToken(string.Format("[{0}]", m));

                                                if (mealItem.Parent.ToString().Contains("朝食"))
                                                {
                                                    breakfast = true;
                                                }
                                                else if (mealItem.Parent.ToString().Contains("昼食"))
                                                {
                                                    lunch = true;
                                                }
                                                else if (mealItem.Parent.ToString().Contains("夕食"))
                                                {
                                                    dinner = true;
                                                }
                                            }
                                        }

                                        break;
                                    }
                                }
                            }

                            item.MealFlag = GetRegularMealFlag(breakfast, lunch, dinner);

                            // --> ota_price, tax, service_fee, tax_included_price, tax_excluded_price
                            //
                            test = null;
                            test = roomItem.SelectToken("pricing")?.SelectToken("displayPrice");

                            if (test == null)
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrOtaPrice);
                                item.OtaPrice = 0;
                            }
                            else
                            {
                                int price;
                                if (int.TryParse(test.Value<string>(), out price))
                                {
                                    item.OtaPrice = price;
                                }
                                else
                                {
                                    NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseErrPrice, "PARSE-ERR-E"));
                                }
                            }

                            test = null;
                            test = roomItem.SelectToken("exclusivePrice")?.SelectToken("display");
                            int exclusivePrice = 0;

                            if (test == null)
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrExclusivePrice);
                            }
                            else
                            {
                                int price;
                                if (int.TryParse(test.Value<string>(), out price))
                                {
                                    exclusivePrice = price;
                                }
                                else
                                {
                                    NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseErrPrice, "PARSE-ERR-E"));
                                }
                            }

                            double tmpValue = exclusivePrice * (GetTaxRate(DateTime.Parse(_searchedDate)) / 100d);

                            item.Tax = (int)Math.Round(tmpValue, 0, MidpointRounding.AwayFromZero);
                            item.ServiceFee = 0;
                            item.TaxIncludedPrice = exclusivePrice + item.Tax;
                            item.TaxExcludedPrice = exclusivePrice;

                            _analysisResults.Add(item);
                        }
                    }
                }

                if (_analysisResults.Count < 1)
                {
                    NetworkManager.Instance.SendLog(accountId, LogType.INFO.ToString().ToLower(), string.Format(_msgNoResult, AgencyType.RURUBU.ToString().ToLower(), _crawlerId));
                }
                else
                {
                    string filename = string.Format("{4}{5}_{0}_{1}_{2}_{3}.ar", DateTime.Today.ToString("yyMMdd"),
                                                                                 AgencyType.RURUBU.ToString().ToLower(),
                                                                                 accountId,
                                                                                 hotelId,
                                                                                 filenameSplit[3].Substring(0, 1),
                                                                                 _crawlerId);
                    // WRITE RESULT TO TEXT
                    using (var sw = new StreamWriter(filename, false, Encoding.UTF8))
                    {
                        foreach (var item in _analysisResults)
                        {
                            int timer_id = 0;
                            int realtime_id = 0;

                            if (_isInstantCrawling)
                            {
                                realtime_id = item.CrawlerId;
                            }
                            else
                            {
                                timer_id = item.CrawlerId;
                            }

                            string line = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22}",
                                                        item.AccountId,
                                                        item.HotelId,
                                                        item.SmokingFlag,
                                                        item.MealFlag,
                                                        item.RoomType,
                                                        item.RoomName,
                                                        item.PlanPoint,
                                                        item.PlanCode,
                                                        item.RoomCode,
                                                        item.PlanUrl,
                                                        item.PlanName,
                                                        item.RoomVacancy,
                                                        item.OtaPrice,
                                                        item.TaxExcludedPrice,
                                                        item.Tax,
                                                        item.ServiceFee,
                                                        item.TaxIncludedPrice,
                                                        item.Person,
                                                        item.Days,
                                                        item.SearchedDate,
                                                        item.CrawledDate,
                                                        timer_id,
                                                        realtime_id);
                            sw.WriteLine(line);
                        }
                    }

                    // SEND FILE TO SERVER
                    int tried;
                    if (SendAnalysisResult(new FileInfo(filename), 3, out tried))
                    {
                        NetworkManager.Instance.SendLog(accountId, LogType.INFO.ToString().ToLower(), string.Format(_msgResultUploaded, filename, tried));
                    }
                    else
                    {
                        NetworkManager.Instance.SendLog(accountId, LogType.ERROR.ToString().ToLower(), string.Format(_msgDefaultError, AgencyType.RURUBU.ToString().ToLower(),
                                                                                                                                        _crawlerId,
                                                                                                                                        _msgErrorSendFile));
                        result = false;
                    }
                }
            }
            catch (Exception e)
            {
                NetworkManager.Instance.SendLog(accountId, LogType.ERROR.ToString().ToLower(), string.Format(_msgDefaultError, AgencyType.RURUBU.ToString().ToLower(),
                                                                                                                               _crawlerId,
                                                                                                                               e.Message));
                result = false;
            }

            return result;
        }
    }
}