﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using Psinc.Analyst.DataStructure;
using Psinc.Util;

namespace Psinc.Analyst
{
    /// <summary>
    /// じゃらんデータ分析器
    /// </summary>
    public class JalanAnalyser : Analyser
    {
        private readonly string _parseErrRooms     = "[jalan] NO ROOM ELEMENT";
        private readonly string _parseErrPlanName  = "[jalan] PLAN NAME IS MISSING";
        private readonly string _parseErrMealFlag  = "[jalan] MEAL FLAG IS MISSING";
        private readonly string _parseErrRoomInfo  = "[jalan] ROOM INFO IS MISSING";
        private readonly string _parseErrRoomType  = "[jalan] ROOM TYPE IS MISSING";
        private readonly string _parseErrExtraInfo = "[jalan] ROOM EXTRA INFO IS MISSING";
        private readonly string _parseErrPriceInfo = "[jalan] PRICE INFO IS MISSING";


        /// <summary>
        /// Analyses target files.
        /// </summary>
        /// <param name="accountId">account ID</param>
        /// <param name="hotelId">hotel ID</param>
        /// <param name="targets">target file list</param>
        /// <returns>success or not</returns>
        public override bool DoAnalysis(int accountId, int hotelId, FileInfo[] targets)
        {
            bool result = true;

            try
            {
                string tmp = string.Empty;
                string loc = string.Empty;
                HtmlDocument doc0, doc1, doc2, doc3;

                // SET SHARED INFO.
                string[] filenameSplit = targets[0].Name.Split('_');

                _crawledDate = string.Format("20{0}-{1}-{2}", filenameSplit[5].Substring(0, 2), filenameSplit[5].Substring(2, 2), filenameSplit[5].Substring(4, 2));

                if (filenameSplit[3].Substring(0, 1).Equals("T"))
                {
                    _isInstantCrawling = false;
                }
                else
                {
                    _isInstantCrawling = true;
                }

                _crawlerId = int.Parse(filenameSplit[3].Substring(1));

                // DO ANALYSIS
                _analysisResults = new List<AnalysisResultBase>();

                foreach (var fi in targets)
                {
                    filenameSplit = fi.Name.Split('_');
                    _searchedDate = string.Format("20{0}-{1}-{2}", filenameSplit[4].Substring(0, 2), filenameSplit[4].Substring(2, 2), filenameSplit[4].Substring(4, 2));

                    _hotelId = int.Parse(filenameSplit[6]);

                    doc0 = new HtmlDocument();
                    doc0.Load(fi.FullName);

                    var plans = doc0.DocumentNode.SelectNodes("//li[contains(@class, 'p-planCassette p-searchResultItem')]");

                    if (plans == null)
                    {
                        continue;
                    }

                    for (int p = 0; p < plans.Count; p++)
                    {
                        // PLAN
                        doc1 = new HtmlDocument();
                        doc1.LoadHtml(plans[p].InnerHtml);

                        HtmlNodeCollection test;

                        // --> plan_name
                        //
                        test = doc1.DocumentNode.SelectNodes("//div[contains(@class, 'p-planCassette__header')]/p[@class='p-searchResultItem__catchPhrase']");

                        if (test == null)
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrPlanName);
                            continue;
                        }

                        string planName = RemoveNewLine(test.First().InnerText).Replace(",", string.Empty);

                        // --> meal_flg
                        //
                        bool breakfast = false;
                        bool lunch     = false;
                        bool dinner    = false;

                        loc  = "//div[contains(@class, 'p-planCassette__body')]/div[@class='p-planCassette__row']";
                        loc += "/div[@class='p-planCassette__summary']/div/dl[@class='p-mealType']/dd[contains(@class, 'p-mealType__value')]";

                        test = doc1.DocumentNode.SelectNodes(loc);

                        if (test == null)
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrMealFlag);
                        }
                        else
                        {
                            tmp = test.First().InnerText.Trim();
                        }

                        if (tmp.Contains("朝のみ"))
                        {
                            breakfast = true;
                        }
                        else if (tmp.Contains("夕のみ"))
                        {
                            dinner = true;
                        }
                        else if (tmp.Contains("朝・夕"))
                        {
                            breakfast = true;
                            dinner    = true;
                        }
                        else if (tmp.Contains("昼のみ"))
                        {
                            lunch = true;
                        }
                        else if (tmp.Contains("朝・昼"))
                        {
                            breakfast = true;
                            lunch     = true;
                        }
                        else if (tmp.Contains("昼・夕"))
                        {
                            lunch  = true;
                            dinner = true;
                        }
                        else if (tmp.Contains("朝昼夕"))
                        {
                            breakfast = true;
                            lunch     = true;
                            dinner    = true;
                        }
                        else /* 食事なし、その他 */
                        {
                            // NOTHING TO DO
                        }

                        string mealFlag = GetRegularMealFlag(breakfast, lunch, dinner);

                        // ROOM
                        doc2 = new HtmlDocument();
                        doc2.LoadHtml(doc1.DocumentNode.SelectNodes("//div[contains(@class, 'p-planCassette__body')]/table[contains(@class, 'p-planTable')]").First().InnerHtml);

                        var rooms = doc2.DocumentNode.SelectNodes("//tbody/tr");

                        if (rooms == null)
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrRooms);
                            continue;
                        }

                        for (int r = 0; r < rooms.Count; r++)
                        {
                            if (rooms[r].Attributes["id"] == null)
                            {
                                continue;
                            }
                            else if (!rooms[r].Attributes["id"].Value.Contains("yd"))
                            {
                                continue;
                            }

                            // --> account_id, hotel_id, timer_id or realtime_id, plan_name, meal_flg, search_date, crawl_date
                            //
                            AnalysisResultBase item = new AnalysisResultBase();
                            item.AccountId    = accountId;
                            item.HotelId      = _hotelId;
                            item.CrawlerId    = _crawlerId;
                            item.PlanName     = planName;
                            item.MealFlag     = mealFlag;
                            item.SearchedDate = _searchedDate;
                            item.CrawledDate  = _crawledDate;

                            doc3 = new HtmlDocument();
                            doc3.LoadHtml(rooms[r].InnerHtml);

                            // --> plan_url, plan_code, room_name, room_code, adult_cnt, stay_days
                            //
                            test = doc3.DocumentNode.SelectNodes("//td[@class='p-searchResultItem__planNameCell']/div[@class='p-searchResultItem__planNameAndHorizontalLabels']/dl/dd/a");

                            if (test == null)
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrRoomInfo);
                                item.PlanUrl  = string.Empty;
                                item.PlanCode = string.Empty;
                                item.RoomName = string.Empty;
                                item.RoomCode = string.Empty;
                                item.Person   = 0;
                                item.Days     = 0;
                            }
                            else
                            {
                                tmp = test.First().Attributes["href"].Value.Replace("&amp;", "&");

                                item.PlanUrl  = string.Format("http://www.jalan.net{0}", tmp);
                                item.RoomName = RemoveNewLine(test.First().InnerText.Trim()).Replace(",", string.Empty);

                                var parameters = tmp.Split('&');

                                foreach (var parameter in parameters)
                                {
                                    if (parameter.Contains("planCd"))
                                    {
                                        item.PlanCode = parameter.Split('=')[1];
                                    }
                                    else if (parameter.Contains("roomTypeCd"))
                                    {
                                        item.RoomCode = parameter.Split('=')[1];
                                    }
                                    else if (parameter.Contains("adultNum"))
                                    {
                                        item.Person = int.Parse(parameter.Split('=')[1]);
                                    }
                                    else if (parameter.Contains("stayCount"))
                                    {
                                        item.Days = int.Parse(parameter.Split('=')[1]);
                                    }
                                }
                            }

                            // --> plan_point, smoking_flg
                            //
                            test = doc3.DocumentNode.SelectNodes("//td[@class='p-searchResultItem__planNameCell']/div[@class='p-searchResultItem__planNameAndHorizontalLabels']/dl/dd/ul");

                            if (test == null)
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrExtraInfo);
                                item.PlanPoint   = 0;
                                item.SmokingFlag = 9;
                            }
                            else
                            {
                                tmp = Regex.Match(test.First().InnerText.Replace(" ", string.Empty).Replace("　", string.Empty), @"[ポ][イ][ン][ト]\d+[%]").Value;
                                tmp = Regex.Match(tmp, @"\d+").Value;

                                if (string.IsNullOrEmpty(tmp))
                                {
                                    item.PlanPoint = 0;
                                }
                                else
                                {
                                    item.PlanPoint = int.Parse(tmp);
                                }

                                if (test.First().InnerText.Contains("禁煙ルーム"))
                                {
                                    item.SmokingFlag = 0;
                                }
                                else
                                {
                                    item.SmokingFlag = 1;
                                }
                            }

                            // --> room_type
                            //
                            loc  = "//td[@class='p-searchResultItem__planNameCell']/div[@class='p-searchResultItem__planName']";
                            loc += "/ul[@class='p-searchResultItem__verticalLabels']/li[contains(@class, 'roomChar')]";

                            test = doc3.DocumentNode.SelectNodes(loc);
                            if (test == null)
                            {
                                loc = "//td[@class='p-searchResultItem__planNameCell']/div[@class='p-searchResultItem__planName jsc-planDetailLink']";
                                loc += "/ul[@class='p-searchResultItem__verticalLabels']/li";
                                test = doc3.DocumentNode.SelectNodes(loc);
                                if (test == null)
                                {
                                    //NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrRoomType);
                                    item.RoomType = GetRegularRoomType(string.Empty);
                                }
                                else
                                {
                                    item.RoomType = GetRegularRoomType(test.First().InnerText);
                                }
                            }
                            else
                            {
                                item.RoomType = GetRegularRoomType(test.First().InnerText);
                            }

                            // --> ota_price, service_fee, tax_excluded_price, tax, tax_included_price
                            //
                            test = doc3.DocumentNode.SelectNodes("//td[@class='p-searchResultItem__totalCell']/span[@class='p-searchResultItem__total']");

                            if (test == null)
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrPriceInfo);
                                item.OtaPrice = 0;
                            }
                            else
                            {
                                if (test.First().InnerText.Contains(","))
                                {
                                    tmp = Regex.Match(test.First().InnerText, @"\d+(\,\d+)+").Value.Replace(",", string.Empty);
                                }
                                else
                                {
                                    tmp = Regex.Match(test.First().InnerText, @"\d+").Value;
                                }

                                if (string.IsNullOrEmpty(tmp))
                                {
                                    item.OtaPrice = 0;
                                }
                                else
                                {
                                    item.OtaPrice = int.Parse(tmp);
                                }
                            }

                            //税抜
                            //item.ServiceFee       = 0;
                            //item.TaxExcludedPrice = item.OtaPrice + item.ServiceFee;

                            //double tmpValue = item.OtaPrice * (GetTaxRate(DateTime.Parse(_searchedDate)) / 100d);

                            //item.Tax              = (int)Math.Round(tmpValue, 0, MidpointRounding.AwayFromZero);
                            //item.TaxIncludedPrice = item.TaxExcludedPrice + item.Tax;

                            //税込
                            item.ServiceFee = 0;
                            item.TaxIncludedPrice = item.OtaPrice + item.ServiceFee;

                            int tax = GetTaxRate(DateTime.Parse(_searchedDate));
                            double tmpValue = (item.TaxIncludedPrice - item.ServiceFee) / (1 + tax / 100d) * (tax / 100d);

                            item.Tax = (int)Math.Round(tmpValue, 0, MidpointRounding.AwayFromZero);
                            item.TaxExcludedPrice = item.TaxIncludedPrice - item.Tax;

                            // --> stock
                            //
                            item.RoomVacancy = 0;

                            if (r + 1 < rooms.Count &&
                                rooms[r + 1].Attributes["id"] == null)
                            {
                                doc3 = new HtmlDocument();
                                doc3.LoadHtml(rooms[r + 1].InnerHtml);

                                test = doc3.DocumentNode.SelectNodes("//td[@class='p-searchResultItem__restCell']/span[@class='c-label p-searchResultItem__rest']");

                                if (test != null)
                                {
                                    tmp = Regex.Match(test.First().InnerText, @"\d+").Value;

                                    if (!string.IsNullOrEmpty(tmp))
                                    {
                                        item.RoomVacancy = int.Parse(tmp);
                                    }
                                }
                            }

                            _analysisResults.Add(item);
                        }
                    }
                }

                if (_analysisResults.Count < 1)
                {
                    NetworkManager.Instance.SendLog(accountId, LogType.INFO.ToString().ToLower(), string.Format(_msgNoResult, AgencyType.JALAN.ToString().ToLower(), _crawlerId));
                }
                else
                {
                    string filename = string.Format("{4}{5}_{0}_{1}_{2}_{3}.ar", DateTime.Today.ToString("yyMMdd"),
                                                                                 AgencyType.JALAN.ToString().ToLower(),
                                                                                 accountId,
                                                                                 hotelId,
                                                                                 filenameSplit[3].Substring(0, 1),
                                                                                 _crawlerId);
                    // WRITE RESULT TO TEXT
                    using (var sw = new StreamWriter(filename, false, Encoding.UTF8))
                    {
                        foreach (var item in _analysisResults)
                        {
                            int timer_id = 0;
                            int realtime_id = 0;

                            if (_isInstantCrawling)
                            {
                                realtime_id = item.CrawlerId;
                            }
                            else
                            {
                                timer_id = item.CrawlerId;
                            }

                            string line = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22}",
                                                        item.AccountId,
                                                        item.HotelId,
                                                        item.SmokingFlag,
                                                        item.MealFlag,
                                                        item.RoomType,
                                                        item.RoomName,
                                                        item.PlanPoint,
                                                        item.PlanCode,
                                                        item.RoomCode,
                                                        item.PlanUrl,
                                                        item.PlanName,
                                                        item.RoomVacancy,
                                                        item.OtaPrice,
                                                        item.TaxExcludedPrice,
                                                        item.Tax,
                                                        item.ServiceFee,
                                                        item.TaxIncludedPrice,
                                                        item.Person,
                                                        item.Days,
                                                        item.SearchedDate,
                                                        item.CrawledDate,
                                                        timer_id,
                                                        realtime_id);
                            sw.WriteLine(line);
                        }
                    }

                    // SEND FILE TO SERVER
                    int tried;
                    if (SendAnalysisResult(new FileInfo(filename), 3, out tried))
                    {
                        NetworkManager.Instance.SendLog(accountId, LogType.INFO.ToString().ToLower(), string.Format(_msgResultUploaded, filename, tried));
                    }
                    else
                    {
                        NetworkManager.Instance.SendLog(accountId, LogType.ERROR.ToString().ToLower(), string.Format(_msgDefaultError, AgencyType.JALAN.ToString().ToLower(),
                                                                                                                                        _crawlerId,
                                                                                                                                        _msgErrorSendFile));
                        result = false;
                    }
                }
            }
            catch (Exception e)
            {
                NetworkManager.Instance.SendLog(accountId, LogType.ERROR.ToString().ToLower(), string.Format(_msgDefaultError, AgencyType.JALAN.ToString().ToLower(),
                                                                                                                               _crawlerId,
                                                                                                                               e.Message));
                result = false;
            }

            return result;
        }
    }
}