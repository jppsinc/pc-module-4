﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using Psinc.Analyst.DataStructure;
using Psinc.Util;

namespace Psinc.Analyst
{
    /// <summary>
    /// 楽天データ分析器
    /// </summary>
    public class RakutenAnalyser : Analyser
    {
        private string _urlBaseModified = string.Empty;
        private string _veriTube = string.Empty;

        private int _person = 0; //宿泊人数
        private int _days = 0;   //宿泊日数

        private readonly string _parseErrRooms       = "[rakuten] COUNT OF ROOMS NODE IS NOT MATCHED";
        private readonly string _parseErrRoomCode    = "[rakuten] ROOM CODE VERIFICATION FAILED";
        private readonly string _parseErrTaxIncluded = "[rakuten] PRICE INCLUDES A TAX";
        private readonly string _parseErrUrl         = "[rakuten] URL BUILD FAILED";


        /// <summary>
        /// Analyses target files.
        /// </summary>
        /// <param name="accountId">account ID</param>
        /// <param name="hotelId">hotel ID</param>
        /// <param name="targets">target file list</param>
        /// <returns>success or not</returns>
        public override bool DoAnalysis(int accountId, int hotelId, FileInfo[] targets)
        {
            bool result = true;

            try
            {
                string dummy = string.Empty;
                HtmlNode node0, node1;
                HtmlDocument doc0, doc1, doc2;

                // SET SHARED INFO.
                string[] filenameSplit = targets[0].Name.Split('_');

                _crawledDate = string.Format("20{0}-{1}-{2}", filenameSplit[5].Substring(0, 2), filenameSplit[5].Substring(2, 2), filenameSplit[5].Substring(4, 2));

                if (filenameSplit[3].Substring(0, 1).Equals("T"))
                {
                    _isInstantCrawling = false;
                }
                else
                {
                    _isInstantCrawling = true;
                }

                _crawlerId = int.Parse(filenameSplit[3].Substring(1));

                // DO ANALYSIS
                _analysisResults = new List<AnalysisResultBase>();

                string urlBaseOriginal = @"https://rsvh.travel.rakuten.co.jp/rsv/RsvLogin.do?f_dhr_rsv_pgm=ry_kensaku&f_no={0}&f_syu={1}&f_hi1={2}&f_hi2={3}&f_adult_su={4}&f_s1={5}&f_s2={6}&f_y1={7}&f_y2={8}&f_y3={9}&f_y4={10}&f_heya_su={11}&f_ninzu={12}&f_teikei={13}&f_camp_id={14}&f_otona_su={4}&f_id_yami=&f_service=";

                foreach (var fi in targets)
                {
                    filenameSplit = fi.Name.Split('_');
                    _hotelId = int.Parse(filenameSplit[6]);
                    
                    _person = 0;
                    _days = 0;

                    doc0 = new HtmlDocument();
                    doc0.Load(fi.FullName);

                    var plans = doc0.DocumentNode.SelectNodes("//li[@class='planThumb' or @class='planThumb rm-type-wrapper']");

                    if (plans == null)
                    {
                        continue;
                    }

                    for (int p = 0; p < plans.Count; p++)
                    {
                        // --> plan_code, plan_name
                        //
                        string planId = plans[p].Attributes["id"].Value;
                        string planName = string.Empty;

                        for (int j = 0; j < plans[p].ChildNodes["h4"].ChildNodes.Count; j++)
                        {
                            var test1 = plans[p].ChildNodes["h4"].ChildNodes[j];

                            if (test1.Name.Equals("#text"))
                            {
                                string test2 = test1.InnerText.Trim();

                                if (!string.IsNullOrEmpty(test2))
                                {
                                    planName = test2.Replace(",", string.Empty);
                                }
                            }
                        }

                        if (planId.Contains("noplan"))
                        {
                            if (!MakeUrl(plans[p].ChildNodes["form"], urlBaseOriginal))
                            {
                                _searchedDate = string.Empty;
                                _urlBaseModified = string.Empty;

                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrUrl);
                            }
                        }

                        doc1 = new HtmlDocument();
                        doc1.LoadHtml(doc0.DocumentNode.SelectNodes("//div[@class='htlPlnRmTyp expanded hotel-plan-room-type-dated']")[p].OuterHtml);

                        var rooms = doc1.DocumentNode.SelectNodes("//ul[@class='htlPlnRmTypLst coupon-popup-container']/li");
                        if (rooms.Count == 0)
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrRooms);
                            continue;
                        }

                        int roomsCnt = rooms.Count;
                        for (int r = 0; r < roomsCnt; r++)
                        {
                            AnalysisResultBase item = new AnalysisResultBase();
                            item.AccountId = accountId;
                            item.HotelId   = _hotelId;
                            item.PlanName  = planName;
                            item.PlanCode  = planId;
                            
                            // div-class-htlPlnRmTypInfo (node0)
                            // --> room_name, smoking_flg, room_type, meal_flg, plan_point
                            //
                            node0 = doc1.DocumentNode.SelectNodes("//div[@class='htlPlnRmTypInfo']")[r];
                            
                            if (planId.Contains("noplan"))
                            {
                                item.RoomName = item.PlanName;
                                item.RoomCode = item.PlanCode;

                                // VERIFICATION
                                if (!string.IsNullOrEmpty(_veriTube))
                                {
                                    int idx = item.RoomCode.IndexOf('-');

                                    if (!item.RoomCode.Substring(idx + 1).Equals(_veriTube))
                                    {
                                        NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrRoomCode);
                                    }
                                }
                            }
                            else
                            {
                                node1 = node0.SelectNodes("//h6")[r];
                                
                                item.RoomName = node1.InnerText.Trim().Replace(",", string.Empty);
                                item.RoomCode = rooms[r].Attributes["id"].Value;
                                
                                if (!MakeUrl(rooms[r].SelectNodes("//form")[r], urlBaseOriginal))
                                {
                                    _searchedDate = string.Empty;
                                    _urlBaseModified = string.Empty;
                                    
                                    NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrUrl);
                                }
                            }
                            
                            if (item.RoomName.Contains("禁煙"))
                            {
                                item.SmokingFlag = 0;
                            }
                            else
                            {
                                item.SmokingFlag = 1;
                            }

                            node1 = node0.SelectNodes("//dd[@class='htlPlnTypTxt']/span[@data-locate='roomType-Info']")[r];

                            item.RoomType = GetRegularRoomType(node1.InnerText);

                            node1 = node0.SelectNodes("//div[@class='htlPlnTypOpt']")[r];

                            for (int k = 0; k < node1.ChildNodes.Count; k++)
                            {
                                if (node1.ChildNodes[k].Name.Equals("span") && node1.ChildNodes[k].Attributes["data-locate"].Value.Equals("roomType-option-meal"))
                                {
                                    dummy = node1.ChildNodes[k].InnerText;

                                    bool breakfastFlag = false;
                                    bool lunchFlag     = false;
                                    bool dinnerFlag    = false;

                                    if (dummy.Contains("朝食あり"))
                                    {
                                        breakfastFlag = true;
                                    }
                                    if (dummy.Contains("夕食あり"))
                                    {
                                        dinnerFlag = true;
                                    }

                                    item.MealFlag = GetRegularMealFlag(breakfastFlag, lunchFlag, dinnerFlag);

                                    continue;
                                }

                                if (node1.ChildNodes[k].Name.Equals("span") && node1.ChildNodes[k].Attributes["data-locate"].Value.Equals("roomType-option-point"))
                                {
                                    dummy = Regex.Match(node1.ChildNodes[k].InnerText, @"\d+").Value;

                                    item.PlanPoint = int.Parse(dummy);

                                    continue;
                                }
                            }
                            
                            // ul-class-htlPlnRmTypPrcArea (node0)
                            // --> ota_price, service_fee, tax_excluded_price, tax, tax_included_price, stock
                            //
                            doc2 = new HtmlDocument();
                            doc2.LoadHtml(rooms[r].InnerHtml);

                            var nodes0 = doc2.DocumentNode.SelectNodes("//ul");
                            var nodes1 = doc2.DocumentNode.SelectNodes("//span[@class='ndPrice']");

                            //価格が出ずに、「空室なし　ご希望の日程に該当する空室が見つかりませんでした。」と表示される場合があるため
                            if (nodes1 == null)
                            {
                                continue;
                            }
                            node0 = nodes0.First();
                            node1 = nodes1.First();

                            //node0 = doc1.DocumentNode.SelectNodes("//ul[@class='htlPlnRmTypPrcArea']")[r];
                            //node1 = node0.SelectNodes("//li/dl/dd[@class='cmn_rbAndNvrWrap clearfix']/span[@class='vPriceAndIncldTaxWrap']/span[@class='vPrice']")[r];

                            dummy = Regex.Match(node0.InnerText, @"\d+(\,\d+)+").Value.Replace(",", string.Empty);
                            
                            item.OtaPrice         = int.Parse(dummy);
                            item.ServiceFee       = 0;

                            //税抜 
                            //item.TaxExcludedPrice = item.OtaPrice + item.ServiceFee;

                            //double tmpValue = item.OtaPrice * (GetTaxRate(DateTime.Parse(_searchedDate)) / 100d);

                            //item.Tax              = (int)Math.Round(tmpValue, 0, MidpointRounding.AwayFromZero);
                            //item.TaxIncludedPrice = item.TaxExcludedPrice + item.Tax;

                            //税込 2021.03.30
                            item.TaxIncludedPrice = item.OtaPrice + item.ServiceFee;

                            int tax = GetTaxRate(DateTime.Parse(_searchedDate));
                            double tmpValue = (item.TaxIncludedPrice - item.ServiceFee) / (1 + tax / 100d) * (tax / 100d);

                            item.Tax = (int)Math.Round(tmpValue, 0, MidpointRounding.AwayFromZero);
                            item.TaxExcludedPrice = item.TaxIncludedPrice - item.Tax;

                            if (!node1.InnerHtml.Contains("includiTaxDis")) // INCLUDE TAX
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrTaxIncluded);
                            }

                            item.RoomVacancy = 0; // INITIAL VALUE FOR 'stock'
                            
                            if (node0.InnerText.Contains("残り"))
                            {
                                dummy = Regex.Match(node0.InnerText, @"[残][り]\d+").Value;
                                dummy = Regex.Match(dummy, @"\d+").Value;

                                item.RoomVacancy = int.Parse(dummy);
                            }

                            // --> timer_id or realtime_id, plan_url, adult_cnt, stay_days, search_date, crawl_date
                            //
                            item.CrawlerId    = _crawlerId;
                            item.PlanUrl      = _urlBaseModified;
                            item.Person       = _person;
                            item.Days         = _days;
                            item.SearchedDate = _searchedDate;
                            item.CrawledDate  = _crawledDate;
                            
                            _analysisResults.Add(item);

                            // VERIFICATION
                            if (!string.IsNullOrEmpty(_veriTube))
                            {
                                int idx = item.RoomCode.IndexOf('-');

                                if (!item.RoomCode.Substring(idx + 1).Equals(_veriTube))
                                {
                                    NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrRoomCode);
                                }
                            }
                            
                        }
                    }
                }

                if (_analysisResults.Count < 1)
                {
                    NetworkManager.Instance.SendLog(accountId, LogType.INFO.ToString().ToLower(), string.Format(_msgNoResult, AgencyType.RAKUTEN.ToString().ToLower(), _crawlerId));
                }
                else
                {
                    string filename = string.Format("{4}{5}_{0}_{1}_{2}_{3}.ar", DateTime.Today.ToString("yyMMdd"),
                                                                                 AgencyType.RAKUTEN.ToString().ToLower(),
                                                                                 accountId,
                                                                                 hotelId,
                                                                                 filenameSplit[3].Substring(0, 1),
                                                                                 _crawlerId);
                    // WRITE RESULT TO TEXT
                    using (var sw = new StreamWriter(filename, false, Encoding.UTF8))
                    {
                        foreach (var item in _analysisResults)
                        {
                            int timer_id = 0;
                            int realtime_id = 0;

                            if (_isInstantCrawling)
                            {
                                realtime_id = item.CrawlerId;
                            }
                            else
                            {
                                timer_id = item.CrawlerId;
                            }

                            string line = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22}",
                                                        item.AccountId,
                                                        item.HotelId,
                                                        item.SmokingFlag,
                                                        item.MealFlag,
                                                        item.RoomType,
                                                        item.RoomName,
                                                        item.PlanPoint,
                                                        item.PlanCode,
                                                        item.RoomCode,
                                                        item.PlanUrl,
                                                        item.PlanName,
                                                        item.RoomVacancy,
                                                        item.OtaPrice,
                                                        item.TaxExcludedPrice,
                                                        item.Tax,
                                                        item.ServiceFee,
                                                        item.TaxIncludedPrice,
                                                        item.Person,
                                                        item.Days,
                                                        item.SearchedDate,
                                                        item.CrawledDate,
                                                        timer_id,
                                                        realtime_id);
                            sw.WriteLine(line);
                        }
                    }

                    // SEND FILE TO SERVER
                    int tried;
                    if (SendAnalysisResult(new FileInfo(filename), 3, out tried))
                    {
                        NetworkManager.Instance.SendLog(accountId, LogType.INFO.ToString().ToLower(), string.Format(_msgResultUploaded, filename, tried));
                    }
                    else
                    {
                        NetworkManager.Instance.SendLog(accountId, LogType.ERROR.ToString().ToLower(), string.Format(_msgDefaultError, AgencyType.RAKUTEN.ToString().ToLower(),
                                                                                                                                        _crawlerId,
                                                                                                                                        _msgErrorSendFile));
                        result = false;
                    }
                }
            }
            catch (Exception e)
            {
                NetworkManager.Instance.SendLog(accountId, LogType.ERROR.ToString().ToLower(), string.Format(_msgDefaultError, AgencyType.RAKUTEN.ToString().ToLower(),
                                                                                                                               _crawlerId,
                                                                                                                               e.Message));
                result = false;
            }

            return result;
        }

        private bool MakeUrl(HtmlNode targetNode, string urlBaseOriginal)
        {
            bool result = true;

            try
            {
                var urlParams = Enumerable.Repeat(string.Empty, 16).ToArray();

                for (int i = 0; i < targetNode.ChildNodes.Count; i++)
                {
                    string n = targetNode.ChildNodes[i].Name;
                    string v = string.Empty;

                    var tmp = targetNode.ChildNodes[i].Attributes["name"];

                    if (tmp != null)
                    {
                        v = tmp.Value;
                    }

                    if (string.IsNullOrEmpty(n) || string.IsNullOrEmpty(v))
                    {
                        continue;
                    }

                    if (n.Equals("input") && v.Equals("f_no"))
                    {
                        urlParams[0] = targetNode.ChildNodes[i].Attributes["value"].Value.Trim();
                    }
                    else if (n.Equals("input") && v.Equals("f_otona_su"))
                    {
                        urlParams[1] = targetNode.ChildNodes[i].Attributes["value"].Value.Trim();
                        _person = int.Parse(urlParams[1]);
                    }
                    else if (n.Equals("input") && v.Equals("f_s1"))
                    {
                        urlParams[2] = targetNode.ChildNodes[i].Attributes["value"].Value.Trim();
                    }
                    else if (n.Equals("input") && v.Equals("f_s2"))
                    {
                        urlParams[3] = targetNode.ChildNodes[i].Attributes["value"].Value.Trim();
                    }
                    else if (n.Equals("input") && v.Equals("f_y1"))
                    {
                        urlParams[4] = targetNode.ChildNodes[i].Attributes["value"].Value.Trim();
                    }
                    else if (n.Equals("input") && v.Equals("f_y2"))
                    {
                        urlParams[5] = targetNode.ChildNodes[i].Attributes["value"].Value.Trim();
                    }
                    else if (n.Equals("input") && v.Equals("f_y3"))
                    {
                        urlParams[6] = targetNode.ChildNodes[i].Attributes["value"].Value.Trim();
                    }
                    else if (n.Equals("input") && v.Equals("f_y4"))
                    {
                        urlParams[7] = targetNode.ChildNodes[i].Attributes["value"].Value.Trim();
                    }
                    else if (n.Equals("input") && v.Equals("f_heya_su"))
                    {
                        urlParams[8] = targetNode.ChildNodes[i].Attributes["value"].Value.Trim();
                    }
                    else if (n.Equals("input") && v.Equals("f_teikei"))
                    {
                        urlParams[9] = targetNode.ChildNodes[i].Attributes["value"].Value.Trim();
                    }
                    else if (n.Equals("input") && v.Equals("f_camp_id"))
                    {
                        urlParams[10] = targetNode.ChildNodes[i].Attributes["value"].Value.Trim();
                    }
                    else if (n.Equals("input") && v.Equals("f_syu"))
                    {
                        urlParams[11] = targetNode.ChildNodes[i].Attributes["value"].Value.Trim();
                        _veriTube = urlParams[11]; // FOR VERIFICATION
                    }
                    else if (n.Equals("input") && v.Equals("f_ninzu"))
                    {
                        urlParams[12] = targetNode.ChildNodes[i].Attributes["value"].Value.Trim();
                    }
                    else if (n.Equals("input") && v.Equals("f_kin_kubun"))
                    {
                        urlParams[13] = targetNode.ChildNodes[i].Attributes["value"].Value.Trim();
                    }
                    else if (n.Equals("input") && v.Equals("f_hi1"))
                    {
                        urlParams[14] = targetNode.ChildNodes[i].Attributes["value"].Value.Trim();
                    }
                    else if (n.Equals("input") && v.Equals("f_hi2"))
                    {
                        urlParams[15] = targetNode.ChildNodes[i].Attributes["value"].Value.Trim();
                    }
                }

                if (!string.IsNullOrEmpty(urlParams[14]) && !string.IsNullOrEmpty(urlParams[15]))
                {
                    DateTime sd = DateTime.Parse(urlParams[14]);
                    DateTime ed = DateTime.Parse(urlParams[15]);

                    _days = (ed - sd).Days;
                }

                _searchedDate    = urlParams[14];
                _urlBaseModified = string.Format(urlBaseOriginal, urlParams[0],
                                                                  urlParams[11],
                                                                  urlParams[14],
                                                                  urlParams[15],
                                                                  urlParams[1],
                                                                  urlParams[2],
                                                                  urlParams[3],
                                                                  urlParams[4],
                                                                  urlParams[5],
                                                                  urlParams[6],
                                                                  urlParams[7],
                                                                  urlParams[8],
                                                                  urlParams[12],
                                                                  urlParams[9],
                                                                  urlParams[10]);
            }
            catch
            {
                result = false;
            }

            return result;
        }
    }
}