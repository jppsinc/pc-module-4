﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using Psinc.Analyst.DataStructure;
using Psinc.Util;

namespace Psinc.Analyst
{
    /// <summary>
    /// Booking.comデータ分析器
    /// </summary>
    public class BookingAnalyser : Analyser
    {
        private readonly string _parseErrPlanName         = "[booking] PLAN NAME IS MISSING";
        private readonly string _parseErrPlanPrice        = "[booking] PLAN PRICE IS MISSING";
        private readonly string _parseErrRoomCode         = "[booking] ROOM CODE IS MISSING";
        private readonly string _parseErrTaxInfo          = "[booking] TAX INFO IS MISSING";
        private readonly string _parseErrLengthOfStayInfo = "[booking] LENGTH OF STAY INFO IS MISSING ({0})";

        private readonly string _roomVacancyTag = "span class=\"only_x_left urgency_message_red\"";
        private readonly string _mealFlagTag    = "span class=\"bicon-coffee mp-icon meal-plan-icon\"";
        private readonly string _soldOutTag     = "span class=\"important_text\"";

        private readonly string _planUrlBase = "https://secure.booking.com/book.html?from_source=hotel&hotel_id={0}&label={1}&lang=ja&hostname=www.booking.com&stage=1&checkin={2}&interval={3}&nr_rooms_{4}=1&rt_pos_selected=1";


        /// <summary>
        /// Analyses target files.
        /// </summary>
        /// <param name="accountId">account ID</param>
        /// <param name="hotelId">hotel ID</param>
        /// <param name="targets">target file list</param>
        /// <returns>success or not</returns>
        public override bool DoAnalysis(int accountId, int hotelId, FileInfo[] targets)
        {
            bool result = true;

            try
            {
                HtmlNode node;
                HtmlDocument doc0, doc1, doc2;

                string dummy = string.Empty;

                // SET SHARED INFO.
                string[] filenameSplit = targets.First().Name.Split('_');

                _crawledDate = string.Format("20{0}-{1}-{2}", filenameSplit[5].Substring(0, 2), filenameSplit[5].Substring(2, 2), filenameSplit[5].Substring(4, 2));

                if (filenameSplit[3].Substring(0, 1).Equals("T"))
                {
                    _isInstantCrawling = false;
                }
                else
                {
                    _isInstantCrawling = true;
                }

                _crawlerId = int.Parse(filenameSplit[3].Substring(1));

                // DO ANALYSIS
                _analysisResults = new List<AnalysisResultBase>();

                foreach (var fi in targets)
                {
                    filenameSplit = fi.Name.Split('_');
                    _searchedDate = string.Format("20{0}-{1}-{2}", filenameSplit[4].Substring(0, 2), filenameSplit[4].Substring(2, 2), filenameSplit[4].Substring(4, 2));

                    _hotelId = int.Parse(filenameSplit[6]);

                    string hotelCode = filenameSplit[2];

                    string contents = File.ReadAllLines(fi.FullName).First();

                    if (contents.Contains("http"))
                    {
                        continue;
                    }

                    var tmpStr   = contents.Split('_');
                    int person   = int.Parse(tmpStr[0]);
                    string label = tmpStr[1];

                    doc0 = new HtmlDocument();
                    doc0.Load(fi.FullName);

                    var whole = doc0.DocumentNode.SelectNodes("//tr[@data-block-id]");

                    if (whole == null)
                    {
                        if (doc0.DocumentNode.InnerHtml.Contains("ご提供できるこの宿泊施設の空室がありません"))
                        {
                            // NO VACANCY CASE
                            // e.g. "現在当サイトでは、2018年4月5日(木)～2018年4月6日(金)の間にご提供できるこの宿泊施設の空室がありません。"
                            continue;
                        }
                        else if (doc0.DocumentNode.InnerText.Contains("System.") && doc0.DocumentNode.InnerText.Contains("Exception"))
                        {
                            // ERROR OCCURRED CASE
                            continue;
                        }
                        else
                        {
                            // DEPRECATED: 190814
                            //private readonly string _parseErrWholeStructure   = "[booking] WHOLE STRUCTURE IS MAY BEEN CHANGED";
                            //NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrWholeStructure);
                            continue;
                        }
                    }

                    string planCode = string.Empty;
                    string planName = string.Empty;
                    
                    int days        = 0;
                    int roomVacancy = 0;

                    // --> adult_cnt, stay_days
                    //
                    var nodes = doc0.DocumentNode.SelectNodes("//div[contains(@class, 'av-summary-content')]/div[contains(@class, 'av-summary-section')]/div[contains(@class, 'bui-date-range')]/div[contains(@class, 'bui-date-range')]");

                    if (nodes == null)
                    {
                        NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseErrLengthOfStayInfo, "WHOLE"));
                    }
                    else
                    {
                        for (int i = 0; i < nodes.Count; i++)
                        {
                            if (nodes[i].InnerText.Contains("チェックアウト日") && nodes[i].InnerText.Contains("泊"))
                            {
                                dummy = Regex.Match(nodes[i].InnerText, @"\d+[泊]").Value;

                                if (!string.IsNullOrEmpty(dummy))
                                {
                                    days = int.Parse(Regex.Match(dummy, @"\d+").Value);
                                }

                                continue;
                            }
                        }
                    }
                    
                    for (int w = 0; w < whole.Count; w++)
                    {
                        int position = 0;

                        AnalysisResultBase item = new AnalysisResultBase();
                        item.AccountId = accountId;
                        item.HotelId   = _hotelId;
                        item.Person    = person;
                        item.Days      = days;

                        if (string.IsNullOrEmpty(whole[w].InnerHtml))
                        {
                            continue;
                        }

                        if (whole[w].InnerHtml.Contains("hprt-table-cheapest-block-banner")) // USELESS BLOCK
                        {
                            continue;
                        }

                        doc1 = new HtmlDocument();
                        doc1.LoadHtml(whole[w].InnerHtml);

                        bool isSamePlan = false;

                        if (whole[w].Attributes["data-block-id"] == null)
                        {
                            continue;
                        }
                        
                        // --> room_code, plan_code
                        //
                        dummy = whole[w].Attributes["data-block-id"].Value;

                        if (string.IsNullOrEmpty(dummy))
                        {
                            var tmp = doc1.DocumentNode.SelectNodes("//td[contains(@class, 'hprt-table-cell')]/span[contains(@class, 'important_text')]");

                            if (tmp == null)
                            {
                                node = null;
                            }
                            else
                            {
                                node = tmp.First();
                            }

                            if (node == null)
                            {
                                position = doc1.DocumentNode.InnerHtml.IndexOf(_soldOutTag); // Backup detection for fail to picking up tag.

                                if (position < 0)
                                {
                                    NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrRoomCode);
                                    item.PlanCode = string.Empty;
                                    item.RoomCode = string.Empty;

                                    position = 0;
                                }
                                else
                                {
                                    continue;
                                }
                            }
                            else
                            {
                                continue;
                            }
                        }
                        else
                        {
                            item.RoomCode = dummy.Trim();
                            item.PlanCode = item.RoomCode.Split('_').First();

                            if (!planCode.Equals(item.PlanCode))
                            {
                                planCode = item.PlanCode;
                            }
                        }

                        //person check
                        nodes = doc1.DocumentNode.SelectNodes("//span[contains(@class, 'c-occupancy-icons__adults')]/i");
                        
                        if (nodes == null)
                        {
                            continue;
                        }
                        
                        if (nodes.Count != person)
                        {
                            continue;
                        }
                        
                        // --> plan_name, room_type, room_name, smoking_flg
                        //
                        nodes = doc1.DocumentNode.SelectNodes("//span[contains(@class, 'hprt-roomtype-icon-link')]");

                        if (nodes == null)
                        {
                            if (!string.IsNullOrEmpty(planName))
                            {
                                isSamePlan = true;
                                
                                //item.PlanName = planName;
                                item.PlanName = planName.Replace(",", string.Empty);
                            }
                            else
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrPlanName);
                                item.PlanName = string.Empty;
                            }
                        }
                        else
                        {
                            node = nodes.First();

                            planName = node.InnerText.Trim();

                            item.PlanName = planName.Replace(",", string.Empty);
                        }

                        item.RoomType = GetRegularRoomType(item.PlanName);
                        item.RoomName = item.PlanName;

                        if (item.RoomName.Contains("禁煙"))
                        {
                            item.SmokingFlag = 0;
                        }
                        //else if (item.RoomName.Contains("喫煙"))
                        //{
                        //    item.SmokingFlag = 1;
                        //}
                        else
                        {
                            item.SmokingFlag = 1;//9;
                        }

                        // --> stock: tag position is vary
                        //
                        if (isSamePlan)
                        {
                            item.RoomVacancy = roomVacancy;
                        }
                        else
                        {
                            position = doc1.DocumentNode.InnerHtml.IndexOf(_roomVacancyTag);

                            if (position >= 0)
                            {
                                position += _roomVacancyTag.Length;

                                dummy = doc1.DocumentNode.InnerHtml.Substring(position + 1, 10);
                                dummy = Regex.Match(dummy, @"\d+").Value;

                                if (string.IsNullOrEmpty(dummy))
                                {
                                    item.RoomVacancy = 0;
                                }
                                else
                                {
                                    item.RoomVacancy = int.Parse(dummy);
                                }

                                roomVacancy = item.RoomVacancy;

                                position = 0;
                            }
                        }

                        nodes = doc1.DocumentNode.SelectNodes("//td[contains(@class, 'hprt-table-cell hprt-table-cell-price')]/div[contains(@class, 'hprt-price-block')]");

                        if (nodes == null)
                        {
                            continue;
                        }
                        else
                        {
                            doc2 = new HtmlDocument();
                            doc2.LoadHtml(nodes.First().InnerHtml);

                            // --> ota_price, tax_included_price, service_fee, tax, tax_excluded_price
                            //
                            nodes = doc2.DocumentNode.SelectNodes("//div[contains(@class, 'hprt-price-price')]/span");

                            if (nodes == null)
                            {
                                // FOUND: 190722
                                nodes = doc2.DocumentNode.SelectNodes("//div[contains(@class, 'bui-price-display__value')]");
                            }

                            if (nodes == null)
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrPlanPrice);
                                item.OtaPrice         = 0;
                                item.TaxIncludedPrice = 0;
                            }
                            else
                            {
                                string tmp = nodes.First().InnerText;

                                if (tmp.Contains(","))
                                {
                                    dummy = Regex.Match(tmp, @"\d+(\,\d+)+").Value.Replace(",", string.Empty);
                                }
                                else
                                {
                                    dummy = Regex.Match(tmp, @"\d+").Value;
                                }

                                item.OtaPrice         = int.Parse(dummy);
                                item.TaxIncludedPrice = item.OtaPrice;
                            }

                            nodes = doc2.DocumentNode.SelectNodes("//div[@class='hprt-roomtype-block']/div[@class='hptr-taxinfo-block']/div[@class='hptr-taxinfo-details']");

                            bool calculateMore = false;

                            if (nodes == null)
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrTaxInfo);
                                calculateMore = true;
                            }
                            else
                            {
                                string tmp = nodes.First().InnerText;

                                if (tmp.Contains("込"))
                                {
                                    tmp = tmp.Replace(" ", string.Empty).Replace("　", string.Empty);

                                    dummy = Regex.Match(tmp, @"[V][A][T]\d+[%]").Value;
                                    dummy = Regex.Match(dummy, @"\d+").Value;

                                    int tax = int.Parse(dummy);

                                    dummy = Regex.Match(tmp, @"[サ][ー][ビ][ス][料]\d+[%]").Value;

                                    int serviceChargeRate = 0;

                                    if (!string.IsNullOrEmpty(dummy))
                                    {
                                        dummy = Regex.Match(dummy, @"\d+").Value;

                                        serviceChargeRate = int.Parse(dummy);
                                    }

                                    double tmpValue = 0d;

                                    if (serviceChargeRate == 0)
                                    {
                                        item.ServiceFee = 0;
                                    }
                                    else
                                    {
                                        tmpValue = item.TaxIncludedPrice / (1 + serviceChargeRate / 100d);

                                        item.ServiceFee = item.TaxIncludedPrice - (int)Math.Round(tmpValue, 0, MidpointRounding.AwayFromZero);
                                    }

                                    tmpValue = (item.TaxIncludedPrice - item.ServiceFee) / (1 + tax / 100d) * (tax / 100d);

                                    item.Tax              = (int)Math.Round(tmpValue, 0, MidpointRounding.AwayFromZero);
                                    item.TaxExcludedPrice = item.TaxIncludedPrice - item.Tax;
                                }
                                else
                                {
                                    calculateMore = true;
                                }
                            }

                            if (calculateMore)
                            {
                                item.ServiceFee = 0;

                                double tmpValue = item.TaxIncludedPrice * (GetTaxRate(DateTime.Parse(_searchedDate)) / 100d);

                                item.Tax              = (int)Math.Round(tmpValue, 0, MidpointRounding.AwayFromZero);
                                item.TaxExcludedPrice = item.TaxIncludedPrice - item.Tax;
                            }
                        }

                        // --> plan_url
                        //
                        item.PlanUrl = string.Format(_planUrlBase, hotelCode, label, _searchedDate, days, item.RoomCode);

                        // --> meal_flg: tag can't picked up, xpath = //td[@class='hprt-table-cell hprt-table-cell-conditions']/div[@class='hprt-block']/ul/li
                        //
                        bool breakfast = false;
                        bool lunch     = false;
                        bool dinner    = false;

                        nodes = doc1.DocumentNode.SelectNodes("//td[contains(@class, 'hprt-table-cell-conditions')]/div[contains(@class, 'hprt-block')]/ul/li/div/div[contains(@class, 'bui-list__description')]");

                        if (nodes == null)
                        {
                            //
                        }
                        else
                        {
                            dummy = nodes.First().InnerText;

                            if (dummy.Contains("朝食込"))
                            {
                                breakfast = true;
                            }
                            else if (dummy.Contains("朝食") && dummy.Contains("夕食込み"))
                            {
                                breakfast = true;
                                dinner = true;
                            }
                        }

                        item.MealFlag = GetRegularMealFlag(breakfast, lunch, dinner);

                        // --> timer_id or realtime_id, plan_point, search_date, crawl_date
                        //
                        item.CrawlerId    = _crawlerId;
                        item.PlanPoint    = 0;
                        item.SearchedDate = _searchedDate;
                        item.CrawledDate  = _crawledDate;

                        _analysisResults.Add(item);
                    }
                }

                if (_analysisResults.Count < 1)
                {
                    NetworkManager.Instance.SendLog(accountId, LogType.INFO.ToString().ToLower(), string.Format(_msgNoResult, AgencyType.BOOKING.ToString().ToLower(), _crawlerId));
                }
                else
                {
                    string filename = string.Format("{4}{5}_{0}_{1}_{2}_{3}.ar",
                                                    DateTime.Today.ToString("yyMMdd"), AgencyType.BOOKING.ToString().ToLower(), accountId, hotelId, filenameSplit[3].Substring(0, 1), _crawlerId);

                    // WRITE RESULT TO TEXT
                    using (var sw = new StreamWriter(filename, false, Encoding.UTF8))
                    {
                        foreach (var item in _analysisResults)
                        {
                            int timer_id = 0;
                            int realtime_id = 0;

                            if (_isInstantCrawling)
                            {
                                realtime_id = item.CrawlerId;
                            }
                            else
                            {
                                timer_id = item.CrawlerId;
                            }

                            string line = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22}",
                                                        item.AccountId,
                                                        item.HotelId,
                                                        item.SmokingFlag,
                                                        item.MealFlag,
                                                        item.RoomType,
                                                        item.RoomName,
                                                        item.PlanPoint,
                                                        item.PlanCode,
                                                        item.RoomCode,
                                                        item.PlanUrl,
                                                        item.PlanName,
                                                        item.RoomVacancy,
                                                        item.OtaPrice,
                                                        item.TaxExcludedPrice,
                                                        item.Tax,
                                                        item.ServiceFee,
                                                        item.TaxIncludedPrice,
                                                        item.Person,
                                                        item.Days,
                                                        item.SearchedDate,
                                                        item.CrawledDate,
                                                        timer_id,
                                                        realtime_id);
                            sw.WriteLine(line);
                        }
                    }

                    // SEND FILE TO SERVER
                    int tried;
                    if (SendAnalysisResult(new FileInfo(filename), 3, out tried))
                    {
                        NetworkManager.Instance.SendLog(accountId, LogType.INFO.ToString().ToLower(), string.Format(_msgResultUploaded, filename, tried));
                    }
                    else
                    {
                        NetworkManager.Instance.SendLog(accountId, LogType.ERROR.ToString().ToLower(), string.Format(_msgDefaultError, AgencyType.BOOKING.ToString().ToLower(),
                                                                                                                                        _crawlerId,
                                                                                                                                        _msgErrorSendFile));
                        result = false;
                    }
                }
            }
            catch (Exception e)
            {
                NetworkManager.Instance.SendLog(accountId, LogType.ERROR.ToString().ToLower(), string.Format(_msgDefaultError, AgencyType.BOOKING.ToString().ToLower(),
                                                                                                                               _crawlerId,
                                                                                                                               e.Message));
                result = false;
            }

            return result;
        }
    }
}