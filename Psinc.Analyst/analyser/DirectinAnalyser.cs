﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using Psinc.Analyst.DataStructure;
using Psinc.Util;

namespace Psinc.Analyst
{
    /// <summary>
    /// Direct Inデータ分析器
    /// </summary>
    public class DirectinAnalyser : Analyser
    {
        private readonly string _parseErrSearchCondition  = "[directin] SEARCH CONDITION INFO IS MISSING ({0})";
        private readonly string _parseErrPlanUrl          = "[directin] PLAN URL IS MISSING";
        private readonly string _parseErrPlanName         = "[directin] PLAN NAME IS MISSING";
        private readonly string _parseErrPlanPrice        = "[directin] PRICE IS MISSING";
        private readonly string _parseErrRooms            = "[directin] ROOM IS MISSING";
        private readonly string _parseErrRoomName         = "[directin] ROOM NAME IS MISSING";
        private readonly string _parseErrRoomCode         = "[directin] ROOM CODE IS MISSING";
        private readonly string _parseErrMealFlag         = "[directin] MEAL FLAG IS MISSING";
        private readonly string _parseErrMealFlagVerifier = "[directin] UNDEFINED MEAL FLAG : {0}";

        private readonly string _planUrlBase = @"https://asp.hotel-story.ne.jp/ver3d/ASPP0200.asp?hidSELECTCOD1={0}&hidSELECTCOD2={1}&hidSELECTPLAN={2}&pac={3}&hidSELECTARRYMD={4}&hidSELECTHAKSU={5}&rooms=1&selectptyp={6}&selectppsn={7}&hidk=&reffrom=&LB01=server7";


        /// <summary>
        /// Analyses target files.
        /// </summary>
        /// <param name="accountId">account ID</param>
        /// <param name="hotelId">hotel ID</param>
        /// <param name="targets">target file list</param>
        /// <returns>success or not</returns>
        public override bool DoAnalysis(int accountId, int hotelId, FileInfo[] targets)
        {
            bool result = true;

            try
            {
                HtmlNode node;
                HtmlDocument doc0, doc1, doc2;

                // SET SHARED INFO.
                string[] filenameSplit = targets.First().Name.Split('_');

                _crawledDate = string.Format("20{0}-{1}-{2}", filenameSplit[6].Substring(0, 2), filenameSplit[6].Substring(2, 2), filenameSplit[6].Substring(4, 2));

                if (filenameSplit[4].Substring(0, 1).Equals("T"))
                {
                    _isInstantCrawling = false;
                }
                else
                {
                    _isInstantCrawling = true;
                }

                _crawlerId = int.Parse(filenameSplit[4].Substring(1));

                // DO ANALYSIS
                _analysisResults = new List<AnalysisResultBase>();

                foreach (var fi in targets)
                {
                    filenameSplit = fi.Name.Split('_');
                    _searchedDate = string.Format("20{0}-{1}-{2}", filenameSplit[5].Substring(0, 2), filenameSplit[5].Substring(2, 2), filenameSplit[5].Substring(4, 2));

                    _hotelId = int.Parse(filenameSplit[7]);

                    doc0 = new HtmlDocument();
                    doc0.Load(fi.FullName);

                    var plans = doc0.DocumentNode.SelectNodes("//div[@class='plan-list']");

                    if (plans == null)
                    {
                        continue;
                    }

                    string dummy = string.Empty;

                    // --> adult_cnt, stay_days
                    //
                    // SEARCH CONDITION
                    int person = 0;
                    int days   = 0;

                    var nodes = doc0.DocumentNode.SelectNodes("//table[@id='search']/tr/td/span[@class='ssearch']");

                    if (nodes == null)
                    {
                        NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseErrSearchCondition, "WHOLE"));
                    }
                    else
                    {
                        dummy = Regex.Match(nodes.First().InnerText, @"\d+[名][様]").Value;
                        dummy = Regex.Match(dummy, @"\d+").Value;

                        if (string.IsNullOrEmpty(dummy))
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseErrSearchCondition, "PERSON"));
                        }
                        else
                        {
                            person = int.Parse(dummy);
                        }

                        dummy = Regex.Match(nodes.First().InnerText, @"\d+[泊]").Value;
                        dummy = Regex.Match(dummy, @"\d+").Value;

                        if (string.IsNullOrEmpty(dummy))
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseErrSearchCondition, "DAYS"));
                        }
                        else
                        {
                            days = int.Parse(dummy);
                        }
                    }

                    for (int p = 0; p < plans.Count; p++)
                    {
                        doc1 = new HtmlDocument();
                        doc1.LoadHtml(plans[p].InnerHtml);

                        // --> plan_name
                        //
                        string planName = string.Empty;

                        nodes = doc1.DocumentNode.SelectNodes("//h3[@class='pointer']");

                        if (nodes == null)
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrPlanName);
                        }
                        else
                        {
                            planName = nodes.First().InnerText.Trim();

                            if (planName.Contains(","))
                            {
                                planName = planName.Replace(",", string.Empty);
                            }
                        }
                        
                        // --> meal_flg
                        //
                        string mealFlag = string.Empty;

                        bool breakfast = false;
                        bool lunch     = false;
                        bool dinner    = false;

                        nodes = doc1.DocumentNode.SelectNodes("//div/table[@class='plan-data']/tr");

                        if (nodes == null)
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrMealFlag);
                        }
                        else
                        {
                            for (int i = 0; i < nodes.Count; i++)
                            {
                                if (nodes[i].InnerText.Contains("お食事"))
                                {
                                    dummy = nodes[i].InnerText.Trim();
                                    break;
                                }
                            }

                            if (dummy.Contains("なし"))
                            {
                                // NOTHING TO DO
                            }
                            else if (dummy.Contains("朝食付"))
                            {
                                breakfast = true;
                            }
                            else if (dummy.Contains("夕食付"))
                            {
                                dinner = true;
                            }
                            else if (dummy.Contains("2食付"))
                            {
                                breakfast = true;
                                dinner = true;
                            }
                            else
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseErrMealFlagVerifier, dummy));
                            }
                        }

                        mealFlag = GetRegularMealFlag(breakfast, lunch, dinner);

                        var rooms = doc1.DocumentNode.SelectNodes("//table[@class='plan-listtable reserve_available']/tbody/tr");
                        
                        if (rooms == null)
                        {
                            // FOUND: 180413 (旧デザイン)
                            rooms = doc1.DocumentNode.SelectNodes("//table[@class='plan-listtable']/tr/td/div[@class='plan-datainsideBox']/table[@class='plan-datainside']/tr");
                        }
                        if (rooms == null)
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrRooms);
                            continue;
                        }

                        for (int r = 0; r < rooms.Count; r++)
                        {
                            AnalysisResultBase item = new AnalysisResultBase();
                            item.AccountId = accountId;
                            item.HotelId   = _hotelId;
                            item.PlanName  = planName;
                            item.MealFlag  = mealFlag;
                            item.Person    = person;
                            item.Days      = days;

                            doc2 = new HtmlDocument();
                            doc2.LoadHtml(rooms[r].InnerHtml);


                            nodes = doc2.DocumentNode.SelectNodes("//th");
                            if (nodes != null)
                            {
                                continue;
                            }

                            bool parseFailed = false;

                            // --> plan_code, room_code
                            //
                            if (string.IsNullOrEmpty(rooms[r].Attributes["id"].Value))
                            {
                                parseFailed = true;
                            }
                            else
                            {
                                dummy = Regex.Match(rooms[r].Attributes["id"].Value, @"\d+[_]\d+").Value;

                                if (string.IsNullOrEmpty(dummy))
                                {
                                    parseFailed = true;
                                }
                                else
                                {
                                    item.PlanCode = dummy.Split('_').First();
                                    item.RoomCode = dummy;
                                }
                            }
                            
                            if (parseFailed)
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrRoomCode);
                                item.PlanCode = string.Empty;
                                item.RoomCode = string.Empty;

                                parseFailed = false;
                            }

                            // --> room_name, room_type, smoking_flg
                            //
                            nodes = doc2.DocumentNode.SelectNodes("//td[@class='col01']/a");

                            if (nodes == null)
                            {
                                // FOUND: 180413 (旧デザイン)
                                nodes = doc2.DocumentNode.SelectNodes("//td[@class='planname']/span[@class='planname']");
                            }
                            if (nodes == null)
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrRoomName);
                                item.RoomName = string.Empty;
                            }
                            else
                            {
                                node = nodes.First();
                                item.RoomName = node.InnerText.Trim();
                            }
                            
                            item.RoomType = GetRegularRoomType(item.RoomName);

                            if (item.RoomName.Contains("禁煙"))
                            {
                                item.SmokingFlag = 0;
                            }
                            //else if (item.RoomName.Contains("喫煙"))
                            //{
                            //    item.SmokingFlag = 1;
                            //}
                            else
                            {
                                item.SmokingFlag = 1;//9;
                            }

                            // --> ota_price, tax_excluded_price, tax, service_fee, tax_included_price
                            //
                            string tmp1 = string.Empty;
                            bool isSecondAttempt = false;

                            nodes = doc2.DocumentNode.SelectNodes("//td[@class='col03']/span[@class='total_price']");

                            if (nodes == null)
                            {
                                isSecondAttempt = true;

                                // FOUND: 180413 (旧デザイン)
                                nodes = doc2.DocumentNode.SelectNodes("//td");
                            }
                            if (nodes == null)
                            {
                                parseFailed = true;
                            }
                            else
                            {
                                if (isSecondAttempt)
                                {
                                    isSecondAttempt = false;

                                    tmp1 = nodes[2].InnerText;
                                }
                                else
                                {
                                    tmp1 = nodes.First().InnerText;
                                }
                            }

                            if (parseFailed)
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrPlanPrice);
                                item.OtaPrice = 0;

                                parseFailed = false;
                            }
                            else
                            {
                                if (tmp1.Contains(","))
                                {
                                    dummy = Regex.Match(tmp1, @"\d+(\,\d+)+").Value.Replace(",", string.Empty);
                                }
                                else
                                {
                                    dummy = Regex.Match(tmp1, @"\d+").Value;
                                }

                                if (string.IsNullOrEmpty(dummy))
                                {
                                    item.OtaPrice = 0;
                                }
                                else
                                {
                                    item.OtaPrice = int.Parse(dummy);
                                }
                            }

                            double tmpValue = item.OtaPrice / (1 + GetTaxRate(DateTime.Parse(_searchedDate)) / 100d);

                            item.TaxExcludedPrice = (int)Math.Round(tmpValue, 0, MidpointRounding.AwayFromZero);
                            item.Tax              = item.OtaPrice - item.TaxExcludedPrice;
                            item.ServiceFee       = 0;
                            item.TaxIncludedPrice = item.OtaPrice;

                            // --> plan_url
                            //
                            nodes = doc2.DocumentNode.SelectNodes("//td[@class='col04']/div[@class='detail_btn']/a");

                            if (nodes == null)
                            {
                                isSecondAttempt = true;

                                // FOUND: 180413 (旧デザイン)
                                nodes = doc2.DocumentNode.SelectNodes("//td[@class='planname']");
                            }
                            if (nodes == null)
                            {
                                parseFailed = true;
                            }
                            else
                            {
                                dummy = nodes.First().Attributes["onclick"].Value;

                                if (string.IsNullOrEmpty(dummy))
                                {
                                    parseFailed = true;
                                }
                                else
                                {
                                    string seed = "toplanlink(";
                                    int position = dummy.IndexOf(seed);

                                    if (position < 0)
                                    {
                                        parseFailed = true;
                                    }
                                    else
                                    {
                                        string[] tmp2 = dummy.Substring(position + seed.Length).Split('"');

                                        if (tmp2.Length != 13)
                                        {
                                            parseFailed = true;
                                        }
                                        else
                                        {
                                            item.PlanUrl = string.Format(_planUrlBase, tmp2[1], tmp2[3], tmp2[7], tmp2[9], _searchedDate.Replace("-", "/"), days, tmp2[5], string.Format("000{0}0", person));
                                        }
                                    }
                                }
                            }

                            if (parseFailed)
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrPlanUrl);
                                item.PlanUrl = string.Empty;

                                parseFailed = false;
                            }

                            // --> timer_id or realtime_id, plan_point, stock, search_date, crawl_date
                            //
                            item.CrawlerId    = _crawlerId;
                            item.PlanPoint    = 0;
                            item.RoomVacancy  = 0;
                            item.SearchedDate = _searchedDate;
                            item.CrawledDate  = _crawledDate;

                            _analysisResults.Add(item);
                        }
                    }
                }

                if (_analysisResults.Count < 1)
                {
                    NetworkManager.Instance.SendLog(accountId, LogType.INFO.ToString().ToLower(), string.Format(_msgNoResult, AgencyType.DIRECTIN.ToString().ToLower(), _crawlerId));
                }
                else
                {
                    string filename = string.Format("{4}{5}_{0}_{1}_{2}_{3}.ar", DateTime.Today.ToString("yyMMdd"),
                                                                                 AgencyType.DIRECTIN.ToString().ToLower(),
                                                                                 accountId,
                                                                                 hotelId,
                                                                                 filenameSplit[4].Substring(0, 1),
                                                                                 _crawlerId);
                    // WRITE RESULT TO TEXT
                    using (var sw = new StreamWriter(filename, false, Encoding.UTF8))
                    {
                        foreach (var item in _analysisResults)
                        {
                            int timer_id = 0;
                            int realtime_id = 0;

                            if (_isInstantCrawling)
                            {
                                realtime_id = item.CrawlerId;
                            }
                            else
                            {
                                timer_id = item.CrawlerId;
                            }

                            string line = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22}",
                                                        item.AccountId,
                                                        item.HotelId,
                                                        item.SmokingFlag,
                                                        item.MealFlag,
                                                        item.RoomType,
                                                        item.RoomName,
                                                        item.PlanPoint,
                                                        item.PlanCode,
                                                        item.RoomCode,
                                                        item.PlanUrl,
                                                        item.PlanName,
                                                        item.RoomVacancy,
                                                        item.OtaPrice,
                                                        item.TaxExcludedPrice,
                                                        item.Tax,
                                                        item.ServiceFee,
                                                        item.TaxIncludedPrice,
                                                        item.Person,
                                                        item.Days,
                                                        item.SearchedDate,
                                                        item.CrawledDate,
                                                        timer_id,
                                                        realtime_id);
                            sw.WriteLine(line);
                        }
                    }

                    // SEND FILE TO SERVER
                    int tried;
                    if (SendAnalysisResult(new FileInfo(filename), 3, out tried))
                    {
                        NetworkManager.Instance.SendLog(accountId, LogType.INFO.ToString().ToLower(), string.Format(_msgResultUploaded, filename, tried));
                    }
                    else
                    {
                        NetworkManager.Instance.SendLog(accountId, LogType.ERROR.ToString().ToLower(), string.Format(_msgDefaultError, AgencyType.DIRECTIN.ToString().ToLower(),
                                                                                                                                        _crawlerId,
                                                                                                                                        _msgErrorSendFile));
                        result = false;
                    }
                }
            }
            catch (Exception e)
            {
                NetworkManager.Instance.SendLog(accountId, LogType.ERROR.ToString().ToLower(), string.Format(_msgDefaultError, AgencyType.DIRECTIN.ToString().ToLower(),
                                                                                                                               _crawlerId,
                                                                                                                               e.Message));
                result = false;
            }

            return result;
        }
    }
}