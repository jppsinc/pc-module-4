﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using Psinc.Analyst.DataStructure;
using Psinc.Util;

namespace Psinc.Analyst
{
    /// <summary>
    /// ベストリザーブデータ分析器
    /// </summary>
    public class BestrsvAnalyser : Analyser
    {
        private readonly string _parseErrPlans            = "[bestrsv] PLAN IS MISSING";
        private readonly string _parseErrPlanUrl          = "[bestrsv] PLAN URL IS MISSING ({0}/{1})";
        private readonly string _parseErrPlanName         = "[bestrsv] PLAN NAME IS MISSING";
        private readonly string _parseErrPlanCode         = "[bestrsv] PLAN CODE VERIFICATION FAILED";
        private readonly string _parseErrPlanCodeVerifier = "[bestrsv] MAYBE THERE IS SOME CHANGE IN PLAN NAME URL";
        private readonly string _parseErrPlanPrice        = "[bestrsv] PRICE IS MISSING";
        private readonly string _parseErrPlanPoint        = "[bestrsv] PLAN POINT IS MISSING";
        private readonly string _parseErrRoomType         = "[bestrsv] ROOM TYPE IS MISSING";
        private readonly string _parseErrRoomName         = "[bestrsv] ROOM NAME IS MISSING";
        private readonly string _parseErrRoomCodeVerifier = "[bestrsv] MAYBE THERE IS SOME CHANGE IN ROOM NAME URL";
        private readonly string _parseErrRoomVacancy      = "[bestrsv] ROOM VACANCY IS MISSING";
        private readonly string _parseErrMealFlag         = "[bestrsv] MEAL FLAG IS MISSING";
        private readonly string _parseErrTaxIncluded      = "[bestrsv] TAX INCLUDED TAG IS MISSING";
        private readonly string _parseErrTaxExcluded      = "[bestrsv] TAX MAY BE EXCLUDED";


        /// <summary>
        /// Analyses target files.
        /// </summary>
        /// <param name="accountId">account ID</param>
        /// <param name="hotelId">hotel ID</param>
        /// <param name="targets">target file list</param>
        /// <returns>success or not</returns>
        public override bool DoAnalysis(int accountId, int hotelId, FileInfo[] targets)
        {
            bool result = true;

            try
            {
                HtmlNode node;
                HtmlDocument doc0, doc1, doc2;
                HtmlNodeCollection testBlock;

                // SET SHARED INFO.
                string[] filenameSplit = targets.First().Name.Split('_');

                _crawledDate = string.Format("20{0}-{1}-{2}", filenameSplit[5].Substring(0, 2), filenameSplit[5].Substring(2, 2), filenameSplit[5].Substring(4, 2));

                if (filenameSplit[3].Substring(0, 1).Equals("T"))
                {
                    _isInstantCrawling = false;
                }
                else
                {
                    _isInstantCrawling = true;
                }

                _crawlerId = int.Parse(filenameSplit[3].Substring(1));

                // DO ANALYSIS
                _analysisResults = new List<AnalysisResultBase>();

                foreach (var fi in targets)
                {
                    filenameSplit = fi.Name.Split('_');
                    _searchedDate = string.Format("20{0}-{1}-{2}", filenameSplit[4].Substring(0, 2), filenameSplit[4].Substring(2, 2), filenameSplit[4].Substring(4, 2));

                    _hotelId = int.Parse(filenameSplit[6]);

                    doc0 = new HtmlDocument();
                    doc0.Load(fi.FullName);

                    // If page has plan(s), 'test' is not null.
                    testBlock = doc0.DocumentNode.SelectNodes("//div[@class='hi-box advance']");

                    if (testBlock == null)
                    {
                        continue;
                    }

                    var plans = doc0.DocumentNode.SelectNodes("//div[@class='pi-box']");

                    if (plans == null)
                    {
                        NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrPlans);
                        continue;
                    }

                    string dummy = string.Empty;

                    for (int p = 0; p < plans.Count; p++)
                    {
                        doc1 = new HtmlDocument();
                        doc1.LoadHtml(plans[p].InnerHtml);

                        // --> plan_name
                        //
                        string planName = string.Empty;
                        string planCodeVerifier = string.Empty;

                        testBlock = doc1.DocumentNode.SelectNodes("//div[contains(@class, 'pi-head')]/div[@class='pi-name']/a");

                        if (testBlock == null)
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrPlanName);
                        }
                        else
                        {
                            node = testBlock.First();

                            planName = node.InnerText.Trim();

                            string[] test = node.Attributes["href"].Value.Trim().Split('/');

                            if (test.Length != 5)
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrPlanCodeVerifier);
                            }
                            else
                            {
                                planCodeVerifier = test[3];
                            }
                        }

                        // --> meal_flg
                        //
                        string mealFlag = string.Empty;

                        bool breakfast = false;
                        bool lunch     = false;
                        bool dinner    = false;

                        testBlock = doc1.DocumentNode.SelectNodes("//div[contains(@class, 'pi-head')]/div[@class='pi-icons']/img");

                        if (testBlock == null)
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrMealFlag);
                        }
                        else
                        {
                            node = testBlock.First();

                            dummy = node.Attributes["alt"].Value;

                            if (dummy.Contains("食事なし"))
                            {
                                // NOTHING TO DO
                            }
                            else
                            {
                                if (dummy.Contains("朝食"))
                                {
                                    breakfast = true;
                                }
                                if (dummy.Contains("昼食"))
                                {
                                    lunch = true;
                                }
                                if (dummy.Contains("夕食"))
                                {
                                    dinner = true;
                                }
                            }
                        }

                        mealFlag = GetRegularMealFlag(breakfast, lunch, dinner);

                        // VERIFICATION
                        string taxVerifier = string.Empty;

                        testBlock = doc1.DocumentNode.SelectNodes("//div[contains(@class, 'pi-head')]/div[@class='pi-icons']/span[@class='tax_included']/span");

                        if (testBlock == null)
                        {
                            NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrTaxIncluded);
                        }
                        else
                        {
                            node = testBlock.First();

                            taxVerifier = node.InnerText.Trim();
                        }

                        var rooms = doc1.DocumentNode.SelectNodes("//div[@class='ri-box gi-box border-bd']");

                        for (int r = 0; r < rooms.Count; r++)
                        {
                            AnalysisResultBase item = new AnalysisResultBase();
                            item.AccountId = accountId;
                            item.HotelId   = _hotelId;
                            item.PlanName  = planName;
                            item.MealFlag  = mealFlag;

                            doc2 = new HtmlDocument();
                            doc2.LoadHtml(rooms[r].InnerHtml);

                            // --> room_name, plan_code, room_code, smoking_flg
                            //
                            testBlock = doc2.DocumentNode.SelectNodes("//table/tr/td[@class='gi-expand']/div[@class='gi-name']/a");

                            if (testBlock == null)
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrRoomName);
                                item.RoomName = string.Empty;
                            }
                            else
                            {
                                node = testBlock.First();

                                item.RoomName = node.InnerText.Trim();

                                string[] test = node.Attributes["href"].Value.Trim().Split('/');

                                if (test.Length != 7)
                                {
                                    NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrRoomCodeVerifier);
                                }
                                else
                                {
                                    item.PlanCode = test[3];
                                    item.RoomCode = test[4];

                                    // VERIFICATION
                                    if (!item.PlanCode.Equals(planCodeVerifier))
                                    {
                                        NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrPlanCode);
                                    }
                                }
                            }

                            if (item.RoomName.Contains("禁煙"))
                            {
                                item.SmokingFlag = 0;
                            }
                            //else if (item.RoomName.Contains("喫煙"))
                            //{
                            //    item.SmokingFlag = 1;
                            //}
                            else
                            {
                                item.SmokingFlag = 1;//9;
                            }

                            // --> plan_point
                            //
                            testBlock = doc2.DocumentNode.SelectNodes("//table/tr/td[@class='gi-advantage']/div[@class='gi-point']/img");

                            if (testBlock == null)
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrPlanPoint);
                                item.PlanPoint = 0;
                            }
                            else
                            {
                                node = testBlock.First();

                                dummy = Regex.Match(node.Attributes["alt"].Value, @"\d+").Value;

                                item.PlanPoint = int.Parse(dummy);
                            }

                            // --> ota_price, tax_excluded_price, tax, service_fee, tax_included_price
                            //
                            bool includeTax = false;

                            if (!string.IsNullOrEmpty(taxVerifier))
                            {
                                if (taxVerifier.Contains("税込"))
                                {
                                    includeTax = true;
                                }
                                else
                                {
                                    NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrTaxExcluded);
                                }
                            }

                            testBlock = doc2.DocumentNode.SelectNodes("//table/tr/td[@class='gi-price gi-pricefix']/span[@class='gi-currency']");

                            if (testBlock == null)
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrPlanPrice);
                                item.OtaPrice = 0;
                            }
                            else
                            {
                                if (testBlock.Count != 2)
                                {
                                    NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrPlanPrice);
                                }
                                else
                                {
                                    string tmp = testBlock[1].InnerText;

                                    if (tmp.Contains(","))
                                    {
                                        dummy = Regex.Match(tmp, @"\d+(\,\d+)+").Value.Replace(",", string.Empty);
                                    }
                                    else
                                    {
                                        dummy = Regex.Match(tmp, @"\d+").Value;
                                    }

                                    if (includeTax)
                                    {
                                        item.OtaPrice = int.Parse(dummy);
                                    }
                                    else
                                    {
                                        item.OtaPrice = 0;
                                    }
                                }
                            }

                            double tmpValue = item.OtaPrice / (1 + GetTaxRate(DateTime.Parse(_searchedDate)) / 100d);

                            item.TaxExcludedPrice = (int)Math.Round(tmpValue, 0, MidpointRounding.AwayFromZero);
                            item.Tax              = item.OtaPrice - item.TaxExcludedPrice;
                            item.ServiceFee       = 0;
                            item.TaxIncludedPrice = item.OtaPrice;
                            
                            // --> plan_url, adult_cnt, stay_days, stock
                            //
                            testBlock = doc2.DocumentNode.SelectNodes("//table/tr/td[@class='gi-action']/div[@class='btn-b04-098-s']/a[@class='btnimg']");

                            if (testBlock == null)
                            {
                                bool isError = false;

                                testBlock = doc2.DocumentNode.SelectNodes("//table/tr/td[@class='gi-action']/div[@class='btn-b04-098-s']");

                                if (testBlock == null)
                                {
                                    isError = true;
                                }
                                else
                                {
                                    // FOUND: 190729
                                    dummy = testBlock.First().InnerHtml;

                                    if (dummy.Contains("soldout") || dummy.Contains("満室"))
                                    {
                                        continue;
                                    }
                                    else
                                    {
                                        isError = true;
                                    }
                                }

                                if (isError)
                                {
                                    NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), string.Format(_parseErrPlanUrl, accountId, hotelId));
                                    item.PlanUrl     = string.Empty;
                                    item.RoomVacancy = 0;
                                }
                            }
                            else
                            {
                                node = testBlock.First();

                                item.PlanUrl = node.Attributes["href"].Value.Trim();

                                var tmpStr = item.PlanUrl.Split(new char[] { '/', '&' }, StringSplitOptions.RemoveEmptyEntries);

                                if (tmpStr.Length > 9)
                                {
                                    item.Person = int.Parse(tmpStr[9].Split('=')[1]);
                                    item.Days   = int.Parse(tmpStr[7]);
                                }

                                var testNode = node.FirstChild;

                                if (testNode == null)
                                {
                                    NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrRoomVacancy);
                                    item.RoomVacancy = 0;
                                }
                                else
                                {
                                    string tmp = testNode.Attributes["alt"].Value;

                                    if (tmp.Contains("あと") && tmp.Contains("室"))
                                    {
                                        dummy = Regex.Match(tmp, @"\d+").Value;

                                        item.RoomVacancy = int.Parse(dummy);
                                    }
                                    else
                                    {
                                        item.RoomVacancy = 0;
                                    }
                                }
                            }

                            // --> room_type
                            //
                            testBlock = doc2.DocumentNode.SelectNodes("//div[@class='gi-icons']");

                            if (testBlock == null)
                            {
                                NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrRoomType);
                                item.RoomType = GetRegularRoomType(item.RoomName);
                            }
                            else
                            {
                                node = testBlock.First();

                                if (node.ChildNodes.Count > 2)
                                {
                                    dummy = node.ChildNodes[0].Attributes["alt"].Value; // ROOM TYPE ICON

                                    if (!string.IsNullOrEmpty(dummy))
                                    {
                                        item.RoomType = GetRegularRoomType(dummy);
                                    }
                                    else
                                    {
                                        NetworkManager.Instance.SendLog(0, LogType.SYS.ToString().ToLower(), _parseErrRoomType);
                                    }
                                }
                            }

                            // --> timer_id or realtime_id, search_date, crawl_date
                            //
                            item.CrawlerId    = _crawlerId;
                            item.SearchedDate = _searchedDate;
                            item.CrawledDate  = _crawledDate;

                            _analysisResults.Add(item);
                        }
                    }
                }

                if (_analysisResults.Count < 1)
                {
                    NetworkManager.Instance.SendLog(accountId, LogType.INFO.ToString().ToLower(), string.Format(_msgNoResult, AgencyType.BESTRSV.ToString().ToLower(), _crawlerId));
                }
                else
                {
                    string filename = string.Format("{4}{5}_{0}_{1}_{2}_{3}.ar", DateTime.Today.ToString("yyMMdd"),
                                                                                 AgencyType.BESTRSV.ToString().ToLower(),
                                                                                 accountId,
                                                                                 hotelId,
                                                                                 filenameSplit[3].Substring(0, 1),
                                                                                 _crawlerId);
                    // WRITE RESULT TO TEXT
                    using (var sw = new StreamWriter(filename, false, Encoding.UTF8))
                    {
                        foreach (var item in _analysisResults)
                        {
                            int timer_id = 0;
                            int realtime_id = 0;

                            if (_isInstantCrawling)
                            {
                                realtime_id = item.CrawlerId;
                            }
                            else
                            {
                                timer_id = item.CrawlerId;
                            }

                            string line = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22}",
                                                        item.AccountId,
                                                        item.HotelId,
                                                        item.SmokingFlag,
                                                        item.MealFlag,
                                                        item.RoomType,
                                                        item.RoomName,
                                                        item.PlanPoint,
                                                        item.PlanCode,
                                                        item.RoomCode,
                                                        item.PlanUrl,
                                                        item.PlanName,
                                                        item.RoomVacancy,
                                                        item.OtaPrice,
                                                        item.TaxExcludedPrice,
                                                        item.Tax,
                                                        item.ServiceFee,
                                                        item.TaxIncludedPrice,
                                                        item.Person,
                                                        item.Days,
                                                        item.SearchedDate,
                                                        item.CrawledDate,
                                                        timer_id,
                                                        realtime_id);
                            sw.WriteLine(line);
                        }
                    }

                    // SEND FILE TO SERVER
                    int tried;
                    if (SendAnalysisResult(new FileInfo(filename), 3, out tried))
                    {
                        NetworkManager.Instance.SendLog(accountId, LogType.INFO.ToString().ToLower(), string.Format(_msgResultUploaded, filename, tried));
                    }
                    else
                    {
                        NetworkManager.Instance.SendLog(accountId, LogType.ERROR.ToString().ToLower(), string.Format(_msgDefaultError, AgencyType.BESTRSV.ToString().ToLower(),
                                                                                                                                        _crawlerId,
                                                                                                                                        _msgErrorSendFile));
                        result = false;
                    }
                }
            }
            catch (Exception e)
            {
                NetworkManager.Instance.SendLog(accountId, LogType.ERROR.ToString().ToLower(), string.Format(_msgDefaultError, AgencyType.BESTRSV.ToString().ToLower(),
                                                                                                                               _crawlerId,
                                                                                                                               e.Message));
                result = false;
            }

            return result;
        }
    }
}