﻿using System;
using System.Collections.Generic;
using System.IO;
using Psinc.Analyst.DataStructure;

namespace Psinc.Analyst
{
    /// <summary>
    /// Class for analyse crawled data.
    /// </summary>
    public class AnalysisManager
    {   // Created: 2017.09.07

        private static AnalysisManager _instance = null;

        private Dictionary<string, List<FileInfo>> _arrangedTargetInfo;
        private string _errorMessage = string.Empty;


        /// <summary>
        /// Private constructor.
        /// </summary>
        private AnalysisManager()
        {
        }

        /// <summary>
        /// Gets an instance of 'AnalysisManager'.
        /// </summary>
        public static AnalysisManager Instance
        {
            get
            {
                _instance = _instance ?? new AnalysisManager();
                return _instance;
            }
        }

        /// <summary>
        /// Gets a message of last occurred error.
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return _errorMessage;
            }
        }

        /// <summary>
        /// Starts to analyse files in passed folder.
        /// </summary>
        /// <param name="accountId">identification number of account</param>
        /// <param name="hotelId">identification number of hotel</param>
        /// <param name="di">information of target folder</param>
        /// <returns>success or not</returns>
        public bool StartAnalysis(int accountId, int hotelId, DirectoryInfo di)
        {
            bool result = true;

            ArrangeTargetDirectory(di);

            foreach (KeyValuePair<string, List<FileInfo>> item in _arrangedTargetInfo)
            {
                if (!Analyser.AnalysisFactory(GetAgencyTypeFromString(item.Key)).DoAnalysis(accountId, hotelId, item.Value.ToArray()))
                {
                    _errorMessage = "[Analyst] PROCESSING ERROR";
                }
            }

            return result;
        }

        private bool ArrangeTargetDirectory(DirectoryInfo di)
        {
            // Arranges an analysis directory by each OTA.

            bool result = true;
            string agencyType = string.Empty;

            _arrangedTargetInfo = new Dictionary<string, List<FileInfo>>();

            foreach (var fi in di.GetFiles())
            {
                if (fi.Name.StartsWith(AgencyType.RAKUTEN.ToString().ToLower()))
                {
                    agencyType = AgencyType.RAKUTEN.ToString().ToLower();
                }
                else if (fi.Name.StartsWith(AgencyType.JALAN.ToString().ToLower()))
                {
                    agencyType = AgencyType.JALAN.ToString().ToLower();
                }
                else if (fi.Name.StartsWith(AgencyType.IKYU.ToString().ToLower()))
                {
                    if (fi.Name.Contains(AgencyType.IKYUBIZ.ToString().ToLower()))
                    {
                        agencyType = AgencyType.IKYUBIZ.ToString().ToLower();
                    }
                    else if (fi.Name.Contains(AgencyType.IKYUCAZ.ToString().ToLower()))
                    {
                        agencyType = AgencyType.IKYUCAZ.ToString().ToLower();
                    }
                    else
                    {
                        agencyType = AgencyType.IKYU.ToString().ToLower();
                    }
                }
                else if (fi.Name.StartsWith(AgencyType.RURUBU.ToString().ToLower()))
                {
                    agencyType = AgencyType.RURUBU.ToString().ToLower();
                }
                else if (fi.Name.StartsWith(AgencyType.JTB.ToString().ToLower()))
                {
                    agencyType = AgencyType.JTB.ToString().ToLower();
                }
                else if (fi.Name.StartsWith(AgencyType.BESTRSV.ToString().ToLower()))
                {
                    agencyType = AgencyType.BESTRSV.ToString().ToLower();
                }
                else if (fi.Name.StartsWith(AgencyType.AGODA.ToString().ToLower()))
                {
                    agencyType = AgencyType.AGODA.ToString().ToLower();
                }
                else if (fi.Name.StartsWith(AgencyType.BOOKING.ToString().ToLower()))
                {
                    agencyType = AgencyType.BOOKING.ToString().ToLower();
                }
                else if (fi.Name.StartsWith(AgencyType.EXPEDIA.ToString().ToLower()))
                {
                    agencyType = AgencyType.EXPEDIA.ToString().ToLower();
                }
                else if (fi.Name.StartsWith(AgencyType.DIRECTIN.ToString().ToLower()))
                {
                    agencyType = AgencyType.DIRECTIN.ToString().ToLower();
                }

                if (!string.IsNullOrEmpty(agencyType))
                {
                    if (!_arrangedTargetInfo.ContainsKey(agencyType))
                    {
                        _arrangedTargetInfo.Add(agencyType, new List<FileInfo>());
                    }

                    _arrangedTargetInfo[agencyType].Add(fi);

                    agencyType = string.Empty;
                }
            }

            return result;
        }

        private AgencyType GetAgencyTypeFromString(string target)
        {
            // Returns an 'AgencyType' that same with passed string.

            if (target.Equals(AgencyType.RAKUTEN.ToString().ToLower()))
            {
                return AgencyType.RAKUTEN;
            }
            else if (target.Equals(AgencyType.JALAN.ToString().ToLower()))
            {
                return AgencyType.JALAN;
            }
            else if (target.Equals(AgencyType.IKYU.ToString().ToLower()))
            {
                return AgencyType.IKYU;
            }
            else if (target.Equals(AgencyType.IKYUBIZ.ToString().ToLower()))
            {
                return AgencyType.IKYUBIZ;
            }
            else if (target.Equals(AgencyType.IKYUCAZ.ToString().ToLower()))
            {
                return AgencyType.IKYUCAZ;
            }
            else if (target.Equals(AgencyType.RURUBU.ToString().ToLower()))
            {
                return AgencyType.RURUBU;
            }
            else if (target.Equals(AgencyType.JTB.ToString().ToLower()))
            {
                return AgencyType.JTB;
            }
            else if (target.Equals(AgencyType.BESTRSV.ToString().ToLower()))
            {
                return AgencyType.BESTRSV;
            }
            else if (target.Equals(AgencyType.AGODA.ToString().ToLower()))
            {
                return AgencyType.AGODA;
            }
            else if (target.Equals(AgencyType.BOOKING.ToString().ToLower()))
            {
                return AgencyType.BOOKING;
            }
            else if (target.Equals(AgencyType.EXPEDIA.ToString().ToLower()))
            {
                return AgencyType.EXPEDIA;
            }
            else if (target.Equals(AgencyType.DIRECTIN.ToString().ToLower()))
            {
                return AgencyType.DIRECTIN;
            }

            throw new NotSupportedException("[Analyst] UNKNOWN TYPE");
        }
    }
}