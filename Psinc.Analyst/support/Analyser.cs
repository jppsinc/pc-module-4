﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using Psinc.Analyst.DataStructure;
using Psinc.Util;

namespace Psinc.Analyst
{
    /// <summary>
    /// Abstract class for analyser.
    /// </summary>
    public abstract class Analyser
    {
        internal List<AnalysisResultBase> _analysisResults;

        internal bool _isInstantCrawling = true;
        internal int  _crawlerId = 0;
        internal int  _hotelId = 0;
        internal string _searchedDate = string.Empty; // yyyy-MM-dd
        internal string _crawledDate  = string.Empty; // yyyy-MM-dd

        internal readonly string _msgNoResult        = "NO ANALYSED RESULT: {0} ({1})";
        internal readonly string _msgResultUploaded  = "RESULTS UPLOADED: {0} ({1})";
        internal readonly string _msgDefaultError    = "ERROR OCCURRED (A): {0} ({1}) / {2}";
        internal readonly string _msgErrorSendFile   = "WHILE SEND";


        /// <summary>
        /// Analyses target files.
        /// </summary>
        /// <param name="accountId">account ID</param>
        /// <param name="hotelId">hotel ID</param>
        /// <param name="targets">target file list</param>
        /// <returns>success or not</returns>
        public abstract bool DoAnalysis(int accountId, int hotelId, FileInfo[] targets);

        /// <summary>
        /// Returns an analyser that matched with passed type.
        /// </summary>
        /// <param name="type">type</param>
        /// <returns>analyser</returns>
        public static Analyser AnalysisFactory(AgencyType type)
        {
            switch (type)
            {
                case AgencyType.RAKUTEN:
                    return new RakutenAnalyser();

                case AgencyType.JALAN:
                    return new JalanAnalyser();

                case AgencyType.IKYU:
                    return new IkyuAnalyser(AgencyType.IKYU);

                case AgencyType.IKYUBIZ:
                    return new IkyuAnalyser(AgencyType.IKYUBIZ);

                case AgencyType.IKYUCAZ:
                    return new IkyuAnalyser(AgencyType.IKYUCAZ);

                case AgencyType.RURUBU:
                    return new RurubuAnalyser();

                case AgencyType.JTB:
                    return new JtbAnalyser();

                case AgencyType.BESTRSV:
                    return new BestrsvAnalyser();

                case AgencyType.AGODA:
                    return new AgodaAnalyser();

                case AgencyType.BOOKING:
                    return new BookingAnalyser();

                case AgencyType.EXPEDIA:
                    return new ExpediaAnalyser();

                case AgencyType.DIRECTIN:
                    return new DirectinAnalyser();
            }

            throw new NotSupportedException("[Analyst] UNKNOWN TYPE");
        }

        internal string GetRegularRoomType(string target)
        {
            string result = string.Empty;

            if (target.Contains("スイート"))
            {
                result = RoomType.SWEET.ToString();
            }
            else if (target.Contains("シングル"))
            {
                if (target.Contains("ベッド") && target.Contains("台"))
                {
                    string tmp = Regex.Match(target, @"\d+").Value;

                    if (!string.IsNullOrEmpty(tmp) && int.Parse(tmp) > 1)
                    {
                        result = RoomType.TWIN.ToString();
                    }
                    else
                    {
                        result = RoomType.SINGLE.ToString();
                    }
                }
                else
                {
                    result = RoomType.SINGLE.ToString();
                }
            }
            else if (target.Contains("セミダブル"))
            {
                result = RoomType.SEMI_DOUBLE.ToString().Replace("_", "-");
            }
            else if (target.Contains("ダブル") || target.IndexOf("double", StringComparison.OrdinalIgnoreCase) >= 0)
            {
                result = RoomType.DOUBLE.ToString();
            }
            else if (target.Contains("和洋室"))
            {
                result = RoomType.JP_WESTERN_STYLE.ToString().Replace("_", "-");
            }
            else if (target.Contains("ツイン") || target.Contains("洋室") || target.IndexOf("twin", StringComparison.OrdinalIgnoreCase) >= 0)
            {
                result = RoomType.TWIN.ToString();
            }
            else if (target.Contains("トリプル") || target.IndexOf("triple", StringComparison.OrdinalIgnoreCase) >= 0)
            {
                result = RoomType.TRIPLE.ToString();
            }
            else if (target.Contains("4ベッド") || target.Contains("フォース"))
            {
                result = "4BED";
            }
            else if (target.Contains("特別室"))
            {
                result = RoomType.SPECIAL.ToString();
            }
            else if (target.Contains("和室"))
            {
                result = RoomType.JP_STYLE.ToString().Replace("_", "-");
            }
            else if (target.Contains("その他"))
            {
                result = RoomType.OTHER.ToString();
            }
            else
            {
                result = RoomType.NONE.ToString();
            }

            return result;
        }

        internal string GetRegularMealFlag(bool breakfast, bool lunch, bool dinner)
        {
            string result = string.Empty;

            if (breakfast)
            {
                result = "1";
            }
            else
            {
                result = "0";
            }

            if (lunch)
            {
                result += "1";
            }
            else
            {
                result += "0";
            }

            if (dinner)
            {
                result += "1";
            }
            else
            {
                result += "0";
            }

            return result;
        }

        internal int GetTaxRate(DateTime today) // HARD-CODED
        {
            int rate = 8;

            if (today >= new DateTime(2019, 10, 1, 0, 0, 0))
            {
                rate = 10;
            }            

            return rate;
        }

        internal bool SendAnalysisResult(FileInfo target, int repeat, out int tried) // HARD-CODED
        {
            tried = 0;

            bool result = true;

            var src = NetworkManager.Instance.UrlFormat.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);

            string httpServer = string.Format("{0}//{1}/upload.php", src[0], src[1]);

            int threadSleepInterval = 1500; // 1.5 SEC.

            if (!Tools.TransferFileToServer(httpServer, target.DirectoryName, target.Name))
            {
                Thread.Sleep(threadSleepInterval);

                for (int i = 0; i < repeat; i++)
                {
                    if (Tools.TransferFileToServer(httpServer, target.DirectoryName, target.Name))
                    {
                        tried = i + 1;
                        result = true;
                        break;
                    }
                    else
                    {
                        result = false;
                        Thread.Sleep(threadSleepInterval);
                    }
                }
            }

            return result;
        }

        internal string RemoveNewLine(string input)
        {
            return input.Replace("\r\n", string.Empty).Replace("\r", string.Empty).Replace("\n", string.Empty).Trim();
        }

        internal string ReplaceHtmlSpecialCharacters(string input)
        {
            string output = input;

            if (input.Contains("&lt;"))
            {
                output = output.Replace("&lt;", "<");
            }
            if (input.Contains("&gt;"))
            {
                output = output.Replace("&gt;", ">");
            }
            if (input.Contains("&quot;"))
            {
                output = output.Replace("&quot;", "\"");
            }
            if (input.Contains("&amp;"))
            {
                output = output.Replace("&amp;", "&");
            }
            if (input.Contains("&yen;"))
            {
                output = output.Replace("&yen;", "¥");
            }
            if (input.Contains("&brvbar;"))
            {
                output = output.Replace("&brvbar;", "¦");
            }
            if (input.Contains("&copy;"))
            {
                output = output.Replace("&copy;", "©");
            }
            if (input.Contains("&reg;"))
            {
                output = output.Replace("&reg;", "®");
            }
            if (input.Contains("&deg;"))
            {
                output = output.Replace("&deg;", "°");
            }
            if (input.Contains("&plusmn;"))
            {
                output = output.Replace("&plusmn;", "±");
            }
            if (input.Contains("&times;"))
            {
                output = output.Replace("&times;", "×");
            }
            if (input.Contains("&divide;"))
            {
                output = output.Replace("&divide;", "÷");
            }
            if (input.Contains("&acute;"))
            {
                output = output.Replace("&acute;", "´");
            }
            if (input.Contains("&micro;"))
            {
                output = output.Replace("&micro;", "µ");
            }
            if (input.Contains("&middot;"))
            {
                output = output.Replace("&middot;", "·");
            }

            return output;
        }
    }
}