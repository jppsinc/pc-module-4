﻿namespace Psinc.Analyst.DataStructure
{   // Created: 2017.07.06

    /// <summary>
    /// 媒体タイプ
    /// </summary>
    public enum AgencyType
    {
        RAKUTEN,

        JALAN,

        IKYU,

        IKYUBIZ,

        IKYUCAZ,

        RURUBU,

        JTB,

        BESTRSV,

        AGODA,

        BOOKING,

        EXPEDIA,

        DIRECTIN
    }

    /// <summary>
    /// ルームタイプ
    /// </summary>
    public enum RoomType
    {
        NONE,

        SINGLE,

        SEMI_DOUBLE,

        DOUBLE,

        TWIN,

        TRIPLE,

        FOUR_BED,

        SWEET,

        SPECIAL,

        JP_STYLE,

        JP_WESTERN_STYLE,

        OTHER
    }

    /// <summary>
    /// データ分析結果ベース
    /// </summary>
    public struct AnalysisResultBase
    {
        public int AccountId;

        public int HotelId;

        public int CrawlerId;

        public string PlanUrl;

        public string PlanName;

        public string PlanCode;

        public int PlanPoint;

        public string RoomType;

        public string RoomName;

        public string RoomCode;

        public int RoomVacancy;

        public int OtaPrice;

        public int TaxExcludedPrice;

        public int Tax;

        public int ServiceFee;

        public int TaxIncludedPrice;

        public string MealFlag;

        public int SmokingFlag;     // 0, 1, 9

        public int Person;

        public int Days;

        public string SearchedDate; // yyyy-MM-dd

        public string CrawledDate;  // yyyy-MM-dd
    }

    /// <summary>
    /// ログ種類
    /// (参考：Psinc.Crawler.DataStructure)
    /// </summary>
    public enum LogType
    {
        INFO,

        ERROR,

        PULSE,

        SYS
    }
}