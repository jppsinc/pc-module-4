﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using Psinc.Util.DataStructure;

namespace Psinc.Util
{
    /// <summary>
    /// Class for jobs that uses network.
    /// </summary>
    public class NetworkManager
    {
        private static NetworkManager _instance = null;

        private string _urlFormat = string.Empty;
        private ProxySetting _proxySetting;

        private string _errorMessage = string.Empty;

        private readonly string _msgNoUrl = "No URL. #PU1413";
        private readonly string _msgRequireProxySetting = "Proxy settings required. #PU1413";

        private readonly string _paramLog = "account_id={0}&type={1}&msg={2}";

        private readonly string _predefinedContentsType = "application/x-www-form-urlencoded; charset=UTF-8";


        /// <summary>
        /// Private constructor.
        /// </summary>
        private NetworkManager()
        {
            // Initial value for '_proxySetting' variable.
            _proxySetting = new ProxySetting();
            _proxySetting.Ip = null;
            _proxySetting.Port = -1;
        }

        /// <summary>
        /// Gets an instance of 'NetworkManager'.
        /// </summary>
        public static NetworkManager Instance
        {
            get
            {
                _instance = _instance ?? new NetworkManager();
                return _instance;
            }
        }

        #region :: PROPERTIES

        /// <summary>
        /// Gets or sets a format of URL.
        /// </summary>
        public string UrlFormat
        {
            get
            {
                return _urlFormat;
            }

            set
            {
                _urlFormat = value;
            }
        }

        /// <summary>
        /// Gets or sets a proxy setting to use.
        /// </summary>
        public ProxySetting ProxySetting
        {
            get
            {
                return _proxySetting;
            }
            set
            {
                _proxySetting = value;
            }
        }

        /// <summary>
        /// Gets a message of last occurred error.
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return _errorMessage;
            }
        }

        #endregion

        #region :: METHODS

        /// <summary>
        /// Gets a response of specified URL (GET). Can send parameters.
        /// </summary>
        /// <param name="url">URL that want to get a response</param>
        /// <param name="response">response data</param>
        /// <param name="parameters">multiple parameters</param>
        /// <returns>success or not</returns>
        public bool GetUrlResponse(string url, out string response, params string[] parameters)
        {
            bool result = true;

            response = string.Empty;

            if (string.IsNullOrEmpty(url))
            {
                _errorMessage = _msgNoUrl;
                result = false;
            }
            else if (_proxySetting.InUse && _proxySetting.Ip == null && _proxySetting.Port == -1)
            {
                _errorMessage = _msgRequireProxySetting;
                result = false;
            }
            else
            {
                try
                {
                    string modifiedUrl = url;

                    for (int i = 0; i < parameters.Length; i++)
                    {
                        modifiedUrl += "/";
                        modifiedUrl += parameters[i];
                    }

                    HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(modifiedUrl);

                    WebProxy proxy = null;

                    if (_proxySetting.InUse)
                    {
                        proxy = new WebProxy(_proxySetting.Ip, _proxySetting.Port);
                        webReq.Proxy = proxy;
                    }

                    webReq.Method = WebRequestMethods.Http.Get;
                    webReq.ContentType = _predefinedContentsType;

                    HttpWebResponse webRes = (HttpWebResponse)webReq.GetResponse();

                    using (var responseStream = webRes.GetResponseStream())
                    {
                        using (var sr = new StreamReader(responseStream, Encoding.UTF8))
                        {
                            response = sr.ReadToEnd();
                        }
                    }
                    webRes.Close();
                }
                catch (Exception ex)
                {
                    _errorMessage = ex.Message;
                    result = false;
                }
            }

            return result;
        }

        /// <summary>
        /// Gets a response of specified URL with parameters (POST).
        /// </summary>
        /// <param name="url">URL that want to get a response</param>
        /// <param name="parameters">string of parameters</param>
        /// <param name="response">response data</param>
        /// <returns>success or not</returns>
        public bool GetUrlResponse(string url, string parameters, out string response)
        {
            bool result = true;

            response = string.Empty;

            if (string.IsNullOrEmpty(url))
            {
                _errorMessage = _msgNoUrl;
                result = false;
            }
            else if (_proxySetting.InUse && _proxySetting.Ip == null && _proxySetting.Port == -1)
            {
                _errorMessage = _msgRequireProxySetting;
                result = false;
            }
            else
            {
                try
                {
                    byte[] paramsByte = Encoding.UTF8.GetBytes(parameters);

                    HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(url);

                    WebProxy proxy = null;

                    if (_proxySetting.InUse)
                    {
                        proxy = new WebProxy(_proxySetting.Ip, _proxySetting.Port);
                        webReq.Proxy = proxy;
                    }

                    webReq.Method = WebRequestMethods.Http.Post;
                    webReq.ContentType = _predefinedContentsType;
                    webReq.ContentLength = paramsByte.Length;

                    Stream requestStream = webReq.GetRequestStream();
                    requestStream.Write(paramsByte, 0, paramsByte.Length);
                    requestStream.Close();

                    HttpWebResponse webRes = (HttpWebResponse)webReq.GetResponse();

                    using (var responseStream = webRes.GetResponseStream())
                    {
                        using (var sr = new StreamReader(responseStream, Encoding.UTF8))
                        {
                            response = sr.ReadToEnd();
                        }
                    }
                    webRes.Close();
                }
                catch (Exception ex)
                {
                    _errorMessage = ex.Message;
                    result = false;
                }
            }

            return result;
        }

        /// <summary>
        /// Gets a response of specified URL (GET). Can send parameters and extra sentence.
        /// </summary>
        /// <param name="url">URL that want to get a response</param>
        /// <param name="response">response data</param>
        /// <param name="extraSentence">extra sentence that placed in tail of URL</param>
        /// <param name="parameters">multiple parameters</param>
        /// <returns>success or not</returns>
        public bool GetUrlResponseWithTail(string url, out string response, string extraSentence, params string[] parameters)
        {
            bool result = true;

            response = string.Empty;

            if (string.IsNullOrEmpty(url))
            {
                _errorMessage = _msgNoUrl;
                result = false;
            }
            else if (_proxySetting.InUse && _proxySetting.Ip == null && _proxySetting.Port == -1)
            {
                _errorMessage = _msgRequireProxySetting;
                result = false;
            }
            else
            {
                try
                {
                    string modifiedUrl = url;

                    for (int i = 0; i < parameters.Length; i++)
                    {
                        modifiedUrl += "/";
                        modifiedUrl += parameters[i];
                    }

                    if (!string.IsNullOrEmpty(extraSentence))
                    {
                        modifiedUrl += extraSentence;
                    }

                    HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(modifiedUrl);

                    WebProxy proxy = null;

                    if (_proxySetting.InUse)
                    {
                        proxy = new WebProxy(_proxySetting.Ip, _proxySetting.Port);
                        webReq.Proxy = proxy;
                    }

                    webReq.Method = WebRequestMethods.Http.Get;
                    webReq.ContentType = _predefinedContentsType;

                    HttpWebResponse webRes = (HttpWebResponse)webReq.GetResponse();

                    using (var responseStream = webRes.GetResponseStream())
                    {
                        using (var sr = new StreamReader(responseStream, Encoding.UTF8))
                        {
                            response = sr.ReadToEnd();
                        }
                    }
                    webRes.Close();
                }
                catch (Exception ex)
                {
                    _errorMessage = ex.Message;
                    result = false;
                }
            }

            return result;
        }

        /// <summary>
        /// Sends a log to server. Use for 'RepChecker PC Module (x86)'. [HARD-CODED]
        /// </summary>
        /// <param name="accountId">account ID</param>
        /// <param name="type">type of log</param>
        /// <param name="message">contents of log</param>
        /// <returns>success or not</returns>
        public bool SendLog(int accountId, string type, string message)
        {
            bool result = true;

            string garbage;

            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(message))
            {
                result = false;
            }
            else
            {
                int repeat = 2;
                int threadSleepInterval = 1000; // 1 SEC.

                result = GetUrlResponse(string.Format(_urlFormat, "log"), string.Format(_paramLog, accountId, type, message), out garbage);

                if (result == false || garbage.Equals("err"))
                {
                    Thread.Sleep(threadSleepInterval);

                    message += " (R)";

                    // Repeats n-times if failed.
                    for (int i = 0; i < repeat; i++)
                    {
                        result = GetUrlResponse(string.Format(_urlFormat, "log"), string.Format(_paramLog, accountId, type, message), out garbage);

                        if (result == false || garbage.Equals("err"))
                        {
                            Thread.Sleep(threadSleepInterval);
                        }
                        else
                        {
                            result = true;
                            break;
                        }
                    }
                }
            }

            return result;
        }

        #endregion
    }
}