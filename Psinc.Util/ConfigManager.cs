﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using Psinc.Util.DataStructure;

namespace Psinc.Util
{
    /// <summary>
    /// Class for managing configurations using .ini file.
    /// </summary>
    public class ConfigManager
    {
        private static ConfigManager _instance = null;

        private string _filename = "config.ini";
        private string _fullpath = string.Empty;

        private MultiKeyDictionary<SectionNames, string, object> _configs = null;

        private int _configSize = 200;

        [DllImport("kernel32.dll")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        [DllImport("kernel32.dll")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);


        /// <summary>
        /// Private constructor.
        /// </summary>
        private ConfigManager()
        {
            _configs = new MultiKeyDictionary<SectionNames, string, object>();
            _fullpath = Path.Combine(Environment.CurrentDirectory, _filename);
        }

        /// <summary>
        /// Get an instance of 'ConfigManager'.
        /// </summary>
        public static ConfigManager Instance
        {
            get
            {
                _instance = _instance ?? new ConfigManager();
                return _instance;
            }
        }


        /// <summary>
        /// Load a configuration file from local.
        /// </summary>
        public void LoadConfigs()
        {
            foreach (string sn in Enum.GetNames(typeof(SectionNames)))
            {
                SectionNames sectionName = (SectionNames)Enum.Parse(typeof(SectionNames), sn);

                DisplayAttribute sectionAttrib = typeof(SectionNames).GetMember(sn).First().GetCustomAttribute<DisplayAttribute>();

                if (!_configs.ContainsKey(sectionName))
                {
                    _configs.Add(sectionName, new Dictionary<string, object>());
                }

                Type keyType = Type.GetType(string.Format("Psinc.Util.DataStructure.{0}", sectionName));

                foreach (string kn in Enum.GetNames(keyType))
                {
                    DisplayAttribute keyAttrib = keyType.GetMember(kn).First().GetCustomAttribute<DisplayAttribute>();

                    string value = ReadConfigValue(sectionAttrib.Name, kn);

                    if (!_configs[sectionName].ContainsKey(kn))
                    {
                        if (!string.IsNullOrEmpty(value))
                        {
                            _configs[sectionName].Add(kn, value);
                        }
                        else
                        {
                            _configs[sectionName].Add(kn, keyAttrib.Description);
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(value))
                        {
                            _configs[sectionName][kn] = value;
                        }
                        else
                        {
                            _configs[sectionName][kn] = keyAttrib.Description;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Save configurations to a file on local.
        /// </summary>
        public void SaveConfigs()
        {
            for (int i = 0; i < _configs.Count; i++)
            {
                for (int j = 0; j < _configs.ElementAt(i).Value.Count; j++)
                {
                    string sectionName = _configs.ElementAt(i).Key.ToString();

                    DisplayAttribute keyAttirb = typeof(SectionNames).GetMember(sectionName).First().GetCustomAttribute<DisplayAttribute>();

                    string section = keyAttirb.Name;
                    string key = _configs.ElementAt(i).Value.ElementAt(j).Key.ToString();
                    string value = _configs.ElementAt(i).Value.ElementAt(j).Value.ToString();

                    WriteConfigValue(section, key, value);
                }
            }
        }

        /// <summary>
        /// Get a configuration item from configurations on memory.
        /// </summary>
        /// <typeparam name="KeyEnum">section info</typeparam>
        /// <param name="key">key info</param>
        /// <returns>configuration item</returns>
        public string GetConfigData<KeyEnum>(KeyEnum key)
        {
            DisplayAttribute keyAttrib = typeof(KeyEnum).GetMember(key.ToString()).First().GetCustomAttribute<DisplayAttribute>();

            SectionNames sectionName = (SectionNames)Enum.Parse(typeof(SectionNames), typeof(KeyEnum).Name);

            return _configs[sectionName][key.ToString()].ToString();
        }

        /// <summary>
        /// Set a configuration item to configurations on memory.
        /// </summary>
        /// <typeparam name="KeyEnum">section info</typeparam>
        /// <param name="key">key info</param>
        /// <param name="value">value to set</param>
        public void SetConfigData<KeyEnum>(KeyEnum key, string value)
        {
            DisplayAttribute keyAttrib = typeof(KeyEnum).GetMember(key.ToString()).First().GetCustomAttribute<DisplayAttribute>();

            SectionNames sectionName = (SectionNames)Enum.Parse(typeof(SectionNames), typeof(KeyEnum).Name);

            _configs[sectionName][key.ToString()] = value;
        }


        private string ReadConfigValue(string section, string key)
        {
            // Read a configuration value from ini file.

            StringBuilder tmp = new StringBuilder(_configSize);
            int i = GetPrivateProfileString(section, key, "", tmp, _configSize, _fullpath);

            return tmp.ToString();
        }

        private void WriteConfigValue(string section, string key, string value)
        {
            // Write a configuration value to ini file.

            WritePrivateProfileString(section, key, value, _fullpath);
        }
    }
}