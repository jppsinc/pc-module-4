﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Psinc.Util.DataStructure
{
    [Serializable]
    public class MultiKeyDictionary<K1, K2, V> : Dictionary<K1, Dictionary<K2, V>>
    {
        public V this[K1 key1, K2 key2]
        {
            get
            {
                if (!ContainsKey(key1) || !this[key1].ContainsKey(key2))
                {
                    return default(V);
                }

                return base[key1][key2];
            }
            set
            {
                if (!ContainsKey(key1))
                {
                    this[key1] = new Dictionary<K2, V>();
                }
                this[key1][key2] = value;
            }
        }

        public void Add(K1 key1, K2 key2, V value)
        {
            if (!ContainsKey(key1))
            {
                this[key1] = new Dictionary<K2, V>();
            }
            this[key1][key2] = value;
        }

        public bool ContainsKey(K1 key1, K2 key2)
        {
            return ContainsKey(key1) && this[key1].ContainsKey(key2);
        }

        public new IEnumerable<V> Values
        {
            get
            {
                return from baseDict in base.Values
                       from baseKey in baseDict.Keys
                       select baseDict[baseKey];
            }
        }
    }

    public enum TransferDirection
    {
        Upload,

        Download
    }

    public enum SectionNames
    {
        [Display(Name = "Login")]
        LoginSection,

        [Display(Name = "Proxy")]
        ProxySection
    }

    public enum LoginSection
    {
        [Display(Description = "0")]
        InUse,

        [Display(Description = "sample@psinc.jp")]
        UserId,

        [Display(Description = "0")]
        AccountId
    }

    public enum ProxySection
    {
        [Display(Description = "0")]
        InUse,

        [Display(Description = "")]
        Name,

        [Display(Description = "127.0.0.1")]
        Ip,

        [Display(Description = "8089")]
        Port
    }

    public struct ProxySetting
    {
        public bool InUse;

        public string Name;

        public string Ip;

        public int Port;
    }
}