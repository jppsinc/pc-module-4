﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Timers;
using Psinc.Util.DataStructure;

namespace Psinc.Util
{
    /// <summary>
    /// Class for common tools.
    /// </summary>
    public class Tools
    {
        private const int _keySize = 256;
        private const int _derivationIterations = 1000;

        private static Timer _pulse;
        private static double _pulseInterval;

        private static RunWithoutParameter[] _syncFunc = null;

        public delegate void RunWithoutParameter();


        /// <summary>
        /// Gets a local IP address of version 4.
        /// </summary>
        /// <returns>local IP address</returns>
        public static string GetLocalIPAddress()
        {
            string result = string.Empty;

            var host = Dns.GetHostEntry(Dns.GetHostName());

            foreach (IPAddress ip in host.AddressList)
            {
                // IP ADDRESS V4
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    result = ip.ToString();
                    break;
                }
            }

            return result;
        }

        /// <summary>
        /// Transfers a specified file to server or vice versa.
        /// </summary>
        /// <param name="direction">transferring direction</param>
        /// <param name="serverPath">server path</param>
        /// <param name="localPath">local path</param>
        /// <param name="filename">target file</param>
        /// <param name="id">connection ID</param>
        /// <param name="pw">connection password</param>
        /// <returns>success or not</returns>
        public static bool TransferFileBetweenLocalAndServer(TransferDirection direction, string serverPath, string localPath, string filename, string id, string pw)
        {
            bool result = true;

            string server = string.Format("{0}/{1}", serverPath, filename);
            string local  = string.Format("{0}\\{1}", localPath, filename);

            try
            {
                ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

                using (WebClient wc = new WebClient())
                {
                    wc.Credentials = new NetworkCredential(id, pw);

                    if (direction == TransferDirection.Upload)
                    {
                        wc.UploadFile(server, local);
                    }
                    else if (direction == TransferDirection.Download)
                    {
                        wc.DownloadFile(server, local);
                    }
                }
            }
            catch
            {
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Transfers a specified file to server. Uses 'multipart/form-data'.
        /// </summary>
        /// <param name="serverPath">server path</param>
        /// <param name="localPath">local path</param>
        /// <param name="filename">target file</param>
        /// <returns>success or not</returns>
        public static bool TransferFileToServer(string serverPath, string localPath, string filename)
        {
            bool result = true;

            string local = string.Format("{0}\\{1}", localPath, filename);

            try
            {
                ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

                var fileContent = new StreamContent(File.OpenRead(local));
                fileContent.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                {
                    FileName = Path.GetFileName(local),
                    Name     = @"file[]"
                };

                var content = new MultipartFormDataContent();
                content.Add(fileContent);

                var hc = new HttpClient();
                var res = hc.PostAsync(serverPath, content).Result;

                if (!res.IsSuccessStatusCode)
                {
                    result = false;
                }
            }
            catch
            {
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Reads contents from specified file.
        /// </summary>
        /// <param name="fullpath">full path with filename</param>
        /// <returns>read data</returns>
        public static string ReadContentsFromFile(string fullpath)
        {
            string result = null;
            StringBuilder dummy1 = new StringBuilder();

            try
            {
                using (var sr = new StreamReader(fullpath, Encoding.UTF8))
                {
                    string dummy2;

                    while ((dummy2 = sr.ReadLine()) != null)
                    {
                        dummy1.Append(dummy2);
                        dummy1.AppendLine();
                    }

                    result = dummy1.ToString().Trim();
                }
            }
            catch { }

            return result;
        }

        /// <summary>
        /// Writes contents to specified file.
        /// </summary>
        /// <param name="fullpath">full path with filename</param>
        /// <param name="contents">data to write</param>
        /// <param name="append">append or not</param>
        /// <returns>success or not</returns>
        public static bool WriteContentsToFile(string fullpath, string contents, bool append)
        {
            bool result = true;

            try
            {
                using (var sw = new StreamWriter(fullpath, append, Encoding.UTF8))
                {
                    sw.WriteLine(contents);
                }
            }
            catch
            {
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Gets a list of specified files in specified folder. If target folder is 'null', uses current folder.
        /// </summary>
        /// <param name="extensions">target extensions (e.g. ".jpg")</param>
        /// <param name="list">list of files for return</param>
        /// <param name="source">target folder</param>
        /// <returns>success or not</returns>
        public static bool GetFilesByExtensions(string[] extensions, out FileInfo[] list, DirectoryInfo source = null)
        {
            bool result = true;

            if (source == null)
            {
                source = new DirectoryInfo(Environment.CurrentDirectory);
            }

            if (extensions.Length == 0)
            {
                list = null;
                result = false;
            }
            else
            {
                list = source.GetFiles().Where(f => extensions.Contains(f.Extension.ToLower())).ToArray();
            }

            return result;
        }

        /// <summary>
        /// If passed string is not empty, adds a space to end of that.
        /// </summary>
        /// <param name="input">target string</param>
        /// <returns>modified string</returns>
        public static string AddSpace(string input)
        {
            string result = input;

            if (!string.IsNullOrEmpty(result))
            {
                result += " ";
            }

            return result;
        }

        /// <summary>
        /// Runs passed methods periodically. Default setting of interval is 24 hours('0' means default).
        /// </summary>
        /// <param name="hour">interval of run (available time: 0 ~ 72 hour)</param>
        /// <param name="func">passed no-parameter methods</param>
        public static void PeriodicRun(int hour, params RunWithoutParameter[] func)
        {
            _pulseInterval = 24.0 * 60.0 * 60.0 * 1000.0; // 86,400,000 ms = 24 hrs.

            if (hour > 0 && hour <= 72)
            {
                _pulseInterval = hour * 60.0 * 60.0 * 1000.0;
            }

            _syncFunc = func;

            // SET
            _pulse = new Timer();
            _pulse.AutoReset = false;
            _pulse.Elapsed += PeriodicRun_Elapsed;
            _pulse.Enabled = true;
            _pulse.Interval = _pulseInterval;
        }

        /// <summary>
        /// Encrypts a plain text using passed passphrase.
        /// </summary>
        /// <param name="plainText">plain text to encrypt</param>
        /// <param name="passphrase">password</param>
        /// <returns>encrypted text</returns>
        public static string EncryptString(string plainText, string passphrase)
        {
            var saltStringBytes = Generate256BitsOfRandomEntropy();
            var ivStringBytes = Generate256BitsOfRandomEntropy();
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            using (var password = new Rfc2898DeriveBytes(passphrase, saltStringBytes, _derivationIterations))
            {
                var keyBytes = password.GetBytes(_keySize / 8);

                using (var symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.BlockSize = 256;
                    symmetricKey.Mode = CipherMode.CBC;
                    symmetricKey.Padding = PaddingMode.PKCS7;

                    using (var encryptor = symmetricKey.CreateEncryptor(keyBytes, ivStringBytes))
                    {
                        using (var memoryStream = new MemoryStream())
                        {
                            using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                            {
                                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                                cryptoStream.FlushFinalBlock();

                                var cipherTextBytes = saltStringBytes;
                                cipherTextBytes = cipherTextBytes.Concat(ivStringBytes).ToArray();
                                cipherTextBytes = cipherTextBytes.Concat(memoryStream.ToArray()).ToArray();

                                memoryStream.Close();
                                cryptoStream.Close();

                                return Convert.ToBase64String(cipherTextBytes);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Decrypts a encrypted text using passed passphrase.
        /// </summary>
        /// <param name="encryptedText">encrypted text to decrypt</param>
        /// <param name="passphrase">password</param>
        /// <returns>plain text</returns>
        public static string DecryptString(string encryptedText, string passphrase)
        {
            var cipherTextBytesWithSaltAndIv = Convert.FromBase64String(encryptedText);

            var saltStringBytes = cipherTextBytesWithSaltAndIv.Take(_keySize / 8).ToArray();
            var ivStringBytes = cipherTextBytesWithSaltAndIv.Skip(_keySize / 8).Take(_keySize / 8).ToArray();
            var cipherTextBytes = cipherTextBytesWithSaltAndIv.Skip((_keySize / 8) * 2).Take(cipherTextBytesWithSaltAndIv.Length - ((_keySize / 8) * 2)).ToArray();

            using (var password = new Rfc2898DeriveBytes(passphrase, saltStringBytes, _derivationIterations))
            {
                var keyBytes = password.GetBytes(_keySize / 8);

                using (var symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.BlockSize = 256;
                    symmetricKey.Mode = CipherMode.CBC;
                    symmetricKey.Padding = PaddingMode.PKCS7;

                    using (var decryptor = symmetricKey.CreateDecryptor(keyBytes, ivStringBytes))
                    {
                        using (var memoryStream = new MemoryStream(cipherTextBytes))
                        {
                            using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                            {
                                var plainTextBytes = new byte[cipherTextBytes.Length];
                                var decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);

                                memoryStream.Close();
                                cryptoStream.Close();

                                return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                            }
                        }
                    }
                }
            }
        }


        private static void PeriodicRun_Elapsed(object sender, ElapsedEventArgs e)
        {
            foreach (var func in _syncFunc)
            {
                func?.Invoke();
            }

            // RELEASE
            _pulse.Elapsed -= PeriodicRun_Elapsed;
            _pulse.Enabled = false;
            _pulse.Dispose();

            // SET
            _pulse = new Timer();
            _pulse.AutoReset = false;
            _pulse.Elapsed += PeriodicRun_Elapsed;
            _pulse.Enabled = true;
            _pulse.Interval = _pulseInterval;
        }

        private static byte[] Generate256BitsOfRandomEntropy()
        {
            // REFERENCE -> https://stackoverflow.com/questions/10168240/encrypting-decrypting-a-string-in-c-sharp

            var randomBytes = new byte[32];

            using (var rcsp = new RNGCryptoServiceProvider())
            {
                rcsp.GetBytes(randomBytes);
            }

            return randomBytes;
        }
    }
}