﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;

namespace Psinc.Util
{
    /// <summary>
    /// Class for jobs that uses SQLite database. Can only uses a single database.
    /// </summary>
    public class SQLiteManager
    {
        private static SQLiteManager _instance = null;

        private SQLiteConnection _mainConn = null;
        private bool _connected = false;

        private string _fullpath = string.Empty;


        /// <summary>
        /// Private constructor.
        /// </summary>
        private SQLiteManager()
        {
        }

        /// <summary>
        /// Get an instance of 'SQLiteManager'.
        /// </summary>
        public static SQLiteManager Instance
        {
            get
            {
                _instance = _instance ?? new SQLiteManager();
                return _instance;
            }
        }

        #region :: METHODS

        /// <summary>
        /// Open a connection to target database.
        /// </summary>
        /// <param name="dbFileName">target database filename</param>
        /// <returns>success or not</returns>
        public bool ConnectionOpen(string dbFileName)
        {
            bool result = false;

            if (_connected)
            {
                return result;
            }

            try
            {
                _fullpath = Path.Combine(Environment.CurrentDirectory, dbFileName);

                // Check existence of target file.
                if (!File.Exists(_fullpath))
                {
                    throw new Exception();
                }

                _mainConn = new SQLiteConnection("Data Source=" + _fullpath);
                _mainConn.Open();

                _connected = true;

                result = true;
            }
            catch
            {
                _connected = false;
            }

            return result;
        }

        /// <summary>
        /// Close a connection to database.
        /// </summary>
        public void ConnectionClose()
        {
            if (_connected)
            {
                try
                {
                    _mainConn.Close();
                    _mainConn = null;

                    _connected = false;
                }
                catch { }
            }
        }

        /// <summary>
        /// Execute a query that passed.
        /// </summary>
        /// <param name="query">query to execute</param>
        /// <returns>success or not</returns>
        public bool ExecuteNonQuery(string query)
        {
            bool result = false;

            if (_connected)
            {
                try
                {
                    SQLiteCommand command = _mainConn.CreateCommand();
                
                    command.CommandText = query;
                    command.ExecuteNonQuery();

                    result = true;
                }
                catch { }
            }

            return result;
        }

        /// <summary>
        /// Execute a query that passed and return results of target field.
        /// </summary>
        /// <param name="query">query to execute</param>
        /// <param name="field">target field</param>
        /// <param name="resultSet">set of results</param>
        /// <returns>success or not</returns>
        public bool ExecuteQuery(string query, string field, out List<string> resultSet)
        {
            resultSet = new List<string>();

            bool result = false;

            if (_connected)
            {
                try
                {
                    SQLiteCommand command = _mainConn.CreateCommand();

                    command.CommandText = query;

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            resultSet.Add(reader[field].ToString());
                        }
                    }

                    result = true;
                }
                catch { }
            }

            return result;
        }

        /// <summary>
        /// Execute a query that passed and return whole results.
        /// </summary>
        /// <param name="query">query to execute</param>
        /// <param name="ds">results</param>
        /// <returns>success or not</returns>
        public bool ExecuteQuery(string query, out DataSet ds)
        {
            ds = new DataSet("query_result");

            bool result = false;

            if (_connected)
            {
                try
                {
                    SQLiteCommand command = _mainConn.CreateCommand();

                    command.CommandText = query;

                    SQLiteDataAdapter adapter = new SQLiteDataAdapter(command);

                    adapter.Fill(ds);

                    result = true;
                }
                catch { }
            }

            return result;
        }

        /// <summary>
        /// Execute a query that passed and return whole results.
        /// </summary>
        /// <param name="query">query to execute</param>
        /// <param name="dataSetName">name for results</param>
        /// <param name="ds">results</param>
        /// <returns>success or not</returns>
        public bool ExecuteQuery(string query, string dataSetName, out DataSet ds)
        {
            ds = new DataSet(dataSetName);

            bool result = false;

            if (_connected)
            {
                try
                {
                    SQLiteCommand command = _mainConn.CreateCommand();

                    command.CommandText = query;

                    SQLiteDataAdapter adapter = new SQLiteDataAdapter(command);

                    adapter.Fill(ds);

                    result = true;
                }
                catch { }
            }

            return result;
        }

        /// <summary>
        /// Execute a queary that passed and return count of results.
        /// </summary>
        /// <param name="query">query to execute</param>
        /// <param name="count">count of results</param>
        /// <returns>success or not</returns>
        public bool ExecuteQuery(string query, out int count)
        {
            count = 0;

            bool result = false;

            if (_connected)
            {
                try
                {
                    SQLiteCommand command = _mainConn.CreateCommand();

                    command.CommandText = query;

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            count++;
                        }
                    }

                    result = true;
                }
                catch { }
            }

            return result;
        }

        #endregion
    }
}